$(document).ready(function(){

	var currentDateTime = new Date();
	currentDateTime = "date=" + currentDateTime;

	$('link[href="reset.css"]').attr('href','css/reset.css?' + currentDateTime);
	$('link[href="sti-headers.css"]').attr('href','css/sti-headers.css?' + currentDateTime);
	$('link[href="sti-fonts.css"]').attr('href','css/sti-fonts.css?' + currentDateTime);
	$('link[href="sti-common-layout.css"]').attr('href','css/sti-common-layout.css?' + currentDateTime);
	$('link[href="sti-components.css"]').attr('href','css/sti-components.css?' + currentDateTime);
	$('link[href="sti-popups.css"]').attr('href','css/sti-popups.css?' + currentDateTime);

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		   $('link[href="sti-components-mobile.css"]').attr('href','css/sti-components-mobile.css?' + currentDateTime);
		   $('link[href="sti-mobile-headers.css"]').attr('href','css/sti-headers-mobile.css?' + currentDateTime);
		   $('link[href="sti-common-layout-mobile.css"]').attr('href','css/sti-common-layout-mobile.css?' + currentDateTime);
		   $('link[href="sti-popups-mobile.css"]').attr('href','css/sti-popups-mobile.css?' + currentDateTime);
		   
		   DEBUG &&  console.log("$('script");
		   // DEBUG &&  console.log($('script[src="static/stackFramesNew.js"]'));
		   // $('script[src="static/stackFramesNew.js"]').attr('src','static/stackFramesNewMobile.js');
		   // DEBUG &&  console.log($('script[src="static/stackFramesNewMobile.js"]'));
		   $.getScript("mobile_stack/stackFramesNewMobile.js", function(data, status, jqxhr) {
		   });
		   $.getScript("mobile_stack/stackFollowersNewMobile.js", function(data, status, jqxhr) {
		   });

			



	}else{
		$.getScript("static_extras/stackFramesNew.js", function(data, status, jqxhr) {
		});
		$.getScript("static_extras/stackEntities.js", function(data, status, jqxhr) {
		});
	}
});


