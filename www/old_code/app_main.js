
var stichoapp= angular.module('stichio', [ 'ngStorage', 'ui.router', '720kb.tooltips','ngCookies', 'ngFileUpload','ngImgCrop', 'me-lazyload','ngSanitize','ngMeta', "firstWord", "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.buffering",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster"]);

var stichio = angular.module('stichio')

angular.module('stichio').run(['commonService', function(commonService) {
    commonService.setLoggedInStatus();
}]);
angular.module('stichio').run(['ngMeta',function(ngMeta){
    ngMeta.init();
}]);

angular.module('stichio').config(['ngMetaProvider',function(ngMetaProvider){

    ngMetaProvider.setDefaultTitle("STICHIO");
    ngMetaProvider.setDefaultTag("url", "stichio.in");
    ngMetaProvider.setDefaultTag("keywords", 'social, pictures, frames, stiches, connect, follow, flowers, friends, chat, interest');

}]);

angular.module('stichio').directive('onLongPress', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.longPress = true;

                // We'll set a timeout for 800 ms for a long press
                $timeout(function() {
                    if ($scope.longPress) {
                        // If the touchend event hasn't fired,
                        // apply the function given in on the element's on-long-press attribute
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                }, 800);
            });


            $elm.bind('touchend', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.longPress = false;
                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onTouchEnd)
                    });
                }
            });
        }
    };
})

angular.module('stichio').directive('onShortPress', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.shortPress = true;

                // We'll set a timeout for 800 ms for a long press
                //$timeout(function() {
                if ($scope.shortPress) {
                    // If the touchend event hasn't fired,
                    // apply the function given in on the element's on-long-press attribute
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onShortPress)
                    });
                }
                //}, 10);
            });

            $elm.bind('touchend', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.shortPress = false;
                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onTouchEnd)
                    });
                }
            });
        }
    };
});


//

//
//angular.module('stichio').factory('httpRequestInterceptor', ['$cookies', function ($cookies) {
//  return {
//    request: function (config) {
//            var authenticationToken = $cookies.get('sti_authenticationToken');
//
//            if(!authenticationToken){
//                authenticationToken = "92d4153e29450a3cbfdd133f8ca5f7e458b25c9a";
//            }
//
//      config.headers['Authorization'] = 'Token ' + authenticationToken;
//      //config.headers['Accept'] = 'application/json;odata=verbose';
//      //config.headers['Content-Type'] = 'application/json';
//
//      return config;
//    }
//  };
//}]);
//
//angular.module('stichio').config(function ($httpProvider) {
//  $httpProvider.interceptors.push('httpRequestInterceptor');
//});


var DEBUG = true;
dat = "84cd2ca348fa73e98d1f9f2f081e60370494d7ec";
var bsi1="52.35"
var bsn1 = "dev-sti-1"





angular.module('stichio').run(['$http','$cookies',function($http,$cookies) {
    var authenticationToken = $cookies.get('sti_authenticationToken');

    if(!authenticationToken){
        authenticationToken = dat;
    }

    $http.defaults.headers.common.Authorization = 'Token ' + authenticationToken ;

}]);


angular.module('stichio').provider('modalState', ['$stateProvider', function($stateProvider) {
    var provider = this;
    var modalResult;
    this.$get = function() {
        return provider;
    };
    this.state = function(stateName, options) {
        var modalInstance;
        $stateProvider.state(stateName, {
            url: options.url,
            onEnter: ['$uibModal', '$state', function($uibModal, $state) {
                modalInstance = $uibModal.open(options);
                // When the modal uses $close({..}), the data (=result) will be assigned to the parent state as 'modalResult'.
                modalInstance.result.then(function(result) {
                    modalResult = result;
                }).finally(function() { // modal closes
                    if(modalResult) {
                        $state.get('^').modalResult = modalResult;
                    }
                    modalInstance = modalResult = null;
                    if ($state.$current.name === stateName) {
                        $state.go('^'); // go to parent state
                    }
                });
            }],
            onExit: function() {
                if (modalInstance) {
                    modalInstance.close();
                }
            }
        });
        return provider;
    };
}])

angular.module('stichio').directive('onLongPress', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.longPress = true;

                // We'll set a timeout for 800 ms for a long press
                $timeout(function() {
                    if ($scope.longPress) {
                        // If the touchend event hasn't fired,
                        // apply the function given in on the element's on-long-press attribute
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                }, 800);
            });

            $elm.bind('touchend', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.longPress = false;
                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onTouchEnd)
                    });
                }
            });
        }
    };
})

angular.module('stichio').directive('onShortPress', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.shortPress = true;

                // We'll set a timeout for 800 ms for a long press
                //$timeout(function() {
                if ($scope.shortPress) {
                    // If the touchend event hasn't fired,
                    // apply the function given in on the element's on-long-press attribute
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onShortPress)
                    });
                }
                //}, 10);
            });

            $elm.bind('touchend', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.shortPress = false;
                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onTouchEnd)
                    });
                }
            });
        }
    };
});



angular.module('stichio').config(['$stateProvider', 'modalStateProvider' , '$urlRouterProvider', function ($stateProvider, modalStateProvider , $urlRouterProvider) {

//angular.module('stichio').config(['$routeProvider', function ($routeProvider) {

    $stateProvider
    // HOME STATES AND NESTED VIEWS =======l=================================

        .state('profile', {
            abstract: true,
            url: '/profile/:profileId',
            templateUrl: 'userProfile/userProfile.html',
            controller: 'userProfileController',
            controllerAs: 'parentController'
        })
        .state('profile.frames', {
            url: '/frames',
            templateUrl: 'userProfile/userProfileFrames.html',
            controller: "userProfileFramesController",
            controllerAs: 'userProfileFrameChildController'
        })
        .state('profile.truths', {
            url: '/truths',
            templateUrl: 'userProfile/userProfileTruths.html',
            controller: "userProfileTruthsController",
            controllerAs: 'userProfileTruthsChildController'
        })
        .state('profile.dares', {
            url: '?dareId&dareZone&adminDareId',
            cache : true,
            templateUrl: 'userProfile/userProfileDares.html',
            controller: "userProfileDaresController",
            controllerAs: 'userProfileDaresChildController'
        })
        .state('profile.framesAdded', {
            url: '/framesAdded/:frameName',
            templateUrl: 'userProfile/frame-added-mob.html',
            controller: "userProfileFrameAddedController",
            controllerAs: 'userProfileFrameAddedChildController'
        })
        .state('profile.followers', {
            url: '/followers',
            templateUrl: 'userProfile/userProfileFollowers.html',
            controller: "userProfileFollowersController",
            controllerAs: 'userProfileFollowersChildController'
        })
        .state('profile.following', {
            url: '/following',
            templateUrl: 'userProfile/userProfileFollowing.html',
            controller: "userProfileFollowingController",
            controllerAs: 'userProfileFollowingChildController'
        })
        .state('profile.friends',{
            url: '/friends',
            templateUrl:'userProfile/UserProfileFriends.html',
            controller:'userProfileFriendsController'
        })
        .state('contact',{
            url : '/contact',
            templateUrl : 'contact/contact.html',
            controller : 'contactController'
        })
        .state('contact.us',{
            url : '/us',
            templateUrl : 'contact/contactAbout.html'
        })
        .state('contact.hiring',{
            url : '/careers',
            templateUrl : 'contact/contactHiring.html'
        })
        .state('profile.stiches', {
            url: '/stiches',
            templateUrl: 'userProfile/userProfileStiches.html',
            controller: "userProfileStichesController",
            controllerAs: 'userProfileStichesChildController'
        })
        .state('profile.likes', {
            url: '/likes',
            templateUrl: 'userProfile/userProfileLikes.html',
            controller: "userProfileLikesController",
            controllerAs: 'userProfileLikesChildController'
        })
        .state('frames', {
            abstract : true,
            url: '/frames/:frameId',
            templateUrl: 'frameDetails/frameDetails.html',
            controller: 'frameDetailsController',
            controllerAs: 'framesParentController'
        })
        .state('frames.stiches', {
            url: '?stichId&questionId',
            cache : true,
            templateUrl: 'frameDetails/frameStiches.html',
            controller: 'frameStichesController',
            controllerAs: 'frameStichesChildController'
        })
        .state('frames.followers', {
            url: '/followers',
            cache : true,
            templateUrl: 'frameDetails/frameFollowers.html',
            controller: 'frameFollowersController',
            controllerAs: 'frameFollowersChildController'
        })
        .state('home', {
            abstract : true,
            url: '',
            templateUrl: 'home/home.html',
            controller: 'homeController',
            controllerAs: 'homeParentController'

        })
        .state('home.stiches', {
            url: '',
            templateUrl: 'home/homeStiches.html',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
        .state('home1', {
            abstract : true,
            url: '/',
            templateUrl: 'home/home.html',
            controller: 'homeController',
            controllerAs: 'homeParentController'

        })
        .state('home1.stiches', {
            url: '',
            templateUrl: 'home/homeStiches.html',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
        .state('policy',{
            url : '/policy',
            templateUrl : 'policies/policy.html',
            controller : 'policyController',
            controllerAs : 'policyParentController'
        })
        .state('policy.terms',{
            url : '/terms',
            templateUrl : 'policies/policyTerms.html',
            controller : 'policyTermsController',
            controllerAs : 'policyTermsChildController'
        })
        .state('policy.privacy',{
            url : '/privacy',
            templateUrl : 'policies/policyPrivacy.html',
            controller : 'policyPrivacyController',
            controllerAs : 'policyPrivacyChildController'
        })
        .state('search', {
            abstract : true,
            url: '/search',
            templateUrl: 'search/search.html',
            controller: 'searchController',
            controllerAs: 'searchParentController'
        })
        .state('search.stiches', {
            url: '?q&frames',
            templateUrl: 'search/searchStiches.html',
            controller: 'searchStichesController',
            controllerAs: 'searchStichesChildController'
        })
        //     .state('search.frames', {
        //     url: '/frames',
        //     templateUrl: 'search/searchFrames.html',
        //     controller: 'searchFramesController',
        //     controllerAs: 'searchFramesChildController'
        // })
        .state('topics', {
            url: '/topics',
            templateUrl: 'topics/selectTopics.html',
            controller: 'selectTopicsController',
            controllerAs: 'selectTopicsController'
        }).state('todaysPicks', {
            abstract : true,
            url: '/todaysPicks/:frameId',
            templateUrl: 'TodaysPicks/TodaysPicks.html',
            controller: 'todaysPicksController',
            controllerAs: 'todaysPicksParentController'
        })
        .state('todaysPicks.stiches', {
            url: '',
            cache : true,
            templateUrl: 'TodaysPicks/todaysPicksStiches.html',
            controller: 'todaysPicksStichesController',
            controllerAs: 'todaysPicksStichesChildController'
        }).state('events', {
            abstract : true,
            url: '/events/:eventName',
            templateUrl: 'Events/event.html',
            controller: 'eventController',
            controllerAs: 'eventParentController'
        })
        .state('events.dares', {
            url: '',
            cache : true,
            templateUrl: 'Events/eventDares.html',
            controller: 'eventDaresController',
            controllerAs: 'todaysPicksStichesChildController'
        })
        .state('categories',{
            url: '/category/:categoryId',
            cache :true,
            templateUrl : 'CategoryPage/category.html',
            controller : 'categoryController',
            controllerAs: 'categoryParentController'
        })
        .state('reset', {
            url: '/reset/:token',
            templateUrl: 'templates/reset.html'
        })
        .state('verify',{
            url: '/verify/:token',
            templateUrl: '/verify/verify.html',
            controller: 'verifyController'
        })

        .state('explore',{
            abstract: true,
            url:'/explore',
            templateUrl:'ExplorePage/explore.html',
            controller:'exploreController' ,
            controllerAs:'exploreParentCtrl'
        })
        .state('explore.exploreDares',{
            url:'',
            templateUrl:'ExplorePage/exploreDares.html',
            controller:'exploreDaresController'
        })
        .state('explore.dares',{
            url:'/DARESuggestions',
            templateUrl:'ExplorePage/exploreDareSuggestions.html?0.01',
            controller:'ExploreDareSuggestionsController'
        });




    $urlRouterProvider.otherwise('');

    // $routeProvider
    // 	.when("/frames", {
    // 		controller: 'userProfileController',
    //            templateUrl: 'userProfile/frames.html'
    // 	})
    // 	.when("/followers", {
    // 		controller: 'userProfileController',
    //            templateUrl: 'userProfile/followers.html'
    // 	})
    // 	.when("/following", {
    // 		controller: 'userProfileController',
    //            templateUrl: 'userProfile/following_frames.html'
    // 	})
    // 	.when("/userFollowingFollow", {
    // 		controller: 'userProfileController',
    //            templateUrl: 'userProfile/following_frames.html'
    // 	})
    // 	.when("/userFollowing", {
    // 		controller: 'userProfileController',
    //            templateUrl: 'userProfile/following_users.html'
    // 	})
    // 	.otherwise({ redirectTo: '/frames' });
}]);

//

var maxHeightCreateFramePopUp = 531;
var minHeightCreateFramePopUp = 398;
var selectedPage = "frames";

$(document).ready(function(){
    setTimeout(expand, 500);

    //TODO: Maintain aspect ratio on window resize.
    //$(window).resize($(".create-frame-cnt").click());



    function expand(){

        $('.deckgrid').append('<span class="stretch"></span>')
    }

    $(".load-left").click(function(e){
        $(".frames-nav").hide();
        $(".stiches-nav").hide();
        $(".likes-nav").hide();
        $(".load-left").hide();
        $(".followers-nav").show();
        $(".following-nav").show();
        $(".load-right").show();
    });

    $(".load-right").click(function(e){
        $(".frames-nav").show();
        $(".stiches-nav").show();
        $(".likes-nav").show();
        $(".load-left").show();
        $(".followers-nav").hide();
        $(".following-nav").hide();
        $(".load-right").hide();
    });

});

function setNavOnClickEvents(){
    $(".right-nav-load").click(function(){
        $(".frames-nav, .stiches-nav, .right-nav-load").css("display", "none");
        $(".likes-nav, .followers-nav, .following-nav, .left-nav-load").css("display", "inline-block");
    });

    $(".left-nav-load").click(function(){
        $(".frames-nav, .stiches-nav, .likes-nav, .right-nav-load").css("display", "inline-block");
        $(".followers-nav, .following-nav, .left-nav-load").css("display", "none");
    });
}

$(function(){
    $(window).resize(recalculate);
});

$(function(){
    $(window).resize(calculateImageHeight);
});

$(function(){
    $(window).resize(recalculatecreateframeboxwidth);
});


var marginRight1 = 22.22; // To display search and stichers on same line.
function recalculatecreateframeboxwidth() {
    var search = $(".search-cnt").width() - $(".added-stichers-cnt-var").width() - marginRight1;
    $(".search-input-cnt").css("width", search);

}


function recalculate() {
    w = $('.create-frame-body').width() - 240;

    $('.after-stich-added').css('width', w)
    $('.create-frame-popup.after-stiching .create-frame-header').css('width', w + 21)
    $('.frame-cover-image-wrapper').css('height', $('.after-stich-added').height() + 30)
}

function calculateImageHeight(){

    var imageht= $('.main-image-cnt img').height()
    $('.main-image').css('padding-bottom', '0px')
    $('.main-image-cnt').css('position', 'absolute')
    if (imageht <=600)
    {

        if (imageht < 480)
        {
            $('.main-image-cnt').css('position', 'relative')
            if (imageht > 360 ){
                var ht = 120-((600-imageht)/2)
                $('.main-image').css('padding-bottom', ht + 'px')
            }


        }
        else
        {

            var ht = 610-imageht

            $('.main-image').css('padding-bottom', ht + 'px')
        }
    }
}

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}


function calculateHeight(element, aspectRatio, finalHeight, containerWidth){
    var finalWidth = 1 / aspectRatio * finalHeight;
    var marginLeft = (finalWidth - containerWidth) / 2;

}

function removePopups(){
    $('.modalDialog.target').removeClass('target')
    $('body').removeClass('prevent-scroll')
    $('body').removeClass('stop-scrolling')
}

function millisecondsToStr(milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();

    function numberEnding(number) {
        return (number > 1) ? 's' : '';
    }

    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
        return years + 'y'
    }
    var months = Math.floor((temp %= 31536000) / 2592000);
    if (months) {
        return months + 'mth'
    }
    //TODO: Months! Maybe weeks?
    var weeks = Math.floor((temp %= 31536000) / 604800);
    if (weeks) {
        return weeks + 'w'
    }
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
        return days + 'd'
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
        return hours + 'h'
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
        return minutes + 'm'
    }
    var seconds = temp % 60;
    if (seconds) {
        return seconds + 's'
    }
    return 'less than a second'; //'just now' //or other string you like;
}

if (!DEBUG)
    console.log = function() {}


var reg = ".us-west-2"
ags = "stcprodv1";
var bsi2 = ".106.118"


bsi = bsi1 + bsi2
bsn = bsn1 + reg

var bodyScrollTop = 0;
function openPopup(className) {
    console.log('config')
    $('.'+className).addClass('target');
    bodyScrollTop = $('body').scrollTop();
    $('body').addClass('stop-scrolling')
}
function closePopup(className) {
    console.log('unconfig')
    $('.'+className).removeClass('target');
    $('body').removeClass('stop-scrolling');
    $('body').animate({scrollTop: bodyScrollTop}, 0);
}

stichio.filter('parseUrl', function() {
    var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim
    var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim

    return function(text) {
        //console.log("urls", urls, $1)
        if (text){
            if(text.match(urls)) {
                console.log("urls", urls)
                text = text.replace(urls, "<a href=\"$1\" target=\"_blank\" class=\"hyperlink\">$1</a>")
            }
            if(text.match(emails)) {
                text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>")
            }
        }


        return text
    }
})


stichio.filter('timeStamp', function() {
    return function(date) {
        var myDate = new Date(date);
        var result = myDate.getTime();
        var myDate2 = new Date();
        var result2 = myDate2.getTime();
        var timeDiff = result2 - result;
        var timeDiffString = millisecondsToStr(timeDiff);
        if (timeDiffString == 'less than a second') {
            return "Just Now";
        }
        else {
            return timeDiffString + " ago";
        }
    };
});

//stichio.filter('creator', function() {
//    return function(data) {
//        if (!data) {
//            return "STICHIO";
//        }
//    };
//});

angular.module('firstWord', [])
    .filter('firstWord', function() {
        return function(data) {
            if(!data) return data;
            data = data.split(' ');
            return data[0];
        };
    });
$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.top > bounds.bottom));

};
function parseUrl(text) {
    var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim
    var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim
    if (text) {
        if(text.match(urls)) {
            console.log("urls", urls)
            text = text.replace(urls, "<a href=\"$1\" target=\"_blank\" class=\"link\">$1</a>")
        }
        if(text.match(emails)) {
            text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>")
        }
    }
    return text
}

stichio.directive('autoGrow', [function () {
    function link(scope, element, attrs) {
        element.on('input paste', function() {
            var $el = element,
            offset = $el.innerHeight() - $el.height();

            if ($el.innerHeight < this.scrollHeight) {
              //Grow the field if scroll height is smaller
              $el.height(this.scrollHeight - offset);
            } else {
              //Shrink the field and then re-set it to the scroll height in case it needs to shrink
              $el.height(1);
              $el.height(this.scrollHeight - offset);
            }
        });
    }
    return {
        restrict: 'A',
        link: link
    }
}]);