var stichoapp= angular.module('stichio', ['ionic','ionic.native','ngCordova','angular-cache', 'ngStorage', 'ui.router', '720kb.tooltips','ngCookies', 'ngFileUpload','ngImgCrop', 'me-lazyload','ngSanitize', "filters", "timestamp","firstWord", "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.buffering",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster",
    "com.2fdevs.videogular.plugins.analytics",
    'angular-inview',
    'monospaced.elastic'
]);

var stichio = angular.module('stichio');

if ('ontouchstart' in document) {
    $('body').removeClass('no-touch');
}

//ionic.Platform.ready(function(){
//    console.log("platform ready");
//    angular.element(document).ready(function() {
//        angular.bootstrap(document, ['stichio']);
//    });
//});
defaultHexCodeStatusBar= "#E2E6E9"
angular.module('stichio').run(['$ionicPlatform', '$cordovaSplashscreen' , 'commonService', '$ionicHistory', '$rootScope', '$state', '$cordovaPushV5','PushProcessingService', '$cordovaDeeplinks', '$timeout','cacheService', function($ionicPlatform, $cordovaSplashscreen,commonService, $ionicHistory, $rootScope,$state, $cordovaPushV5, PushProcessingService, $cordovaDeeplinks, $timeout, cacheService) {
    $rootScope.newNotification = false;

    commonService.setLoggedInStatus();
    $rootScope.online=true;

    try {
        $ionicPlatform.ready(function () {
            if (window.cordova) {
                // Hide Splash Screen when App is Loaded
                try {
                    $cordovaSplashscreen.hide();
                } catch (e) {
                }
                console.log("splash screen hide2")
            }

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            //cordova.plugins.backgroundMode.enable();
            if (!$rootScope.userLoggedIn) {
                //$rootScope.appReady=false;
                console.log("$state.current.name", $state.current.name);
                setTimeout(function () {
                    if ($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                        $rootScope.openIntroPopup();
                    }
                    else {
                        $rootScope.displayloginpopup();
                    }
                }, 10);
            }
            else {
                $rootScope.appReady = true;
            }

            //if(window.navigator && window.navigator.splashscreen) {
            //    window.plugins.orientationLock.unlock();
            //}cordova.plugins.backgroundMode.enable();
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
                StatusBar.overlaysWebView(false);
                StatusBar.backgroundColorByHexString(defaultHexCodeStatusBar);
            }



            PushProcessingService.initialize();
            PushProcessingService.registerID();
            cacheService.initialize()

            if ((typeof window.analytics !== "undefined") && (ags == "stcprodv1")) {
                window.analytics.startTrackerWithId("UA-88878307-1");
            } else {
                console.log("Google Analytics Unavailable");
            }

            //if ($rootScope.userLoggedIn) {
            //    commonService.checkPermissions();
            //}


            document.addEventListener("offline", onOffline, false);
            document.addEventListener("online", onOnline, false);
            setTimeout(onOffline, 2000);

            try {
                cordova.plugins.notification.badge.configure({autoClear: true});
            } catch (e) {
            }
            AppRate.preferences.customLocale = {
                title: "Rate STICHIO",
                message: "If you loved DARES on STICHIO, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",
                cancelButtonLabel: "Not Now",
                laterButtonLabel: "Remind Later",
                rateButtonLabel: "Rate It Now"
            }
            AppRate.preferences.usesUntilPrompt = 3;
            AppRate.preferences.useCustomRateDialog = false;
            AppRate.preferences.callbacks.onButtonClicked = function (buttonIndex) {
                console.log("onButtonClicked -> " + buttonIndex);
                if (buttonIndex == 1) {
                    commonService.user_rated_app = true;
                    commonService.setCookie("app_rated", "yes");
                    commonService.analytics('event', {
                        category: 'App Rating',
                        action: 'Rate Now',
                        label: commonService.loggedInUserId
                    });
                }
                else if (buttonIndex == 2) {
                    commonService.user_rated_app = true;
                    commonService.analytics('event', {
                        category: 'App Rating',
                        action: 'Remind Me Later',
                        label: commonService.loggedInUserId
                    });
                }
                else {
                    commonService.user_rated_app = true;
                    commonService.analytics('event', {
                        category: 'App Rating',
                        action: 'Not Now',
                        label: commonService.loggedInUserId
                    });
                }
            }
            //if (!commonService.user_rated_app)
            //    AppRate.promptForRating();
            //Custom Back Method
            $rootScope.customBack = function () {
                console.log("back event registered")
                $ionicPlatform.registerBackButtonAction(function (event) {
                    console.log("back event", event)
                    if (!$rootScope.userLoggedIn) {
                        return;
                    }
    //var path = $location.path()
                    if (commonService.currentPlayingVideo) {
                        if (commonService.currentPlayingVideo.isFullScreen) {
                            commonService.currentPlayingVideo.toggleFullScreen();
                            return;
                        }
                        try {
                            commonService.currentPlayingVideo.stop();
                        } catch (e) {
                        }
                    }

                    if (commonService.openPopups.length > 0) {

                        var classname = commonService.openPopups.pop();
                        console.log('classname closed', classname)
                        if (classname !== "follow-sugg-popup") {
                            commonService.closePopupOnBack(classname);
                        }
                        else {
                            commonService.openPopups.push(classname)
                        }

                    }
                    else {
                        if ($state.current.name === "home.stiches" || $state.current.name === "home1.stiches") {
                            event.preventDefault();
                            commonService.introVideoOpened = false;

                            //Prompt to rate every 10 times they minimize
                            if (commonService.app_visits % 10 == 0 && !commonService.user_rated_app) {
                                AppRate.promptForRating();
                            }
                            else {
                                window.plugins.appMinimize.minimize();
                                //navigator.app.exitApp()
                            }
                            commonService.increase_app_visits();


                        }
                        else {
                            try {
                                if (window.history.length > 1) {
                                    window.history.back();
                                }
                                else {
                                    $state.go('home.stiches', {})
                                }
                            } catch (e) {
                                window.plugins.appMinimize.minimize();
                                //navigator.app.exitApp()
                            }
                        }
                    }
                }, 100);
            };
            $rootScope.customBack();
            try {
                cordova.plugins.notification.badge.clear();
            } catch (e) {
            }

            //  push.on('notification', function(data) {
            //    console.log('notification data', data)
            //});

            //ga('create', 'UA-84487207-1', 'auto');
            //ga('create', 'UA-88878307-1', 'auto', 'UsernameTracker');

        });
    } catch (e) {
    }



    function onOffline(){
        if (window.Connection) {
            console.log(navigator.connection.type)
            if (navigator.connection.type == Connection.NONE) {
                commonService.showMessage("Internet not available. Please check your connection.", 3000, 'bottom')
                $rootScope.online = false;
            }
        }
        else {
            console.log("No Connection Plugin")
        }
    }
    function onOnline(){

        commonService.reconnectSocket();
        commonService.hideMessage();
        commonService.showMessage("Internet is back. Please refresh to load new content.", 3000, 'bottom')
        $rootScope.online = true;
    }

    var clearHomeTimeout=null;
    var exitTimer=null;
    var socketDisconnectTimeout=null;
    $ionicPlatform.on('resume', function(){
        clearTimeout(exitTimer);
        clearTimeout(socketDisconnectTimeout);
        commonService.reconnectSocket();
        $cordovaDeeplinks.route({
            'dares1/:dareSuggestionId': {
                target: 'dare',
                parent: 'dare'
            }
        }).subscribe(function(match) {
            console.log("deep linking", match)
            $timeout(function() {
                console.log("link match", match.$route.parent, match.$args)
                $state.go(match.$route.parent, match.$args);
                //$timeout(function() {
                //    $state.go(match.$route.target, match.$args);
                //}, 800);
            }, 100);
        }, function(nomatch) {
            console.log('No match', nomatch);
            //$location.path(nomatch.$link.fragment)
            if (nomatch.$link.fragment) {
                window.location.href = "file:///android_asset/www/index.html#" + nomatch.$link.fragment
            }
        });
        console.log("resume");
        $rootScope.newNotification=false;
        try {
            cordova.plugins.notification.badge.clear();
        } catch (e) {
        }
        var  current_time_milliseconds = new Date().getTime();
        if(!$rootScope.userLoggedIn && !commonService.clickedAuth) {
            $rootScope.appReady=false;
            setTimeout(function(){
                console.log("$state.current.name 2", $state.current.name);
                if($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                    $rootScope.openIntroPopup();
                }
                else{
                    $rootScope.displayloginpopup();
                }
            }, 100);
        }
        else{
            $timeout.cancel(clearHomeTimeout);

            if (commonService.minimise_time) {
                if (current_time_milliseconds - commonService.minimise_time > 10*60*1000) {
                    if ($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                        $rootScope.refreshHome('top');
                    }
                }
            }
        }
        if (commonService.minimise_time) {
            if (current_time_milliseconds - commonService.minimise_time > 1 * 60 * 1000) {
                setTimeout(function () {

                    if (window.Connection) {
                        console.log(navigator.connection.type)
                        if (navigator.connection.type == Connection.NONE) {
                            commonService.showMessage("Internet not available. Please check your connection.", 3000, 'bottom')
                        }
                        if ($rootScope.userLoggedIn && $rootScope.appReady) {
                            setTimeout(function () {
                                try {
                                    $rootScope.loadNotifications(true);
                                } catch (e) {
                                }

                            }, 500);
                        }
                    }
                }, 3000);
            }
        }
        try {
            console.log("socket status on resume", commonService.socket.socket.connected,commonService.socket.socket.connecting )
            if (!commonService.socket.socket.connected && !commonService.socket.socket.connecting) {
                console.log("Socket disconnected. Socket reconnecting")
                commonService.socket.socket.reconnect();
            }
            else{
                commonService.socket_status = true;
                console.log("Socket connection alive")

            }

        } catch (e) {
        }


    });
    $ionicPlatform.on('pause', function(){
        //socketDisconnectTimeout=setTimeout(function () {
        //    if (globalSocket) {
        //        globalSocket.disconnect();
        //    }
        //}, 60*1000);
        console.log("minimizing");
        var  current_time_milliseconds = new Date().getTime();
        commonService.app_minimised=true;
        //console.log("socket status on minimising", commonService.socket.socket, commonService.socket.socket.connected,commonService.socket.socket.connecting )
        commonService.minimise_time=current_time_milliseconds;

        if (commonService.currentPlayingVideo){
            commonService.currentPlayingVideo.stop();
        }
        try {
            clearHomeTimeout=$timeout(function(){
                $rootScope.clearHome('');
            }, 10*60*1000)

        } catch (e) {
        }
        try {
            var myVideo = document.getElementById("introvideo");
            myVideo.pause();
        } catch (e) {
        }
        exitTimer=setTimeout(function(){
            navigator.app.exitApp();
        }, 30*60*1000) // close the app after 30 mins

    });
    try {
        $cordovaDeeplinks.route({
            'dares1/:dareSuggestionId': {
                target: 'dare',
                parent: 'dare'
            }
        }).subscribe(function (match) {
            console.log("deep linking", match)
            $timeout(function () {
                console.log("link match", match.$route.parent, match.$args)
                $state.go(match.$route.parent, match.$args);
                //$timeout(function() {
                //    $state.go(match.$route.target, match.$args);
                //}, 800);
            }, 100);
        }, function (nomatch) {
            console.log('No match', nomatch);
            window.location.href = "file:///android_asset/www/index.html#" + nomatch.$link.fragment
        });
    } catch (e) {
    }



}]);

var DEBUG = true;


angular.module('stichio').run(['$http','$cookies',function($http,$cookies) {
    var authenticationToken = $cookies.get('sti_authenticationToken');

    if(!authenticationToken){
        authenticationToken = dat;
    }
    console.log("$http.defaults", $http.defaults)
    $http.defaults.headers.common.Authorization = 'Token ' + authenticationToken ;

}]);


angular.module('stichio').config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', '$cordovaAppRateProvider', '$httpProvider', function ($stateProvider , $urlRouterProvider, $ionicConfigProvider, $cordovaAppRateProvider, $httpProvider) {

//angular.module('stichio').config(['$routeProvider', function ($routeProvider) {
    try {
        $ionicConfigProvider.scrolling.jsScrolling(false);
    } catch (e) {
    }
    $stateProvider
    // HOME STATES AND NESTED VIEWS =======l=================================

        .state('profile', {
            abstract: true,
            url: '/profile/:profileId',
            cache : true,
            templateUrl: 'components/userProfile/userProfile.html?0.07',
            controller: 'userProfileController',
            controllerAs: 'parentController'
        })
        .state('profile.dares', {
            url: '?dareId&dareZone&adminDareId&nav_from&dareType',
            cache : true,
            templateUrl: 'components/userProfile/userProfileDares.html?0.06',
            controller: "userProfileDaresController",
            controllerAs: 'userProfileDaresChildController'
        })
        .state('contact',{
            abstract:true,
            url : '/contact',
            templateUrl : 'components/contact/contact.html?0.01',
            controller : 'contactController'
        })
        .state('contact.hiring',{
            url : '/careers',
            templateUrl : 'components/contact/contactHiring.html'
        })
        .state('home', {
            abstract : true,
            url: '',
            cache:true,
            templateUrl: 'components/home/home.html?0.07',
            controller: 'homeController',
            controllerAs: 'homeParentController'

        })
        .state('home.stiches', {
            url: '',
            templateUrl: 'components/home/homeStiches.html?0.26',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
        .state('home1', {
            abstract : true,
            url: '/',
            cache:true,
            templateUrl: 'components/home/home.html?0.07',
            controller: 'homeController',
            controllerAs: 'homeParentController'

        })
        .state('home1.stiches', {
            url: '',
            templateUrl: 'components/home/homeStiches.html?0.26',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
        .state('home.dare',{
            url:'/dares/:dareSuggestionId',
            cache:true,
            templateUrl: 'components/home/homeStiches.html?0.26',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
        .state('policy',{
            url : '/policy',
            templateUrl : 'components/policies/policy.html',
            controller : 'policyController',
            controllerAs : 'policyParentController'
        })
        .state('policy.terms',{
            url : '/terms',
            templateUrl : 'components/policies/policyTerms.html',
            controller : 'policyTermsController',
            controllerAs : 'policyTermsChildController'
        })
        .state('policy.privacy',{
            url : '/privacy',
            templateUrl : 'components/policies/policyPrivacy.html',
            controller : 'policyPrivacyController',
            controllerAs : 'policyPrivacyChildController'
        })
        .state('events', {
            abstract : true,
            url: '/events/:eventName',
            cache : true,
            templateUrl: 'components/Events/event.html?0.02',
            controller: 'eventController',
            controllerAs: 'eventParentController'
        })
        .state('events.dares', {
            url: '',
            cache : true,
            templateUrl: 'components/Events/eventDares.html',
            controller: 'eventDaresController',
            controllerAs: 'eventDaresController'
        })
        .state('reset', {
            url: '/reset/:token',
            templateUrl: 'shared/templates/reset.html'
        })
        .state('verify',{
            url: '/verify/:token',
            templateUrl: 'components/verify/verify.html',
            controller: 'verifyController'
        })

        .state('explore',{
            abstract: true,
            url:'/explore',
            cache : true,
            templateUrl:'components/ExplorePage/explore.html?0.02',
            controller:'exploreController' ,
            controllerAs:'exploreParentCtrl'
        })
         .state('explore.dares',{
            url:'?genreId',
            templateUrl:'components/ExplorePage/exploreDareSuggestions.html?0.01',
            cache : true,
            controller:'ExploreDareSuggestionsController'
        })
        .state('popular',{
            abstract: true,
            url:'/popular',
            cache : true,
            templateUrl:'components/Popular/popular.html?0.02',
            controller:'popularController' ,
            controllerAs:'popularParentCtrl'
        })
         .state('popular.dares',{
            url:'/dares',
            templateUrl:'components/Popular/popularDares.html?0.01',
            cache : true,
            controller:'popularDaresController'
        })
        .state('popular.people',{
            url:'/people',
            templateUrl:'components/Popular/popularPeople.html?0.01',
            cache : true,
            controller:'popularPeopleController'
        })
        .state('search',{
            abstract: true,
            url:'/search',
            cache : true,
            templateUrl:'components/Search/search.html?0.02',
            controller:'searchController' ,
            controllerAs:'searchParentCtrl'
        })
        .state('search.dares',{
            url:'/dares?query',
            templateUrl:'components/Search/searchDares.html?0.01',
            controller:'searchDaresController'
        })
        .state('search.people',{
            url:'/people?query',
            templateUrl:'components/Search/searchPeople.html',
            controller:'searchPeopleController'
        })
        .state('chat', {
            url: '/chat/:conversationId',
            templateUrl: 'components/home/homeStiches.html?0.26',
            controller: 'homeStichesController',
            controllerAs: 'homeStichesChildController'
        })
    ;
    $httpProvider.interceptors.push('httpRequestInterceptor');
    $urlRouterProvider.otherwise('/');
    document.addEventListener("deviceready", function () {

        var prefs = {
            language: 'en',
            appName: 'STICHIO',
            iosURL: '<my_app_id>',
            androidURL: 'market://details?id=com.stichio.stichioApp&referrer=utm_source%3DRating%26utm_medium%3Dapp"',
            windowsURL: 'ms-windows-store:Review?name=<...>'
            //useCustomRateDialog : "Hello"
        };

        $cordovaAppRateProvider.setPreferences(prefs)

    }, false);


}]);

//


var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

//function disableScroll() {
//    if (window.addEventListener) // older FF
//        window.addEventListener('DOMMouseScroll', preventDefault, false);
//    window.onwheel = preventDefault; // modern standard
//    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
//    window.ontouchmove  = preventDefault; // mobile
//    document.onkeydown  = preventDefaultForScrollKeys;
//}
//
//function enableScroll() {
//    if (window.removeEventListener)
//        window.removeEventListener('DOMMouseScroll', preventDefault, false);
//    window.onmousewheel = document.onmousewheel = null;
//    window.onwheel = null;
//    window.ontouchmove = null;
//    document.onkeydown = null;
//}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function millisecondsToStr(milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();

    function numberEnding(number) {
        return (number > 1) ? 's' : '';
    }

    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
        return years + 'y'
    }
    var months = Math.floor((temp %= 31536000) / 2592000);
    if (months) {
        return months + 'mth'
    }
    //TODO: Months! Maybe weeks?
    var weeks = Math.floor((temp %= 31536000) / 604800);
    if (weeks) {
        return weeks + 'w'
    }
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
        return days + 'd'
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
        return hours + 'h'
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
        return minutes + 'm'
    }
    var seconds = temp % 60;
    if (seconds) {
        return seconds + 's'
    }
    return 'less than a second'; //'just now' //or other string you like;
}

if (!DEBUG)
    console.log = function() {}


var bodyScrollTop = 0;

angular.module('stichio').filter('thousandSuffix', function () {
    return function (input, decimals) {
        var exp, rounded,
            suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

        if(window.isNaN(input)) {
            return null;
        }

        if(input < 1000) {
            return input;
        }
        if(input>10000){
            decimals=0
        }

        exp = Math.floor(Math.log(input) / Math.log(1000));


        return (input / Math.pow(1000, exp)).toFixed(decimals) + suffixes[exp - 1];
    };
});

angular.module('filters', []).filter('parseUrl', function($sce) {
    return function(text) {
        var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim
        var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim
        if (text) {
            if(text.match(urls)) {
                console.log("urls", urls)
                text = text.replace(urls, "<a href='' onclick=\"openLink(\'$1\'); return false;\" class=\"link\">$1</a>")
            }
            // if(text.match(emails)) {
            //     text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>")
            // }
        }
        return $sce.trustAsHtml(text);
    };
});
;

function openLink(link) {
    if (link.indexOf("www.stichio.co.in") > -1){
        var urlObj = new URL(link)
        console.log(urlObj,  "urlobj")
        window.location.href = "file:///android_asset/www/index.html" + urlObj.hash
    }
    else if (link.indexOf("rb492") > -1){
        window.open(link, '_system');
    }
    else {
        window.open(link, '_blank', 'location=no');
    }
    return false;
}
angular.module('timestamp', []).filter('timeStamp', function() {
    return function(date, onlyNew) {
        if (!date) {
            return ''
        }
        var myDate = new Date(date);
        var result = myDate.getTime();
        var myDate2 = new Date();
        var result2 = myDate2.getTime();
        var timeDiff = result2 - result;
        if (timeDiff <= 1000){
            return "Just Now";
        }
        if (onlyNew && timeDiff > (345600000)){ // greater than 4days, dont' show time
            return ''
        }
        var timeDiffString = millisecondsToStr(timeDiff);
        if (timeDiffString == 'less than a second') {
            return "Just Now";
        }
        else {
            return timeDiffString + " ago";
        }
    };
});

angular.module('stichio').filter('videoPoster', function () {
    return function (source) {
        return source.replace('videos_mobile', 'images').replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
    }
});
angular.module('stichio').filter("trustUrl", function($sce) {
    return function(Url) {
        return $sce.trustAsResourceUrl(Url);
    };
});
angular.module('stichio').filter('formatNumber', function () {
    return function (num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
});

angular.module('firstWord', [])
    .filter('firstWord', function() {
        return function(data) {
            if(!data) return data;
            data = data.split(' ');
            return data[0];
        };
    });
$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};
function parseUrlFn(text) {
    var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim
    var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim
    if (text) {
        if(text.match(urls)) {
            console.log("urls", urls)
            text = text.replace(urls, "<a href=\"$1\" target=\"_blank\" open-link class=\"link\">$1</a>")
        }
        if(text.match(emails)) {
            text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>")
        }
    }
    return text
};

if (!Date.now) {
    Date.now = function () {
        return new Date().getTime();
    }
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

Date.prototype.yyyymmdd = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();
    var hr = this.getHours().toString()
    var mins = this.getMinutes().toString()
    var secs = this.getSeconds().toString()
    var millisecs = this.getMilliseconds().toString()
    millisecs = pad(millisecs, 3).toString() + '0'


    return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]) + (hr[1] ? hr : "0" + hr[0]) + (mins[1] ? mins : "0" + mins[0]) + (secs[1] ? secs : "0" + secs[0]) + millisecs; // padding
};




angular.module('stichio').directive('autoGrow', [function () {
    function link(scope, element, attrs) {
        element.on('input paste', function() {
            var $el = element,
                offset = $el.innerHeight() - $el.height();

            if ($el.innerHeight < this.scrollHeight) {
                //Grow the field if scroll height is smaller
                $el.height(this.scrollHeight - offset);
            } else {
                //Shrink the field and then re-set it to the scroll height in case it needs to shrink
                $el.height(1);
                $el.height(this.scrollHeight - offset);
            }
        });
    }
    return {
        restrict: 'A',
        link: link
    }
}]);

angular.module('stichio').directive('bigFont', ['commonService', function (commonService) {
    function link(scope, element, attrs) {
        var platform = commonService.platform || commonService.checkPlatform();
        var textLength = attrs.bigFont.length;
        console.log('textLength', textLength, attrs.bigFont, scope.bigFontLen)
        var threshold_len=scope.bigFontLen || 140

        if (platform == 'mobile' && textLength <  threshold_len) {
            element.css({'font-size': '20px', 'line-height': '24px', 'font-weight': '300'});
        } else if (platform == 'desktop' && textLength <  threshold_len) {
            element.css({'font-size': '26px', 'line-height': '32px', 'font-weight': '300'});
        }
    }
    return {
        restrict: 'A',
        link: link,
        scope:{
            bigFontLen : "=bigFontLen",
        }
    }
}]);




clearErrorMessageTimeout=null;



function getRandomItem(arr){
    return arr[Math.floor(Math.random() * arr.length)]
}

angular.module('stichio')
    .directive('contenteditable', ['$timeout', function($timeout) { return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {


            console.log("EditAbleV")

            if (!ngModel) {
                return
            }

            // options
            var opts = {}
            angular.forEach([
                'stripBr',
                'noLineBreaks',
                'selectNonEditable',
                'moveCaretToEndOnChange',
                'stripTags'
            ], function(opt) {
                var o = attrs[opt]
                opts[opt] = o && o !== 'false'
            })
            opts.moveCaretToEndOnChange=true;
            opts.selectNonEditable=true;


            // view -> model
            element.bind('input', function(e) {
                scope.$apply(function() {
                    var html, html2, rerender
                    html = element.html()
                    rerender = false
                    console.log("inside input render")
                    if (opts.stripBr) {
                        html = html.replace(/<br>$/, '')
                    }
                    if (opts.noLineBreaks) {
                        html2 = html.replace(/<div>/g, '').replace(/<br>/g, '').replace(/<\/div>/g, '')
                        if (html2 !== html) {
                            rerender = true
                            html = html2
                        }
                    }
                    if (opts.stripTags) {
                        rerender = true
                        html = html.replace(/<\S[^><]*>/g, '')
                    }
                    ngModel.$setViewValue(html)
                    if (rerender) {
                        ngModel.$render()
                    }
                    if (html === '') {
                        // the cursor disappears if the contents is empty
                        // so we need to refocus
                        $timeout(function(){
                            element[0].blur()
                            element[0].focus()
                        })
                    }
                })
            })


            // model -> view
            var oldRender = ngModel.$render;
            element.on('focus', function(){
                try {
                    cordova.plugins.Keyboard.show();
                } catch (e) {
                }
            });

            ngModel.$render = function() {
                var el, el2, range, sel
                if (!!oldRender) {
                    oldRender()
                }
                console.log("inside old render");

                try {
                    var html = ngModel.$viewValue || ''
                    console.log("click2");
                    if (opts.stripTags) {
                        html = html.replace(/<\S[^><]*>/g, '')
                    }
                } catch (e) {
                    console.error(e)
                }
                console.log("click1");
                $timeout(function(){
                    element.html(html);
                    console.log("refocus") ;
                    $timeout(function(){
                        if (html) {
                            element[0].blur();
                            element[0].focus();
                        }

                    },200);
                    if (opts.moveCaretToEndOnChange) {
                    el = element[0];
                    range = document.createRange()
                    sel = window.getSelection()
                    console.log("inside moveCaretToEndOnChange render", range, sel, el.childNodes);
                    if (el.childNodes.length > 0) {
                        el2 = el.childNodes[el.childNodes.length - 1]
                        console.log("inside moveCaretToEndOnChange render", el2);
                        range.setStartAfter(el2)
                    } else {
                        range.setStartAfter(el)
                    }
                    range.collapse(true)
                    sel.removeAllRanges()
                    sel.addRange(range);

                }
                element.bind('click', function(e) {
                    console.log("inside click render", element);
                    var range, sel, target
                    target = e.toElement
                    if (target !== this && angular.element(target).attr('contenteditable') === 'false') {
                        console.log("clicked on the tagged name")
                        range = document.createRange()
                        sel = window.getSelection()
                        range.setStartBefore(target)
                        range.setEndAfter(target)
                        sel.removeAllRanges()
                        sel.addRange(range)
                    }
                })
                });


            }

        }
    }}]);

if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError("Array.from requires an array-like object - not null or undefined");
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

function checkNumberFieldLength(elem, num){
    if (elem.value.length > 10) {
        elem.value = elem.value.slice(0,10);
    }
}

angular.module('stichio').factory('httpRequestInterceptor', function ($q) {
  return {
    request : function(config) {
        var apiPattern = /\/api\/v1\//;

        config.params = config.params || {};

        if (apiPattern.test(config.url)) {
            if (typeof appVersion !== 'undefined') {
                // var appvar = appVersion.replace('.', '-').replace('.', '-')
                // var appvar = 1
                config.params.app_version = appVersion
            }
        }
        return config || $q.when(config);
    }
  };
})