(function () {
	var injectParams = ['$scope','APIService','$state', 'commonService', '$rootScope', '$timeout'];

    var searchController = function ($scope,APIService,$state, commonService, $rootScope, $timeout) {
        setTimeout(function () {
            $.screentime.reset();
        }, 5000)
        commonService.removePopups()
        $rootScope.footerSelectedOption="search";
        $scope.searchQuery=function(){
            $state.go($state.current, {query : $scope.searchSuggText});
            try {
                cordova.plugins.Keyboard.close();
            } catch (e) {
            }
        }



        $scope.setSearchText=function(text){
            $scope.searchSuggText=text
        }
        $scope.goBackSearch=function(){
            try {
                if (window.history.length > 1) {
                    window.history.back();
                }
                else{
                    $state.go('home.stiches', {})
                }
            } catch (e) {
                window.plugins.appMinimize.minimize();
            }
        }

        var $search_input_box = $('.dare-search-input');

        $scope.clearSearchBox=function(){
            $scope.searchSuggText='';
            setTimeout(function(){
                $search_input_box[0].focus();
                $search_input_box[0].click();
            },100)

        }

        $search_input_box.keypress(function (e) {
            if (e.which == 13) {
                $scope.searchQuery();
                return false;
            }
        });


    };
	



    searchController.$inject = injectParams;

    angular.module('stichio').controller('searchController', searchController);
}());