(function(){
    var injectParams = ['$scope','APIService','$location','$timeout','commonService','$window', '$filter']
    var searchPeopleController = function ($scope,APIService,$location,$timeout,commonService,$window, $filter) {
        var timeout;
        var $loadingIcon=$("#search-users-loading-icon");
        var old_data = {"query": 10};
        var max_result_length = 10;
        APIService.scrollOff();
        $scope.allStichers=[]
        var params = $location.search();
        var searchquery = params.query;
        var userIndex1=0;
        var userIndex2=0;
        var userCount=10;
        $scope.setSearchText(searchquery);

        function onSearchUsers(data){
            var filtered_usersId = []
            data = data.filter(function (user) {
                return (filtered_usersId.indexOf(user.userId) < 0)
            })
            $scope.allStichers =$scope.allStichers.concat(data);
            if ($scope.allStichers.length == 0) {
                $scope.searchContactError = "Match Not Found";
                $scope.noUser = true
            }
            else {
                $scope.searchContactError = "";
                $scope.noUser = false
            }

            // console.log($scope.noUser, $scope.emailMatched, $scope.filtered.length, $scope.allStichers.length);

        }
        var loading_users=false;
        var load_users=function() {
            if ($scope.searchSuggText) {
                $timeout.cancel(timeout);
                timeout = $timeout(function () {
                    $scope.searchContactError = "";
                    if ($scope.searchSuggText.length < 3 && $scope.searchSuggText.length > 0) {
                        $scope.searchContactError = "Enter at least 3 characters";
                        console.log($scope.searchContactError, "error");
                    }
                    else if ($scope.searchSuggText.length > 2) {

                        $loadingIcon.show();
                        var socket = commonService.socket;
                        APIService.searchUsers($scope.searchSuggText, userIndex1, userIndex2, userCount).then(function (data) {
                            userIndex1=data.from_index;
                            userIndex2=data.from2;
                            $loadingIcon.hide();
                            loading_users=false;
                            onSearchUsers(data.users)
                            if (data.users.length==0){
                                $(window).off('scroll.userSearchScroll');
                            }

                        }), function () {
                            $loadingIcon.hide();
                        };

                    }
                    commonService.analytics('event', {
                        category: "Search Users",
                        action: $scope.searchSuggText,
                        label: commonService.loggedInUserId
                    }, 5000);
                }, 300);
            }
            else {
                var $search_input_box = $('.dare-search-input');
                try {
                    $search_input_box[0].focus();
                    $search_input_box[0].click();
                }
                catch (e) {
                }
            }
        }
        load_users();
        var $win;

        var onScrollFn = function () {
            // do the onscroll stuff you want here
            console.log("popular scroll", $win.scrollTop(), $(".user-follow")[0].scrollHeight)
            if (!loading_users && ($win.scrollTop() > ($(".user-follow")[0].scrollHeight - 1500))) {
                loading_users = true;
                load_users()
            }
        };

        $win = $(window);
        commonService.infiniteScroll($win,onScrollFn, 'scroll.userSearchScroll');
    }
    searchPeopleController.$inject = injectParams
    angular.module('stichio').controller('searchPeopleController',searchPeopleController)
}());