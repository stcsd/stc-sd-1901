(function(){
    var injectParams = ['$scope','$rootScope','APIService','$timeout','$window', '$location', 'commonService','$state']
    var searchDaresController = function ($scope,$rootScope, APIService,$timeout,$window, $location, commonService,$state) {
        var sug_index = 0,
            sug_count = 10,
            scrollHeight,
            sug_flag = false,
            no_data=false,
            currScroll=0,
            currScroll1= 0,
            minScrollHt= 0,
            genreBoxHt= 0,
            win = $(window);

        console.log("seaarch")
        //$scope.$on('$viewContentLoaded', function(event) {
        //    commonService.analytics('pageview', {page: $location.url(), title: "Search Suggestions"});
        //});

        APIService.scrollOff();
        $scope.suggestions=[]


        var params = $location.search();
        var searchquery = params.query;
        $scope.setSearchText(searchquery);
        $scope.randomGenres = [];
        var $header_sticky = $('.headerSticky');
        $header_sticky.css('position', 'fixed');
        $header_sticky.show();
        $timeout(function () {
            var $win = $(window)
            var lastscrolltop=0;
            var onScrollFn = function () {
                currScroll=$win.scrollTop();
                if(lastscrolltop<currScroll){
                    if (!$scope.loadingDares && (currScroll > ($(".searched_dares")[0].scrollHeight  - 1000))) {
                        $scope.loadingDares = true;
                        searchDareSuggestions()
                    }
                }
                lastscrolltop=currScroll;
            }
            commonService.infiniteScroll($win, onScrollFn, 'scroll.searchSuggestionsScroll')
        },100);

        $scope.openDare = function(dare){
            if(dare.dare_info){
                if (dare.dare_info.dare_type == 'dare'){
                    $rootScope.openDarePopup(null, false, true, false, dare.dare_info.dareId, true);
                }
                else{
                    $rootScope.openAdminDarePopup(null, false, true, false, dare.dare_info.dareId, true)
                }
            }
            else{
                $rootScope.openDareResponsesPopup(dare, null, true);
                $rootScope.closeAdminDarePopup();
            }
        }

        var searchDareSuggestions = function () {
            $scope.loadingDares=true;
            var post_data={
                index:sug_index,
                count:sug_count,
                query:searchquery,
                new:true
            }
            APIService.searchSuggestions(post_data).then(function (data) {

                console.log('exploreFeed', data);
                $scope.loadingDares = false;

                $scope.suggestions = $scope.suggestions.concat(data.results)

                if (data.results.length === 0) {
                    no_data = true;
                    if (sug_index==0){
                        $scope.noResults=true;
                        $scope.randomGenres=[]
                        if ($rootScope.allDareGenres){
                            $scope.randomGenres = angular.copy($rootScope.allDareGenres)
                            shuffle($scope.randomGenres)
                        }
                    }
                    $(window).off('scroll.searchSuggestionsScroll');
                }
                if (data.last_index) {
                    sug_index = data.last_index
                }
                else{
                    sug_index+=sug_count;
                }
            }, function () {
                $scope.loadingDares = false;
                $scope.noResults=true;
                $scope.randomGenres=[]
                if ($rootScope.allDareGenres){
                    $scope.randomGenres = angular.copy($rootScope.allDareGenres)
                    shuffle($scope.randomGenres)
                }
                console.log("$scope.randomGenres", $scope.randomGenres)
            });

        };
        if (searchquery){
            searchDareSuggestions();
        }
        else {
            var $search_input_box = $('.dare-search-input');
            try {
                $search_input_box[0].focus();
                $search_input_box[0].click();
            }
            catch (e) {
            }
        }
        commonService.analytics('event', {
            category: "Search",
            action: "Dare Suggestions",
            label: searchquery
        }, 1000);




    };
    searchDaresController.$inject = injectParams
    angular.module('stichio').controller('searchDaresController',searchDaresController)
}());