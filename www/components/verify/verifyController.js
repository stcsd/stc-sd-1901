(function () {
    var injectParams = ['$scope', '$http','$stateParams', '$state','commonService','loginService', '$location','$window'];

    var verifyController = function ($scope,$http,$stateParams,$state,commonService,loginService,$location,$window) {
        $scope.$on('$viewContentLoaded', function(event) {
            commonService.analytics('pageview', {page: $location.url(), title: "Email Verification"});
        });
        $scope.token = $stateParams.token;
        verifyController.$inject = injectParams;

        var tokenData = {
          "token": $scope.token
        };
        //$http.defaults.headers.common.Authorization = $scope.token;
        $http.put(commonService.baseUrl + "api/v1/verify", tokenData).then(function(results){
                $state.go('home.stiches')
        });
    };

    angular.module('stichio').controller('verifyController', verifyController);
}());