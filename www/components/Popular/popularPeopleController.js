(function(){
    var injectParams = ['$scope','$rootScope','APIService','$timeout','$window', '$location', 'commonService','$state', '$cookies']
    var popularPeopleController = function ($scope,$rootScope, APIService,$timeout,$window, $location, commonService,$state, $cookies) {
        APIService.scrollOff();
        $timeout(function(){

            $("#people-loading").show();
            APIService.getTopDarees("follow_suggestions").then(function (results) {
                $("#people-loading").hide();
                $scope.userSuggestionsNew = results;
                var curr_time=new Date();
                commonService.suggs_fetched_time = curr_time.getTime()/1000;
            }, function () {
                $("#people-loading").hide();
            })
            if (commonService.suggs_fetched_time) {
                var time_now=new Date();
                var timediff=((time_now.getTime() / 1000) - commonService.suggs_fetched_time)
                console.log("suggs_fetched_time",commonService.suggs_fetched_time, time_now.getTime()/1000, timediff)
                if ( timediff > (1*3600)) {
                    $scope.userSuggestionsNew=undefined;

                }
            }
        }, 100);
    };
    popularPeopleController.$inject = injectParams
    angular.module('stichio').controller('popularPeopleController',popularPeopleController)
}());