(function () {
    var injectParams = ['$scope','$rootScope', '$http', 'APIService', 'commonService', '$filter', '$timeout', '$window','$sce', '$ionicPlatform','cacheService', '$state']

    var popularController = function ($scope,$rootScope, $http, APIService, commonService, $filter, $timeout, $window, $sce, $ionicPlatform, cacheService, $state ) {
        //commonService.setLoggedInStatus();
        if (!$rootScope.userLoggedIn){
            return;
        }

        setTimeout(function () {
            $.screentime.reset();
        }, 1000)
        commonService.removePopups();
        var popular_dares_load_time=null;
        $rootScope.footerSelectedOption='popular';
        $scope.selectedOpt=''
        var popular_video_url;
        var suggs_fetched_time=null;

        //APIService.scrollOff();

        var oldSelOpt='popular'

    };

    popularController.$inject = injectParams;

    angular.module('stichio').controller('popularController', popularController);
}());