(function(){
    var injectParams = ['$scope','$rootScope', '$http', 'APIService', 'commonService', '$filter', '$timeout', '$window','$sce', '$ionicPlatform','cacheService', '$state']
    var popularDaresController = function ($scope,$rootScope, $http, APIService, commonService, $filter, $timeout, $window, $sce, $ionicPlatform, cacheService, $state ) {
        APIService.scrollOff();
        var video;
        $scope.topic_index={}
        $scope.openDare = function(dare_type, dareId, dare){
            console.log("dare_type1", dare_type)
            if (dare_type=='dare') {
                $rootScope.openDarePopup(null, false, true, false, dareId, true, true, dare);
            }
            else
            {
                $rootScope.openAdminDarePopup(null, false, true, false, dareId, true, true, dare)
            }
            try {
                if (video.currentState == 'play') {
                    video.stop()
                }
            } catch (e) {
            }
        }
        $scope.searchQuery=function(){
            $state.go('search.dares', {query : $scope.searchSuggText})
            try {
                cordova.plugins.Keyboard.close();
            } catch (e) {
            }
        }
        $('.dare-search-input').keypress(function (e) {
            if (e.which == 13) {
                $scope.searchQuery();
                return false;
            }
        });

        $scope.videoInView = function(inView){
            console.log("inView", inView)
            if (inView){
                if (video.currentState !== 'play') {
                    video.play();
                }

            }
            else{
                video.pause();
            }
        }
        $scope.muteVideo=function(){
            if (video.volume > 0) {
                video.setVolume(0);
                $('.speaker').addClass('mute');
            }
            else{
                video.setVolume(1);
                $('.speaker').removeClass('mute');
            }
        }

        $scope.expandVideo = function(){
            if(!video.isFullScreen){
                try {
                    $ionicPlatform.registerBackButtonAction(
                        function () {
                            video.toggleFullScreen();
                            $rootScope.customBack();
                            console.log("back from camera popup")
                        }, 100);
                } catch (e) {
                    $rootScope.customBack();
                }
                if (video.volume == 0){
                    $scope.muteVideo();
                }
                try {
                    if (video.currentState != 'play') {
                        video.play();
                    }
                } catch (e) {
                }
            }
            else{
                try {
                    $rootScope.customBack();
                } catch (e) {
                }
            }
            video.toggleFullScreen();

            //}
        }
        $scope.toggleUpvotePopularDare= function(response){
            var data={}
            var cachedata=cacheService.get("popular_dares", "temp");
            if(!response.upvoted){
                data.vote_type = 'up';
                try {
                    cachedata.data.popular_dare.upvoted = true;
                    cachedata.cache.put("popular_dares", cachedata.data)
                } catch (e) {
                }

            }
            else{
                data.vote_type = 'cancel';
                try {
                    cachedata.data.popular_dare.upvoted = false;
                    cachedata.cache.put("popular_dares", cachedata.data)
                } catch (e) {
                }
            }

            if (response.dare_type=="dare"){
                data.dareResponseId = response.responseId;
                APIService.voteDareResponse(data, response.dareId, response.user1, response.user2);
            }
            else {
                data.dareResponseId = response.dareId;
                APIService.adminDareVote(data, response.dareId, response.userId);
            }


        }

        $scope.onPlayerReadyNew1 = function(API, index){
            video = API;
            commonService.popularVideo=video

        }
        $scope.popularDares=[]
        $timeout(function () {
            if ($rootScope.isLoggedIn){

                var $loadingIcon=$("#search-users-loading-icon");
                var dareIndex=0;
                var dareCount=12;
                var topic_index=0;
                var opt='popular'
                //$rootScope.openSearchContacts = function (event, opt) {
                // $('html').css("overflow-y","hidden");
                try {
                    if (commonService.currentPlayingVideo && (commonService.currentPlayingVideo!==video) ) {
                        commonService.currentPlayingVideo.pause()
                    }
                } catch (e) {
                }

                // $(".search-contacts-popup").addClass("target");
                //commonService.openPopup('search-contacts-popup')


                var $win;
                var onScrollFn = function () {
                    // do the onscroll stuff you want here
                    //console.log("popular scroll", $win.scrollTop(), $popular_section[0].scrollHeight)
                    if (!loading_dares && ($win.scrollTop() > ($(".popularDares")[0].scrollHeight - 1000))) {
                        loading_dares = true;
                        loadTrendingDares(dareIndex, dareCount, topic_index)
                    }
                };

                $win = $(window);
                commonService.infiniteScroll($win,onScrollFn, 'scroll.popularDaresScroll');
                if (!$scope.popularDarees) {
                    $timeout(function () {
                        APIService.getTopDarees("popular_darees").then(function (results) {
                            $scope.popularDarees = results;
                            $loadingIcon.hide();
                        }, function () {
                            $loadingIcon.hide();
                        })
                    }, 100)
                }

                var loading_dares=true;

                function loadTrendingDares(dareIndex1, dareCount, topic_index1){
                    APIService.getTrendingDares("popular_dares", dareIndex1, dareCount, topic_index1).then(function (results) {
                        if (results.topic){
                            $scope.topic_index[$scope.popularDares.length] = results.topic
                        }
                        $scope.popularDares = $scope.popularDares.concat(results.popular_dares);

                        if (!$scope.popular_dare) {
                            $scope.popular_dare=results.popular_dare;
                            $scope.popular_dare.upvoted=results.popular_dare.upvoted;
                            popular_video_url=results.popular_dare.image
                            $scope.popular_dare.video =
                                [{
                                    src: $sce.trustAsResourceUrl(cacheService.getUrl(popular_video_url)),
                                    type: "video/mp4"
                                }];
                            if (!cacheService.isFileCached(popular_video_url)) {
                                try {
                                    cacheService.download(popular_video_url).then(function (cache) {
                                        console.log("Downloaded:", popular_video_url)
                                    }, function (failedDownloads) {

                                    })
                                } catch (e) {
                                }
                            }
                            $scope.analytics = {
                                category: "Video",
                                label: "Popular Dare" + results.popular_dare.dareId,
                                events: {
                                    ready: false,
                                    play: true,
                                    pause: false,
                                    stop: false,
                                    complete: true,
                                    progress: 25
                                },
                                dareId:results.popular_dare.dareId,
                                userId:commonService.loggedInUserId
                            }
                        }




                        dareIndex = results.last_index;
                        topic_index = results.topic_index || 0;
                        $loadingIcon.hide();
                        loading_dares=false;
                    }, function () {
                        $loadingIcon.hide();
                        loading_dares=false;
                    })

                }

                if ($scope.popularDares.length==0) {
                    loading_dares=true;
                    loadTrendingDares(0, dareCount, 0);

                }
            }



            //}
        }, 100);
    }
    popularDaresController.$inject = injectParams
    angular.module('stichio').controller('popularDaresController',popularDaresController)
}());