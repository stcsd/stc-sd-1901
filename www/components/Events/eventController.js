(function () {
    var injectParams = ['$timeout','$scope','$rootScope', '$window', '$stateParams', '$state', 'homeService','APIService','commonService','$location'];


    var eventController = function ($timeout,$scope,$rootScope, $window, $stateParams, $state, homeService, APIService,commonService,$location) {
        setTimeout(function () {
            $.screentime.reset();
        }, 0)
        $rootScope.footerSelectedOption="event"

        var colors = ['#DDDDDD','#FFA18B','#D1E8FF','#FFD3BD','#C8C8FF','#FFDDFF','#E5E4CA','#FFE8C8'];
        var window_width = Math.min(screen.width, 600) - 15;
        var type = 'live';
        $scope.viewingTab = 'liveDares';
        $rootScope.eventFollowers = $rootScope.eventFollowers || [];

        $('.highlighter').click(function (event) {
            $(this).siblings().css('border-bottom', 'none');
            $(this).css('border-bottom', '2px solid #007ad5');
        });

        $scope.$on('$viewContentLoaded', commonService.observer2);
        window.addEventListener('resize', resizeObserver);

        $scope.ancestor = 'event';

        function resizeObserver() {
            commonService.observer2();
            var followerDisplayCount = parseInt(($('.eventFollowersList').width()) / 85);
            if (!$scope.isMobile) { followerDisplayCount *= 2; }
            $rootScope.topFollowers = $rootScope.eventFollowers.slice(0, followerDisplayCount);
        }

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $scope.isMobile = true;
        } else {
            $scope.isMobile = false;
        }
        commonService.removePopups();
        //commonService.setLoggedInStatus();
        $scope.loggedIn = commonService.userLoggedIn;
        APIService.cancel();
        APIService.scrollOff();
        $scope.goToSearch = function(){
            $state.go('search.stiches')
        }

        $scope.bannerHeight = ($('.imageslide').width() * 350/600) + "px";



        $rootScope.eventAdminDareResponses= [];
        $scope.event_oldDareResponses = [];
        $scope.appendedDaresInLive = [];

        $scope.homeIndex = 0;
        $scope.homeCount = 10;
        $scope.inside = true;
        $scope.scrollheight = $('.main-event-dares-cnt').height();
          var eventName = $stateParams.eventName
         $scope.$on('$viewContentLoaded', function() {
             commonService.analytics('pageview', {page: $location.url(), title:"Event - " + eventName});
        });

        function startEventDareScroll() {
            setTimeout(function () {
                $(window).on('scroll.eventDaresScroll', function () {
                    // DEBUG && console.log($(this).scrollTop(), $scope.scrollheight - APIService.scrollOffset);
                    if ($(this).scrollTop() > $scope.scrollheight - APIService.scrollOffset) {
                        console.log('fetching stiches')
                        if (!$scope.inside) {
                            $scope.inside = true;
                            refreshEventDares(type);
                        }
                    }
                });
            }, 100);
        }
        refreshEventDares(type);
        startEventDareScroll();

        var appendOldDaresToLive = false;
        function refreshEventDares(typeCopy){
            console.log('type', typeCopy);
            $scope.inside = true;
            // if (!type) { type = 'live'};
            APIService.getEventAdminDareResponses(eventName, $scope.homeIndex, $scope.homeCount, typeCopy).then(function (data) {
                $('.loading-icon').hide();
                if (typeCopy === 'live') { $rootScope.eventAdminDareResponses = $rootScope.eventAdminDareResponses.concat(data); }
                else if (typeCopy === 'old') {
                    if (appendOldDaresToLive) {
                        $scope.appendedDaresInLive = $scope.appendedDaresInLive.concat(data);
                    } else {
                        $scope.event_oldDareResponses = $scope.event_oldDareResponses.concat(data);
                    }
                    if (data.length > 0) { $scope.showPreviousDare = true; }
                }

                setTimeout(function() {
                    $scope.homeIndex += $scope.homeCount;
                    $scope.scrollheight = $('.main-event-dares-cnt').height();
                    $scope.inside = false;
                    if (typeCopy === 'live' && data.length === 0) {
                        console.log('changing type')
                        $scope.homeIndex = 0;
                        appendOldDaresToLive = true;
                        $scope.showPreviousDareBanner = true;
                        type = 'old';
                        refreshEventDares('old');
                        return 0;
                    }
                    $scope.inside = (data.length == 0)
                    if($scope.inside) {
                        $(window).off('scroll.eventDaresScroll');
                    }
                }, 0.1);
            });
        }

        //$scope.banners = [
        //    "images/Virat_Kohli_Final.jpg",
        //    "images/Yoga.jpg",
        //    "images/Selfie_dare.jpg",
        //    "images/login_background.png"
        //]
        var slidesInSlideshow;
        var slidesTimeIntervalInMs = 5000;
        APIService.getEventInfo(eventName).then(function(data){
            $scope.slideshow = 0;
            $scope.eventDetails = data;
            $rootScope.eventDetails = data;
            slidesInSlideshow = data.banners.length;
            $scope.bannersCount = slidesInSlideshow;
            $scope.banner = data.banners[$scope.slideshow]

            $scope.eventDetails.selectedTab = 0

            setInterval(function() {
                $scope.nextSlide();
            }, slidesTimeIntervalInMs);
        })

        $scope.nextSlide = function () {
            $scope.slideshow = (($scope.slideshow + 1 + slidesInSlideshow) % slidesInSlideshow);
            $scope.banner = $scope.eventDetails.banners[$scope.slideshow];
        }

        $scope.prevSlide = function () {
            $scope.slideshow = (($scope.slideshow - 1 + slidesInSlideshow) % slidesInSlideshow);
            $scope.banner = $scope.eventDetails.banners[$scope.slideshow];
        }

        $('.loading-icon').show();

        APIService.getEventAdminDares(eventName, $scope.homeIndex, $scope.homeCount).then(function (data) {
            $scope.eventAdminDares = data;

            $scope.card = $scope.eventAdminDares[0]
        });

        $scope.showDareSuggestion = function(index){
            $scope.eventDetails.selectedTab = index;
            $scope.card = $scope.eventAdminDares[index]
            $scope.card.index = index;
            commonService.analytics('event', {category : $scope.eventDetails.eventName, action : 'Event Dare Suggestion Click', label : index});
        }

        $scope.likeEvent = function(){
            if (!$scope.eventDetails.like_status)
            {
                APIService.likeEvent($scope.eventDetails.eventId, true);
                $scope.eventDetails.like_status = true;
                $scope.eventDetails.likes++;
            }
            else {
                APIService.likeEvent($scope.eventDetails.eventId, false);
                $scope.eventDetails.like_status = false;
                $scope.eventDetails.likes--;
            }

        }
        $scope.followEvent = function(){
            if ($scope.eventDetails.follow_status) {
                $scope.eventDetails.follows--;
                $rootScope.eventFollowers.forEach(function (follower, index) {
                    if (follower.userId === APIService.loggedInUserDetailsGlobal.userId) {
                        $rootScope.eventFollowers.splice(index, 1);
                        var followerDisplayCount = parseInt(($('.eventFollowersList').width()) / 85);
                        if (!$scope.isMobile) { followerDisplayCount *= 2; }
                        $rootScope.topFollowers = $rootScope.eventFollowers.slice(0, followerDisplayCount);
                    }
                })
            } else {
                $scope.eventDetails.follows++;
                $rootScope.eventFollowers.unshift(APIService.loggedInUserDetailsGlobal);
                var followerDisplayCount = parseInt(($('.eventFollowersList').width()) / 85);
                if (!$scope.isMobile) { followerDisplayCount *= 2; }
                $rootScope.topFollowers = $rootScope.eventFollowers.slice(0, followerDisplayCount);
            }
            APIService.followEvent($scope.eventDetails.eventId, !$scope.eventDetails.follow_status);
            $scope.eventDetails.follow_status = !$scope.eventDetails.follow_status
            commonService.analytics('event', {category : $scope.eventDetails.eventName, action : 'Follow Event', label : !$scope.eventDetails.follow_status});
        }

        //alert(userId)


        if (!$rootScope.userLoggedInDetails) {
            APIService.getLoggedInUserDetails().then(function (data) {
                //DEBUG &&  console.log(data);
                $scope.loggedInUserDetails = data;
            });
        } else {
            $scope.loggedInUserDetails =$rootScope.userLoggedInDetails
        }

        $scope.viewLiveDares = function () {
            $('.loading-icon').show();
            $rootScope.eventAdminDareResponses = [];
            $scope.viewingTab = 'liveDares';
            $scope.homeIndex = 0;
            if(!$scope.inside) {
                startEventDareScroll();
            }
            refreshEventDares('live');
        };

        $scope.viewPreviousDares = function () {
            $('.loading-icon').show();
            $scope.event_oldDareResponses = [];
            appendOldDaresToLive = false;
            $scope.viewingTab = 'previousDares';
            $scope.homeIndex = 0;
            if(!$scope.inside) {
                startEventDareScroll();
            }
            refreshEventDares('old');
        };

        $scope.viewWinningDares = function () {
            $scope.viewingTab = 'winningDares';
            $(window).off('scroll.eventDaresScroll');
            $scope.homeIndex = 0;
            if (!$rootScope.winningDares) {
                APIService.getEventWinners(eventName).then(function (data) {
                    $rootScope.winningDares = data;
                });
            }
        };

        $scope.viewEventDares = function () {
            $scope.showMedia = false;
            $(window).off('scroll.eventMediaScroll');
            $(window).scrollTop(0);
            if(!$scope.inside) {
                startEventDareScroll();
            }
        };

        $scope.viewMedia = function () {
            $scope.showMedia = true;
            $(window).off('scroll.eventDaresScroll');
            $(window).scrollTop(0);
            $('.highlighter.event-header-buttons').css('border-bottom', 'none');
            $('.highlighter.photos').css('border-bottom', '2px solid #007ad5');
            if (!eventMediaScrollStopped) { startEventMediaScroll(); }
        };

        $scope.eventMedia = [];

        var mediaIndex = 0,
            mediaCount = 12,
            mediaFlag = false,
            mediaScrollHeight,
            win = $(window);

        $rootScope.eventFollowers = [];

        function startEventMediaScroll() {
            setTimeout(function() {
                win.on("orientationchange", function () {
                    mediaScrollHeight = $('.eventMediaCnt').height();
                });
                win.on('scroll.eventMediaScroll', function () {
                    if (mediaFlag && win.scrollTop() > (mediaScrollHeight - APIService.scrollOffset)) {
                        mediaFlag = false;
                        getEventMedia();
                    }
                });
            }, 100);
        }
        var eventMediaScrollStopped = false;
        function getEventMedia() {
            $('.loading-icon').hide();
            APIService.getEventMedia(eventName, mediaIndex, mediaCount).then(function (data) {
                data.media.forEach(function (media) {
                    media.container_background_color = colors[Math.floor(Math.random() * colors.length)];
                    media.container_background_height = (window_width * media.inverseAspectRatio) + 'px';
                })
                $scope.eventMedia = $scope.eventMedia.concat(data.media);
                if (mediaIndex === 0) { $scope.total_mediaCount = data.total_count; }
                $scope.dashboard_eventMedia = $scope.eventMedia.slice(0, 12);
                setTimeout(function() {
                    mediaScrollHeight = $('.eventMediaCnt').height();
                    mediaIndex += mediaCount;
                    mediaFlag = true;
                    if(data.media.length === 0) {
                        win.off('scroll.eventMediaScroll');
                        eventMediaScrollStopped = true;
                    }
                }, 0)
            });
        }

        setTimeout(function () {
            getEventMedia();
        }, 2000);

        $(window).resize(function() {
            $(".event-media-cnt").css('height', 'auto');
        });

        $scope.refreshEvent = function () {
            $state.reload();
        }

        DEBUG && console.log("end of main conroller")
    };

    eventController.$inject = injectParams;

    angular.module('stichio').controller('eventController', eventController);
}());