(function(){
    var injectParams = ['$scope','$rootScope','APIService','$timeout','$window', '$location', 'commonService','$state', '$cookies']
    var ExploreDareSuggestionsController = function ($scope,$rootScope, APIService,$timeout,$window, $location, commonService,$state, $cookies) {
        var sug_index = 0,
            sug_count = 10,
            scrollHeight,
            sug_flag = false,
            no_data=false,
            currScroll=0,
            currScroll1= 0,
            minScrollHt= 0,
            genreBoxHt= 0,
            win = $(window);

        $scope.setSearchText('');
        $scope.$on('$viewContentLoaded', function(event) {
            commonService.analytics('pageview', {page: $location.url(), title: "Explore - Dare Suggestions"});
        });
        if(clearErrorMessageTimeout) {clearTimeout(clearErrorMessageTimeout)};
        commonService.checkOnline();
        APIService.scrollOff();
        var $genresSticky = $('#genresContainerExplore')
        var $dummygenres = $('#dummygenresContainerOuter')
        var genreBoxWidth = $genresSticky.width();
        var appBoxHt = $('#openInAppBox').height();
        var $header_sticky = $('.headerSticky');
        var $header3_sticky = $('.header3-sticky');
        $header_sticky.css('position', 'static');
        $header_sticky.show();
        minScrollHt = $header3_sticky.height() + $('#openInAppBox').height() + $header_sticky.height() + 30;

        var params = $location.search();

        $scope.selectGenre= function(genre){
            $scope.selectedGenreId = genre.id;
            $scope.exploreSuggestionApi=[];

            sug_index=0;

            sug_flag = false;
            scrollHeight=0;
            no_data=false;

            currScroll1 = win.scrollTop();

            $state.go('explore.dares',{genreId : genre.id }, {notify : false, reload : false});
            $scope.exploreSuggestionApi=[];
            $scope.exploreTrendingDare=null;
            $scope.exploreTrendingPeople=null;
            if (currScroll1 > minScrollHt){

                win.scrollTop(minScrollHt+1);
            }
            loadDareSuggestions([genre.id]);
            commonService.analytics('pageview', {page: $location.url(), title: "Explore - Dare Suggestions"});

        }

        $scope.exploreSuggestionApi = [];
        $scope.exploreTrendingDare=null;
            $scope.exploreTrendingPeople=null;
        var $loadingdares= $('#explore-loading-dares');
        var $addSugg= $('#explore-add-sugg');

        setTimeout(function () {

            win.on("orientationchange", function () {
                //scrollHeight = $('#exploreDareSuggestion').height();
                genreBoxWidth = $genresSticky.width();
            });
            win.on('scroll.exploreScroll', function () {

                var currScroll = win.scrollTop();
                if (currScroll > minScrollHt) {
                    $genresSticky.css({'position': 'fixed', 'top': genreBoxHt + 'px', 'width': genreBoxWidth});
                    $dummygenres.show();
                    $("#exploreDareSuggestion").css('min-height', '1000px');
                }
                else {
                    if ($scope.exploreSuggestionApi.length>0){
                        $genresSticky.css({'position': 'static'});
                        $dummygenres.hide();
                        $("#exploreDareSuggestion").css('min-height', '0px');
                    }

                }


            });
            if($cookies.get("newUserExploreHelp")){
                $("#new-user-explore").show()
            }
            else{
                $("#new-user-explore").hide()
            }
            $cookies.remove('exploreHelpBoxHome')
        }, 1000);


        var loadDareSuggestions = function (genres) {
            $loadingdares.show();
            $addSugg.hide();
            if (sug_index==0){
                clearErrorMessageTimeout=setTimeout(function () {
                    console.log("slow")
                    commonService.showMessage(commonService.serverSlowMessage);

                },commonService.apiWaitTime)
            }
            APIService.getExploreSuggestions(sug_index, sug_count, genres).then(function (data) {
                 clearTimeout(clearErrorMessageTimeout);

                if (!data){
                    console.log("error")
                    $loadingdares.hide();
                    $addSugg.show();
                    $('.search-loading-div.explore-dare').hide();
                    return;
                }
                 //commonService.hideMessage();
                console.log('exploreFeed', data);
                $loadingdares.hide();
                $addSugg.show();
                 $scope.exploreSuggestionApi=data.suggestions;
                $timeout(function() {
                    $scope.exploreTrendingDare = data.trending_dare;
                    $scope.exploreTrendingPeople = data.trending_people;
                    var adminDare_res_w = Math.min(screen.width - 12, 588);
                    if ($scope.exploreTrendingDare && $scope.exploreTrendingDare.response_type === "video") {
                        //For Videos for which inverseAspectRatios have not been calculated.
                        //if (scope.card.inverseAspectRatio === 1) {
                        //    scope.card.inverseAspectRatio = .7
                        //}
                        //Setting max inverseAspectRatio
                        $scope.exploreTrendingDare.inverseAspectRatio = Math.min(parseFloat($scope.exploreTrendingDare.inverseAspectRatio), 1.2);
                        $scope.exploreTrendingDare.adminDare_res_h = (adminDare_res_w * $scope.exploreTrendingDare.inverseAspectRatio) + 'px';
                    }
                }, 100);

                //if (scope.card.inverseAspectRatio <= 1) {
                $('.search-loading-div.explore-dare').css('display','none');
                setTimeout(function() {
                    //    if (currScroll1 > minScrollHt){
                    //        win.scrollTop(minScrollHt+1);
                    //}
                    scrollHeight = $('#exploreDareSuggestion').height();
                    sug_index += sug_count;
                    sug_flag = true;
                    if(data.length === 0) { no_data=true;}

                }, 0)
            }, function(){
                clearTimeout(clearErrorMessageTimeout);
                $loadingdares.hide();
                $addSugg.show();
                $('.search-loading-div.explore-dare').css('display','none');
            });
        };
        if ($rootScope.allDareGenres) {
            if (!params.genreId) {
                $scope.selectedGenreId = $rootScope.allDareGenres[0].id

            }
            else{
                $scope.selectedGenreId=params.genreId
                $scope.exploreTrendingDare=null;
                $scope.exploreTrendingPeople=null;
                setTimeout(function() {
                    $('.genresList').animate({scrollLeft: Math.max($('#' + $scope.selectedGenreId).position().left-(genreBoxWidth/2), 0) }, 500);
                });
            }
            loadDareSuggestions([$scope.selectedGenreId]);
        }
        else{
            APIService.getDareGenres().then(function (response) {
                response = response.filter(function (val) {
                    return val.name !== ''
                })
                $rootScope.allDareGenres = angular.copy(response);
                if (!params.genreId) {
                    $scope.selectedGenreId = $rootScope.allDareGenres[0].id
                }
                else{
                    $scope.selectedGenreId=params.genreId;
                    setTimeout(function(){

                        console.log("scrolll", Math.max($('#' + $scope.selectedGenreId).position().left-(genreBoxWidth/2), 0), $('#' + $scope.selectedGenreId).position().left, genreBoxWidth )
                        $('.genresList').animate({scrollLeft: Math.max($('#' + $scope.selectedGenreId).position().left - (genreBoxWidth/2),0)}, 500);

                    });
                }
                loadDareSuggestions([$scope.selectedGenreId]);
            });
        }

        if ((commonService.new_user || $cookies.get('new_user'))){
            $rootScope.new_user=true;
        }

            setTimeout(function () {
                if ($cookies.get("exploreHelpBox")){
                    $("#explore-help-box").show();
                    $("#explore-search-help-box").hide();
                    $('body').addClass('stop-scrolling1');

                }
                else if ($cookies.get("exploreSearchHelpBox")){
                    $("#explore-help-box").hide();
                    $("#explore-search-help-box").show();
                }

            },500);
            $scope.showNextHelpText = function () {
                $("#explore-help-box").hide();
                $("#explore-search-help-box").show();
                $cookies.remove("exploreHelpBox")

            }
            $scope.closeHelpBoxes = function () {
                $("#explore-help-box").hide();
                $("#explore-search-help-box").hide();
                $('body').removeClass('stop-scrolling1')
                $cookies.remove("exploreSearchHelpBox")
            }

    };
    ExploreDareSuggestionsController.$inject = injectParams
    angular.module('stichio').controller('ExploreDareSuggestionsController',ExploreDareSuggestionsController)
}());