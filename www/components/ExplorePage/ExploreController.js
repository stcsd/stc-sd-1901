(function () {
	var injectParams = ['$scope','APIService','$state', 'commonService', '$rootScope', '$timeout'];

    var exploreController = function ($scope,APIService,$state, commonService, $rootScope, $timeout) {
        setTimeout(function () {
            $.screentime.reset();
        }, 1000)
        commonService.removePopups()
        $rootScope.footerSelectedOption="explore";
        $scope.searchSuggText=""
        //$scope.Startsearch = false;

        if(!APIService.loggedInUserDetailsGlobal) {
            setTimeout(function () {
                APIService.getLoggedInUserDetails().then(function (data) {
                    //DEBUG &&  console.log(data);
                    $scope.userDetails = data;
                    console.log($scope.userDetails, 'Details')
                });
            }, 100);
        }
        else{
            $scope.userDetails = APIService.loggedInUserDetailsGlobal
        }

		//$scope.$on('$viewContentLoaded', commonService.observer1);
		//window.addEventListener('resize', commonService.observer2);

        $scope.searchQuery=function(){
            $state.go('search.dares', {query : $scope.searchSuggText})
            try {
                cordova.plugins.Keyboard.close();
            } catch (e) {
            }
        }

        $scope.setSearchText=function(text){
            $scope.searchSuggText=text
        }
        $scope.goBackSearch=function(){
            try {
                if (window.history.length > 1) {
                    window.history.back();
                }
                else{
                    $state.go('home.stiches', {})
                }
            } catch (e) {
                window.plugins.appMinimize.minimize();
            }
        }

        $('.dare-search-input').keypress(function (e) {
            if (e.which == 13) {
                $scope.searchQuery();
                return false;
            }
        });

        if (!$rootScope.isMobile) {
            if (!$rootScope.dareStats) {
                $timeout(function () {
                    APIService.getDareStats().then(function (data) {
                        $rootScope.dareStats = data;
                        console.log("userStats", data);
                    });
                }, 100);
            }
        }

		$scope.buttonViewChange = function (name) {
            $('.explore-btn').removeClass('active');
            $('.explore-btn.' + name).addClass('active');
        };

        //var lastScrollTop = 0,
        //    $header_sticky = $('.headerSticky'),
        //    $header3_sticky = $('.header3-sticky'),
        //    header3_sticky_offset = $header3_sticky.offset();
        //if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        //    var scrollHtLimit=$("#openInAppBox").height();
        //    $(window).scroll(function() {
        //        var st = $(this).scrollTop();
        //        // console.log(st, $dareStatsOffset.top);
        //        if (st > lastScrollTop) {
			//		$header_sticky.css({'position': 'static'});
			//		$header3_sticky.css({'position': 'static'});
        //        } else {
        //            $header_sticky.css({'position': 'fixed', 'top': '0'});
			//		if (st <= (header3_sticky_offset.top - 46)) {
        //                $header3_sticky.css({'position': 'static'});
	     //           }
			//		else {
			//			$header3_sticky.css({'position': 'fixed','top': '46px'});
			//		}
        //        }
        //
        //        if (st === 0) {
        //            $header_sticky.css({'position': 'static', 'display': 'block'});
        //        }
        //        if (st > scrollHtLimit){
        //            $rootScope.showAppOpenFooter = true;
        //        }
        //        lastScrollTop = st;
        //    });
        //} else {
        //    $header_sticky.css({'position': 'fixed', 'top':'0'});
        //    $(window).scroll(function () {
        //        if($(this).scrollTop() > (header3_sticky_offset.top - 46)){
        //            $header3_sticky.css({'position': 'fixed', 'top':'46px'});
        //        }
        //        else{
        //            $header3_sticky.css({'position': 'static'});
        //        }
        //    });
        //}
         $scope.refreshExplore = function () {
            $state.reload();
        }
    };
	



    exploreController.$inject = injectParams;

    angular.module('stichio').controller('exploreController', exploreController);
}());