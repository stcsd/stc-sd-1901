(function () {
    var injectParams = ['$timeout','$scope','$rootScope', '$window', '$stateParams', '$state','APIService','commonService','$location'];


    var dareController = function ($timeout,$scope,$rootScope, $window, $stateParams, $state, APIService,commonService,$location) {
        setTimeout(function () {
            $.screentime.reset();
        }, 3000)

        APIService.scrollOff();
        $rootScope.footerSelectedOption="dare";
        $scope.$on('$viewContentLoaded', function(event) {
            $timeout(function () {
                commonService.observer1();
                commonService.analytics('pageview', {page: $location.url(), title:"Dare Page"});
            }, 3000);
        });

        var dareSuggestionId = $stateParams.dareSuggestionId;
        $scope.completed_dares=[]
        APIService.getAdminDareInfo(dareSuggestionId).then(function(data){
            $scope.card = data;
            if (!data.isEventDare)
                $scope.card.creator = 'STICHIO';
        });
        $scope.new_card=null;
        $scope.$watch('card.dare_submitted', function(key){
            if ($scope.card && $scope.card.dare_submitted) {
                $scope.completed_dares.unshift(angular.copy($scope.card));
                $timeout(function() {
                    if ($scope.card.dare_submitted && $scope.new_card) {
                        $scope.card = angular.copy($scope.new_card);
                        $scope.new_card = null;
                    }
                }, 500);
            }
        });
        $scope.$watch('card.submitting_dare', function(key){
            if($scope.card && $scope.card.submitting_dare) {
                APIService.getNextSuggestion($scope.card.id).then(function(data){
                    $scope.new_card = data;
                    $scope.new_card.creator = 'STICHIO';
                });
            }
        });
    };

    dareController.$inject = injectParams;

    angular.module('stichio').controller('dareController', dareController);
}());