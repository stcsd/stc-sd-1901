(function () {
    var injectParams = ['$timeout', '$rootScope', '$scope','$state', 'commonService'];


    var policyController = function ($timeout, $rootScope, $scope, $state,commonService) {
        commonService.removePopups();
        $rootScope.footerSelectedOption="policy"
        $scope.parentObj = {};
        $scope.parentObj.termsButtonState = "";
        $scope.parentObj.privacyButtonSate="";
        $('.login-popup').removeClass('target')

        $(".policyBotton").click(function (event) {
            event.stopPropagation();
        });

        $scope.open_home_menu = function(){
            $('.ham-popup').addClass("target");
            //$('.main-stiches-cnt').css("display","none");
            $('body').addClass("prevent-scroll");
        }
        $('.close-ham-popup').click(function(){
            $('body').removeClass("prevent-scroll");
            //$('.main-stiches-cnt').css("display","block");
            $('.ham-popup').removeClass("target");
        });

        $scope.goHome =function () {
            $state.go('home.stiches');
        }

	};

    policyController.$inject = injectParams;
    angular.module('stichio').controller('policyController', policyController);
}());