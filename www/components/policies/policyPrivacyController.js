(function(){
    var injectParams = ['$rootScope', '$scope', ];
    var policyPrivacyController = function($rootScope, $scope){
        $scope.parentObj.termsButtonState = "";
        $scope.parentObj.privacyButtonSate="active";
    }

    policyPrivacyController.$inject = injectParams;
    angular.module('stichio').controller('policyPrivacyController', policyPrivacyController);
}());/**
 * Created by stichio-intern on 6/8/16.
 */
