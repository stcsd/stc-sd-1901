(function(){
    var injectParams = ['$rootScope', '$scope', ];
    var policyTermsController = function($rootScope, $scope){
        $scope.parentObj.termsButtonState = "active";
        $scope.parentObj.privacyButtonSate="";
    }

    policyTermsController.$inject = injectParams;
    angular.module('stichio').controller('policyTermsController', policyTermsController);
}());