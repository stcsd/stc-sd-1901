/**
 * Created by stichio-intern on 9/8/16.
 */
/**
 * Created by stichio-intern on 8/8/16.
 */
(function () {
    var injectParams = ['$rootScope', '$scope', 'contactService','APIService','$window','$state', 'commonService', '$timeout'];

    var contactController = function ($rootScope, $scope,contactService,APIService,$window,$state, commonService, $timeout) {
        //$scope.hiring = false;
        if (!$rootScope.userLoggedIn){
            return;
        }
        $timeout(function(){
            $scope.closeContactPopup = function () {
                commonService.closePopup('stichio-form')
            };
            //$rootScope.footerSelectedOption="contact"
            $scope.emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               //if(emailRegEx.test($scope.loginDetails.email) == false){
               //     $scope.loginDetails.emailErrorMsg = "Invalid email";
               //
               // }
            $('body').removeClass('prevent-scroll')
            $('body').removeClass('stop-scrolling')
            DEBUG &&  console.log("inside refreshFrameStiches")
            //if (commonService.loggedIn) {
            //    $timeout(function () {
            //        if ($rootScope.userLoggedInDetails.email.indexOf("facebook.com") > -1) {
            //            $scope.showEmailBox = true;
            //        }
            //    },8000);
            //}
            //else{
            //    $scope.showEmailBox=true;
            //}
            $scope.formData={}
            $scope.formData.email = $rootScope.userLoggedInDetails.email;
            if ($rootScope.userLoggedInDetails.email.indexOf("facebook.com") > -1) {
                $scope.formData.email=''
            }

            $scope.formData.message = "";
            var submitsuccess = true;
            $scope.submitDetails = function () {
                if (!submitsuccess) {
                    return
                }
                console.log("form submitted")
                if ( $scope.formData.message) {
                    submitsuccess = false;
                    var data = {
                        "email":  $scope.formData.email || $rootScope.userLoggedInDetails.email,
                        "message":  $scope.formData.message
                    }
                    contactService.submitDetails(data).then(function () {
                        $rootScope.success_msg = true;
                        $timeout(function(){
                            $scope.formData.message = "";
                            submitsuccess = true;
                            $scope.closeContactPopup();
                        },3000)
                    }, function() {
                        submitsuccess = true;
                    });
                }

            }
            $scope.goToHiring = function () {
                //$scope.hiring = true;
                $state.go('contact.hiring');
                $( "body" ).scrollTop( 0 );
            }
            $scope.goHome =function () {
                $state.go('home.stiches');
            }


            $(window).scroll(function () {
                if($(this).scrollTop() > 20)
                    $('.contact-us').hide();
                else
                    $('.contact-us').show();
            })

            $(".button").click(function (event) {
                event.stopPropagation();
            });

            $scope.contact = {};

            $scope.open_home_menu = function(){
                $('.ham-popup').addClass("target");
                //$('.main-stiches-cnt').css("display","none");
                $('body').addClass("prevent-scroll");
            }
            $('.close-ham-popup').click(function(){
                $('body').removeClass("prevent-scroll");
                //$('.main-stiches-cnt').css("display","block");
                $('.ham-popup').removeClass("target");
            });


        }, 5000)


    };


    DEBUG &&  console.log("inside get FrameStiches controller")
    contactController.$inject = injectParams;

    angular.module('stichio').controller('contactController', contactController);

}());

