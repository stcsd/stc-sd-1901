/**
 * Created by stichio-intern on 9/8/16.
 */
(function () {

    var injectParams = ['$http', '$q','commonService'];
    var contactFactory = function ($http, $q, commonService) {

    var factory = {};

        factory.submitDetails = function(data){
            $(".contact-button span").hide();
            $(".contact-button img").show();
            return $http.put(commonService.baseUrl + "submitcontactform", data).then(function (results) {
                $(".contact-button span").show();
                $(".contact-button img").hide();
            });
        };
        return factory;
    };

    contactFactory.$inject = injectParams;

    angular.module('stichio').factory('contactService', contactFactory);

}());
