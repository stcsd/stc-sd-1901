(function () {
    var vivekId1 = "0046ce99-d8b0-4f4d-bf06-0d69e5de8b5f";
    var VivekId2 = "1fb5cc77-de4b-4444-8fb2-3fe709f9eede";
    var rajanId = "5a9e016f-298a-4f63-9ea9-a64d40d10a2c";
    var swagatika = "ca3a3e25-5e20-4459-a54a-d1222c5d6b00";
    var injectParams = ['$http', '$q', 'commonService'];
    var homeFactory = function ($http, $q, commonService) {

    var factory = {};

factory.loggedInUserId =  "";

    factory.profileUserId =  swagatika;
        //"290b5b87-3a59-429f-89ca-7812acc87f69"; //"b57a91d5-ea5f-442b-8621-f5128b0974b1";

    //var commonService.baseUrl = "https://yudql2tsmh.execute-api.us-east-1.amazonaws.com/testStichio/test/"
    var callbackContinue = "";
    var callback = "";
    var followerUser = "api/v1/users/" + commonService.loggedInUserId + "/followees/users/";
    var userLikes = "api/v1/users/" + commonService.loggedInUserId + "/likes/";
    var serviceBase = '/api/dataservice/';
        factory.searchedQueries = [];   //for saving searched stiches
        factory.searchedQueriesData = [];
        factory.searchedFrameQueries = []; //["keyword"] //for saving searched frames
        factory.searchedFrameQueriesData = []; //["keyword","stiches"]

    var frameId = "426a9af7-43ac-4f7e-8a22-8ac4a86edab1";
     
    //      $(window).load(function() {
    //    console.log("load the page ......................");
    // });
        factory.getFrameStiches = function(index, count){
            if(!index)
                index = 0;
            if(!count)
                count = 16;
            DEBUG &&  console.log("before getting FrameStiches");
            return $http.get(commonService.baseUrl + "api/v1/frames/" + factory.frameId + "/stiches?format=json&index=&" + index + "count=" + count + "&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("after getting FrameStiches");
                DEBUG &&  console.log(results);
                var frameStiches = results.data.stiches;
                DEBUG &&  console.log(frameStiches);
                return frameStiches;
            });
        };

        factory.getHomeStiches = function(index, count, picked_index){
            if(!index)
                index = 0;
            if(!count)
                count = 16;
            DEBUG &&  console.log("before getting homesStiches");
            DEBUG &&  console.log(commonService.baseUrl + "api/v1/home/stiches?format=json&index=" + index + "&count=" + count + "&picked_index=" + picked_index + "&requesterId=" + commonService.loggedInUserId)

            return $http.get(commonService.baseUrl + "api/v1/home/stiches?get_suggestions=True&get_follow_suggs=True&new=True&newFeed=True&format=json&index=" + index + "&count=" + count + "&picked_index=" + picked_index + "&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                try {
                    return results.data;
                } catch (e) {
                    return null;
                }
            });
        };

        factory.getTodaysPicks = function(){
            DEBUG &&  console.log("before getting todayspicks");
            return $http.get(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/frames/?format=json&q=profileView", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("after getting todayspicks");
                DEBUG &&  console.log(results);
                var todaysPicks = results.data.Frames
                return todaysPicks;
            });
        };

        factory.getAllStiches = function(){
            DEBUG &&  console.log("get allstiches")
            return $http.get(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/stiches/?format=json&count=100&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results);
                var allStiches = results.data.stiches;
                DEBUG &&  console.log(allStiches);
                return allStiches;
            });
        };

        factory.getStichesCount = function(){
            return $http.get(commonService.baseUrl + "api/v1/frames/"  + factory.frameId +  "/stiches/?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getLikesCount = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/likes?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getFollowersCount = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followers/?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getFrameDetails = function(){
            return $http.get(commonService.baseUrl + "api/v1/frames/"  + factory.frameId +  "/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };


        factory.getUserDetails = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + commonService.loggedInUserId +  "/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                commonService.setLoggedInUserAvatar(results.data.avatar)
                return results.data;
            });
        };


        factory.getAllFollowers = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followers/?format=json&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results);
                var allFollowers= results.data.Followers;
                DEBUG &&  console.log(allFollowers);
                return allFollowers;
            });
        };

        factory.followUser = function(followedUser){

            return $http.put(commonService.baseUrl + followerUser, followedUser).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                ////alert("Error while following user");
                DEBUG &&  console.log("Error while following user");
                DEBUG &&  console.log(error);
            });
        };

        factory.unFollowUser = function(unFollowedUserId){
            DEBUG &&  console.log("unFollowedUserId");
            DEBUG &&  console.log(unFollowedUserId);
            var url = commonService.baseUrl + "api/v1/users/" + commonService.loggedInUserId + "/followees/users/" + unFollowedUserId;
            DEBUG &&  console.log(url);

            return $http.delete(url).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                ////alert("Error while unfollowing user");
                DEBUG &&  console.log("Error while unfollowing user");
                DEBUG &&  console.log(error);
            });
        };

        factory.getSearchStiches = function(query, index, save, count){
            DEBUG &&  console.log("before getting homesStiches");
            if (query != "")
                return $http.get(commonService.baseUrl + "api/v1/search?format=json&query=" + query  + "&requesterId=" + commonService.loggedInUserId + "&index=" + index + "&count=" + count  , {timeout: commonService.canceler.promise}).then(function (results) {
                    DEBUG &&  console.log("after getting FrameStiches");
                    DEBUG &&  console.log(results);
                    var homeStiches = results.data.stiches;
                    if(save) {
                        factory.searchedQueries.push(query)
                        if (factory.searchedQueries.length > 10) {
                            factory.searchedQueries = factory.searchedQueries.slice(1, factory.searchedQueries.length);
                        }
                        factory.searchedQueriesData.push(query.replace(/ /g, ''), results.data.stiches)
                        if (factory.searchedQueriesData.length > 20) {
                            factory.searchedQueriesData = factory.searchedQueriesData.slice(2, factory.searchedQueriesData.length);
                        }
                        console.log(factory.searchedQueries, "searchedqueries")
                        console.log(factory.searchedQueriesData, "searcheddata")
                    }
                    return homeStiches;
                });
            else {
                return $http.get(commonService.baseUrl + "api/v1/home/stiches?format=json&index=" + index + "&count=" + count + "&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                    DEBUG &&  console.log("after getting FrameStiches");
                    DEBUG &&  console.log(results);
                    var homeStiches = results.data.stiches;
                    if(save) {
                        factory.searchedQueries.push(query)
                        if (factory.searchedQueries.length > 10) {
                            factory.searchedQueries = factory.searchedQueries.slice(1, factory.searchedQueries.length);
                        }
                        factory.searchedQueriesData.push(query.replace(/ /g, ''), results.data.stiches)
                        if (factory.searchedQueriesData.length > 20) {
                            factory.searchedQueriesData = factory.searchedQueriesData.slice(2, factory.searchedQueriesData.length);
                        }
                        console.log(factory.searchedQueries, "searchedqueries")
                        console.log(factory.searchedQueriesData, "searcheddata")
                    }
                    return homeStiches;
                });
            }
        };

        factory.getSearchFrames = function(query, index, count, save){
            return $http.get(commonService.baseUrl + "api/v1/searchframes/" + "?format=json&query=" + query + "&count=" + count + "&index=" + index, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("URLLLLLL")
                DEBUG &&  console.log(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/frames/?format=json&q=profileView&requesterId=" + commonService.loggedInUserId);
                var allFrames = results.data.Frames;
                if(save) {
                    DEBUG && console.log(allFrames);
                    factory.searchedFrameQueries.push(query.replace(/ /g, ''))
                    if (factory.searchedFrameQueries.length > 10) {
                        factory.searchedFrameQueries = factory.searchedFramedQueries.slice(1, factory.searchedFrameQueries.length);
                    }
                    factory.searchedFrameQueriesData.push(query.replace(/ /g, ''), results.data.Frames)
                    if (factory.searchedFrameQueriesData.length > 20) {
                        factory.searchedFrameQueriesData = factory.searchedFrameQueriesData.slice(2, factory.searchedFrameQueriesData.length);
                    }
                    console.log(factory.searchedFrameQueries, "searchedqueries")
                    console.log(factory.searchedFrameQueriesData, "searcheddata")
                }
                return allFrames;
            });
        };

        factory.getUserFollowees = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followees/users", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data;
            });
        };
        factory.getSearchTags = function(){
            return $http.get(commonService.baseUrl + "api/v1/searchtags", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data;
            });
        };

        factory.getFrameFollowees = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followees/frames", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };
        factory.likeStich = function(likedStich){

            return $http.put(commonService.baseUrl + userLikes, likedStich).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                //alert("Error  : not liked");
                DEBUG &&  console.log("Error while following user");
                DEBUG &&  console.log(error);
            });
        };

            factory.unlikeStich = function(stitchId){
            //DEBUG &&  console.log("unFollowedUserId");
            //DEBUG &&  console.log(unFollowedUserId);
            var url = commonService.baseUrl + "api/v1/users/" + commonService.loggedInUserId + "/likes/" + stitchId;
            DEBUG &&  console.log(url);

            return $http.delete(url).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                //alert("Error : not unliked");
                DEBUG &&  console.log("Error while unfollowing user");
                DEBUG &&  console.log(error);
            });
        };

        factory.setProfileUserID = function(tempProfileUserId){
            factory.profileUserId = tempProfileUserId
        };

        factory.setSearchQuery = function(searchquery){
            factory.searchquery = searchquery
        };


        factory.setFrameId = function(tempFrameId){
            factory.frameId = tempFrameId;
        }
        return factory;
    };

    homeFactory.$inject = injectParams;

    angular.module('stichio').factory('homeService', homeFactory);

}());