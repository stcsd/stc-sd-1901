
(function () {
    var injectParams = ['$rootScope', '$scope' , 'homeService', '$window', 'APIService','commonService', '$state', '$cookies', '$stateParams', '$timeout', 'cacheService'];
    DEBUG &&  console.log("inside get home controller")


    var homeStichesController = function ($rootScope, $scope, homeService, $window,APIService, commonService, $state, $cookies, $stateParams, $timeout, cacheService) {

        if (!$rootScope.userLoggedIn){
            return;
        }
        $scope.isHome=true;
        $scope.onPulling = function (data) {
            console.log("data pull", data)
        }
        commonService.checkOnline();
         var dareSuggestionId = $stateParams.dareSuggestionId;
         var conversationId = $stateParams.conversationId;
        console.log("conversationId", conversationId)
        if (dareSuggestionId){
            $timeout(function () {
                $rootScope.openDareResponsesPopup(null, dareSuggestionId);
                $rootScope.closeAdminDarePopup();
            }, 1000);
        }
        if (conversationId){
            $timeout(function () {
                $rootScope.openNewChatPopup();
            }, 3000);
        }

        var recentDaresCache=cacheService.get('recent_dares', 'temp');
        if(recentDaresCache.data){
            $scope.recentDareTiles=recentDaresCache.data;
        }
        if (recentDaresCache.isExpired) {
            APIService.getRecentDares(3).then(function (data) {
                $scope.recentDareTiles= data;
                console.log("RecentDareCount", data);
            });
        }

        //
        //if (!CacheFactory.get('homeCache')) {
        //    // or CacheFactory('bookCache', { ... });
        //    CacheFactory.createCache('homeCache', {
        //        deleteOnExpire: 'aggressive',
        //        storageMode: 'localStorage'
        //    });
        //}
        //var homeCache = CacheFactory.get('homeCache');



        var picked_index = 0;
        var homeIndex = homeService.homeIndex1 || 0;
        var lastScrollTop = 0,
            $homeHeaderSticky = $('.home-header-sticky'),
            $homeFirstLoadIcon=$("#home_first_load"),
            $homeLoadingIcon=$("#home-loading-icon");
        //if (!$rootScope.event_banners) {
        //    APIService.getEventBanners().then(function (data) {
        //        $rootScope.event_banners = data;
        //    })
        //}
        console.log("$homeHeaderSticky",$homeHeaderSticky)
        $homeFirstLoadIcon.hide();
        $homeLoadingIcon.hide();

        //$scope.openTrendingDarePopup = function (card) {
        //    if (card.feed_type === 'dare') {
        //        $scope.openDarePopup(card, true, false, 'trendingDares');
        //    } else {
        //        $scope.openAdminDarePopup(card, true, false, 'trendingDares');
        //    }
        //};

        //$scope.$on('$viewContentLoaded', commonService.observer1);
        //window.addEventListener('resize', function () {
        //    commonService.observer2();
        //    position_find_friends_tab();
        //});

        //function position_find_friends_tab() {
        //    var $ffTab = $('#find-friends-tab');
        //    var $maTab = $('#my-activity-tab');
        //    var mq = window.matchMedia( "(max-width: 881px)" );
        //    if (mq.matches) {
        //        var ma_tab = $maTab.detach();
        //        $('.center-box #my-activity-tab-cnt1').append(ma_tab);
        //
        //        //var ff_tab = $ffTab.detach();
        //        //$('.center-box #find-friends-tab-cnt1').append(ff_tab);
        //    } else {
        //        ma_tab = $maTab.detach();
        //        $('.right-box #my-activity-tab-cnt2').append(ma_tab);
        //
        //        //ff_tab = $ffTab.detach();
        //        //$('.right-box #find-friends-tab-cnt2').append(ff_tab);
        //    }
        //}
        //setTimeout(function () {
        //    position_find_friends_tab();
        //}, 100);

        var dare_sug_cards = angular.copy(homeService.homeDareSuggestion) || []
        var dare_sug_cards_copy = [];
        var stich_cards = [];
        var sug_flag = true;
        var stich_flag = true;
        if(clearErrorMessageTimeout) {clearTimeout(clearErrorMessageTimeout)};
        DEBUG && console.log("inside homeStiches")
        //refreshTodaysPicks();
        APIService.scrollOff();

        $scope.inside = true;
        var scrollheight = $('.main-stiches-cnt').height();
        var stichColHeights = [],
            StichesinitialHeight = 0;
        var homeCount = 4
        //for (var i = 0; i < APIService.stichesColCount; i++) {
        //    stichColHeights.push(StichesinitialHeight);
        //}

        DEBUG && console.log(stichColHeights);

        function reStack() {
            $scope.inside = true;
            APIService.stichesCount = 4
            homeCount = APIService.stichesCount;
            scrollheight= $('.main-stiches-cnt').height();
            DEBUG && console.log(scrollheight);
            $scope.inside = false;
            // }, 100);
        }
        $scope.refreshActive=true;
        var $win=$(window)
        var scrolltimeout;
        var headerVisible=true;
        var $explorehelpbox=null;
        var exploreBoxScrollHt=4000;
        var onScrollFn = function () {
            var st = $win.scrollTop();
            if (st> exploreBoxScrollHt){
                if ($explorehelpbox) {
                    $explorehelpbox.show()
                }
            }
            if (st>20){
                if ($scope.refreshActive) {
                    $scope.refreshActive = false;
                }

            }
            else{
                if(!$scope.refreshActive) {
                    $scope.refreshActive = true;
                }
            }
            if (st > 50 && st > lastScrollTop) { // scroll down

                if(headerVisible) {
                    $homeHeaderSticky.hide();
                    headerVisible=false;
                }
                //$homeIcons.hide();
            }
            else { // scroll up
                if (st<(lastScrollTop-50)) {
                    if (!headerVisible) {
                        $homeHeaderSticky.show();
                        headerVisible=true;
                    }
                }
                //$homeIcons.show();
            }
            lastScrollTop = st;
            if (!$scope.inside) {
                if ($win.scrollTop() > scrollheight - APIService.scrollOffset) {
                    if (!$scope.inside) {
                        console.log('fetching stiches')
                        $scope.inside = true;
                        refreshHomeStiches();
                    }
                }
            }
            // do the onscroll stuff you want here
        };

        $timeout(function () {
            commonService.infiniteScroll($win, onScrollFn, 'scroll.homeStichesScroll')
        },100);


        homeService.cachedHomeStiches = homeService.cachedHomeStiches|| []
        //$scope.stiches=[]
        $scope.stiches = homeService.cachedHomeStiches || [];
        //}, 10);
        homeService.homeDareSuggestion = homeService.homeDareSuggestion || []
        var inverseAspectRatios = [];
        if ($scope.stiches.length==0) {
            if (!$cookies.get("signup_follow")) {
                refreshHomeStiches();
            }
        }
        else{
            $timeout(function(){
                console.log(".main-stiches-cnt.height", $('.main-stiches-cnt').height())
                scrollheight= $('.main-stiches-cnt').height();
                homeIndex=homeService.homeIndex;
                $scope.inside = false;
            },1000);
        }
        //$window.scrollTo(0, 0);

        $scope.$on('loggedIn', function (event, obj) {
            $scope.loggedIn = true
        })

        $scope.$on('loggedOut', function (event, obj) {
            $scope.loggedIn = false;
        })

        //$rootScope.enterDareZone = function(){
        //    commonService.analytics('event', {category : 'DareZone', action : 'Click', label : 'Success'});
        //    $state.go('profile.dares', {"profileId":commonService.loggedInUserId, "dareZone": true})
        //}
        $rootScope.enterEvent = function(eventName){
            commonService.analytics('event', {category : eventName, action : 'Click', label : 'Success'});
            $state.go('events.dares', {"eventName":eventName})
        }

        var card2 = [], card3 = [];

        var stopHomeFeedAppending = false;
        $rootScope.refreshHome = function (from) {
            $win.scrollTop(0)
            homeService.cachedHomeStiches = []
            homeService.homeIndex1 = 0
            if (from == 'top') {
                $scope.stiches = []
            }
            homeIndex = 0
            stich_cards = []
            dare_sug_cards = angular.copy(homeService.homeDareSuggestion)
            stopHomeFeedAppending = true
            if (stich_flag) {
                stich_flag = false
                $('.search-loading-div').show();
                $scope.inside=true;
                refreshHomeStiches(true);
                commonService.analytics('event', {category : 'Refresh Home', action : 'Click', label : 'Success'});
            }


        }
        $rootScope.clearHome=function(){
            $scope.stiches = []
        }
        function refreshHomeStiches(pass) {
            DEBUG && console.log("inside homeStiches function")

            try {
                if (homeIndex==0){
                    var homeCache=cacheService.get('home_feeds', 'temp');
                    $scope.stiches= homeCache.data|| []
                    clearErrorMessageTimeout=setTimeout(function () {
                        console.log("slow")
                        commonService.showMessage(commonService.serverSlowMessage)
                    },commonService.apiWaitTime);
                    if ($scope.stiches.length>0){
                        $homeFirstLoadIcon.show();
                    }
                    else{
                         $homeLoadingIcon.show();
                    }
                    $timeout(function(){APIService.getUnseenPendingDares(commonService.loggedInUserId).then(function (data) {
                        $rootScope.unseenPendingDares= data.count;
                        commonService.unseenPendingDaresCount=$scope.unseenPendingDares;
                        console.log("unseenDares",data.count);
                    });},1000);
                }
                else{
                    $homeLoadingIcon.show();
                }

                homeService.getHomeStiches(homeIndex, homeCount, picked_index).then(function (data) {
                    if (homeIndex==0){
                        $scope.stiches=[]
                    }
                    $homeFirstLoadIcon.hide();
                    $homeLoadingIcon.hide();
                    clearTimeout(clearErrorMessageTimeout);
                    //commonService.hideMessage();
                    if (!data){
                        console.log("error")
                        $scope.inside=false;
                        commonService.showMessage(commonService.serverSlowMessage)

                        return;
                    }

                    picked_index = data.picked_index + 1
                    var stichIndex = data.index
                    data = data.stiches
                    if (!pass) {
                        if (stopHomeFeedAppending) {
                            stopHomeFeedAppending = false
                            $scope.inside = false
                            return
                        }
                    }
                    else {
                        $scope.stiches = []
                    }
                    $('.search-loading-div').hide()
                    $scope.$broadcast('scroll.refreshComplete');
                    DEBUG && console.log(data, homeIndex, $scope.stiches.length);

                    $scope.stiches = $scope.stiches.concat(data);
                    if (homeIndex==0 && data.length>0) {
                        homeCache.cache.put('home_feeds', data)
                    }
                    //console.log("homecache", homeCache.get('feeds'))
                    if ($rootScope.new_user && !$cookies.get("watchDareHelpBox")) {
                        homeService.cachedHomeStiches = $scope.stiches
                        homeService.homeIndex = stichIndex
                    }
                    $timeout(function () {
                        //stichColHeights = stackStiches(StichesDetails);
                        //DEBUG && console.log(stichColHeights);
                        if (stichIndex) {
                            homeIndex = stichIndex
                        }
                        else {
                            homeIndex += homeCount;
                        }

                        scrollheight = $('.main-stiches-cnt').height();
                        $scope.inside = (data.length == 0)
                        stich_flag = true

                        //if (data.length == 0) {
                        //    $(window).on('scroll.dareSugScroll', function () {
                        //        if ($(this).scrollTop() > scrollheight- APIService.scrollOffset) {
                        //            if (sug_flag) {
                        //                getDareSuggestions();
                        //            }
                        //        }
                        //    });
                        //}
                        $scope.$apply();
                    }, 0.1);

                }, function(){
                    $homeFirstLoadIcon.hide();
                    $homeLoadingIcon.hide();
                    clearTimeout(clearErrorMessageTimeout);
                });
            } catch (e) {
                $homeFirstLoadIcon.hide();
                    $homeLoadingIcon.hide();
                clearTimeout(clearErrorMessageTimeout);
                console.error(e);
            }
        };

        $timeout(function () {
            if($cookies.get("newUserHomeHelp")){
                    $("#new-user-home").show()
                    helpboxshown=true;
                }
                else{
                    $("#new-user-home").hide()
                }
            if ($cookies.get("exploreHelpBoxHome")) {
                $explorehelpbox = $("#newUserExploreHelpBox")
                if (!$rootScope.new_user){
                    exploreBoxScrollHt=2000;
                }
            }
        },500);


        //if (!$scope.isMobile) {
        if (!$rootScope.dareStats) {
            APIService.getDareStats().then(function (data) {
                $rootScope.dareStats = data;
                console.log("userStats",data);
            });
        }
        //}
    };
    DEBUG &&  console.log("inside get homeStiches controller")
    homeStichesController.$inject = injectParams;

    angular.module('stichio').controller('homeStichesController', homeStichesController);

}());