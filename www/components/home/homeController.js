(function () {
    var injectParams = ['$scope', '$cookies',  'homeService', '$window', '$stateParams', '$state', 'loginService','commonService', 'APIService', '$filter','$rootScope', '$location'];


    var homeController = function ($scope , $cookies , homeService, $window, $stateParams, $state, loginService, commonService, APIService, $filter, $rootScope, $location) {

        $scope.$on('$viewContentLoaded', function(event) {
            setTimeout(function () {
                if ($rootScope.userLoggedIn) {
                    commonService.analytics('pageview', {page: $location.url(), title:"Home"});
                }
                else {
                    commonService.analytics('pageview', {page: $location.url(), title:'Home - Not logged in'});
                    //$window.ga('UsernameTracker.send', 'pageview', {page: $location.url(), title:"Home - Not logged in"});
                }
            },1000);
        });
        if (!$rootScope.userLoggedIn){
            return;
        }
        setTimeout(function () {
            $.screentime.reset();
        }, 5000)

        console.log("!$rootScope.userLoggedIn", !$rootScope.userLoggedIn)

        $rootScope.footerSelectedOption="home";

        commonService.removePopups();

        $scope.isMobile = false
         if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $scope.isMobile = true;

            }

        APIService.cancel();

        //for (var i=0; i < APIService.topics.length; i++ )
        //{
        //    topicsDimDetails.inverseAspectRatios[i] = 1;
        //}

        if($cookies.get("credChanged") == "false"){
            console.log('CREDENTIAL FLAG NOT SET');
        }else{
            if($cookies.get("credChanged") == "true"){
                $rootScope.displayloginpopup();
                $cookies.remove("credChanged");
            }
        }
        //$scope.homeMenuOpen = false

         /*$scope.open_home_menu = function(){
            if (!$scope.homeMenuOpen ) {
                $scope.homeMenuOpen = true
                $('.home-menu-options').show()
                outerclickscallback();

            }
            else{
                $scope.homeMenuOpen = false
                $('.home-menu-options').hide()
            }
        }

        function outerclickscallback(){
            $('.container').click(function(e){
                if($(e.target).is('#menu-button *')){
                    return
                }
                if($scope.homeMenuOpen = true){
                    $('.home-menu-options').hide()
                    $scope.homeMenuOpen = false
                }

            })
        }*/
        $('.close-ham-popup').click(function(){
            $('body').removeClass("prevent-scroll");
            $('.ham-popup').removeClass("target");
        });

        var scollHeightList = [3500, 2500, 2500],
            allowedScrollHeight = scollHeightList[0];
        var lastScrollTop =0, scrollCount = 1;

         if($scope.loggedIn){
            $(".header-login-btn").hide();
              $('login-popup').removeClass('target')
        }else{
            $scope.loggedIn = false
        }

        //$(window).scroll(function() {
        //if ($(this).scrollTop() > 62) {
        //    $('.home-icons-box.fixed').css("display", "flex")
        //
        //}
        //else {
        //    $('.home-icons-box.fixed').css("display", "none")
        //}

    $scope.home = {
            stichesCount: 0,
            likesCount: 0,
            followersCount: 0,
            userId : "",
            shiftBy:2 //number of Today's picks to shift on clicking on left and right arrows
        };
    //$scope.todaysPicksIndex = 0


        //if (commonService.loggedInUserId ) {
        //    homeService.getUserDetails().then(function (data) {
        //        DEBUG && console.log(data);
        //        $scope.home.userDetails = data;
        //        $rootScope.userDetailsGlobal = data;
        //    });
        //}


        if (!$rootScope.userLoggedInDetails) {
            APIService.getLoggedInUserDetails();
        }


    };

    homeController.$inject = injectParams;
    
    angular.module('stichio').controller('homeController', homeController);
}());