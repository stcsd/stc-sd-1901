(function () {
	var injectParams = ['$scope', '$rootScope', 'APIService', '$window', '$state', 'commonService','$location', '$sce', '$timeout', '$cookies'];

    var userProfileDaresController = function ($scope, $rootScope, APIService, $window, $state, commonService,$location, $sce, $timeout, $cookies) {

        //$scope.openVideo()
        setTimeout(function() {
            console.log("uinside userprofiledares controller", $location.url())

            var directUrl = false;

            $scope.$on('$viewContentLoaded', function (event) {
                if ($scope.isOwnProfile) {
                    commonService.analytics('pageview', {page: $location.url(), title: "Profile-Own-Dares"});
                }
                else {
                    commonService.analytics('pageview', {page: $location.url(), title: "Profile-Others-Dares"});

                }
                //console.log("page_url", $location.url())
            });

            APIService.scrollOff();
            var params = $location.search();
            if (params.dareId) {
                $timeout(function(){
                    $scope.openDarePopup(null, false, true, false, params.dareId);
                }, 100)
                directUrl = true;
                //APIService.getDareInfo(params.dareId).then(function(data){
                //
                //
                //    $scope.openDarePopup(data, false, true);
                //
                //})
            }
            else if (params.adminDareId) {
                $timeout(function() {
                    $scope.openAdminDarePopup(null, $scope.isOwnProfile, true, false, params.adminDareId);
                }, 100);
                directUrl = true;
                //APIService.getAdminDareResponseInfo(params.adminDareId).then(function(data) {
                //    console.log('adminDarePopup', data);
                //    $scope.openAdminDarePopup(data, $scope.isOwnProfile, true);
                //})
            }
            console.log("inside 2")
            if (params.nav_from) {
                commonService.analytics('event', {category: 'My Activity', action: params.nav_from, label: 'success'});
            }


            //$rootScope.urlState = $state.current.name;
            $('.info-btn').removeClass('btn-active');
            $('.info-btn.friends').addClass('btn-normal');
            $('.info-btn.activity').removeClass('btn-normal');
            $('.info-btn.activity').addClass('btn-active');

            var $profile_loading_icon = $('#profile-loading-icon');
            var sug_index = 0,
                sug_count = 3,
                scrollHeight,
                sug_flag = false,
                win = $(window);

            $scope.userDares = {};
            $scope.userDares.dareApi = [];
            $scope.userDares.all = [];
            $scope.userDares.completed = [];
            $scope.userDares.remaining = [];
            $scope.userDares.given = [];

            var helpboxshown=false;
            if($scope.isOwnProfile){
                if($cookies.get("newUserProfileHelp")){
                    $("#new-user-profile").show()
                    helpboxshown=true;
                }
                else{
                    $("#new-user-profile").hide()
                }
            }
            if($rootScope.userLoggedInDetails && $scope.isOwnProfile && $rootScope.userLoggedInDetails.referral_prog.enabled && !helpboxshown) {
                if ($rootScope.userLoggedInDetails.is_featured) {
                    var showCount = $cookies.get('showFeaturedMessage')
                    if (showCount ) {
                        if (parseInt(showCount) < 5) {
                            $cookies.put('showFeaturedMessage', parseInt(showCount) + 1)
                            $("#featured-user-profile").show()

                        }
                    }
                    else {
                        $("#featured-user-profile").show()
                        $cookies.put('showFeaturedMessage', 1)
                    }

                }
                else {
                    $cookies.remove('showFeaturedMessage')
                    $("#featured-user-profile").hide()
                }

            }

            var dareType = 'all';
            dareType = params.dareType;
            if (commonService.unseenPendingDaresCount && params.nav_from=='header'){
                dareType='remaining'
            }
            if (!dareType) {
                if($scope.isOwnProfile){
                    dareType = 'completed';
                }
                else{
                    dareType = 'all';
                }
            }
            else{
                if(dareType=="remaining"){
                    commonService.analytics('event', {category: 'Click', action: "profile_remaining", label:commonService.unseenPendingDaresCount });
                }
            }



            // if (commonService.unseenPendingDaresCount) {
            //     $scope.unseenDaresCount=commonService.unseenPendingDaresCount;
            //     console.log("aaa")
            // }
            // else {
            //    APIService.getUnseenPendingDares(commonService.loggedInUserId).then(function (data) {
            //        $scope.unseenDaresCount=data.count;
            //        commonService.unseenPendingDaresCount=$scope.unseenDaresCount;
            //    });
            // }



            $('.dare-filter-tab-cnt > div').click(function () {
                $(this).siblings().css('border-bottom', '2px solid transparent')
                $(this).siblings().find('p.dare-label').css('color', '#53585f');
                $(this).css('border-bottom', '2px solid #007ad5')
                $(this).find('p.dare-label').css('color', '#007ad5');
            })
            $scope.showBookmarks = function () {
                loadDareBookmarks();
                activateBookmarkScroll();
            }

            $scope.setDareType = function (type) {



                if(type=="remaining"){
                    $scope.unseenDaresList=commonService.unseenPendingDaresCount;
                    commonService.unseenPendingDaresCount=0;
                    $scope.unseenDaresCount=0;
                }


                var elem = $('.dare-filter-tab-cnt .' + type);
                elem.siblings().css('border-bottom', '2px solid transparent')
                elem.siblings().find('p.dare-label').css('color', '#53585f');
                elem.css('border-bottom', '2px solid #007ad5')
                elem.find('p.dare-label').css('color', '#007ad5');
                if (type != "bookmarks") {


                    win.off('scroll.dareBookmarks');
                    dareType = type;
                    sug_index = 0;
                    sug_flag = false;
                    $scope.userDares[dareType] = [];

                    setTimeout(function () {
                        scrollHeight = $('.main-dares-cnt').height();
                        $rootScope.loadActivityFeed(dareType);
                        activateScroll();
                    }, 10);
                }
                $scope.closeShareDare();
            };



            var activateScroll = function () {
                setTimeout(function () {
                    win.on("orientationchange", function () {
                        scrollHeight = $('.main-dares-cnt').height();
                    });
                    win.on('scroll.activityFeedScroll', function () {
                        if (sug_flag && win.scrollTop() > (scrollHeight - APIService.scrollOffset)) {
                            sug_flag = false;
                            $rootScope.loadActivityFeed(dareType);
                        }
                    });
                }, 100);
            };


            var inputdata = {
                userId: APIService.profileUserId
            };
            if (params.dareZone) {
                inputdata["dareZone"] = true;
            }
            var topVideo="";
            var topVideo2="";


            $rootScope.loadActivityFeed = function (dareType) {
                $profile_loading_icon.show();
                $scope.dare_type = dareType;
                setTimeout(function () {
                    APIService.getUserDares(sug_index, sug_count, inputdata, dareType,$scope.profileUserId,topVideo).then(function (data) {

                        console.log('activityFeed', data,topVideo);
                        //if (sug_index === 0) {
                        //    if(data.admin_dares)
                        //    $scope.userDares[dareType] = $scope.userDares[dareType].concat(data.admin_dares);
                        //}
                        $scope.userDares[dareType] = $scope.userDares[dareType].concat(data.dares);
                        $profile_loading_icon.hide();
                        setTimeout(function () {
                            scrollHeight = $('.main-dares-cnt').height();
                            if (!topVideo && data.video_dare){
                                topVideo=data.video_dare;
                                console.log("video_Dare", topVideo)
                            }
                            else{
                                sug_index += sug_count;
                            }
                            console.log('activityFeed1', sug_index,topVideo,topVideo2);
                            sug_flag = true;
                            if (data.length === 0 || data.dares.length === 0) {
                                win.off('scroll.activityFeedScroll');
                            }
                        }, 0)
                    });
                }, 100);
                console.log($scope.userDares);
            };

            if (!directUrl) {
                $scope.setDareType(dareType);
            }
            activateScroll();

            if (!$rootScope.dareStats) {
                setTimeout(function () {
                    APIService.getDareStats().then(function (data) {
                        $rootScope.dareStats = data;
                        console.log("userStats", data);
                    });
                }, 10)

            }

            var bookmark_flag = false;
            var lastScrolltop = 0;
            var count = 10;
            $scope.dareBookmarks = [];

            function activateBookmarkScroll() {
                win.on('scroll.dareBookmarks', function () {
                    if (bookmark_flag && ($(this).scrollTop() > lastScrolltop) && ($(this).scrollTop() > ($(this).height() - APIService.scrollOffset))) {
                        bookmark_flag = false;
                        loadDareBookmarks();
                    }
                    lastScrolltop = $(this).scrollTop();
                })
            }

            function loadDareBookmarks() {
                win.off('scroll.activityFeedScroll');
                $scope.dare_type = 'bookmark';
                $profile_loading_icon.show();
                var index;
                if ($scope.dareBookmarks) {
                    index = $scope.dareBookmarks.length;
                }
                else {
                    index = 0;
                }
                APIService.getDareBookmarks(index, count).then(function (response) {
                    $scope.dareBookmarks = $scope.dareBookmarks.concat(response.dares);
                    $profile_loading_icon.hide();
                    bookmark_flag = true;
                    if (response.dares.length < count) {
                        win.off('scroll.dareBookmarks');
                    }
                })
            }

            if (dareType == "bookmarks") {
                $scope.showBookmarks();
            }


            // scroll effect start =========================

            var lastScrollTop = 0,
                $dareStatsWidget = $('.dare-stats-widget'),
                $dareStatsOffset = $('.dare-stats-widget').offset(),

                $dareSticky = $('.give-dare-box.sticky'),
                $dareOffset = $('.give-dare-box.sticky').offset();

           $(window).on('scroll.activityScroll', function () {
                var st = $(this).scrollTop();
                // console.log(st, $dareStatsOffset.top);
                if (st > lastScrollTop) {
                    if (st >= $dareStatsOffset.top) { // scroll down
                        $dareStatsWidget.css({'position': 'fixed', 'top': '0', 'width': '100%'});
                    }
                } else { // scroll up
                    if (st <= ($dareStatsOffset.top - 46)) {
                        $dareStatsWidget.css({'position': 'static', 'width': '100%'});
                    }
                    else {
                        $dareStatsWidget.css({'position': 'fixed', 'top': '46px', 'width': '100%'});
                    }
                }
                lastScrollTop = st;
            });
        }, 100);
        //  scroll effect end =================================
    };


    userProfileDaresController.$inject = injectParams;

    angular.module('stichio').controller('userProfileDaresController', userProfileDaresController);

}());
