(function () {
	var injectParams = ['$scope', '$rootScope','APIService', '$window', '$stateParams', '$state', '$cookies', 'commonService','$location', 'cacheService', '$timeout'];


	var userProfileController = function ($scope, $rootScope, APIService,$window, $stateParams,$state, $cookies, commonService,$location, cacheService, $timeout) {
		console.log("inside profile controller", $location.url())
		commonService.removePopups();
		console.log("removed popups")
		console.log("inside user profile", $location.url())
		var params = $location.search();
		var directUrl = false
		if (params.dareId || params.admindareId){
			directUrl=true;
		}
		console.log("removed dare popup")
		setTimeout(function () {
            $.screentime.reset();
        }, 2000);
		commonService.checkOnline();
		//removePopups();
		if(clearErrorMessageTimeout) {clearTimeout(clearErrorMessageTimeout)};
        // $('.highlighter.activity').siblings().css('border-bottom', 'none');
    	// $('.highlighter.activity').css('border-bottom', '2px solid #007ad5');
        //
		//$(window).scroll(function() {
		//	 if ($(this).scrollTop() > 300) {
		//		 $('.give-dare-box.fixed').css("display", "flex")
		//		 // $('.home-icons-box.fixed').css("display", "flex")
		//		 // console.log($('body').scrollTop())
		//	 }
		//	 else {
		//		 $('.give-dare-box.fixed').css("display", "none")
		//	 }
		// });
		$scope.isOwnProfile = false
		if ($stateParams.profileId!=='own') {
			$scope.profileUserId = $stateParams.profileId
			if (!$stateParams.profileId){
				$scope.profileUserId= commonService.loggedInUserId;
			}
		}
		else{
			$scope.profileUserId= commonService.loggedInUserId;
		}

		$scope.refreshProfile = function () {
			cacheService.removeProfile($scope.profileUserId)
			$state.reload();

		}

		$scope.openEditProfileFromHam = function () {
			
			$('.close-profile-add-popup').trigger('click');
			$scope.openEditProfileNew()
		}

		//$scope.$on('$viewContentLoaded', commonService.observer1);
        //window.addEventListener('resize', commonService.observer2);
		commonService.setLoggedInStatus();
		$scope.userLoggedIn = commonService.userLoggedIn;
		//$scope.selectedTab = 'collections'
		APIService.cancel();


		//var pages = { "frames": "frames", "stiches" : "stiches", "followers": "followers", "following": "following" };


        DEBUG &&  console.log("stateParams.profileId");
        DEBUG &&  console.log($stateParams);

		//getFollowStatus(commonService.loggedInUserId , $scope.profileUserId);
		$scope.userProfile = {
			framesCount: 0,
			stichesCount: 0,
			likesCount: 0,
			followersCount: 0,
			followingCount: 0
		};

		setTimeout(function () {
			APIService.getUserDaresStats($scope.profileUserId).then(function (data) {
				console.log('userDaresStats', data);
				$scope.userDaresStats = data;
				$scope.userProfile.userDetails.dare_count = $scope.userDaresStats.completed_count
				$scope.userProfile.userDetails.upvote_count = $scope.userDaresStats.upvote_count

			});
		}, 500);

		//alert($stateParams.profileId)
        APIService.setProfileUserID($scope.profileUserId);
		if (commonService.loggedInUserId == APIService.profileUserId)
		{
			$scope.isOwnProfile = true;
			$rootScope.footerSelectedOption="own_profile";
		}
		else {
			$scope.isOwnProfile = false;
			$rootScope.footerSelectedOption = "others_profile";
		}

        //APIService.getCoverPic().then(function (data){
	    //	ngMeta.setTag("image", data.cover_pic)
	    //});


		//code for opening popup
		$scope.popupOpen = false;



		if($state.current.name.indexOf("followers") > -1){
        	$(".followers-nav").find("span").addClass('nav-selected');
        }else if($state.current.name.indexOf("following") > -1){
        	$(".following-nav").find("span").addClass('nav-selected');
        }


		//code for implementing shadowed effect on scrolling



	    this.incrementFollowingCount = function(){
	    	$scope.userProfile.followingCount++;
	    };

	    this.decrementFollowingCount = function(){
	    	$scope.userProfile.followingCount--;
	    };

        //APIService.getFramesCount().then(function(data){
	    //	DEBUG &&  console.log(data);
	    //	$scope.userProfile.framesCount = data;
	    //	DEBUG &&  console.log("$scope.userProfileframesCount" + $scope.userProfileframesCount);
	    //});
	    //APIService.getStichesCount().then(function(data){
	    //	DEBUG &&  console.log(data);
	    //	$scope.userProfile.stichesCount = data;
	    //});
        //
	    //APIService.getLikesCount().then(function(data){
	    //	DEBUG &&  console.log(data);
	    //	$scope.userProfile.likesCount = data;
	    //});

	    //
		setTimeout(function () {
			clearErrorMessageTimeout=setTimeout(function () {
				console.log("slow")
				commonService.showMessage(commonService.serverSlowMessage);

			},commonService.apiWaitTime)
			APIService.getUserDetails().then(function (data) {
				 clearTimeout(clearErrorMessageTimeout);

                if (!data){
                    console.log("error")
                    return;
                }
				//commonService.hideMessage();
				DEBUG && console.log("Profile Details")
				DEBUG && console.log(data);
				$scope.userProfile.userDetails = data;
				if (commonService.loggedInUserId == $scope.profileUserId){
					$scope.userProfile.userDetails.is_featured=$rootScope.userLoggedInDetails.is_featured
				}
				try {
					$scope.userProfile.userDetails.dare_count = $scope.userDaresStats.completed_count
					$scope.userProfile.userDetails.upvote_count = $scope.userDaresStats.upvote_count
				} catch (e) {
				}
				//$scope.userProfile.userDetails.avatar = APIService.userDetailsGlobal.avatar;
				$rootScope.userDetailsGlobal = data;
				if ($scope.loggedIn) {
					console.log('userDetailsGlobal', APIService.userDetailsGlobal)
					if (params.action === 'follow' && APIService.userDetailsGlobal.is_following == false) {
						$scope.followUser(APIService.userDetailsGlobal)
					}
				}
				//ngMeta.setTitle(data.username + " at STICHIO | " + data.top_categories.join(', '));
				//ngMeta.setTag("description", "Check out " + data.username+"'s content at STICHIO | " + data.top_categories.join(', ') )
				//ngMeta.setTag("url" , "http://www.stichio.in/#" + $location.path());
				//ngMeta.setTag("keywords" , data.top_categories.join(', '))
			}, function(){
				clearTimeout(clearErrorMessageTimeout);
			});
		},100);

		if (!$rootScope.userLoggedInDetails) {
			setTimeout(function () {
				APIService.getLoggedInUserDetails().then(function (data) {
					//DEBUG &&  console.log(data);
					$scope.loggedInUserDetails = data;
					$rootScope.userLoggedInDetails = data;
				});
			},100);
		} else {
			$scope.loggedInUserDetails = $rootScope.userLoggedInDetails;
		}


		APIService.getUnseenPendingDares(commonService.loggedInUserId).then(function (data) {
			commonService.unseenPendingDaresCount=data.count;
			$scope.unseenDaresCount=data.count;
			$rootScope.unseenPendingDaresCount=commonService.unseenPendingDaresCount;
		});




	    $scope.displayHeaderDetails = function(pageName) {
			if($scope.userLoggedIn == true){
			$(".nav-wrapper li").find("span").removeClass('nav-selected');

			var windowLocation = window.location.href;
			windowLocation = windowLocation.toLowerCase();

			$(".nav-wrapper li").each(function () {
				var page = $(this).find("span").html().toLowerCase();
				//console.log(pageName.indexOf(page));
				if (pageName.indexOf(page) > -1) {
					$(this).find("span").addClass('nav-selected');
					//console.log('added');
				}
			});
		}
	    }


	    //Edit Profile Section
	    $scope.openEditProfile = function(event){
			event.stopPropagation();
	    	$('.edit-profile-popup').addClass('target');
	    	$('body').addClass("prevent-scroll");
			//setTimeout(function(){
			//	// alert('stacking when opening edit profile');
			//	stackTopicsEP(topicsDimDetails);
			//},300);
		}


		$scope.showActivity = function () {
            $('.info-btn').removeClass('btn-active');
            $('.info-btn.friends').addClass('btn-normal');
            $('.info-btn.activity').removeClass('btn-normal');
            $('.info-btn.activity').addClass('btn-active');
            // console.log($state.params.profileId);
            $state.go('profile.dares');
        };

        //$scope.showFriends = function () {
        //    $('.info-btn').removeClass('btn-active');
        //    $('.info-btn.activity').addClass('btn-normal');
        //    $('.info-btn.friends').removeClass('btn-normal');
        //    $('.info-btn.friends').addClass( 'btn-active');
        //    $state.go('profile.friends');
        //};

		var submitDetails= function(){
			if($rootScope.userLoggedInDetails.bio_change) {
					$scope.submitUserBio();
				}
		}
		$scope.submitUserBio=function(){
			$rootScope.userLoggedInDetails.bio_change=false;
			$rootScope.userLoggedInDetails.user_bio=$rootScope.userLoggedInDetails.user_bio_temp;
			var data={
				"user_bio":$rootScope.userLoggedInDetails.user_bio||"",
				"gender" : $rootScope.userLoggedInDetails.gender
			};
			APIService.updateProfileInfo(data);
			$( "body").not('.edit-bio-box').unbind( "click", submitDetails)


		}




		$scope.editBio=function(){
			$scope.userLoggedInDetails.bio_change=true;
			$scope.userLoggedInDetails.user_bio_temp=$rootScope.userLoggedInDetails.user_bio;
			setTimeout(function(){
				$( "body").not('.edit-bio-box').click(submitDetails);
			},1000)

		}




        var toggle = false;

		
		var lastScrollTop = 0;
		var	$profileHeader = $('.profileHeader');
			
            $(window).on('scroll.userProfileScroll', function () {
                var st = $(this).scrollTop();
                if (st > 46 && st > lastScrollTop) { // scroll down
                    $profileHeader.css('top', '-46px');
                }
                else { // scroll up
                    $profileHeader.css('top', '0')
                }
                lastScrollTop = st;
            });

	};

	userProfileController.$inject = injectParams;

    angular.module('stichio').controller('userProfileController', userProfileController);
}());