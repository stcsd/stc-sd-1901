(function () {

    var injectParams = ['CacheFactory', '$q', '$http'];
    var cacheServiceFactory = function (CacheFactory, $q, $http) {

        var factory={};
        var expireTime = {
            genre:30*24*3600,
            avatar:30*24*3600,
            ds_image:14*24*3600,
            response_image: 7*24*3600,
            video_snapshot:7*24*3600,
            unknown:24*3600
        }
        /* Data Caching */
        if (!CacheFactory.get('persistent')) {
            CacheFactory.createCache('persistent', {
                cacheFlushInterval: 15*24*60 * 60 * 1000, //flush the cache every 15 days
                storageMode: 'localStorage',
                capacity:100
            });
        }
        if (!CacheFactory.get('temp')) {
            CacheFactory.createCache('temp', {
                cacheFlushInterval: 2*24*60 * 60 * 1000,
                deleteOnExpire:'aggressive',
                capacity:100,
                storageMode: 'localStorage',
                recycleFreq:4*60*60*1000 //Check every 4 hours to delete expired cache items
            });
        }

        factory.get=function(key, cacheType){
            cacheType = cacheType || 'persistent'
            var cache = CacheFactory.get(cacheType);
            var keyInfo=cache.info(key);
            var keyData=cache.get(key);
            var curr_time=new Date().getTime();
            var isExpired=true;
            if(keyInfo) {
                isExpired = keyInfo.expires < curr_time;
            }
            return {
                data: keyData,
                isExpired:isExpired,
                cache: cache //cache object may be needed to update data
            }

        };
        factory.remove=function(key, cacheType){
            cacheType = cacheType || 'persistent'
            var cache = CacheFactory.get(cacheType);
            cache.remove(key);
        };


        factory.removeProfile=function(profileId){
            profileId = profileId || globalLoggedInUserId;
            var cache = CacheFactory.get('temp');
             console.log("thiscache",cache)
            cache.remove("profile_" + profileId + "_dares_all")
            cache.remove("profile_" + profileId + "_dare_stats")
            cache.remove("profile_" + profileId + "_info")
            cache.remove("profile_" + profileId + "_dares_" + 'given')
            cache.remove("profile_" + profileId + "_dares_" + 'completed')
            cache.remove("profile_" + profileId + "_dares_" + 'remaining')
        };
        factory.removeProfileDares=function(profileId, dareType){
            try {
                dareType = dareType || 'all'
                profileId = profileId || globalLoggedInUserId;
                console.log("removing profile dares", profileId, dareType)
                var cache = CacheFactory.get('temp');
                cache.remove("profile_" + profileId + "_dares_" + dareType)
                cache.remove("profile_" + profileId + "_dare_stats")
                if (dareType == 'all') {
                    cache.remove("profile_" + profileId + "_dares_" + 'given')
                    cache.remove("profile_" + profileId + "_dares_" + 'completed')
                    cache.remove("profile_" + profileId + "_dares_" + 'remaining')
                }
            } catch (e) {
                console.error(e)
            }
        };
        factory.removeProfileDareStats=function(profileId){
            try {
                profileId = profileId || globalLoggedInUserId;
                var cache = CacheFactory.get('temp');
                cache.remove("profile_" + profileId + "_dare_stats")
            } catch (e) {
                console.error(e)
            }
        };
        factory.removeProfileInfo=function(profileId){
            try {
                profileId = profileId || globalLoggedInUserId;
                var cache = CacheFactory.get('temp');
                cache.remove("profile_" + profileId + "_info")
                cache.remove("loggedInUserDetails")
            } catch (e) {
                console.error(e)
            }
        }


        factory.addFileInfo=function(url, imageType){
            imageType = imageType || 'unknown';
            var fileObj={
                name:url,
                expiry:Date.now() + (expireTime[imageType]*1000)
            }
            var cache = CacheFactory.get('persistent');
            var filenames=cache.get('cachedFiles') || [];
            filenames.push(fileObj);
            cache.put('cachedFiles', filenames);
        };

        factory.get_or_update=function(apiUrl, cacheKey, options){
            var deferred=$q.defer();
            options=options||{};
            var cacheType=options.cacheType || 'temp'
            var maxAge=options.maxAge||6*3600*1000;
            var force_update=options.force_update;

            var cacheData = factory.get(cacheKey, cacheType);
            if (cacheData.data && !cacheData.isExpired && !force_update) {
                deferred.resolve(cacheData.data);
            } else {
                $http.get(apiUrl).then(function(result){
                    cacheData.cache.put(cacheKey, result.data, {maxAge: maxAge});
                    deferred.resolve(result.data);
                });
            }
            return deferred.promise;
        };
        factory.clearAll = function(){
            CacheFactory.clearAll();
        }
        /* Data Caching over*/


        //File Caching
        factory.initialize= function() {
            try {
                factory.fileCache = new CordovaFileCache({
                    fs: new CordovaPromiseFS({ // An instance of CordovaPromiseFS is REQUIRED
                        Promise: Promise, // <-- your favorite Promise lib (REQUIRED),
                        storageSize: 100 * 1024 * 1024,
                        persistent: false
                    }),
                    mode: 'hash', // or 'mirror', optional
                    localRoot: 'data', //optional,
                    //serverRoot: 'http://yourserver.com/files/', // optional, required on 'mirror' mode
                    cacheBuster: false  // optional
                });

                factory.fileCache.ready.then(function (list) {
                    console.log("Cache ready")
                    // Promise when cache is ready.
                    // Returns a list of paths on the FileSystem that are cached.
                })
            } catch (e) {
            }
        };
        factory.getUrl=function(url){
            try {
                return factory.fileCache.toURL(url);
            } catch (e) {
                return url;
            }
        };
        factory.isFileCached=function(url){
            try {
                return factory.fileCache.isCached(url);
            } catch (e) {
                return false;
            }
        };
        factory.download=function(url){
            try {
                factory.fileCache.add(url);
                return factory.fileCache.download(null, false);
            } catch (e) {
            }
        };
        factory.addUrl=function(url){
            factory.fileCache.add(url);
        };
        factory.set=function(key, data, cacheType, options){
            cacheType = cacheType || 'persistent'
            var cache = CacheFactory.get(cacheType);
            cache.put(key, data, options)
        }


        return factory;
    };

    cacheServiceFactory.$inject = injectParams;

    angular.module('stichio').factory('cacheService', cacheServiceFactory);

}());