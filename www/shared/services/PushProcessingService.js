(function () {

    var injectParams = ['$rootScope', 'commonService', '$state', 'APIService', '$timeout'];
    var pushFactory = function ($rootScope, commonService, $state, APIService, $timeout ) {

        //var factory = {};
        try {
            var push = new Ionic.Push({
                //debug: true,
                canShowAlert: true, //Can pushes show an alert on your screen?
                canSetBadge: true, //Can pushes update app icon badges?
                canPlaySound: true, //Can notifications play a sound?
                canRunActionsOnWake: true,
                onNotification: function (e) {
                    console.log("push notif data", e)
                    var payload = (e._raw.additionalData)
                    if (payload.foreground) {
                        //  if the push is recieved inline
                        //  storage the value of  playoad.id,  the extra value sent by push
                        console.log("e.payload.id foreground", payload.notId)
                        var data = payload.data;
                        if (data.element_type != 'message') {
                            $rootScope.loadNotifications(true);
                        }
                        if (commonService.active_convo_id != data.element_id) {
                            setTimeout(function () {
                                $rootScope.newNotification = true;
                                navigator.notification.beep(1);
                            }, 1000);
                        }
                        //window.localStorage.setItem("push_que", e.payload.id);
                        //var push_que = payload.notId

                        try {
                            sendAnalyticsData(data, "Received - App Open", e.text)
                        } catch (e) {
                        }
                    }
                    else {
                        // otherwise we were launched because the user touched a notification in the notification tray

                        if (payload.coldstart) {
                            console.log("e.payload.id coldstart", payload.data)
                            var data = payload.data;
                            $rootScope.newNotification = true;
                            cordova.plugins.notification.badge.increase(1, function (badge) {
                                console.log("badge number after receiving", badge)
                            });
                            try {
                                sendAnalyticsData(data, "Received - App Closed", e.text)
                            } catch (e) {
                            }
                            if (!commonService.app_minimised) {
                                try {
                                    sendAnalyticsData(data, "Clicked", e.text)
                                } catch (e) {
                                }
                                openNotification(data);
                            }

                            //  storage the value of  playoad.numero, the extra value sent by push
                            //window.localStorage.setItem("push_que", payload.notId);

                        }
                        else {
                            console.log("e.payload.id", payload.data);

                            //var link = commonService.get_link(payload.data);
                            var data = payload.data;

                            if (commonService.app_minimised) {
                                try {
                                    sendAnalyticsData(data, "Clicked", e.text)
                                } catch (e) {
                                }
                                openNotification(data);
                            }
                            else {
                                try {
                                    sendAnalyticsData(data, "Received - App Closed", e.text)
                                } catch (e) {
                                }
                            }
                            //  storage the value of  playoad.numero, the extra value sent by push
                            //window.localStorage.setItem("push_que", payload.notId);

                        }
                    }
                }

            });
        } catch (e) {
        }

        var openNotification=function(data){
            if (data.element_type) {
                commonService.removePopups();
                switch (data.element_type) {
                    case 'dare':
                        $rootScope.openDarePopup(null, false, true, false, data.element_id, true);
                        break;

                    case 'taken_dare':
                        $rootScope.openAdminDarePopup(null, false, true, false, data.element_id, true)
                        break;
                    case 'admin_dare':
                        $rootScope.openDareResponsesPopup(null, data.element_id);
                        $rootScope.closeAdminDarePopup();
                        break;
                    case 'user':
                        $state.go('profile.dares', {profileId: data.element_id}, {notify: true, reload: true});
                        break;
                    case 'message':
                        if ($rootScope.openNewChatPopup) {
                            $rootScope.openNewChatPopup(data.element_id, data.dare_info.from_user);
                        }
                        else{
                            $timeout(function () {
                                $rootScope.openNewChatPopup();
                            }, 3000);
                        }
                        break;

                }
            }
            else {
                if (data.notification_link){
                    var link=data.notification_link
                    if (link.indexOf("http") > -1)
                        window.open(link,'_system', 'location=no');
                    else{
                        if (link.indexOf("popup") > -1){
                            var popupName = link.split('=')[1]
                            var popupFn=commonService.popUpOpenFn(popupName)
                            if (popupFn){
                                popupFn()
                            }
                            else{
                                popupFn=commonService.popUpOpenFn(popupName)
                                $timeout(popupFn, 4000);
                            }
                        }
                        else if (link.indexOf("ownProfile") > -1){
                            $state.go('profile.dares', {profileId: 'own'}, {notify: true, reload: true});
                        }
                        else {
                            window.location.href = "file:///android_asset/www/index.html" + link
                        }
                    }
                }

            }
        }

        var sendAnalyticsData = function(data, actionText, labelText){
            console.log("sendAnalyticsData",data, actionText, labelText)
            if (data.element_type) {
                var dareText;
                if (data.dare_info){
                    dareText=data.dare_info.dareText
                }
                dareText = dareText || data.element_id;
                switch (data.element_type) {

                    case 'dare':
                        try {
                            commonService.analytics('event', {
                                category: "Push Notification",
                                action: actionText,
                                label: labelText + " P2P: " + dareText
                            });
                        } catch (e) {
                            console.log("sendAnalyticsData error", e)
                        }
                        break;

                    case 'taken_dare':
                        try {
                            commonService.analytics('event',{
                                category: "Push Notification",
                                action: actionText,
                                label: labelText + " TD: " + dareText
                            });
                        } catch (e) {
                            console.log("sendAnalyticsData error", e)
                        }
                        break;
                    case 'user':
                        try {
                            commonService.analytics('event',{
                                category: "Push Notification",
                                action: actionText,
                                label: labelText + " Profile: " + data.element_id
                            });
                        } catch (e) {
                            console.log("sendAnalyticsData error", e)
                        }
                        break;

                }
            }
            else {
                try {
                    commonService.analytics('event',{
                        category: "Push Notification",
                        action:actionText,
                        label: labelText
                    });
                } catch (e) {
                }

            }
        }
        // function onDeviceReady() {
        //    console.info('NOTIFY  Device is ready.  Registering with GCM server');
        //     push.register(function(token) {
        //      console.log("Device token:",token.token);
        //      $rootScope.device_reg_id=token.token
        //  });
        //
        //}
        return {
            initialize : function () {
                console.info('NOTIFY  initializing');
            },
            registerID : function (id) {
                console.info('NOTIFY  Device is ready.  Registering with GCM server');
                push.register(function(token) {
                    console.log("Device token:",token.token, $rootScope.userLoggedIn);
                    $rootScope.device_reg_id=token.token
                    if(!$rootScope.userLoggedIn){
                        var data = {device_reg_id : token.token}
                        APIService.registerDevice(data)
                    }
                });
            },
            //unregister can be called from a settings area.
            unregister : function () {
                console.info('unregister')
                if (push) {
                    push.unregister(function () {
                        console.info('unregister success')
                    });
                }
            }
        }

        //return factory;

    };

    pushFactory.$inject = injectParams;

    angular.module('stichio').factory('PushProcessingService', pushFactory);

}());