(function () {


    var injectParams = ['$http', '$q', '$cookies', '$rootScope', '$timeout', 'Upload', '$sce', '$cordovaFile', '$cordovaFileTransfer', '$window', '$cordovaToast', '$state'];
    var commonFactory = function ($http, $q, $cookies, $rootScope, $timeout, Upload, $sce, $cordovaFile, $cordovaFileTransfer, $window, $cordovaToast, $state) {
    	var factory = {};
        var highQuality = false;
        factory.introVideoOpened=false;
        factory.takeDareHelpBox=false;
        factory.watchDareHelpBox=false;
        factory.new_user=false;
        var video_pattern = (/\.(mp4|mov|3gp|m4v|mpeg|flv|wmv|ogg|webm|mkv|avi)$/i);
        factory.checkPlatform = function () {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                factory.platform = 'mobile';
                return 'mobile';
            } else {
                factory.platform = 'desktop';
                return 'desktop';
            }
        }

        factory.unseenPendingDaresCount;


        factory.previousPlayingVideo=null;
        factory.videoplaytimeout=null;
        $rootScope.openIntroPopup = function() {
            try {
                $rootScope.closeLoginPopup();
            } catch (e) {
            }
            factory.openPopup('intro-videos-popup');
            if (window.StatusBar)
                StatusBar.hide()
            var myVideo = document.getElementById("introvideo");
            myVideo.play();
            factory.introVideoOpened = true;
            //$('#introvideo').get(0).play()

        }
        factory.serverSlowMessage="We are experiencing heavy traffic now. Please wait or try after some time."
        factory.apiWaitTime=10000;
        factory.currentPlayingVideo=""
        factory.tempPlayingVideo="";


        $rootScope.closeIntroPopup = function(e){
            //$('#introvideo').get(0).stop()

            var myVideo = document.getElementById("introvideo");
            myVideo.pause();
            if (window.StatusBar){
                StatusBar.show()
            }
            factory.closePopup('intro-videos-popup');

            $rootScope.displayloginpopup(e);

            setTimeout(function(){factory.introVideoOpened = false;}, 100);

        }

        factory.openPopups=[]
        factory.openPopup = function (className) {
            console.log('config')
            $('.'+className).addClass('target');
            $('.menuPopup').hide()
            //$('body').addClass('stop-scrolling')
            if (factory.openPopups.length==0) {

                var bodyScrollTop = -($('body').scrollTop());
                $('body').css('top', bodyScrollTop + 'px')
                $('body').addClass('no-scroll')
            }
            if (factory.openPopups.indexOf(className) < 0) {
                factory.openPopups.push(className);
            }
            try {
                factory.currentPlayingVideo.pause();
            } catch (e) {
            }
            // Stop the popular section video if available on screen
            if($state.current.name==='popular.dares' && factory.popularVideo){
                console.log("factory.popularVideo",factory.popularVideo)
                $timeout(function(){
                    if(factory.popularVideo.currentState === 'play'){
                        factory.popularVideo.stop()
                    }
                },300);
            }
        }
        factory.closePopup =  function (className) {
            console.log('unconfig')
            $('.'+className).removeClass('target');

            var index = factory.openPopups.indexOf(className);
            if (index>-1)
                factory.openPopups.splice(index, 1);
            if (factory.openPopups.length == 0) {
                 $('body').removeClass('stop-scrolling');
                $('body').removeClass('stop-scrolling1')
                $('body').removeClass('no-scroll')
                var topVal = 0-(parseInt(($('body').css('top')), 10))
                if (topVal>0){
                    $('body').scrollTop(topVal)
                    $('body').css('top', 0)
                };
                //Start the popular section video if available on screen
                if($state.current.name==='popular.dares' && factory.popularVideo){
                    console.log("factory.popularVideo",factory.popularVideo)
                    $timeout(function(){
                        if(factory.popularVideo.currentState === 'stop'){
                            factory.popularVideo.play()
                        }
                    },300);
                }
            }
            try {
                factory.currentPlayingVideo.pause();
            } catch (e) {
            }

            //$('body').animate({scrollTop: bodyScrollTop}, 0);
        }
        factory.currentPopupOpen=function(){
            if (factory.openPopups.length > 0){
                return factory.openPopups[factory.openPopups.length-1]
            }
            else{
                return ''
            }
        }
        factory.removePopups  = function (){
            $(".modalDialog:not(.login-popup, .follow-sugg-popup, .imp-alert-popup).target").removeClass('target')
            $('.modal:not(.login-popup, .follow-sugg-popup, .imp-alert-popup).target').removeClass('target')
            $('body').removeClass('prevent-scroll')
            $('body').removeClass('stop-scrolling')
            $('body').removeClass('no-scroll')
            $('.menuPopup').hide()
            try {
                var topVal = 0 - (parseInt(($('body').css('top')), 10))
                if (topVal > 0) {
                    $('body').scrollTop(topVal)
                    $('body').css('top', 0)
                }
            } catch (e) {
            }
           factory.openPopups=[]
            try {
                $rootScope.customBack();
            } catch (e) {
            }
            $('body').removeClass('stop-scrolling1');
            try {
                $rootScope.hideDarePopup(true);
            } catch (e) {
            }
            try {
                $rootScope.closeAdminDarePopup(true);
            } catch (e) {
            }
            try {
                $rootScope.hideVideo();
            } catch (e) {
            }
            try {
                $rootScope.close_create_dare();
            } catch (e) {
            }
            try {
                $rootScope.closeEditProfile();
            } catch (e) {
            }
            try {
                $rootScope.closeImagePopup();
            } catch (e) {
            }
            try {
                $rootScope.closeShareDare();
            } catch (e) {
            }
            try {
                $rootScope.closeChatPopup();
            } catch (e) {
            }
            try {
                $rootScope.closeDareResponsesPopup();
            } catch (e) {
            }
            try {
                $rootScope.closeNewChatPopup()
            } catch (e) {
            }
            try {
                $rootScope.closeErrorPopup();
            } catch (e) {
            }
            try {
                $rootScope.closeReferralForm();
            } catch (e) {
            }

            //$('body').css('overflow', 'visible')
        }
        $rootScope.removePopups=factory.removePopups;

        var default_message_show_time=5000 //millisecs
        var default_message_show_posn='bottom'
        factory.showMessage = function (message, time, position){
            time = time || default_message_show_time;
            position = position || default_message_show_posn;
            try {
                $cordovaToast.show(message, time, position).then(function (success) {
                    console.log("The toast was shown");
                }, function (error) {
                    console.log("The toast was not shown due to " + error);
                });
            } catch (e) {
            }
        }
        factory.hideMessage = function(){
             $cordovaToast.hide();
        }

        factory.checkOnline= function(){
            if (window.Connection) {
                console.log(navigator.connection.type)
                if (navigator.connection.type == Connection.NONE) {
                    factory.showMessage("Internet not available. Please check your connection.", 3000, 'bottom')
                    return false;
                }
            }
            else {
                console.log("No Connection Plugin")
            }
            return true;
        }
        factory.closePopupOnBack = function(classname){
            try {
                factory.currentPlayingVideo.stop();
            } catch (e) {
            }
            console.log('classname closed1', classname)
            switch(classname) {
                    case "dare-view-popup":
                        $rootScope.hideDarePopup();
                        console.log('closed dare-view-popup');
                        break;
                    case "admin-dare-popup":
                        $rootScope.closeAdminDarePopup();
                        console.log('closed admin-dare-popup');
                        break;
                    case "video-popup":
                        $rootScope.hideVideo();
                        console.log('closed video-popup');
                        break;
                    case "takeDare-popup":
                        $rootScope.close_takeDare();
                        console.log('closed takeDare-popup');
                        break;
                    case "create-dare-popup":
                        $rootScope.close_create_dare();
                        console.log('closed createDare-popup');
                        break;
                    case "dare-forward":
                        $rootScope.close_dare_forward();
                        console.log('closed dareForward-popup');
                        break;
                    case "login-popup":
                        $rootScope.closeLoginPopup();
                        console.log('closed login-popup');
                        break;
                    case "edit-profile-popup":
                        $rootScope.closeEditProfile();
                        console.log('closed edit-profile-popup');
                        break;
                    case "imagePopup":
                        $rootScope.closeImagePopup();
                        break;
                    case "share-dare":
                        $rootScope.closeShareDare();
                        break;
                    case "chat-popup":
                        $rootScope.closeChatPopup();
                        break;
                    case "intro-videos-popup":
                        $rootScope.closeIntroPopup();
                        break;
                    case "dare-responses-popup":
                        $rootScope.closeDareResponsesPopup();
                        break;
                    case "error-popup":
                        $rootScope.closeErrorPopup();
                        break;
                    case "new-chat-popup":
                        $rootScope.closeNewChatPopup();
                        break;
                    case "dare-menu":
                        $rootScope.closeDareMenu();
                        break;
                    default:
                        console.log('closed pop-up');
                        factory.closePopup(classname)
                }
        }

        resultImageGlobal = ""
        $rootScope.vtheme= "js_external/videogular-themes-default/videogular.css?0.02";
    	factory.observer1 = function () {
            position_leftBoxCtx();
            fb_widget_insert();
            tw_widget_insert();
            // ig_widget_insert();
            factory.pane_scroll();
        };

        factory.observer2 = function () {
            position_leftBoxCtx();
            // position_find_friends_tab();
            factory.pane_scroll();
        };

        factory.pane_scroll = function () {
            setTimeout(function () {
                if ($('.left-box').height() < ($(window).height() - 92)) {
                    $('.left-box').css({'position': 'fixed', 'top': '46px'});
                    if ($(window).width() > 1162) {
                        $('.center-box').css('margin-left', '281px');
                    } else {
                        $('.center-box').css('margin-left', 'auto');
                    }
                } else {
                    $('.left-box').css({'position': 'static'});
                    $('.center-box').css('margin-left', 'auto');
                }

                if ($('.right-box').height() < ($(window).height() - 92)) {
                    $('.right-box').css({'position': 'fixed', 'top': '46px'});
                    if ($(window).width() > 1162) {
                        $('.right-box').css('left', 'calc(50% + 300px)');
                    } else {
                        $('.right-box').css('left', 'calc(50% + 160px)');
                    }
                } else {
                    $('.right-box').css({'position': 'static'});
                }
            }, 2000)
        };

        function position_leftBoxCtx() {
            var mq1 = window.matchMedia( "(max-width: 1177px)" );
            if (mq1.matches) {
                $('.right-box').append($('.left-box .box-ctx').detach());
                $('.left-box').hide();
            } else {
                $('.left-box').show();
                $('.left-box').prepend($('.right-box .box-ctx').detach());
            }
        }

        function fb_widget_insert() {
            if (!$rootScope.fb_layout_clone) {
                $rootScope.fb_layout_clone = $('.fb-page.fb-layout').detach();
                $rootScope.fb_layout_clone.width(266);
            }
            $('.fb-layout-clone').html($rootScope.fb_layout_clone);
        }

        function tw_widget_insert() {
            if (!$rootScope.tw_widget_clone) {
                $rootScope.tw_widget_clone = $('.twitter-widget').detach();
            }
            $('.tw-widget-clone').html($rootScope.tw_widget_clone);
        }

        function ig_widget_insert() {
            if (!$rootScope.ig_widget_clone) {
                $rootScope.ig_widget_clone = $('.ig-follow').detach();
            }
            $('.ig-widget-clone').html($rootScope.ig_widget_clone);
        }

        function position_find_friends_tab() {
            var $ffTab = $('#find-friends-tab');
            var mq = window.matchMedia( "(max-width: 881px)" );
            if (mq.matches) {
                var ff_tab = $ffTab.detach();
                $('.center-box #find-friends-tab-cnt1').append(ff_tab);
            } else {
                ff_tab = $ffTab.detach();
                $('.right-box #find-friends-tab-cnt2').append(ff_tab);
            }
        }


    	factory.uploadFile = function (files) {
            if (files.length > 0) {
                var file = files[0];
                var  filename = new Date().yyyymmdd();
                var  fileType = (file.type.indexOf('video') > -1) ? 'video' : 'image',
                    fileUrl = (fileType === 'video') ? ('videos/' + filename + '.mp4') : ('images/' + filename + '.jpg'),
                    mimeType = (fileType === 'video') ? 'video/mp4' : 'image/jpg',
                    address = 'https://eb-stichio.s3.amazonaws.com/' + fileUrl,
                    storeAuth = $http.defaults.headers.common.Authorization;
                delete $http.defaults.headers.common.Authorization;
                return Upload.upload({
                    url: 'https://eb-stichio.s3.amazonaws.com/',
                    method: 'POST',
                    data: {
                        key: fileUrl,
                        acl: 'public-read',
                        'Content-Type': mimeType,
                        filename: file.name,
                        file: file
                    }
                }).then(function () {
                    $http.defaults.headers.common.Authorization = storeAuth;
                    return {
                        url: address,
                        type: fileType,
                        sources: [{src: $sce.trustAsResourceUrl(address), type: "video/mp4"}]
                    };
                }, function (e) {
                    console.log('upload ERR', e)
                }, function (evt) {
                    $rootScope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + $rootScope.progressPercentage + '%');
                });
            }
        };

        // var video = document.getElementById('my-video');
        // var sources = video.getElementsByTagName('source');

        // var canvas = document.getElementById('my-canvas');


        factory.extractFrame = function (source) {
            console.log('video frame extracting', source);

            var video = document.createElement("VIDEO");
            var sources = document.createElement("SOURCE");
            sources.src = source;
            sources.type ="video/mp4";
            video.appendChild(sources);
            video.autoplay = true;
            video.crossOrigin = "anonymous";
            video.id = 'myVideo';
            video.load();
            video.onloadedmetadata = function () {
                console.log('video metadata loaded');
                var canvas1 = document.createElement("CANVAS");
                var canvas2 = document.createElement("CANVAS");
                var ctx1 = canvas1.getContext('2d');
                var ctx2 = canvas2.getContext('2d');
                var playIcon = document.getElementById('playIcon');

                canvas1.width = 600;
                canvas1.height = (video.videoHeight / video.videoWidth) * 600;
                canvas2.width = 600;
                canvas2.height = (video.videoHeight / video.videoWidth) * 600;
                ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
                ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
                var z = (video.videoHeight / video.videoWidth) * 600;
                //var y = (250 - z) / 2;
                var y = 0;
                video.addEventListener("play", function () {
                    console.log('playing', video);
                    ctx1.drawImage(video, 0, y, 600, z);
                    ctx1.drawImage(playIcon, 200, 75, 100, 100);
                    ctx2.drawImage(video, 0, y, 600, z);
                    video.pause();

                    var url1 = source.replace('https://eb-stichio.s3.amazonaws.com/videos/', '').replace('https://eb-stichio.s3.amazonaws.com/videos_mobile/', '');
                    var fileName1 = url1.replace('.mp4', '.jpg');
                    var fileName2 = url1.replace('.mp4', '_snapshot.jpg');

                    uploadCanvas(canvas1, fileName1);
                    uploadCanvas(canvas2, fileName2);
                }, false);
            };
        };

        factory.addPlayIconOnImage = function (imageUrl, fileName) {
            var image = document.createElement("IMG");
            var canvas = document.createElement("CANVAS");
            canvas.width = 500;
            canvas.height = 500;
            var ctx = canvas.getContext('2d');
            image.onload = function () {
                console.log('image loaded', image.width, image.height)
                // ctx.drawImage(imageElement, xPos, yPos, width, height);
                if (image.height > image.width) {
                    var newht = (image.height / image.width) * 500;
                    var y = (newht - 500) / 2;
                    ctx.drawImage(image, 0, y, 500, 500, 0, 0, 500, 500);
                }
                else {
                    var newwidth = (image.width / image.height) * 500;
                    var y = (newwidth - 500) / 2;
                    ctx.drawImage(image, y, 0, 500, 500, 0, 0, 500, 500);
                }

                ctx.drawImage(playIcon, 200, 200, 100, 100);
                uploadCanvas(canvas, fileName);
            }
            image.src = imageUrl;
        }

        var uploadCanvas = function (canvas, fileName) {
            var dataUri = canvas.toDataURL('image/jpg');
            var file = Upload.dataUrltoBlob(dataUri, 'test.jpg');

            var storeAuth = $http.defaults.headers.common.Authorization;
            delete $http.defaults.headers.common.Authorization;
            Upload.upload({
                url: 'https://eb-stichio.s3.amazonaws.com/',
                method: 'POST',
                data: {
                    key: 'images/' + fileName,
                    acl: 'public-read',
                    'Content-Type': file.type === null || file.type === '' ? 'application/octet-stream' : file.type,
                    filename: file.name,
                    file: file
                }
            }).success(function (data, status, headers, config) {
                console.log("canvas upload success: https://eb-stichio.s3.amazonaws.com/images/" + fileName);
                delete video;
                return ("https://eb-stichio.s3.amazonaws.com/images/" + fileName);
            }).error(function (data, status, headers, config) {
                console.log('canvas upload error: ' + status);
                return false;
            });
            $http.defaults.headers.common.Authorization = storeAuth;
        };
        var successCb, errorCb, progressCb, uuid;
        var defaultVideoOptions = {
                saveToLibrary: true,
                maintainAspectRatio: true,
                height: 960,
                videoBitrate: 600000, // 1 megabit
                audioChannels: 2,
                audioSampleRate: 44100,
                audioBitrate: 128000, // 128 kilobits
                progress: function(info) {
                    console.log('transcodeVideo progress, info: ' + info);
                    progressCb({
                        status: true,
                        progress: parseInt(info * 50),
                        message: 'Compressing Video...'
                    })
                }
            };
        var fileUri = null;
        var videofilename = null;
        var lowQualityVideoOptions, highQualityVideoOptions;

        factory.selectMedia = function (scb, ecb, pcb, mediaType) {
            var mediaVal=2
            if (mediaType == 'image'){
                mediaVal=0
            }
            successCb = scb;
            errorCb = ecb;
            progressCb = pcb;
            navigator.camera.getPicture(
                onSuccess,
                onFail,
                {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    targetWidth:600,
                    sourceType: 0,      // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                    //encodingType: 0,     // 0=JPG 1=PNG
                    mediaType: mediaVal,         // 0:Picture, 1=Video, 2=AllMedia
                    //allowEdit: true,
                    correctOrientation:true
                }
            );
        }

        factory.captureImage = function(scb, ecb, pcb){
            successCb = scb;
            errorCb = ecb;
            progressCb = pcb;
            navigator.camera.getPicture(
                captureImageSuccess,
                onFail,
                {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                    //encodingType: 0,     // 0=JPG 1=PNG
                    mediaType: 1,         // 0:Picture, 1=Video, 2=AllMedia
                    //allowEdit: true,
                    correctOrientation:true
                }
            );
        }

        function captureImageSuccess(fileUri) {
            var newFileUri = fileUri.replace('file://', '');
            onSuccess(newFileUri);
        }

        factory.captureVideo = function(scb, ecb, pcb){
            successCb = scb;
            errorCb = ecb;
            progressCb = pcb;
            navigator.device.capture.captureVideo(
                captureVideoSuccess,
                captureVideoError,
                {
                    limit: 1,
                }
            );
        }

        function captureVideoSuccess(mediaFiles) {
            // Wrap this below in a ~100 ms timeout on Android if
            // you just recorded the video using the capture plugin.
            // For some reason it is not available immediately in the file system.
            console.log('captureVideoSuccess: ' + mediaFiles);
            var file = mediaFiles[0];
            var fileUri = file.fullPath.replace('file://', '');
            onSuccess(fileUri);
        }

        function captureVideoError(error) {
            console.log('captureVideoError: ' + error);
        }

        function onSuccess(FILE_URI) {
            // Wrap this below in a ~100 ms timeout on Android if
            // you just recorded the video using the capture plugin.
            // For some reason it is not available immediately in the file system.
            defaultVideoOptions.outputFileType=VideoEditorOptions.OutputFileType.MPEG4;
            defaultVideoOptions.optimizeForNetworkUse=VideoEditorOptions.OptimizeForNetworkUse.YES;
            console.log('FILE_URI', FILE_URI);

            uuid = new Date().yyyymmdd();
            if (highQuality) {
                uuid+="_h";
            }
            else{
                uuid+="_m";
            }
            fileUri = 'file://' + FILE_URI;
            var isVideo = video_pattern.test(FILE_URI);
            console.log('isVideo', isVideo);
            if (!isVideo) {
                uploadFileOnS3(FILE_URI, uuid+'.jpg', successCb, errorCb, progressCb);
                var fileName = fileUri.split('/').pop();
                var dir = fileUri.replace(fileName, '');
                $cordovaFile.copyFile(dir, fileName, cordova.file.externalRootDirectory+'Stichio/', uuid+'.jpg')
                .then(function (success) {
                        console.log('file copied: ' + JSON.stringify(success));
                    }, function (error) {
                        console.log('file not copied: ' + JSON.stringify(error));
                        if (error.code === 1) {
                            $cordovaFile.createDir(cordova.file.externalRootDirectory, "Stichio", false)
                            .then(function (success) {
                                console.log('file copied: ' + JSON.stringify(success));
                                $cordovaFile.copyFile(dir, fileName, cordova.file.externalRootDirectory+'Stichio/', uuid+'.jpg')
                            }, function (error) {});
                        }
                    });
                return;
            }
            lowQualityVideoOptions = angular.copy(defaultVideoOptions);
            lowQualityVideoOptions.fileUri = fileUri;
            highQualityVideoOptions = angular.copy(defaultVideoOptions);
            highQualityVideoOptions.fileUri = fileUri;
            highQualityVideoOptions.outputFileName = uuid;
            lowQualityVideoOptions.outputFileName = uuid;
            highQualityVideoOptions.videoBitrate = 900000;

            var newVideoOptions = lowQualityVideoOptions;
            if (highQuality){
                newVideoOptions = highQualityVideoOptions;

            }
            VideoEditor.transcodeVideo(
                    videoTranscodeSuccess,
                    videoTranscodeError,
                    newVideoOptions
                );



        }

        function onFail(error) {
            console.log("On fail " + error);
        }

        function videoTranscodeSuccess(result) {
            // result is the path to the transcoded video on the device
            var fileName = result.split('/').pop();
            console.log("filename:", fileName)
            console.log('videoTranscodeSuccess  Compressed, result: ' + result);

            uploadFileOnS3(result, fileName, successCb, errorCb, progressCb, true);


        }

        function videoTranscodeError(err) {
            console.log('videoTranscodeError, err: ' + err);
        }

        function createThumbnailSuccess(result) {
            // result is the path to the jpeg image on the device
            console.log('createThumbnailSuccess, result: ' + result);
            var filename = uuid+'_snapshot.jpg'
            var url = "https://eb-stichio.s3.amazonaws.com/";
            var fileUri = 'file://' + result;
            $cordovaFileTransfer.upload(url, fileUri, {
                'fileKey': "file",
                'fileName': filename,
                'chunkedMode': false,
                'mimeType': 'image/jpg',
                'params' : {
                    'key': 'images/' + filename,
                    'acl': 'public-read',
                    'Content-Type': 'image/jpg'
                }
            }).then(function (success) {
                console.log("upload success: snapshot" );
            }, function (err) {
                console.log("upload snapshot error: ", JSON.stringify(err));

            });
            //uploadFileOnS3(result, uuid+'_snapshot.jpg', successCb, errorCb, progressCb);
            var imageUrl = 'file://' + result;
            var fileName = uuid + '.jpg';
            factory.addPlayIconOnImage(imageUrl, fileName);
        }

        function createThumbnailError(err) {
            console.log('createThumbnailError, err: ' + err);
        }

        function uploadFileOnS3(filePath, fileName, successCb, errorCb, progressCb) {
            var isVideo = video_pattern.test(filePath);
            console.log('isVideo', isVideo);
            var folder = isVideo ? 'videos/' : 'dareResponseThumbnails/w800/';
            if (isVideo){
                if (!highQuality){
                    folder="videos_mobile/";
                }
            }
            var fileType = isVideo ? 'video' : 'image';
            var videoInverseAspectRatio = 1
            // var fileName = uuid + (isVideo ? '.mp4' : '_snapshot.jpg');
            var mimeType = isVideo ? 'video/mp4' : 'image/jpg';
            var url = "https://eb-stichio.s3.amazonaws.com/";
            var fileUri = 'file://' + filePath;
            console.log('uploading ' + fileUri);
            var initialpercentage = isVideo ? 50 : 0;
            var finalpercentage = isVideo ? 50 : 100;

            if (fileType == 'video') {
                VideoEditor.getVideoInfo(
                    getVideoInfoSuccess,
                    getVideoInfoError,
                    {
                        fileUri: fileUri
                    }
                );
            }
            function getVideoInfoSuccess(info) {
                VideoEditor.createThumbnail(
                    createThumbnailSuccess, // success cb
                    createThumbnailError, // error cb
                    {
                        fileUri: fileUri, // the path to the video on the device
                        outputFileName: 'snapshot', // the file name for the JPEG image
                        atTime: 1, // optional, location in the video to create the thumbnail (in seconds)
                        //width: 960, // optional, width of the thumbnail
                        //height: 480, // optional, height of the thumbnail
                        quality: 100 // optional, quality of the thumbnail (between 1 and 100)
                    }
                );
                console.log('getVideoInfoSuccess, info: ' + JSON.stringify(info, null, 2));
                try {
                    if (info && info.width > 0) {
                        if (info.orientation == "portrait") {
                            videoInverseAspectRatio = info.width / info.height;
                            if (videoInverseAspectRatio < 1) {
                                videoInverseAspectRatio = 1 / videoInverseAspectRatio;
                            }
                        }
                        else {
                            videoInverseAspectRatio = info.height / info.width;
                            if (info.orientation == "landscape") {
                                if (videoInverseAspectRatio > 1) {
                                    videoInverseAspectRatio = 1 / videoInverseAspectRatio;
                                }
                            }
                        }
                    }
                } catch (e) {
                }
                console.log('videoInverseAspectRatio, info: ' + videoInverseAspectRatio);
            }
            function getVideoInfoError(info) {
                console.log('getVideoInfoError, info: ' + JSON.stringify(info, null, 2));
            }

            // upload file on server
            $cordovaFileTransfer.upload(url, fileUri, {
                'fileKey': "file",
                'fileName': fileName,
                'chunkedMode': false,
                'mimeType': mimeType,
                'params' : {
                    'key': folder + fileName,
                    'acl': 'public-read',
                    'Content-Type': mimeType
                }
            }).then(function (success) {
                console.log("upload success: " + url + folder + fileName);
                var remoteUrl = url + folder + fileName;
                if (successCb) {
                    successCb(fileType, remoteUrl, videoInverseAspectRatio)
                }
            }, function (err) {
                console.log("upload error: ", JSON.stringify(err));
                if (errorCb) {
                    errorCb();
                }
            }, function (progress) {
                // PROGRESS HANDLING GOES HERE
                if (progressCb) {
                    progressCb({
                        status: true,
                        progress: parseInt(initialpercentage + ((progress.loaded / progress.total) * finalpercentage)),
                        message: 'Uploading ' + fileType + '...'
                    })
                }
                console.log('uploading ' + fileType + ': ' + parseInt((progress.loaded / progress.total) * 100) + '%');
            });
        }


        //factory.baseUrl2 = "https://uueam7ohlk.execute-api.us-east-1.amazonaws.com/preAlpha/";
        factory.baseUrl2 = "https://" + apigs + ".execute-api.us-west-2.amazonaws.com/" + ags + "/";
        // factory.baseUrl2 = "http://192.168.1.116:9000/";
        factory.baseUrl = factory.baseUrl2;
        factory.loggedInUserId =  "";
    	factory.profileUserId =  "";
    	factory.userLoggedIn =  false;
        factory.userHasInterests = true;
        factory.canceler = $q.defer();
        factory.userHasPassword = true;
        var socketJoined = false
        globalSocket={}

        globalLoggedInUserId = $cookies.get('userId');

    	factory.setLoggedInStatus = function(){
            if ( $rootScope.userLoggedIn == undefined) {
                if ($cookies.get('sti_authenticationToken') !== "" && $cookies.get('userId') !== "" && localStorage.getItem("logged_in") === "true") {
                    factory.userLoggedIn = true;
                    $rootScope.userLoggedIn = true;
                    factory.loggedInUserId=globalLoggedInUserId;

                }
                else {
                    $rootScope.userLoggedIn = false;
                    factory.userLoggedIn = false;
                    console.log('userlogged in staus', factory.userLoggedIn);
                }
            }
    	};
        factory.user_rated_app = ($cookies.get('app_rated') === "yes");
        factory.setLoggedInStatus();
        factory.setCookie = function(key, value, expiryDays){
            var expireDate = new Date();
            expiryDays = expiryDays || (2*365);
            expireDate.setDate(expireDate.getDate() + expiryDays); //2 Years
            // Setting a cookie
            $cookies.put(key, value,{'expires': expireDate});
        }
        factory.app_visits=$cookies.get('app_visits') || 1;
        factory.increase_app_visits = function(){
             factory.app_visits++;
            factory.setCookie('app_visits', factory.app_visits)
        }

        setTimeout(function(){
            if ($rootScope.userLoggedIn) {
                factory.socket = io.connect('http://' + bsi + ':80/chat');
                // factory.socket = io.connect('http://' + bsi + ':9000/chat');
                globalSocket = factory.socket
                if (!socketJoined && factory.socket) {
                    setTimeout(function () {
                        if ($.cookie("sti_authenticationToken")) {
                            factory.socket.emit('join', $.cookie("sti_authenticationToken"));
                        }
                        else {
                            factory.socket.emit('join', dat);
                        }
                        socketJoined = true;
                    }, 5);
                }
            }
            globalSocket.updateViewHttp = function(elementId, userId){
                var obj={
                    elementId : elementId,
                    userId : userId
                }
                return $http.post(factory.baseUrl + "api/v1/updateView/",obj).then(function (results) {
                    return results.data;
                });
            };

        }, 1000);

        factory.connectSocket = function() {
            console.log('connection failed, it seems. Let me try to connect again using connectSocket function.');
            factory.socket = io.connect('http://' + bsi + ':80/chat');
            // factory.socket = io.connect('http://' + bsi + ':9000/chat');
            console.log('connection to socket re-established')
        }

        factory.reconnectSocket=function(){
            try {
                console.log("socket status on resume", factory.socket.socket.connected,factory.socket.socket.connecting )
                if (!factory.socket.socket.connected && !factory.socket.socket.connecting) {
                    console.log("Socket disconnected. Socket reconnecting")
                    factory.socket.socket.reconnect();
                }
                else {
                    factory.socket_status = true;
                    console.log("Socket connection alive")
                }

            } catch (e) {
            }
        }

        console.log('userlogged in staus',factory.userLoggedIn);

        factory.setLoggedInUserId = function(tempLoggedInUserId){
            DEBUG &&  console.log("setting logged in user");
            factory.loggedInUserId =  tempLoggedInUserId;
            loggedInUserId=tempLoggedInUserId;
        };
        factory.loggedInUserAvatar =  "";
        factory.setLoggedInUserAvatar = function(tempLoggedInUserId){
            DEBUG &&  console.log("setting logged in user");
            factory.loggedInUserAvatar =  tempLoggedInUserId;
        };

        if (globalLoggedInUserId) {
            factory.setLoggedInUserId(globalLoggedInUserId);
            factory.loggedIn = true;
        } else {
            factory.setLoggedInUserId("");
            factory.loggedIn = false;
        }


        factory.get_link = function (data) {
            switch (data.element_type) {
                case 'dare':
                    if (data.dare_info) {
                        return '#/profile/' + data.dare_info.user2 + '?dareId=' + data.element_id
                    }
                    else  return ""
                case 'taken_dare':
                    return '#/profile/' + data.dare_info.userId + '?adminDareId=' + data.element_id
                case 'user':
                    return '#/profile/' + data.element_id;

            }

        }
        factory.analytics = function(type, data){
            try {
                switch (type) {
                    case 'pageview' :
                        setTimeout(function () {
                            if (typeof window.analytics !== "undefined")
                                if (!data.page) {
                                    data.page = "/"
                                }
                            window.analytics.trackView(data.page, "stichio://www.stichio.co.in" + data.page);
                            window.FirebasePlugin.setScreenName(data.page);
                        }, 1000);
                        //window.ga('send', 'pageview', data);
                        //window.ga('UsernameTracker.send', 'pageview', data);
                        break;
                    case 'event' :
                        setTimeout(function () {
                            if (typeof window.analytics !== "undefined")
                                window.analytics.trackEvent(data.category, data.action, data.label, 1);
                            //var params = {
                            //    action: String(data.action).substring(0, 99),
                            //    label: String(data.label).substring(0, 99),
                            //    value: String(data.value) || 1
                            //};
                            //var eventName = data.category.replace(' ', '_')
                            //window.FirebasePlugin.logEvent(eventName, params);
                        }, 1000);
                        break;
                    case 'set' :
                        if (data.key == 'userId') {
                            setTimeout(function () {
                                if (typeof window.analytics !== "undefined")
                                    window.analytics.setUserId(data.value + " " + data.username);
                                window.FirebasePlugin.setUserId(data.value);
                                //window.FirebasePlugin.setUserProperty("userid", data.value );
                                window.FirebasePlugin.setUserProperty("username", data.username);
                            }, 1000);
                        }
                        break;
                }
            } catch (e) {
            }
            return true;

        }

        factory.checkPermissions = function(){
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.CAMERA, checkCameraPermissionCallback, null);
            permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkWritePermissionCallback, null);
            permissions.hasPermission(permissions.GET_ACCOUNTS, checkAccountsPermissionCallback, null);
            permissions.hasPermission(permissions.USE_CREDENTIALS, checkUserCredentialsPermissionCallback, null);
            //permissions.hasPermission(permissions.RECORD_AUDIO,checkWritePermissionCallback, null);
            permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkReadPermissionCallback, null);

        }

         factory.infiniteScroll= function($win, onScrollFn, scrollName) {
             var onScroll = onScrollFn;
             var waiting = false;
             var scrolltimeout = null;
             var endScrollHandle;

             $win.on(scrollName, function () {
                 if (waiting) {
                     return;
                 }
                 waiting = true;
                 if (endScrollHandle)
                    $timeout.cancel(endScrollHandle);

                 onScroll();

                 setTimeout(function () {
                     waiting = false;
                 }, 300);

                 endScrollHandle = $timeout(function () {
                     onScroll();
                 }, 400);
             });
         }


        function checkCameraPermissions(){
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.CAMERA, checkCameraPermissionCallback, null);
            permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkWritePermissionCallback, null);
        }


        function checkCameraPermissionCallback(status) {
            var permissions = cordova.plugins.permissions;
            console.log("status", status);
            if(!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Camera permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.CAMERA,
                    function(status) {
                        if(!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }
        function checkWritePermissionCallback(status) {
            var permissions = cordova.plugins.permissions;
            if(!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Write permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.WRITE_EXTERNAL_STORAGE,
                    function(status) {
                        if(!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }

        function checkReadPermissionCallback(status) {
            var permissions = cordova.plugins.permissions;
            if(!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Write permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.READ_EXTERNAL_STORAGE,
                    function(status) {
                        if(!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }

        function checkAccountsPermissionCallback(status) {
            var permissions = cordova.plugins.permissions;
            if(!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Write permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.GET_ACCOUNTS,
                    function(status) {
                        if(!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }

        function checkUserCredentialsPermissionCallback(status) {
            var permissions = cordova.plugins.permissions;
            if(!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Write permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.USE_CREDENTIALS,
                    function(status) {
                        if(!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }

        factory.generateShareLink = function(data) {
            /*sample data
            link:"http://www.stichio.co.in/#/explore/dares",
            image : "http://searchengineland.com/figz/wp-content/seloads/2016/03/google-photos-images-camera-ss-1920-800x450.jpg",
            title: "DARE: Share any writing that you did when you were the happiest. #happy #momentstocherish #thoughts #mymusing",
            description : "hello
            */
            var firebase_api="https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyC0OrA5oi-02W_Rf7TbtG5QuWUb48s6eVs"
            var body = {
                "dynamicLinkInfo": {
                    "dynamicLinkDomain": "rb492.app.goo.gl",
                    "link": data.link,
                    "androidInfo": {
                        "androidPackageName": "com.stichio.stichioApp",
                        "androidFallbackLink": data.link,
                        "androidMinPackageVersionCode": "18",
                        "androidLink": ""
                    },
                    "analyticsInfo": {
                        "googlePlayAnalytics": {
                            "utmSource": data.utm_source || "",
                            "utmMedium": data.utm_medium || "",
                            "utmCampaign": data.utm_campaign || "",
                            "utmTerm": data.utm_term || "",
                            "utmContent": data.utm_content || ""
                        }
                    },
                    "socialMetaTagInfo": {
                        "socialTitle": data.title || "",
                        "socialDescription": data.description || "",
                        "socialImageLink": data.image || ""
                    }
                },
                "suffix": {
                    "option": "SHORT"
                }
            }
             return $http.post(firebase_api, body).then(function (results) {
                DEBUG &&  console.log("trendingDares",results.data);
                return results.data.shortLink;
            });

        }

        factory.generateUUID = function() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random()*16)%16 | 0; d = Math.floor(d/16);
                return (c=='x' ? r : (r&0x3|0x8)).toString(16);    });
            return uuid
        };

        factory.popUpOpenFn = function (popup) {
             switch(popup) {
                 case "create-dare":
                     return $rootScope.open_dare_form;
                 case "edit-profile":
                     return $rootScope.openEditProfileNew;
                 case "alerts":
                     return $rootScope.openChatPopup;
                 case "new-chat-popup":
                     return $rootScope.openNewChatPopup;
                 case "contactus":
                     return factory.contactUs
                 case "referral":
                     return $rootScope.openReferralPopup;
                 default:
                     console.log('no pop-up found');
                     return null;
             }
        }

        factory.openLink = function (link) {
            if (link.indexOf("http") > -1) {
                window.open(link, '_system', 'location=no');
            }
            else{
                if (link.indexOf("/dares/") > -1){
                    var tokens=link.split("/")
                    var dareId=tokens[tokens.length-1]
                    $rootScope.openDareResponsesPopup(null, dareId);
                    $rootScope.closeAdminDarePopup();
                }
                else if (link.indexOf("popup") > -1){
                    var popupName = link.split('=')[1]
                    var popupFn=factory.popUpOpenFn(popupName)
                    if (popupFn){
                        popupFn()
                    }
                    else{
                        popupFn=factory.popUpOpenFn(popupName)
                        $timeout(popupFn, 4000);
                    }
                }
                else if (link.indexOf("ownProfile") > -1){
                    $state.go('profile.dares', {profileId: 'own'}, {notify: true, reload: true});
                }
                else {
                    window.location.href = "file:///android_asset/www/index.html" + link;
                }
            }

        }

        return factory;
    };

    commonFactory.$inject = injectParams;

    angular.module('stichio').factory('commonService', commonFactory);

}());