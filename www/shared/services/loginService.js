(function () {

    var injectParams = ['$http', '$q', 'commonService', '$cookies', '$window', '$rootScope', 'cacheService'];
    var loginFactory = function ($http, $q, commonService, $cookies, $window, $rootScope, cacheService) {

        var factory = {};

        factory.profileUserId =  "290b5b87-3a59-429f-89ca-7812acc87f69";

        factory.signin = function(signinDetails){
            return $http.put(commonService.baseUrl + "api/v1/login/", signinDetails).then(function (results) {
             // return $http.put(commonService.chatUrl + "api/v1/login/", signinDetails).then(function (results) {
                DEBUG &&  console.log("login result");
                DEBUG &&  console.log(results);
                return results.data;
            });
        }

        factory.signup = function(signupDetails){
            DEBUG &&  console.log("signupDetails")
            DEBUG &&  console.log(signupDetails)
            return $http.post(commonService.baseUrl + "api/v1/register/", signupDetails).then(function (results) {
                DEBUG &&  console.log("signup result");
                DEBUG &&  console.log(results);
                return results.data;
            });
        }
        factory.updatePassword = function(passworddetails){
            console.log('updating password...')
            console.log(passworddetails)
            return $http.put(commonService.baseUrl + "api/v1/updatepassword/", passworddetails).then(function(result){
                return result;
            })
        }
        factory.generateResetPassLink = function(email){
            return $http.get(commonService.baseUrl + "api/v1/recover/?email=" + email, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results);
                return results;
            }, function(){
                //alert("Error while resetting password");
            });
        };

        factory.getPasswordResetUser = function(token){
            return $http.get(commonService.baseUrl + "api/v1/reset/?token=" + token, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results);
                return results;
            }, function(){
                DEBUG &&  console.log("Error while getting user info for the token");
            });
        };

        factory.resetPassword = function(passwordDetails){
            return $http.put(commonService.baseUrl + "api/v1/reset", passwordDetails).then(function (results) {
                DEBUG &&  console.log("login result");
                DEBUG &&  console.log(results);
                return results;
            });
        }

        factory.setCookie = function(key, value){
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + (2*365)); //Two Years
            // Setting a cookie
            $cookies.put(key, value,{'expires': expireDate});
        }

        factory.setSessionCookie = function(key, value){
            // Setting a cookie
            $cookies.put(key, value);
        }

        factory.clearCache=function(){
            cacheService.clearAll();
        }

        factory.logout = function(){
            try{
                window.localStorage.setItem('logged_in', false);
                window.localStorage.setItem('uhasP', false);
            }
            catch(err){
                console.log(err)
                $cookies.remove("logged_in");

            }
            if ($rootScope.userLoggedInDetails) {
                commonService.analytics('event', {category : 'Logout', action :  $rootScope.userLoggedInDetails.userId, label : $rootScope.userLoggedInDetails.username});
            }
            // window.localStorage.removeItem('uhasP');
            // window.localStorage.removeItem('uhasI');
            $cookies.remove("uhasI");
            $cookies.remove("uhasP");
            $cookies.remove("sti_authenticationToken");
            $cookies.remove("userId");
            $cookies.remove("enterReferralCode");
            $cookies.remove("newUserProfileHelp");
            $cookies.remove("watchDareHelpBox");
            $cookies.remove("takeDareHelpBox");
            $cookies.remove("exploreHelpBox");
            $cookies.remove("exploreSearchHelpBox");
            cacheService.clearAll();
            factory.logoutAPI().then(function(){
                location.reload(true);
            });
            // document.cookie = 'sti_authenticationToken=; expires=Thu, 10 Jan 1970 00:00:00 UTC';
            // document.cookie = 'userId=; expires=Thu, 10 Jan 1970 00:00:00 UTC';

        };

        factory.logoutAPI = function() {
            return $http.put(commonService.baseUrl + "api/v1/logout/", {"device_reg_id" : $rootScope.device_reg_id}).then(function (results) {
                return results;
            }, function () {
                //alert("error while Logging Out");
            });
        };




        factory.authenticateUserToken = function(){
            return $http.get(commonService.baseUrl + "api/v1/tokenvalidation", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("authentication result");
                DEBUG &&  console.log(commonService.baseUrl + "api/v1/tokenvalidation")
                DEBUG &&  console.log(results);
                return results;
            },function(){
                alert("error while validating token");
            });

            // alert(3)
            //  $http({

            // url : commonService.baseUrl + "api/v1/tokenvalidation",
            // method : 'GET',
            // header : {
            //       Content-Type : 'application/json',    
            //       Authorization: "92d4153e29450a3cbfdd133f8ca5f7e458b25c9a"
            //       }
            // }).success(function(data){
            //     alert("login Successfully");
            //     DEBUG &&  console.log("login Successfully");
            //     DEBUG &&  console.log(data);
                
            // }).error(function(error){
            //     alert("login Successfully");

            // })
        }

        return factory;

    };

    loginFactory.$inject = injectParams;

    angular.module('stichio').factory('loginService', loginFactory);

}());