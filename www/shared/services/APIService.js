(function () {

    var injectParams = ['$http', '$q', 'commonService', '$rootScope', 'loginService', '$state', 'cacheService', '$timeout'];
    var APIFactory = function ($http, $q,commonService, $rootScope, loginService, $state, cacheService, $timeout) {
        //offline.start($http);

        var factory = {};
        var profileUserId="0";
        factory.getUnseenPendingDares = function (userId) {
            return $http.get(commonService.baseUrl + "api/v1/unseenPendingDaresCount/?userId=" + userId , {timeout: commonService.canceler.promise}).then(function (result) {
                if (result.data && (result.data.count>0)){
                    cacheService.removeProfileDares(null,'remaining');
                }
                return result.data;
            });
        };

        factory.getRecentDares = function (count) {
            var api_url=commonService.baseUrl + "api/v1/recentdares/?count=" + count;
            var cache_key="recent_dares";
            return cacheService.get_or_update(api_url, cache_key, {maxAge:5*60*1000})
        };

        factory.getUserVideos = function (userId, count) {
            return $http.get(commonService.baseUrl + "api/v1/topDares/?userId=" + userId + "&n=" + count + "&sort_by=upvotes", {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.getEventBanners = function () {
            return $http.get(commonService.baseUrl + "api/v1/liveEvents/", {timeout: commonService.canceler.promise}).then(function (response) {
                return response.data;
            });
        };

        factory.bookmarkDare = function (data) {
            return $http.put(commonService.baseUrl + "api/v1/bookmarks/", data, {timeout: commonService.canceler.promise}).then(function (response) {
                cacheService.removeProfileDares(null,'bookmarks');
                return response.data;
            });
        };

        factory.unbookmarkDare = function (element_id) {
            return $http.delete(commonService.baseUrl + "api/v1/bookmarks/?elementId=" + element_id, {timeout: commonService.canceler.promise}).then(function (response) {
                cacheService.removeProfileDares(null,'bookmarks');
                return response.data;
            });
        };

        factory.getDareBookmarks = function (index, count) {
            return $http.get(commonService.baseUrl + "api/v1/bookmarks/?index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                return response.data;
            });
        };

        factory.getEventFollowers = function (eventName, index, count) {
            return $http.get(commonService.baseUrl + "/api/v1/eventDetails/?followers=True&uniqueUserName=" + eventName + "&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                return response.data;
            });
        };

        factory.getEventMedia = function (eventName, index, count) {
            return $http.get(commonService.baseUrl + "/api/v1/eventDetails/?media=True&uniqueUserName=" + eventName + "&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                return response.data;
            });
        };

        factory.getEventWinners = function (eventName, index, count) {
            return $http.get(commonService.baseUrl + "/api/v1/eventDetails/?winners=True&uniqueUserName=" + eventName + "&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                return response.data;
            });
        };

        factory.getDareGenres = function () {
            var deferred = $q.defer();
            var genreCache = cacheService.get('genres');
            if (genreCache.data && !genreCache.isExpired) {
                deferred.resolve(genreCache.data);
            } else {
                try {
                    $http.get(commonService.baseUrl + "api/v1/genredata/").then(function (response) {
                        if (response.data) {
                            genreCache.cache.put('genres', response.data, {maxAge: 24*3600*1000}); //cache for 1 day
                            deferred.resolve(response.data);
                        }
                    },function(){
                        deferred.resolve(genreCache.data);
                    });
                } catch (e) {
                    deferred.resolve(genreCache.data);
                }
            }
            return deferred.promise;
        };

        factory.getExploreFeed = function (index, count) {
            return $http.get(commonService.baseUrl + "api/v1/explorefeed?new=True&format=json&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                console.log('exploreFeed', response.data);
                return response.data;
            });
        };

        factory.getTrendingDares = function (extraparam, index, count, topic_index) {
            var firstLoad=false;
            if (!commonService.popularLoaded) {
                commonService.popularLoaded = true;
                firstLoad = true;
            }
            if (!index){
                index=0
            }
            if (!count)
            {
                count=9
            }
             var extraparam1="?index=" + index + "&count=" + count + "&new=True" + "&topic_index=" + topic_index;
            if (extraparam){
                extraparam1+= "&" + extraparam + "=True"
            }

            if (index==0 && topic_index==0){
                var apiUrl=commonService.baseUrl + "api/v1/trendingdares" + extraparam1;
                return cacheService.get_or_update(apiUrl,'popular_dares', {force_update:firstLoad});

            }
            else {
                return $http.get(commonService.baseUrl + "api/v1/trendingdares" + extraparam1, {timeout: commonService.canceler.promise}).then(function (results) {
                    DEBUG && console.log("trendingDares", results.data);
                    return results.data;

                });
            }
        };

        factory.getExploreSuggestions = function (index, count, genres) {
            if (!genres) {
                return $http.get(commonService.baseUrl + "api/v1/exploresuggestions?new=True&format=json&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (response) {
                    console.log('exploreSuggestions', response.data);
                    return response.data;
                });
            }
            else{
                var data = {
                    genres : genres,
                    "new" : true
                }
                var deferred = $q.defer();
                var genreCache = cacheService.get('genres_' + genres[0], 'temp');
                if (genreCache.data && !genreCache.isExpired) {
                    deferred.resolve(genreCache.data);
                } else {
                    $http.post(commonService.baseUrl + "api/v1/exploresuggestions?new=True&format=json&index=" + index + "&count=" + count, data, {timeout: commonService.canceler.promise}).then(function (response) {
                        try {
                            console.log('exploreSuggestions', response.data);
                            genreCache.cache.put('genres_' + genres[0], response.data, {maxAge: 6*3600*1000});
                            deferred.resolve(response.data);
                        } catch (e) {
                            deferred.resolve(genreCache.data);
                        }
                    }, function(){
                        deferred.resolve(genreCache.data);
                    });
                }
                return deferred.promise;

            }
        };

        factory.submitAdminDareResponse = function(data, dareId){
            return $http.put(commonService.baseUrl + "api/v1/admindares/" + dareId + "/", data).then(function(results){
                cacheService.removeProfileDares();
                return results;
            });
        }

        factory.submitAdminDareComment = function(data, dare_res_id, userId){
            if (userId) {
                    cacheService.removeProfileDares(userId);
                }
            return $http.put(commonService.baseUrl + "/api/v1/admindareresponses/" + dare_res_id + "/", data);
        };

        factory.deleteAdminDareComment = function(commentId, userId){

            return $http.delete(commonService.baseUrl + "api/v1/deletedarecomment/?commentId=" + commentId).then(function (results) {
                if (userId) {
                    console.log("deleted comment", userId)
                    cacheService.removeProfileDares(userId);
                }
                DEBUG &&  console.log('comment DELETED', results);
                return results;
            });
        };

        factory.getAdminDareComment = function(dare_res_id){
            return $http.get(commonService.baseUrl + "/api/v1/admindareresponses/" + dare_res_id + "/?comments=true");
        };

        factory.adminDareVote = function(data, dare_res_id, userId, upvote_count){
            data.action = 'voteDareResponse';
            data.upvote_count = upvote_count;
            return $http.put(commonService.baseUrl + "api/v1/admindareresponses/" + dare_res_id + "/", data).then(function(result){
                console.log(result.data);
                if (userId) {
                    cacheService.removeProfileDares(userId)
                }
                return result.data;
            });
        }

        factory.getAdminDareInfo = function(adminDareId){
            return $http.get(commonService.baseUrl + "api/v1/admindares/" + adminDareId+ "/").then(function(results){
                console.log("admin dare info", results.data)
                return results.data;
            });
        }
        factory.getNextSuggestion = function(adminDareId){
            return $http.get(commonService.baseUrl + "api/v1/admindares/" + adminDareId+ "/?next_suggestion=True").then(function(results){
                console.log("admin dare info", results.data)
                return results.data;
            });
        }

        factory.getAdminDareResponseInfo = function(adminDareId){
            return $http.get(commonService.baseUrl + "api/v1/admindareresponses/" + adminDareId+ "/").then(function(results){
                console.log('adminDarePopup', results.data);
                return results.data;
            });
        }

        factory.suggestDare = function(data){
            return $http.post(commonService.baseUrl + "api/v1/admindares/", data).then(function (result) {
                console.log('suggestDare', result.data);
                return results.data;
            });
        };
        factory.searchSuggestions = function(data){
            return $http.post(commonService.baseUrl + "api/v1/admindares/", data).then(function (result) {
                return result.data;
            });
        };

        factory.scrapedImages = {
            "images" : []
        }
        factory.selectedImages = {
            "images" : []
        }
        factory.scrapedDomain = ""

        factory.deferTime = 6000;

        //"290b5b87-3a59-429f-89ca-7812acc87f69"; //"b57a91d5-ea5f-442b-8621-f5128b0974b1";
        var localUrl = "http://localhost:8889/userProfileData.json";
        var recommendedFrames = "api/v1/users/" + profileUserId + "/frames/?format=json&q=recommended";
        var callbackContinue = "";
        var callback = "";
        var followersUrl = "api/v1/users/" + profileUserId + "/followers/?format=json&requesterId=" + commonService.loggedInUserId;
        var followerUser = "api/v1/users/" + commonService.loggedInUserId + "/followees/users/";
        var userLikes = "api/v1/users/" + commonService.loggedInUserId + "/likes/";
        var serviceBase = '/api/dataservice/';
        factory.todaysPickArray = [];
        factory.todaysPickData = [];
        factory.topics = []
        factory.selectedTopics={};
        factory.topicsComputed = false;
        factory.categoryStichers = [];
        factory.categoryFrames = [];
        factory.setTopics = function(topics,selectedTopics){
            console.log("UPDATING TOPICS IN USER PROFILE SERVICE")
            factory.topics = topics;
            factory.selectedTopics = selectedTopics;
        }

        //factory.stichesColCount = stichColCount(); #cleanUp
        factory.stichesColCount = 1;
        factory.stichesCount = 8 * factory.stichesColCount;
        factory.scrollOffset = screen.height * 3;
        //factory.framesColCount = calculateColumnCount(); #cleanUp
        factory.framesColCount = 1;
        //factory.framesCount = 4 * factory.framesColCount;
        //factory.similarFramesCount = 16;
        factory.defaultContacts = [];
        factory.screenWidth = document.documentElement.clientWidth
        DEBUG && console.log("width screen: " , screen.width)

        factory.dare_share = function(dare_data){
            return $http.post(commonService.baseUrl + "api/v1/share/", dare_data).then(function (results) {
                console.log('dare_share', results);
                return results.data;
            });
        };


        factory.getDareSuggestions = function(){
            return $http.get(commonService.baseUrl + "api/v1/admindares/").then(function (results) {

                return results.data;
            });
        };

        factory.scrollOff = function () {
            $(window).off('scroll.dareBookmarks');
            $(window).off('scroll.dareSugScroll');
            $(window).off('scroll.followerScroll');
            $(window).off('scroll.followeeScroll');
            $(window).off('scroll.profileFolloweeScroll');
            $(window).off('scroll.profileFollowerScroll');
            $(window).off('scroll.eventMediaScroll');
            $(window).off('scroll.homeStichesScroll');
            $(window).off('scroll.eventDaresScroll');
            $(window).off('scroll.exploreScroll');
            $(window).off('scroll.activityFeedScroll');
            $(window).off('scroll.activityScroll');
            $(window).off('scroll.popularDaresScroll');
            $(window).off('scroll.searchSuggestionsScroll');
            $(window).off('scroll.searchSuggestionsScroll');
            $(window).off('scroll.userSearchScroll');

            if ($state.current.name.indexOf('profile') < 0){
                $(window).off('scroll.userProfileScroll');
            }
        };



        factory.cancel = function () {
            commonService.canceler.resolve();
            commonService.canceler = $q.defer();
        };

        factory.getUpvoteUsers = function(dareId,dareResId){
            return $http.get(commonService.baseUrl + "api/v1/dares/"+dareId +"/?upvotes=true&dareResponseId="+dareResId + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log('resultData',results.data);
                return results.data;
            });
        };
        factory.getDownvoteUsers = function(dareId,dareResId){
            return $http.get(commonService.baseUrl + "api/v1/dares/"+dareId +"/?downvotes=true&dareResponseId="+dareResId + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log('resultData',results.data);
                return results.data;
            });
        };
        factory.getAdminDareUpvoteUsers = function(dareResId){
            return $http.get(commonService.baseUrl + "api/v1/admindareresponses/"+dareResId +"/?upvotes=True"+ callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log('resultData',results.data);
                return results.data;
            });
        };
        factory.getAdminDareDownvoteUsers = function(dareResId){
            return $http.get(commonService.baseUrl + "api/v1/admindareresponses/"+dareResId +"/?downvotes=True"+ callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log('resultData',results.data);
                return results.data;
            });
        };

        factory.getLikesCount = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/likes?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getFollowersCount = function(){
            DEBUG &&  console.log('getFollowersCount');
            DEBUG &&  console.log(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followers/?q=count" + callbackContinue)
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followers/?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {

                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getFollowingCount = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/"  + factory.profileUserId +  "/followees/users?q=count" + callbackContinue, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data.count);
                return results.data.count;
            });
        };

        factory.getCoverPic = function() {
            return $http.get(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/coverpic/",{timeout : commonService.canceler.promise}).then(function(results){
                DEBUG &&  console.log(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/frames/?q=topFrames");
                // var allFrames = results.data.Frames;
                DEBUG &&  console.log(results.data);
                return results.data;
            })
        }

        factory.getUserDetails = function(){
            var deferred = $q.defer();
            var cacheKey = 'profile_'+factory.profileUserId+"_info";
            //if(factory.profileUserId==commonService.loggedInUserId){
            //    cacheKey='own_profile_info';
            //}
            var cacheData = cacheService.get(cacheKey, 'temp');
            if (cacheData.data && !cacheData.isExpired) {
                deferred.resolve(cacheData.data);
            } else {
                $http.get(commonService.baseUrl + "api/v1/users/" + factory.profileUserId + "/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                    try {
                        factory.othersProfileData = results.data;
                        factory.userDetailsGlobal = results.data;
                        cacheData.cache.put(cacheKey, results.data, {maxAge:6*3600*1000}) //6hrs
                        deferred.resolve(results.data);
                    } catch (e) {
                        return null;
                    }
                });
            }
            return deferred.promise;
        };

        factory.getLoggedInUserDetails = function(force_update){
            var extraText=""
            if (typeof appVersion !== 'undefined'){
                extraText="?appVersion=" + appVersion
            }
            var cacheKey="loggedInUserDetails"

            var apiUrl= commonService.baseUrl + "api/v1/loggedinuserdetails/" + extraText;
            return cacheService.get_or_update(apiUrl, cacheKey, {maxAge : 2 * 3600 * 1000, force_update:force_update}).then(function(results){
                factory.loggedInUserDetailsGlobal = results;
                $rootScope.userLoggedInDetails = results;


                if ($rootScope.userLoggedInDetails.userId != "f1735284-e51a-4de8-add7-2898eee9e37b") {
                    commonService.analytics('set', {key : 'userId', value : $rootScope.userLoggedInDetails.userId, username : $rootScope.userLoggedInDetails.username});
                    //ga('UsernameTracker.set', 'userId', $rootScope.userLoggedInDetails.userId + " " + $rootScope.userLoggedInDetails.username);
                    $rootScope.isLoggedIn = true
                    //$rootScope.userLoggedInDetails={}
                    //factory.loggedInUserDetailsGlobal={}
                }

                if (results.detail)
                {
                    if (results.detail == "Invalid token.")
                        loginService.logout();
                }

                return results;
            })
        };

        factory.getCategoryData = function(){
            return $http.get(commonService.baseUrl + "api/v1/categories/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };

        factory.getComments = function(stichId){
            return $http.get(commonService.baseUrl + "api/v1/comments/?stichId=" + stichId + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };

        // factory.postComment = function(stichId, comment, userId){
        //     var  commentdata = {
        //          "comment" : comment,
        //          "userId" : userId,
        //         "stichId" : stichId
        //
        //      }
        //
        //     return $http.post(commonService.baseUrl + "api/v1/comments/" + callback, commentdata).then(function (results) {
        //         DEBUG &&  console.log(results.data);
        //         return results.data;
        //     });
        // };

        factory.getStichersToSelect = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };

        factory.createFrame = function(createFrameData){
            createFrameData = angular.copy(createFrameData)
            if(createFrameData.isPrivate == "Private"){
                createFrameData.isPrivate = true;
            }else{
                createFrameData.isPrivate = false;
            }
            return $http.post(commonService.baseUrl + "api/v1/users/"  + commonService.loggedInUserId  +  "/frames/" + callback, createFrameData).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                DEBUG &&  console.log("Error while createFrame ");
                DEBUG &&  console.log(error);
            });
        }

        factory.saveFrame = function(createFrameData){
            if(createFrameData.isPrivate == "Private"){
                createFrameData.isPrivate = true;
            }else{
                createFrameData.isPrivate = false;
            }
            createFrameData.frameDescription = createFrameData.description
            return $http.put(commonService.baseUrl + "api/v1/users/"  + commonService.loggedInUserId +  "/frames/" + callback, createFrameData).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                DEBUG &&  console.log("Error while createFrame ");
                DEBUG &&  console.log(error);
            });
        }

        factory.scrapeImages  = function(weblink){
            factory.scrapedImages.images = []
            factory.selectedImages.images = []
            var weblinkdata = {
                "url" : weblink,
                "min_width" : 120
            }
            return $http.put(commonService.baseUrl + "api/v1/users/scrapeimages/" + callback, weblinkdata).then(function (results) {
                factory.scrapedImages = results.data;
                factory.domain = results.data.domain
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                DEBUG &&  console.log("Error while scrapingImages ");
                DEBUG &&  console.log(error);
            });
        }


        factory.getAllFollowers = function(userId, index, count){
            if(!index)
                index = 0;
            if(!count)
                count = 16;
            return $http.get(commonService.baseUrl + "api/v1/users/"  + userId +  "/followers/?format=json&index=" + index + "&count=" + count + "&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                return results.data;
            });
        };


        $rootScope.followUser = function (User, medium) {
            User.is_following_daree = true
            User.is_following = true
            User.followStatus = true
            User.justFollowed = true
            $timeout(function(){
                User.justFollowed=false;
                // User.is_following_daree = true
            }, 1000)

            console.log(User.userId);
            // $scope.user.showfollowDropdown = false
            var followedUser = { "followeeId" :  User.userId};
            if (medium){
                followedUser.medium=medium
            }
            factory.followUser(followedUser).then(function(){
            });
        }

        $rootScope.unFollowUser = function (User) {
            // $scope.user.showfollowDropdown = false
            User.is_following = false
            User.followStatus = false
            factory.unFollowUser(User.userId).then(function(){

            });
        }

        factory.followUser = function(followedUser){

            return $http.put(commonService.baseUrl + followerUser, followedUser).then(function (results) {
                cacheService.removeProfileInfo();
                cacheService.removeProfileInfo(followedUser.followeeId);
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                //alert("Error while following user");
                DEBUG &&  console.log("Error while following user");
                DEBUG &&  console.log(error);
            });
        };


        factory.unFollowUser = function(unFollowedUserId){
            DEBUG &&  console.log("unFollowedUserId");
            DEBUG &&  console.log(unFollowedUserId);
            var url = commonService.baseUrl + "api/v1/users/" + commonService.loggedInUserId + "/followees/users/" + unFollowedUserId;
            DEBUG &&  console.log(url);

            return $http.delete(url).then(function (results) {
                cacheService.removeProfileInfo();
                cacheService.removeProfileInfo(unFollowedUserId);
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                //alert("Error while unfollowing user");
                DEBUG &&  console.log("Error while unfollowing user");
                DEBUG &&  console.log(error);
            });
        };

        factory.getUserFollowees = function(userId, index, count){
            if(!index)
                index = 0;
            if(!count)
                count = 8;
            return $http.get(commonService.baseUrl + "api/v1/users/" + userId + "/followees/users/?format=json&index=" + index + "&count=" + count + "&requesterId=" + commonService.loggedInUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results);
                return results.data;
            });
        };

        factory.getFollowStatus =  function(loggedinUserId, profileUserId){

            return $http.get(commonService.baseUrl + "api/v1/getfollowstatus/?followerId=" + loggedinUserId + "&followeeId=" + profileUserId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            }, function(error){
                //alert("Error  : not liked");
                DEBUG &&  console.log("Error while following user");
                DEBUG &&  console.log(error);
            });

        }

        factory.setProfileUserID = function(tempProfileUserId){
            factory.profileUserId = tempProfileUserId;
        }


        factory.getContacts = function(){

            return $http.get(commonService.baseUrl + "api/v1/" + "contacts/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data, "contacs");
                contacts = []
                angular.forEach(results.data, function (contact) {
                    contacts.push({"user" : contact})
                })
                factory.defaultContacts = contacts
                return contacts;
            });

        }

        factory.getTopFriends = function(){
            return $http.get(commonService.baseUrl + "api/v1/users/" + commonService.loggedInUserId +"/followers/?display_followers=true&count=8").then(function (results) {
                DEBUG &&  console.log(results.data, "4contacs");
                var contacts = [];
                angular.forEach(results.data, function (contact) {
                    contacts.push({"user" : contact})
                });
                return contacts;
            });
        };

        factory.getFriends = function(profileUserId, index, count){
            return $http.get(commonService.baseUrl + "api/v1/contacts/?userId=" + profileUserId + "&index=" + index + "&count=" + count + callback,{timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log(results.data,'friends')
                return results.data
            })
        }
        factory.getGoogleFriends = function (refresh_token, isServerAuthCode) {

            var api1 = ""
            if(refresh_token){
                api1 = "api/v1/getgooglecontacts?refresh_token=" + refresh_token ;
            }
            else{
                api1 = "api/v1/getgooglecontacts" ;
            }
            if (isServerAuthCode){
                api1 = "api/v1/getgooglecontacts?serverAuthCode=" + refresh_token ;
            }

           return $http.get(commonService.baseUrl + api1 ,{timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log(results.data,'friends')
               return results.data
            })
        }
         factory.getFacebookFriends = function (uid, email) {

            var api1 = ""
            if(uid){
                api1 = "api/v1/getfacebookcontacts?uid=" + uid+'&email=' + email;
            }
            else{
                api1 = "api/v1/getfacebookcontacts" ;
            }

           return $http.get(commonService.baseUrl + api1 ,{timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log(results.data,'friends')
                return results.data
            })
        }
        factory.sendContactRequest = function (contactId, toAdmin) {
            var data = {
                "contactId" : contactId
            }
            if (toAdmin)
                data["toAdmin"] = true;
            return $http.put(commonService.baseUrl + "api/v1/" +"contacts/" + callback, data).then(function (results) {
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.cancelFriendRequest = function (contactId) {
            return $http.delete(commonService.baseUrl + "api/v1/" +"contacts/?contactId=" + contactId + "&cancel_request=true" + callback).then(function (results) {
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.sendContributorResponse = function(frameId, status){
            var data = {
                "userIds" : [commonService.loggedInUserId]
            }
            return $http.put(commonService.baseUrl + "api/v1/" + "frames/"+ frameId + '/collaborators/?accepted=' + status, data).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        }

        factory.deleteContact = function (contactId){
            return $http.delete(commonService.baseUrl + "api/v1/" +"contacts/" + "?contactId=" +contactId + callback).then(function (results) {
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.getPendingRequest = function(){
            return $http.get(commonService.baseUrl + "api/v1/" + "responsecontacts/" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log('pending reqs',results.data);
                return results.data;
            });
        };
        factory.getTopDarees = function(extraparam){
            var extraparam1="";
            if (extraparam){
                extraparam1="?" + extraparam + "=True"
            }
            else{
                extraparam="follow_suggs_signup"
            }

            var apiUrl= commonService.baseUrl + "api/v1/gettopdarees" + extraparam1 + callback;
            return cacheService.get_or_update(apiUrl, extraparam)
        };

        factory.sendResponse =function(contactId,response){
            console.log(contactId);
            console.log(response);
            var data = {
                "contactId" : contactId,
                "Response" : response

            }
            return $http.put(commonService.baseUrl + "api/v1/" + "responsecontacts/" , data).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };


        factory.deleteComment = function(stichId,commentId){
            return $http.delete(commonService.baseUrl + "api/v1/stiches/"+stichId+"/comments/"+commentId+"/").then(function (results) {
                DEBUG &&  console.log('COMMENT DELETED', results);
                return results;
            });
        };

        factory.hideDare = function(dareId, hide){
            var data={
                "dareId" : dareId,
                "hide" : hide
            }
            return $http.put(commonService.baseUrl + "api/v1/hidedare/", data).then(function (results){
               DEBUG && console.log('TRUTH DELETED', results);
                cacheService.removeProfileDares();
                return results;
            })
        }
        factory.restichImage = function(stichdata){
            DEBUG &&  console.log("deleedata", stichdata)
            stichdata["userId"] = commonService.loggedInUserId
            return $http.post(commonService.baseUrl + "api/v1/restich/" + callback,stichdata).then(function (results) {
                DEBUG &&  console.log('after stich', results);
                return results;
            });
        };
        factory.updateEmail = function (data) {
            return $http.put(commonService.baseUrl + "api/v1/updateemail/",data).then(function (results) {
                DEBUG &&  console.log('after updating email', results.data);
                return results;
            });
        }
        factory.updateProfileInfo = function(data){
            return $http.put(commonService.baseUrl + "api/v1/updateuser",data).then(function(results){
                try {
                    cacheService.removeProfileInfo();
                    if (data.username || data.profile_pic) {
                        cacheService.removeProfileDares();
                    }
                } catch (e) {
                }
                return results.data
                DEBUG && console.log('profile pic and gender status', results);
            });
        }

        factory.recentConversations = function () {
            return $http.get(commonService.baseUrl + "api/v1/recentconversations/" + callback, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.otheruserstats = function(other_user_id) {
            var userIds = { 'userId': other_user_id };
            return $http.post(commonService.baseUrl + "api/v1/otheruserstats/", userIds).then(function(results) {
                return results.data;
            });
        }

        factory.getConversationHistory = function (timestamp) {
            if (timestamp) {
                timestamp = parseInt(timestamp)
                console.log(timestamp);
            }
            var apiurl = commonService.baseUrl + "api/v1/recentconversations/" + '?user_type=' + 'new' + '&timestamp=' + timestamp;
            return $http.get(apiurl, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.recentNotifications = function () {
            return $http.get(commonService.baseUrl + 'api/v1/recentnotifications/' + callback, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.getNotificationHistory = function (index, count) {
            var apiUrl=commonService.baseUrl + 'api/v1/recentnotifications/?index=' + index + "&count=" + count;
            return $http.get(apiUrl, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.deleteConversation = function (data) {
            return $http.put(commonService.baseUrl + 'api/v1/deleteconversation/' + callback, data).then(function (result) {
                return result.data;
            });
        };

        factory.deleteMessage = function (data) {
            return $http.put(commonService.baseUrl + 'api/v1/deletemessage/' + callback, data).then(function (result) {
                return result.data;
            });
        };

        factory.readConversation = function (data) {
            return $http.put(commonService.baseUrl + 'api/v1/readconversation/' + callback, data).then(function (result) {
                return result.data;
            });
        };
        factory.sendChat = function (data) {
            return $http.post(commonService.baseUrl + 'api/v1/sendchatmsg/' + callback, data).then(function (result) {
                return result.data;
            });
        }

        factory.getChatHistory = function(data1){
            return $http.post(commonService.baseUrl + "api/v1/chathistory/", data1 ).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };

        factory.getSingleConversation = function(userId){
            return $http.get(commonService.baseUrl + "api/v1/getconversation/?userId=" + userId + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        };

        factory.getDefaultSearchList = function(){
            return $http.get(commonService.baseUrl + "api/v1/defaultsearchlist" + callback, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log(results.data);
                return results.data;
            });
        }

        factory.searchContacts = function (search_string, max_length) {
            return $http.get(commonService.baseUrl + 'api/v1/searchcontact/?count=' + max_length + '&q=' + search_string, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.searchUsers = function (search_string, index1, index2, count) {
            count=count||10
            index1=index1||0
            return $http.get(commonService.baseUrl + 'api/v1/searchusersnew/?count=' + count + '&from_index=' + index1 + '&from2=' + index2+ '&tags=' + search_string, {timeout: commonService.canceler.promise}).then(function (result) {
                return result.data;
            });
        };

        factory.getTodaysPicks = function () {
            return $http.get(commonService.baseUrl + "api/v1/todaysPicks/", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('TodaysPicks', results);
                factory.todaysPickArray = results.data;
                return results.data;
            });
        };
        factory.getTodaysPickData = function (pickId, index, count) {
            if(!index)
                index = 0;
            if(!count)
                count = 50;
            return $http.get(commonService.baseUrl + "api/v1/pick/" + pickId + "?index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('TodaysPick', results);
                factory.todaysPickData.push(results.data);
                console.log("todayspickdata", factory.todaysPickData);
                return results.data;
            });
        };
        factory.getCategoryStiches = function (index, count, categoryId) {
            if(!index)
                index = 0;
            if(!count)
                count = 16;
            return $http.get(commonService.baseUrl + "api/v1/categorystiches/?categoryId=" + categoryId + "&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('CategoryStiches', results);
                return results.data;
            });
        };
        factory.getCategoryStichers = function (count,categoryId) {
            return $http.get(commonService.baseUrl + "api/v1/topstichers?interestId="+categoryId+"&count="+count, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('CategoryStichers', results);
                factory.categoryStichers.push(categoryId);
                factory.categoryStichers.push(results.data);
                console.log(factory.categoryStichers,"category Stichers");
                return results.data;
            });
        };
        factory.getCategoryFrames = function (count,categoryId) {
            return $http.get(commonService.baseUrl + "api/v1/topframes?interestId="+categoryId+"&count="+count, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('CategoryFrames', results);
                factory.categoryFrames.push(categoryId);
                factory.categoryFrames.push(results.data);
                console.log(factory.categoryFrames,"category Frames");
                return results.data;
            });
        };
        factory.getCategoryInfo = function (categoryId) {
            return $http.get(commonService.baseUrl + "api/v1/category/"+categoryId, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG && console.log('CategoryInfo', results);
                return results.data;
            });
        };
        factory.getQuestions = function (imageId){
            return $http.get(commonService.baseUrl + 'api/v1/questions/?image_id='+ imageId).then(function (result) {
                return result.data;
                console.log(data);
            });
        };
        factory.postQuestions = function (question){
            return $http.post(commonService.baseUrl + 'api/v1/questions',question).then(function (result) {
                return result.data;
                console.log(data);
            });
        };
        // factory.editQuestions = function (questionId,question){
        //      return $http.put(commonService.baseUrl + 'api/v1/questions'+questionId,question).then(function (result) {
        //         return result.data;
        //         console.log(data);
        //     });
        // };
        //
        factory.deleteQuestions = function(questionId){
            return $http.delete(commonService.baseUrl + 'api/v1/questions/' + questionId ).then(function (result) {
                return result.data;
                console.log(data);
            });
        };
        factory.getAnswers = function (questionId,count){
            return $http.get(commonService.baseUrl + 'api/v1/questions/' + questionId + "/answers/?count="+count).then(function (result) {
                return result.data;
                console.log(data);
            });
        };
        factory.answerQuestions = function(questionId, answer1){
            var answer = {
                "answer":answer1
            }
            return $http.post(commonService.baseUrl + 'api/v1/questions/' + questionId + "/answers", answer).then(function (result) {
                return result.data;
                console.log(result.data);
            });
        };
        factory.deleteAnswers = function(questionId, answerId){
            return $http.delete(commonService.baseUrl + 'api/v1/questions/' + questionId + "/answers/" + answerId).then(function (result) {
                return result.data;
                console.log(data);
            });
        };
        // factory.editAnswer = function(questionId, answerId,answer){
        //    return $http.put(commonService.baseUrl + 'api/v1/questions/' + questionId + "/answers" + answerId, answer).then(function (result) {
        //         return result.data;
        //         console.log(data);
        //     });
        // };
        factory.getComments = function (stichId) {
            return $http.get(commonService.baseUrl + "api/v1/stiches/" + stichId + "/comments/" + callback).then(function (results) {
                DEBUG && console.log(results.data, "commentsdata");
                return results.data;
            });
        };

        factory.postComment = function (stichId, comment, userId) {
            // var commentdata = {
            //     "comment": comment
            // }

            return $http.post(commonService.baseUrl + "api/v1/stiches/" + stichId + "/comments/" + callback, comment).then(function (results) {
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.upVoteAnswer = function(answerId){
            var answerId = {
                answer_id: answerId
            }
            return $http.put(commonService.baseUrl + "api/v1/voteanswer?action=upvote",answerId).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.unUpVoteAnswer = function(answerId){
            var answerId = {
                answer_id: answerId
            }
            return $http.put(commonService.baseUrl + "api/v1/voteanswer?action=unupvote",answerId).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.downVoteAnswer = function(answerId){
            var answerId = {
                answer_id: answerId
            }
            return $http.put(commonService.baseUrl + "api/v1/voteanswer?action=downvote",answerId).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.unDownVoteAnswer = function(answerId){
            var answerId = {
                answer_id: answerId
            }
            return $http.put(commonService.baseUrl + "api/v1/voteanswer?action=undownvote",answerId).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.submitTruth = function(data){

            return $http.post(commonService.baseUrl + "api/v1/truths/", data).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.submitDare = function(data){

            return $http.post(commonService.baseUrl + "api/v1/dares/", data).then(function(results){
                DEBUG && console.log(results.data);
                cacheService.removeProfileDares(null,'given');
                return results.data;
            });
        };
        factory.createPublicDare = function(data){
            data.public_DS=true;
            return $http.post(commonService.baseUrl + "api/v1/admindares/", data).then(function(results){
                DEBUG && console.log(results.data);
                cacheService.removeProfileDares(null,'given');
                return results.data;

            });
        };

        factory.getUserDares = function(index, count, data, dareType, userId, videoId){
            var cacheType='temp';
            var cacheKey='profile_' + factory.profileUserId + '_dares_'+dareType
            //if (userId == commonService.loggedInUserId){
            //    cacheKey='own_profile_dares_'+dareType
            //}

            if (!data.dareZone) {
                var deferred = $q.defer();
                if (index==0 && !videoId) {
                    var cacheData = cacheService.get(cacheKey, cacheType);
                    if (cacheData.data && !cacheData.isExpired) {
                        deferred.resolve(cacheData.data);
                    } else {
                        $http.get(commonService.baseUrl + "api/v1/dares/?feed_type=" + dareType + "&index=" + index + "&count=" + count + "&userId=" + data.userId + "&newOthersProfile=True", {timeout: commonService.canceler.promise}).then(function (results) {
                            DEBUG && console.log(results.data);
                            cacheData.cache.put(cacheKey, results.data, {maxAge: 2 * 3600 * 1000}) // 2 hours
                            deferred.resolve(results.data);
                        });
                    }
                }
                else{
                    var videoText=""
                    if (videoId)
                        videoText="&video_dare=" + videoId
                    $http.get(commonService.baseUrl + "api/v1/dares/?feed_type=" + dareType + "&index=" + index + "&count=" + count + "&userId=" + data.userId + "&newOthersProfile=True" + videoText, {timeout: commonService.canceler.promise}).then(function (results) {
                        DEBUG && console.log(results.data);
                        deferred.resolve(results.data);
                    });
                }
                return deferred.promise;
            }
            else{
                var videoText=""
                if (videoId)
                    videoText="&video_dare=" + videoId
                return $http.get(commonService.baseUrl + "api/v1/dares/?request_type=None" + "&index=" + index + "&count=" + count + "&userId=" + data.userId + "&newOthersProfile=True" + videoText+ "&dareZone=True" ).then(function (results) {
                    DEBUG && console.log(results.data);
                    return results.data;
                });
            }
        };

        factory.getUserDaresStats = function(userId){
            var apiUrl= commonService.baseUrl + "api/v1/dares/?dare_stats=true&userId=" + userId + "&dare_upvotes=True";
            var cache_key='profile_' + userId + '_dare_stats'
            //if (userId == commonService.loggedInUserId){
            //    cache_key='own_profile_dare_stats'
            //}
            return cacheService.get_or_update(apiUrl, cache_key, {maxAge:2*3600*1000})

        };

        factory.likeDare = function(dareId){
            data = {}
            data.action = 'like'
            data.userId = commonService.loggedInUserId
            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.unlikeDare = function(dareId){
            data = {}
            data.action = 'unlike'
            data.userId = commonService.loggedInUserId
            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.submitDareResponse= function(data){
            data.action = 'submitResponse';
            return $http.put(commonService.baseUrl + "api/v1/dares/"+data.dareId+"/", data).then(function(results){
                DEBUG && console.log(results.data);
                cacheService.removeProfileDares();
                return results.data;
            });
        };

        factory.rateDare= function(dareId, rating, user1, user2){
            data = {}
            data.action = 'rate';
            data.rating = rating;
            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1.userId);
                    cacheService.removeProfileDares(user2.userId);
                }
                DEBUG && console.log(results.data);
                return results.data;
            });
        };

        factory.acceptDare= function(dareId, user1, user2){
            data = {}
            data.action = 'respondToRequest';
            data.response = 'accepted'
            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1.userId);
                    cacheService.removeProfileDares(user2.userId);
                }
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.rejectDare= function(dareId, user1, user2){
            data = {}
            data.action = 'respondToRequest';
            data.response = 'rejected'
            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1.userId);
                    cacheService.removeProfileDares(user2.userId);
                }
                DEBUG && console.log(results.data);
                return results.data;
            });
        };
        factory.submitComment = function(dareId, data, user1, user2){

            return $http.put(commonService.baseUrl + "api/v1/dares/"+dareId+"/", data).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1.userId);
                    cacheService.removeProfileDares(user2.userId);
                }
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.getDareComments = function(dareId, data){
            return $http.get(commonService.baseUrl + "api/v1/dares/"+dareId+"/?comments=True").then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.getDareLikeUsers = function (dareId) {
            return $http.get(commonService.baseUrl + "api/v1/dares/"+dareId+"/?likes=True").then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }

        factory.deleteDareComment = function(commentId, user1, user2){

            return $http.delete(commonService.baseUrl + "api/v1/deletedarecomment/?commentId="+commentId).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1, user2);
                }
                DEBUG && console.log(results.data);
                return results.data;
            });
        }

        factory.voteTruth = function(data, truthId){
            data.action = 'vote'
            return $http.put(commonService.baseUrl + "api/v1/truths/" + truthId + "/", data).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.voteDareResponse = function(data, dareId,user1, user2, upvote_count){
            data.action = 'voteDareResponse';
            data.upvote_count=upvote_count;
            return $http.put(commonService.baseUrl + "api/v1/dares/" + dareId + "/", data).then(function(results){
                if (user1 && user2) {
                    cacheService.removeProfileDares(user1.userId);
                    cacheService.removeProfileDares(user2.userId);
                }
                return results.data;
            });
        }

        factory.sendEmailInvite = function(data){
            return $http.post(commonService.baseUrl + "api/v1/inviteemail/", data,{}).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.likeAdminDare = function(data, dareId){
            return $http.put(commonService.baseUrl + "api/v1/admindares/" + dareId + "/", data).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.getOtherTruths = function(imageId){
            return $http.get(commonService.baseUrl + "api/v1/getothertruths/?imageId=" + imageId ).then(function(results){
                DEBUG && console.log(results.data);
                return results.data;
            });
        }
        factory.getUserTruths = function(userId){
            return $http.get(commonService.baseUrl + "api/v1/truths/?userId=" + userId).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }

        factory.getDareInfo = function(dareId){
            return $http.get(commonService.baseUrl + "api/v1/dares/" + dareId+ "/").then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }

        factory.getEventInfo = function(uniqueUserName){
            return $http.get(commonService.baseUrl + "api/v1/eventDetails/?uniqueUserName=" + uniqueUserName).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }
        factory.likeEvent = function(eventId, like){
            var data = {
                "eventId" : eventId
            }
            if (like){
                data.action="like"
            }
            else{
                data.action="unlike"
            }
            return $http.put(commonService.baseUrl + "api/v1/eventDetails/", data).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }
        factory.followEvent = function(eventId, follow){
            var data = {
                "eventId" : eventId
            }
            if (follow){
                data.action="follow"
            }
            else{
                data.action="unfollow"
            }
            return $http.put(commonService.baseUrl + "api/v1/eventDetails/", data).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }

        factory.getEventAdminDares = function(uniqueUserName){
            return $http.get(commonService.baseUrl + "api/v1/eventDares/?uniqueUserName=" + uniqueUserName).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }

        factory.getEventAdminDareResponses = function(uniqueUserName, index, count, type){
            return $http.get(commonService.baseUrl + "api/v1/eventDareResponses/?uniqueUserName=" + uniqueUserName + "&" + type + "=True" + "&index=" + index + "&count=" + count ).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }
        factory.getImportantAlert = function(){
            return $http.get(commonService.baseUrl + "api/v1/getimportantalert/" ).then(function(results){
                DEBUG && console.log(results);
                return results.data;
            });
        }


        factory.getDareStats = function(){
            var apiUrl= commonService.baseUrl + "api/v1/dares/?dare_stats=true&userId=" + commonService.loggedInUserId  + "&dare_upvotes=True";
            var cache_key='profile_' + commonService.loggedInUserId + '_dare_stats'
            //if (userId == commonService.loggedInUserId){
            //    cache_key='own_profile_dare_stats'
            //}
            return cacheService.get_or_update(apiUrl, cache_key, {maxAge:2*3600*1000})
        };
        factory.getDareResponses = function(dareId, index, count){
            return $http.get(commonService.baseUrl + "api/v1/dareresponses/?dareId="  + dareId + "&index=" + index + "&count=" + count, {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("userStats",results.data);
                return results.data;
            });
        };

        factory.sendVerifyEmail = function(){
            return $http.get(commonService.baseUrl + "api/v1/sendverifyemail", {timeout: commonService.canceler.promise}).then(function (results) {
                DEBUG &&  console.log("userStats",results.data);
                return results.data;
            });
        };
        factory.registerDevice = function(details) {
            return $http.post(commonService.baseUrl + "api/v1/registerDevice/", details).then(function (results) {
                DEBUG && console.log("authentication result");
                DEBUG && console.log(commonService.baseUrl + "api/v1/tokenvalidation")
                DEBUG && console.log(results);
                return results;
            }, function () {
                console.log("error while validating token");
            });
        };
        factory.applyReferralCode = function(details) {
            return $http.post(commonService.baseUrl + "api/v1/applyreferralcode/", details).then(function (results) {
                return results.data;
            }, function () {
                console.log("error while validating referral code");
            });
        };



        return factory;
    };

    APIFactory.$inject = injectParams;

    angular.module('stichio').factory('APIService', APIFactory);

}());