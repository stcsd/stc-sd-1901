angular.module('stichio').directive('errorPopup',  ['commonService', '$rootScope', function(commonService,$rootScope) {
    return {

        restrict: 'E',
        //transclude: true,
        templateUrl: 'shared/templates/errorPopup.html?0.26',
        link : function(scope, element, attribute) {
            $rootScope.openErrorPopup = function(){
                commonService.openPopup('error-popup')
            }
            $rootScope.closeErrorPopup = function(){
                commonService.closePopup('error-popup')
            }
        }
    }
}]);