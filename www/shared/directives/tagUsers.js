angular.module('stichio').directive('tagUsers', ['$timeout','commonService','APIService', '$filter',function($timeout, commonService,APIService, $filter) {
	return {
		restrict: 'E',
		templateUrl: 'shared/templates/userTagging.html?0.02',
		scope: {
			textData: '=textData',
			friendDetails: '=friendDetails',
			peopleInvolved : '=peopleInvolved'
		},
		link: function(scope, element, attrs) {


			var n;
			var i;
			var count=0;
			var OldOccurence = 0;
			var Occurence = 0;
			var patt1 = /[@]/g;
			var noOfAt;
			var appendTagAt;
			var result;
			var textLength;
			var textLengthDarePopUp;
			var selectedText;


			scope.TagSug = false;

			scope.YourFriends = [];

			var cursorPosition;
			var clearSearchTimeout;
			function getCaretPosition(element) {
				var caretOffset = 0;
				var doc = element.ownerDocument || element.document;
				var win = doc.defaultView || doc.parentWindow;
				var sel;
				if (typeof win.getSelection != "undefined") {
					sel = win.getSelection();
					if (sel.rangeCount > 0) {
						var range = win.getSelection().getRangeAt(0);
						var preCaretRange = range.cloneRange();
						preCaretRange.selectNodeContents(element);
						preCaretRange.setEnd(range.endContainer, range.endOffset);
						caretOffset = preCaretRange.toString().length;
					}
				} else if ( (sel = doc.selection) && sel.type != "Control") {
					var textRange = sel.createRange();
					var preCaretTextRange = doc.body.createTextRange();
					preCaretTextRange.moveToElementText(element);
					preCaretTextRange.setEndPoint("EndToEnd", textRange);
					caretOffset = preCaretTextRange.text.length;
				}
				return caretOffset;
			}


			var update = function() {
				cursorPosition=getCaretPosition(this);
			};

			$('.ta').on("mousedown mouseup keydown keyup", update);
			$('.dare-comment-input').on("mousedown mouseup keydown keyup", update);

			function nth_occurrence (string, char, nth) {
				var first_index = string.indexOf(char);
				var length_up_to_first_index = first_index + 1;

				if (nth == 1) {
					return first_index;
				} else {
					var string_after_first_occurrence = string.slice(length_up_to_first_index);
					var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

					if (next_occurrence === -1) {
						return -1;
					} else {
						return length_up_to_first_index + next_occurrence;
					}
				}
			}
			function removeCursorFromMention() {
				var target = null;

				if (window.getSelection) {
					target = window.getSelection().getRangeAt(0).commonAncestorContainer;
					target = (target.nodeType === 1) ? target : target.parentNode;

					if (target.tagName == 'SPAN') {
						var tmp = $('<span>').insertAfter(target),
							textNode = document.createTextNode("\u200b"),
							node = tmp.get(0),
							range = null,
							sel = null;

						range = document.createRange();
						range.selectNode(node);
						range.setStartAfter(node);
						range.insertNode(textNode);
						range.setStartAfter(textNode);
						sel = window.getSelection();
						sel.removeAllRanges();
						sel.addRange(range);
						tmp.remove();
					}
				}
			}
			function captureTextSelection() {
				if (window.getSelection().anchorNode.parentNode.tagName == 'SPAN') {
					selectedText.push(window.getSelection().anchorNode.parentNode);
				}

				if (window.getSelection().focusNode.parentNode.tagName == 'SPAN') {
					selectedText.push(window.getSelection().focusNode.parentNode);
				}
			}




			scope.$watch('textData', function (newValue, oldValue) {

				$timeout.cancel(clearSearchTimeout);
				clearSearchTimeout = $timeout(function () {

					result=scope.textData.replace(/<\/span>/g, '');

					for (i = 0; i <= count; i++) {
						var RGX=new RegExp('<span id="first'+i+'">', 'g');
						result=result.replace(new RegExp('<span id="first'+i+'">', 'g'), '');
					}
                    // result=result.replace(new RegExp('<span id="first0">', 'g'), '');
                    result=result.replace(/&nbsp;/g, ' ');
                    result=result.replace(/&#8203;/g, '');
					var pos = result.substring(0, cursorPosition);
					n = pos.lastIndexOf("@");
					console.log("Repeat",scope.textData,"Repeat1",n,result)


					scope.res = result.substring(n + 1, cursorPosition);

					console.log("info", scope.res, n, cursorPosition,result);

					var stringOfAt = result.substring(0, cursorPosition);
					var patt3 = /[@]/g;
					if(stringOfAt.match(patt3)){
						noOfAt = stringOfAt.match(patt3).length;
						appendTagAt=nth_occurrence(scope.textData, '@', noOfAt);
					}



					var DataFilter = scope.textData.match(patt1);
					if (!DataFilter) {
						Occurence = 0;
						OldOccurence = 0;
						scope.TagSug = false;

					}
					if (DataFilter) {
						Occurence = DataFilter.length;
						if (Occurence > OldOccurence) {

							scope.TagSug = true;
						}
						else if (Occurence < OldOccurence) {
							scope.TagSug = false;
						}
						console.log("Num", Occurence, OldOccurence, scope.TagSug)
						OldOccurence = Occurence;
					}

					var range = window.getSelection().getRangeAt(0);

					// for admin dare

					if (textLength && $(".ta").html().length < textLength) {
						if (range.commonAncestorContainer.parentNode.tagName == 'SPAN') {
							$(".ta")[0].removeChild(range.commonAncestorContainer.parentNode);
						}
					}
					if (window.getSelection().toString()) {
						captureTextSelection();
					} else {
						removeCursorFromMention();
					}
					textLength = $(".ta").html().length;

					// for p2p dare

					if (textLengthDarePopUp && $(".dare-comment-input").html().length < textLengthDarePopUp) {
						if (range.commonAncestorContainer.parentNode.tagName == 'SPAN') {
							$(".dare-comment-input")[0].removeChild(range.commonAncestorContainer.parentNode);
						}
					}
					if (window.getSelection().toString()) {
						captureTextSelection();
					} else {
						removeCursorFromMention();
					}
					textLengthDarePopUp = $(".dare-comment-input").html().length;
					console.log("peopleInvolved", scope.peopleInvolved)

					var socket = commonService.socket;
					var peopleInvolvedIds=new Set()
					var peopleInvolvedNew=[]
					angular.forEach(scope.peopleInvolved, function(user){
						if (!peopleInvolvedIds.has(user.userId)){
							peopleInvolvedNew.push(user);
							peopleInvolvedIds.add(user.userId)
						}
					});
					var peopleInvolvedNew1=$filter('filter')(peopleInvolvedNew, {username:scope.res})
					TagUsers(peopleInvolvedNew1);
					if (scope.res.length > 2 && scope.TagSug == true && n != -1) {

						if (socket && socket.socket && socket.socket.open) {
							socket.emit('search_users_new', commonService.loggedInUserId, scope.res, Array.from(peopleInvolvedIds),5, 'tagging' )
							socket.on('search_users_tagging', function (response) {
								var newList=peopleInvolvedNew1.concat(response.result)
								var newList1=$filter('filter')(newList, {username:scope.res})
								TagUsers(newList1);
							})
						}
						else {
						 APIService.searchUsers(scope.res, 0, 0, 5).then(function (data) {
							 var newList=peopleInvolvedNew1.concat(data.users)
							 var newList1=$filter('filter')(newList, {username:scope.res})
                           		$timeout(function(){
									TagUsers(newList1);
								});

                        });
						}
					}
				}, 300);
			});


			String.prototype.replaceBetween = function (start, end, what) {
				return this.substring(0, start) + what + this.substring(end);
			};

			function TagUsers(FriendsList) {
				try {
					scope.$apply(function () {
						scope.YourFriends = FriendsList;

					});
				} catch (e) {
					scope.YourFriends = FriendsList;
				}

			}

			//$('.Hover').mousedown(function(){return false;});
			scope.AppendName = function (friend, evt) {
				if(evt){
					evt.preventDefault();
					evt.stopPropagation();
				}

				var userdata = {}
				userdata.username = "@" + friend.username;
				userdata.id = friend.userId;
                var txt1 = '&#8203;<span id="first'+count+'">'+friend.username+"</span>&#8203;&nbsp;";
				scope.textData=scope.textData.replace(/&nbsp;/g, ' ');
				console.log("scope.textData", scope.textData, appendTagAt, scope.res.length, txt1)
				scope.textData = scope.textData.replaceBetween(appendTagAt, appendTagAt+scope.res.length+1, txt1);
                txt1=txt1.replace(/&nbsp;/g, '');
                txt1=txt1.replace(/&#8203;/g, '');
				//userdata.link = txt1.link("#/profile/" + friend.userId);
				userdata.link = "<a class='taggedUser' href=" + "#/profile/" + friend.userId +">" + txt1 + "</a>"

				userdata.convertLink=txt1;
				scope.friendDetails.push(userdata);
				scope.TagSug = false;
				scope.YourFriends.length = 0;
				count++;



			}
		}


	}
}
]);