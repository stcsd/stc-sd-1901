angular.module('stichio').directive('followSuggestionPopup', ['commonService', 'APIService', '$rootScope', '$cookies', '$ionicSlideBoxDelegate', '$state', '$timeout',function(commonService, APIService, $rootScope, $cookies, $ionicSlideBoxDelegate, $state, $timeout) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/followSuggestionPopup.html',
        link : function(scope, element, attribute) {
            if (commonService.new_user || $cookies.get('new_user') || $cookies.get("signup_follow")) {

                scope.data = {};
                scope.data.slides = [
                    {
                        video: "videos/0_eating_chili_final_sound.mp4",
                        smiley : "images/smileys/crazy.png",
                        videoText : "Can you eat a red chilli?",
                        textColor: "#002545"
                    },
                    {
                        video: "videos/1_dance_final_sound_long.mp4",
                        smiley : "images/smileys/wink.png",
                        videoText : "Can you dance like Dhanno?",
                        textColor: "white"
                    },
                    {
                        video : "videos/2_exercise_final_sound.mp4",
                        smiley : "images/smileys/happy.png",
                        videoText : "Can you do twenty pull ups?",
                        textColor: "#002545"
                    }
                ]

                var setupSlider = function () {
                    //some options to pass to our slider
                    scope.data.sliderOptions = {
                        initialSlide: 0,
                        direction: 'horizontal', //or vertical
                        speed: 300 //0.3s transition
                    };

                    //create delegate reference to link with slider
                    var slider;
                    //watch our sliderDelegate reference, and use it when it becomes available
                    scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                        var vid1 = document.getElementById("intro_video_0");
                        vid1.play();
                        console.log("sliderr", event, data)
                        slider=data.slider;
                        // data.slider is the instance of Swiper
                    });

                    scope.slideNext= function(){
                        slider.slideNext();
                    }
                    scope.$on("$ionicSlides.slideChangeStart", function (event, data) {

                    });

                    scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                        console.log("event", event, data.slider.activeIndex)
                        var vid1 = document.getElementById("intro_video_" + data.slider.activeIndex);
                        var vid2 = document.getElementById("intro_video_" + data.slider.previousIndex);
                        try {
                            vid2.pause();
                        } catch (e) {
                        }
                        try {
                            vid1.play();
                        } catch (e) {
                        }

                        // note: the indexes are 0-based

                    });
                };
                setupSlider();


                var openFollowSuggestionPopup = function () {
                    commonService.openPopup("follow-sugg-popup");
                }
                scope.closeFollowSuggestionPopup = function () {
                    commonService.closePopup("follow-sugg-popup");
                    // if($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                    //     $timeout(function () {
                    //         $('body').addClass('stop-scrolling1');
                    //
                    //     }, 500)
                    // }
                }
                //commonService.new_user = true;

                openFollowSuggestionPopup();
                scope.stepNum=1;
                $timeout(function () {

                    APIService.getTopDarees().then(function (results) {
                        $("#followsuggs-loading").hide();
                        scope.userSuggestions = results;
                        //scope.$apply();

                    },function(){
                        $("#followsuggs-loading").hide();
                    })

                }, 100);
                $timeout(function(){

                    $('body').addClass('stop-scrolling')
                    scope.stepNum=5;
                    setTimeout(function(){
                        if(!scope.userSuggestions)
                            $("#followsuggs-loading").show();
                    }, 100);
                    //scope.$apply()
                    //$timeout(function(){
                    //    scope.stepNum++;
                    //    //scope.$apply();
                    //
                    //},4000)

                },3000);
                $cookies.put("signup_follow", "true")



                scope.nextStep = function () {
                    scope.stepNum++;
                    $timeout(function(){
                        scope.stepNum++;
                        //scope.$apply()
                    }, 3000)
                }

                scope.followAll = function () {
                    var checkedUserIds = []
                    scope.userSuggestions.forEach(function (user) {
                        if (!user.unchecked) {
                            checkedUserIds.push(user.userId)
                        }
                    })
                    console.log("follow users", checkedUserIds)
                    $("#follow-all-button .spinner").show()
                    APIService.followUser({"followeeIdList": checkedUserIds, "medium": "signup"}).then(function () {
                        $("#follow-all-button .spinner").hide();
                        $("#follow-all-button").css('background-color', '#e6e9eb');
                        $("#follow-all-button").text("Followed");
                        if($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                            $rootScope.refreshHome('top');
                        }
                        $timeout(function () {
                            scope.closeFollowSuggestionPopup()
                        }, 1000)
                        var gaLabel = "";
                        var followeesFetched = false;
                        if ($rootScope.userFollowee) {
                            followeesFetched = true;
                        }
                        scope.userSuggestions.forEach(function (user) {
                            if (!user.unchecked) {
                                user.is_following = true;
                                gaLabel += user.userId + ' ' + user.username + ', '
                                if (followeesFetched) {
                                    $rootScope.totalFollowees++;
                                    $rootScope.userFollowee.unshift(user)
                                }
                                //else{
                                //    $rootScope.totalFollowees=1
                                //}
                            }
                            else {
                                commonService.analytics('event', {
                                    category: 'Click',
                                    action: 'Follow_Signup_Uncheck',
                                    label: user.userId + ' ' + user.username
                                })
                            }
                        });
                        try {
                            $cookies.remove("signup_follow")
                        } catch (e) {
                        }
                        commonService.analytics('event', {
                            category: 'Click',
                            action: 'Follow_Signup',
                            label: gaLabel
                        })
                    }, function () {
                        $("#follow-all-button .spinner").hide();
                        $timeout(function () {
                            scope.closeFollowSuggestionPopup()
                        }, 1000)
                    });
                }

            }
        }
    }
}]);