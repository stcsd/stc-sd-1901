angular.module('stichio').directive('dareMenu',['$http', '$sce', '$rootScope','commonService', '$state','APIService',function($http, $sce, $rootScope, commonService, $state, APIService) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/dareMenu.html?0.01',
        link: function (scope, element, attribute) {
            commonService.setLoggedInStatus();
            if (!$rootScope.userLoggedIn){
                return;
            }
            var dareObj;
            scope.openDareMenu = function (objectType,object,justCompleted, type, otherOptions) {
                otherOptions=otherOptions || false;
                // event.stopPropagation()
                commonService.openPopup('dare-menu')
                // $('.share-dare').addClass('target')
                dareObj=object;
                var object = angular.copy(object);
                object.type = objectType;
                console.log(object);



                scope.shareMessage = 'Share with Friends';

                scope.shareMessage = $sce.trustAsHtml(scope.shareMessage);
                scope.showDareMenu = true;
                // $('body').addClass('prevent-scroll')
                scope.isCopied=false;
                setTimeout(function () {

                    $('.share-popup-content').click(function (e) {
                        console.log("click share")
                        e.stopPropagation();
                    })
                    $("#sharePopupBox").click(function (e) {
                        var $copyBtn=$('#copybtn1')[0]
                        console.log("click share11", e.target,$copyBtn)

                        if (e.target !==  $copyBtn)
                            scope.closeDareMenu();
                    });
                },10);
                //if(justCompleted) {
                //    bodyScollpos = $('body').scrollTop()
                //    $('body').animate({scrollTop: bodyScollpos - 381}, 0)
                //}
                //$('body').addClass('prevent-scroll')
                setTimeout(function() {
                    if (objectType == "dare_sug") {
                        $("#share_chat").show()
                    }
                    else {
                        $("#share_chat").hide()
                    }
                    if (otherOptions){
                        $("#take_dare_opt").show()
                        $("#dare_friend_opt").show()
                        $("#share_chat").show()
                        $("#share_whatsapp").show()
                    }
                    else{
                        $("#take_dare_opt").hide()
                        $("#dare_friend_opt").hide()
                        $("#share_chat").hide()
                        $("#share_whatsapp").hide()
                    }
                }, 200);


                if(objectType == 'dare' || objectType == 'taken_dare' || objectType == 'eventDare' || objectType == 'dare_sug' || objectType == 'event'){
                    scope.dare = angular.copy(object);
                }
                else if(objectType == 'stich'){

                }
                //var urlBase = window.location.protocol +'//'+window.location.hostname+":"+window.location.port;
                var urlBase = "http://www.stichio.co.in"

                if (objectType == 'event') {
                    scope.tempdareLink = urlBase + '/#/events/' + object.uniqueUserName;
                }
                else if(objectType == 'dare_sug'){
                    scope.tempdareLink = urlBase + '/#/dares/' + object.id;
                    scope.showHideDareOpt=false;
                }
                else {
                    if (scope.dare.feed_type=='dare') {
                        scope.tempdareLink = urlBase +'/#/profile/'+ scope.dare.user2.userId + '?dareId=' + scope.dare.id ;
                        scope.showHideDareOpt=(scope.dare.status>2) && (scope.dare.user2.userId==commonService.loggedInUserId)
                    }
                    else {
                        scope.tempdareLink = urlBase + '/#/profile/' + scope.dare.user.userId + '?adminDareId=' + scope.dare.id;
                        scope.showHideDareOpt=(scope.dare.user.userId==commonService.loggedInUserId);
                    }
                }

                scope.dareLink = scope.tempdareLink
                scope.copyDareLink();
                setTimeout(function () {
                    var clipboard = new Clipboard('.copybtn1');
                    clipboard.on('success', function(e) {
                        scope.isCopied=true;
                        scope.$apply()
                        console.info('Action:', e.action);
                        console.info('Text:', e.text);
                        console.info('Trigger:', e.trigger);

                        e.clearSelection();
                    });

                    clipboard.on('error', function(e) {
                        console.error(e)
                        //console.error('Action:', e.action);
                        //console.error('Trigger:', e.trigger);
                    });
                },100);

                console.log('instance created')
            }
            $('body').on('keyup',function (e) {
                e.preventDefault()
                e.stopPropagation()
                if(e.keyCode == 27){
                    scope.closeShareDare()
                    scope.$apply();
                }
            });
            $rootScope.closeDareMenu = function () {
                commonService.closePopup('dare-menu')
                scope.showDareMenu = false;
                // $('body').removeClass('prevent-scroll')
            }
            scope.copyDareLink = function () {
                commonService.generateShareLink({link : scope.tempdareLink}).then(function(link){
                     scope.dareLink = link;
                });
            }
            scope.getTinyUrl1 = function(url) {
                var myData = {};
                myData.longUrl = url;
                var key = "AIzaSyB0N1UrT-OxThltr9Lr1bb1IeCuYma-rro";
                $http({
                    method: 'post',
                    url: "https://www.googleapis.com/urlshortener/v1/url?key=" + key,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: myData
                }).then(function (response) {
                    scope.dareLink = response.data.id;
                    console.log('shortUrl',scope.dareLink)
                }, function (response) {
                    console.log(response);
                });

            }
            scope.delayCloseDareMenu = function(){
                setTimeout(function(){
                    scope.closeDareMenu();
                }, 500)
            };
            scope.hideDareClick = function(){
                $("#dare-menu-box").hide()
                $("#hide-dare-conf").show()
            }
            scope.hideDare = function(){
                $("#dare-menu-box").show();
                $("#hide-dare-conf").hide()
                APIService.hideDare(dareObj.id, true).then(function(){
                    $state.reload();
                })
                scope.closeDareMenu();
            }
            scope.cancelHideDareClick = function(){
                $("#dare-menu-box").show()
                $("#hide-dare-conf").hide()
            }

        }

    }
}]);
