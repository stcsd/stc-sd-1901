angular.module('stichio').directive('dareResponses',['$rootScope', '$http', 'APIService', 'commonService',  '$timeout', '$window','$sce', '$ionicPlatform', '$location','$state', function ($rootScope, $http, APIService, commonService,  $timeout, $window, $sce, $ionicPlatform, $location, $state)  {
    return {
        restrict: 'E',

        templateUrl: 'shared/templates/dareResponses.html?0.04',
        link: function(scope,element,attribute) {
            //commonService.setLoggedInStatus();
            if (!$rootScope.userLoggedIn){
                return;
            }
            var listener;

            $timeout(function () {



                scope.openDareResponse = function(dare){
                    var dare_type=dare.dare_type;
                    var dareId=dare.id;
                    if (dare_type=='dare') {
                        $rootScope.openDarePopup(null, false, true, false, dareId, true, true, dare);
                    }
                    else
                    {
                        $rootScope.openAdminDarePopup(null, false, true, false, dareId, true, true, dare)
                    }
                };

                $rootScope.closeDareResponsesPopup = function(popupClose){


                    commonService.closePopup('dare-responses-popup');
                    if (scope.cards && scope.cards[0]) {
                        scope.cards[0].dareResponses = []
                        scope.cards[0].offset = 0
                    }
                    scope.cards=[]
                    $('.dareResponses').off('scroll.dareResponsesScroll')
                    try {
                        listener();
                    } catch (e) {
                    }
                    if ($state.current.name == "home.dare") {
                        $state.go('home.stiches', {
                        }, {
                            reload: false,
                            notify: false,
                            location : 'replace'
                        });
                        $location.path()
                    }
                };
                scope.toggleUpvoteDareResponses= function(response){
                    var data={}
                    if(!response.upvoted){
                        data.vote_type = 'up';

                    }
                    else{
                        data.vote_type = 'cancel';
                    }
                    if (response.dare_type=="dare"){
                        data.dareResponseId = response.responseId;
                        APIService.voteDareResponse(data, response.id, response.user1, response.user2);
                    }
                    else {
                        data.dareResponseId = response.id;
                        APIService.adminDareVote(data, response.id, response.userId);
                    }
                };


                //scope.$watch('card.submitting_dare', function(key){
                //    if(scope.cards[0] && scope.cards[0].submitting_dare) {
                //        APIService.getNextSuggestion(scope.cards[0].id).then(function(data){
                //            scope.new_card = data;
                //            scope.new_card.creator = 'STICHIO';
                //        });
                //    }
                //});
                var dareIndex=0;
                var dareCount=14;
                var $loadingIcon=null;


                $rootScope.openDareResponsesPopup = function (card, dareSuggId, takedare) {
                    takedare = takedare || false;
                    try {
                        $rootScope.close_takeDare();
                    } catch (e) {
                    }
                    commonService.openPopup('dare-responses-popup');
                    try {
                        $rootScope.closeNewChatPopup();
                    } catch (e) {
                    }
                    $loadingIcon = $("#dare-responses-loading-icon")
                    if (!card) {
                        $loadingIcon.show();
                        APIService.getAdminDareInfo(dareSuggId).then(function (data) {
                            card = data;
                            $loadingIcon.hide();
                            if (!data.id){
								console.log("dare data error")
								$rootScope.closeDareResponsesPopup();
								$rootScope.openErrorPopup();
								return;
							}
                            showDareSuggData(card, takedare);
                        }, function () {
                            $rootScope.closeDareResponsesPopup();
                            $rootScope.openErrorPopup();
                            return;
                        });

                    }
                    else {
                        showDareSuggData(card, takedare);
                    }
                }
                var showDareSuggData=function(card, takedare) {
                    scope.cards = [angular.copy(card)]

                    scope.cards[0].dareResponses = []
                    scope.cards[0].textResponses = []
                    scope.cards[0].dare_submitted=false
                    scope.cards[0].response_image=null
                    scope.cards[0].response_image_tmp=null
                    scope.cards[0].response_text=""
                    scope.cards[0].response_text_tmp=""
                    scope.cards[0].dares=null

                    //
                    scope.cards[0].just_completed_dares = []
                    //listener = scope.$watch('cards[0].dare_submitted', function (key) {
                    //    if (scope.cards[0] && scope.cards[0].dare_submitted) {
                    //        scope.cards[0].just_completed_dares.unshift(angular.copy(scope.cards[0]));
                    //        //$timeout(function() {
                    //        //    if (scope.cards[0].dare_submitted && scope.new_card) {
                    //        //        scope.cards[0] = angular.copy(scope.new_card);
                    //        //        scope.new_card = null;
                    //        //    }
                    //        //}, 500);
                    //    }
                    //});






                    if (!takedare) {
                        dareIndex = 0;
                        dareCount = 14;
                        var loading_dares = true;

                        function loadDareResponses(dareIndex1, dareCount) {
                            $loadingIcon.show();
                            APIService.getDareResponses(card.id, dareIndex1, dareCount).then(function (results) {


                                console.log("dare responses", results.image_responses)

                                scope.cards[0].dareResponses = scope.cards[0].dareResponses.concat(results.image_responses);
                                if (results.images_count) {
                                    scope.imagesCount=results.images_count;
                                    scope.cards[0].offset = results.images_count % 3;
                                }
                                scope.cards[0].textResponses = scope.cards[0].textResponses.concat(results.text_responses);

                                dareIndex = results.last_index;
                                $loadingIcon.hide();
                                loading_dares = false;
                                if (results.image_responses.length + results.text_responses.length == 0) {
                                    $('.dareResponses').off('scroll.dareResponsesScroll')
                                }
                            }, function () {
                                $loadingIcon.hide();
                                loading_dares = false;
                                $('.dareResponses').off('scroll.dareResponsesScroll')
                            })

                        }
                        if (scope.cards[0].dareResponses.length + scope.cards[0].textResponses.length == 0) {
                            loading_dares = true;
                            loadDareResponses(dareIndex, dareCount);

                        }
                        $timeout(function () {
                            var $win = $('.dareResponses');
                            var onScrollFn = function () {
                                if (!loading_dares && ($win.scrollTop() > ($win[0].scrollHeight - 1200))) {
                                    loading_dares = true;
                                    loadDareResponses(dareIndex, dareCount)
                                }
                            }
                            commonService.infiniteScroll($win, onScrollFn, 'scroll.dareResponsesScroll')
                        }, 100);
                    }
                    else{

                        setTimeout(function () {
                            var $el = $(".dare-responses-popup .dare-sugg-opts")[0];
                            $el.style.visibility = 'hidden';
                            $el.style.height = '10px';
                        });
                    }

                }


            }, 100);
        }
    }
}]);