angular.module('stichio').directive('videoPlayer', ['$sessionStorage', '$cookies', '$http','APIService','commonService','$rootScope','Upload','$timeout', '$sce','$state','$window', function($sessionStorage, $cookies, $http, APIService, commonService, $rootScope, Upload, $timeout, $sce, $state,$window) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/videoPlayer.html?0.05',
        link : function(scope, element, attribute){
             //commonService.setLoggedInStatus();
                if (!$rootScope.userLoggedIn){
                    return;
                }

            scope.videoUrl = ""

            scope.onPlayerReady = function onPlayerReady(API,index) {
                scope.API = API;
                console.log("API", API)
                //if (!API.isFullScreen)
                //        API.toggleFullScreen()
                //console.log("index", index)
                //scope.players[index]=API
            };

            scope.onUpdateState = function (state, index) {
                console.log("video state", state)
                //if(state=='play') {
                //
                //     //scope.API.playPause()
                //}
                //else if(state=='pause')
                //{
                //     scope.API.playPause()
                //}
            }

            $rootScope.hideVideo = function (e) {
                if (e)
                    e.stopPropagation()
                scope.videoUrl=""
                commonService.closePopup("video-popup")
                if (scope.API) {
                    scope.API.stop();
                    scope.API.sources=""
                }
                commonService.analytics('event', {category : 'Video', action : 'Close Popup', label : ''});
            };

            function fileExists(url){

                var http = new XMLHttpRequest();

                http.open('HEAD', url, false);
                http.send();

                return (http.status != 404 && http.status != 403);

            }


            scope.openVideo = function(video_url, key) {
                scope.video = {}

                scope.videoUrl = video_url
                commonService.openPopup("video-popup");


                //setTimeout(function () {
                    if ($rootScope.isMobile)
                    {
                        console.log("checking if file exists1", video_url)
                        video_url=video_url.replace('videos', 'videos_mobile').replace('.mp4', '_m.mp4');
                        console.log("checking if file exists2", video_url)
                        //try {
                        //    if (fileExists(video_mob_url)) {
                        //        console.log("file exists")
                        //        video_url = video_mob_url
                        //    }
                        //} catch (e) {
                        //    console.log(e)
                        //}
                    }
                    scope.video.sources = [{src: $sce.trustAsResourceUrl(video_url), type: "video/mp4"}]
                    console.log("playing video")
                    setTimeout(function () {
                        if (scope.API.sources) {
                            //scope.API.playPause()
                        }
                        commonService.analytics('event', {category : 'Video', action : 'Play', label : video_url});
                        var socket = commonService.socket;
                        if (key) {
                            socket.emit('view_count', 'internal', key);
                            commonService.analytics('event', {category : 'Internal Views', action : key, label : 'video'});
                        }
                    }, 200);
                //},100);


            }
            scope.playVideo = function(){
                scope.API.play()
            }
            scope.changeSource = function (){
                console.log(scope.videoUrl)
                scope.video.sources = [{src: $sce.trustAsResourceUrl(scope.videoUrl), type: "video/mp4"}]
                //setTimeout(function(){
                //    if (scope.API.sources) {
                //        scope.API.playPause()
                //    }
                //},300)
            }



        }
    }
}]);