angular.module('stichio').directive('backImage', [function(){
    return function(scope, element, attrs){
        attrs.$observe('backImage', function(value) {
            //var colors = ['DDDDDD','FFA18B','D1E8FF','FFD3BD','C8C8FF','FFDDFF','E5E4CA','FFE8C8'];
            //var color = '#' + colors[Math.floor(Math.random() * colors.length)]

            if (attrs.bgc) {
                element.css('background-color', attrs.bgc);
            }

            element.css({
                // 'background-color':color
                'background-size' : 'cover',
                'background-position' : 'center center',
                'background-repeat': 'no-repeat'
            });
            if (value){
                element.css('background-image', 'url(' + value +')')
            }
        });
    };
}]);