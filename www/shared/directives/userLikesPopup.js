/**
 * Created by STICHIO INTERN on 10/5/2016.
 */

angular.module('stichio').directive('userLikesPopup',['commonService','APIService', '$rootScope' ,function (commonService,APIService, $rootScope) {

    return {
        restrict:'E',
        templateUrl:'shared/templates/userLikesPopup.html',
        link: function (scope,element,attribute) {
            //commonService.setLoggedInStatus();
                if (!$rootScope.userLoggedIn){
                    return;
                }

            scope.loggedInUserId = commonService.loggedInUserId
            scope.noOflikes = 0;
            scope.objname = '';

            scope.showUserLikesPopup = function(){
                $rootScope.showFollowers = false;
                commonService.openPopup('userLikes-popup');
                // $('.userLikes-popup').addClass('target');
                // $('body').addClass('prevent-scroll');
                if(userLikePopup.height() > $(window).height()){
                    userLikePopup.removeClass('remove-trans')
                    userLikePopup.css('height',$(window).height()+'px')
                    userLikePopup.css('min-height','0')
                }
                else{
                    // userLikePopup.css('min-height','400px')
                    userLikePopup.css('height','80%')
                    userLikePopup.addClass('remove-trans')


                }
            }
            scope.openUserLikesPopup = function (stichData,noOfLikes) {
                scope.noOflikes = noOfLikes
                $('.search-loading-div.like-search-loading').css('display','block')

                scope.showUserLikesPopup();
                scope.addEventsOnPopup();
                APIService.getLikedStichUser(stichData.stitchId).then(function (data) {
                    scope.userLists = data ;
                    $('.search-loading-div.like-search-loading').css('display','none')
                })
            }
            scope.openUpvoDownvoUsers = function (dareId,noOfLikes,DareRespoId,type) {
                scope.noOflikes = noOfLikes;
                scope.objname = type;
                scope.userLists = []
                $('.search-loading-div.like-search-loading').css('display','block')
                // alert(scope.noOflikes)
                scope.showUserLikesPopup();
                scope.addEventsOnPopup();
                if(scope.objname == 'upvote'){
                    APIService.getUpvoteUsers(dareId,DareRespoId).then(function (data) {
                        scope.userLists = data ;
                        $('.search-loading-div.like-search-loading').css('display','none')

                    })
                }
                else if(scope.objname == 'downvote'){
                    APIService.getDownvoteUsers(dareId,DareRespoId).then(function (data) {
                        scope.userLists = data ;
                        $('.search-loading-div.like-search-loading').css('display','none')

                    })
                }


            }
            scope.openAdminUp_DownvoteUser = function (dareId,dareResponseId,type,votes) {
                scope.noOflikes = votes;
                scope.objname = type;
                scope.userLists = [];
                console.log("open ad")
                $('.search-loading-div.like-search-loading').css('display','block')
                // alert(scope.noOflikes)
                scope.showUserLikesPopup();
                scope.addEventsOnPopup();
                if(scope.objname == 'upvote'){
                    APIService.getAdminDareUpvoteUsers(dareResponseId).then(function (data) {
                        scope.userLists = data ;
                        $('.search-loading-div.like-search-loading').css('display','none')

                    })
                }
                else if(scope.objname == 'downvote'){
                    APIService.getAdminDareDownvoteUsers(dareResponseId).then(function (data) {
                        scope.userLists = data ;
                        $('.search-loading-div.like-search-loading').css('display','none')

                    })
                }
            }
            scope.openDareLikePopup = function (dareId,noOfLikes,type) {
                scope.noOflikes = noOfLikes;
                scope.objname = type;
                scope.userLists =[]
                $('.search-loading-div.like-search-loading').css('display','block')
                // alert(scope.noOflikes)
                //  openPopup('userLikes-popup')
                scope.showUserLikesPopup();
                scope.addEventsOnPopup()
                APIService.getDareLikeUsers(dareId).then(function (data) {
                    // / APIService.getLikedDareUser(dareId).then(function (data) {
                    scope.userLists = data ;
                    $('.search-loading-div.like-search-loading').css('display','none')
                })
            }
            // $('.like-popup-close').on('click',function () {
            //     // $('.userLikes-popup').removeClass('target')
            //     closePopup('userLikes-popup')
            //     scope.userLists = []
            // })
            scope.closeLikePopup = function () {
                commonService.closePopup('userLikes-popup');
                // $('.userLikes-popup').removeClass('target');
                // $('body').removeClass('prevent-scroll');
                scope.userLists = []
            }
            var userLikePopup = $('.userLikes-popup > div:first-child')
            $(window).on('resize',function () {
                if(userLikePopup.height() >= $(window).height()){
                    // landscape
                    userLikePopup.removeClass('remove-trans');
                    userLikePopup.css('height',$(window).height()+'px');
                    userLikePopup.css('min-height','0');
                }
                else{
                    // portrait
                    userLikePopup.addClass('remove-trans')
                    // userLikePopup.css('min-height','400px')
                    userLikePopup.css('height','80%')
                }

            })
            scope.addEventsOnPopup = function(){
                var el =  $('.userLikes-popup');
                var elem =  $('#userLikesPopup');
                // var elcontent = $('#dareviewpopup');
                elem.focus();
                elem.on('keydown',function (e) {
                    if(e.keyCode == 27){
                        console.log("escaa")
                        scope.closeLikePopup(e)
                    }
                    // else if(e.keyCode == 38){
                    //     elcontent.animate({scrollTop: elcontent.scrollTop() - 35},0);
                    //
                    //     console.log('keyup',elcontent.scrollTop())
                    // }else if(e.keyCode == 40){
                    //     elcontent.animate({scrollTop: elcontent.scrollTop() + 35},0);
                    // }
                })
                el.on('click',function (e) {
                    if(e.target != this){
                        return;
                    }
                    scope.closeLikePopup(e)
                })
                
            }

        }

    }

}]);