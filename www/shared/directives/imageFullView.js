/*
 * Created by STICHIO INTERN on 9/22/2016.
 */

angular.module('stichio').directive('imageFullView', ['$rootScope', 'commonService', function ($rootScope, commonService) {

    return{
        restrict : 'E',
        // transclude: true,
        templateUrl: 'shared/templates/imageFullView.html',
        link: function (scope,element,attribute) {
            var body = $('body');
            var imagePopup = $('#imagePopup');
            var popupContent = $('#popupContent');
            var popupImage = $('#popupImage');
            var icons = $('#actionIcons');
            var innerDiv = $('#innerDiv');
            var weblink = $('#weblink');
            var scrollTo=0;
            scope.source = false;

            scope.toggleWeblink = function(event){
                event.stopPropagation();
                scope.source = !scope.source;
            }

            $rootScope.closeImagePopup = function () {
                console.log('closeImagePopup');
                // imagePopup.css({"display": "none"});
                commonService.closePopup('imagePopup');
                popupImage.attr('src', "");
                // body.removeClass('stop-scrolling');
                $('body').animate({scrollTop: scrollTo}, 0);
                scope.source = false;
            }

            scope.openImagePopup = function (clickedImgUrl, card, actionIcons, imageType, event) {
                if(event){
                    event.stopPropagation();
                }
                scope.imageCard = card;
                console.log(card);
                console.log('openImagePopup');

                scope.icons = actionIcons;

                var imageToShow;
                if(imageType == 'stich') {
                    console.log("stichhh")
                    if (!clickedImgUrl) {
                        clickedImgUrl = card.orig_image || card.image || card.full_image;
                    }
                    if(clickedImgUrl == card.full_image) {
                        return;
                    }
                    else if(clickedImgUrl == card.image) {
                        imageToShow = card.full_image || card.orig_image || card.image;
                    }
                    else if(clickedImgUrl == card.orig_image || clickedImgUrl == card.image) {
                        imageToShow = card.orig_image || card.image || card.full_image;
                    }
                }
                else if(imageType == 'avatar') {
                    if (!clickedImgUrl) {
                        clickedImgUrl = card.avatar;
                    }
                    if(clickedImgUrl == card.avatar_full) {
                        imageToShow = card.avatar_full;
                    }
                    else if(clickedImgUrl == card.avatar_compressed) {
                        imageToShow = card.avatar_full;
                    }
                    else if(clickedImgUrl == card.avatar) {
                        imageToShow = card.avatar;
                    }
                }
                else if(imageType == 'dareResponse') {
                    if (!clickedImgUrl) {
                        clickedImgUrl = card.response_image;
                    }
                    if(clickedImgUrl == card.response_image_full) {
                        imageToShow=clickedImgUrl;
                    }
                    else if(clickedImgUrl == card.response_image_compressed) {
                        imageToShow = card.response_image_full;
                        console.log("imageToShow", imageToShow)
                    }
                    else if(clickedImgUrl == card.response_image) {
                        imageToShow = card.response_image;
                    }
                }
                imageToShow = imageToShow || clickedImgUrl
                console.log("clickedImgUrl", clickedImgUrl);
                popupImage.attr('src', clickedImgUrl);
                popupImage.addClass('blur');
                scrollTo = $('body').scrollTop();
                // imagePopup.css("display", "block");
                commonService.openPopup('imagePopup');
                // body.addClass('stop-scrolling');


                imagePopupResize();
                $(window).resize(imagePopupResize);

                function imagePopupResize() {
                    setTimeout(function(){
                        console.log("popupContent.height(), window.innerHeight", popupContent.height(), window.innerHeight)
                        if(popupContent.height() > window.innerHeight) {
                            console.log('bigger image');
                            popupContent.css({"top": "0", "transform": "translate(-50%, 0)"});
                            var scrollWidth = (imagePopup.width() - innerDiv.width())
                            var iconsRightPos = (imagePopup.width() - popupContent.width() - scrollWidth) / 2 + scrollWidth;
                            var iconsElem = icons.detach();
                            imagePopup.append(iconsElem);
                            iconsElem.css('right', iconsRightPos)

                            var xShift = (innerDiv.width())/2;
                            var weblinkElem = weblink.detach();
                            imagePopup.append(weblinkElem);
                            weblinkElem.css({"left": xShift, "bottom": 0});
                        }
                        else {
                            popupContent.css({"top": "50%", "transform": "translate(-50%, -50%)"});
                            var iconsElem = icons.detach();
                            popupContent.append(iconsElem);
                            iconsElem.css('right', 0)

                            var weblinkElem = weblink.detach();
                            popupContent.append(weblinkElem);
                            weblinkElem.css({"left": '50%', 'bottom': 0});
                        }
                    }, 100);
                }

                setTimeout(function () {
                    var image = new Image();
                    image.src = imageToShow;
                    image.onload = function () {
                        popupImage.attr('src', image.src);
                        popupImage.removeClass('blur');
                    };
                    image.onerror = function () {
                        DEBUG && console.log('error in loading image')
                        popupImage.removeClass('blur');
                    };
                }, 10);
            }
        }
    }
}]);
