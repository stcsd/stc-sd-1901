angular.module('stichio').directive('dareFeed', ['$http','APIService','commonService','$rootScope','Upload','$timeout', '$sce', '$ionicPlatform', function($http, APIService, commonService, $rootScope, Upload, $timeout, $sce, $ionicPlatform) {
    return {

        restrict: 'E',
        //transclude: true,
        templateUrl: 'shared/templates/DareFeed.html?0.26',
        link : function(scope, element, attribute, $parent){
            //commonService.setLoggedInStatus();
            if (!$rootScope.userLoggedIn){
                return;
            }
            var flag1=false;
            var videoselected=false;
            console.log("attribute", attribute.popup)
            scope.insidePopup = attribute.popup
            scope.dare = scope.dare || {};
            var colors = ['DDDDDD','FFA18B','D1E8FF','FFD3BD','C8C8FF','FFDDFF','E5E4CA','FFE8C8'];
            var dare_image_w = Math.min(screen.width-12,588)

            scope.ideal_image_iar = 0.7
            scope.ideal_resp_image_iar = 1
            scope.imageboxht = (dare_image_w* scope.ideal_image_iar)+ "px"
            scope.respimageboxht = (dare_image_w* scope.ideal_resp_image_iar)+ "px"

            //console.log(, 'parent')
            if(flag1 == undefined) {
                flag1 = false;
            }



            scope.loggedIn = commonService.loggedIn
            console.log(scope.dare, "scope.dare")
            console.log(scope.card, "scope.card")

            if (scope.card) {
                scope.dare = angular.copy(scope.card);
                if (scope.dare.stitchId || scope.dare.stichId && flag1 == false) {
                    flag1 = true;
                    scope.dare.flag1 = true;
                    console.log('flag1', scope.dare)
                }

                // scope.dare.background_color = '#' + colors[Math.floor(Math.random() * colors.length)]
                //scope.dare.resp_background_color = '#' + colors[Math.floor(Math.random() * colors.length)]
                //scope.dare.height = ($('.dareImageCont').width()*scope.dare.image_iar)-4 + "px"
                //scope.dare.resp_height = ($('.dareResponseContainer').width()*parseFloat(scope.dare.dare_response_iar))-4 + "px"
                //scope.dare.ht = ($('.dareResponseContainer').width() * scope.dare.image_iar)-4
                //scope.dare.resp_ht = ($('.dareResponseContainer').width() *parseFloat(scope.dare.dare_response_iar))-4
                //console.log("scope.dare.resp_ht", scope.dare.resp_ht, scope.dare.dareText)
            }
            scope.dare.autoPlayVideo=false;
            setTimeout(function () {
                if (scope.dare.status>2) {
                    $.screentime({
                        fields: [{
                            selector: '#card-' + scope.dare.id,
                            name: scope.dare.id,
                            media: (scope.dare.response_type === 'video') ? true : false,
                            gaLabel: "P2P: " + scope.dare.id + " " + scope.dare.dareText,
                            userId: commonService.loggedInUserId,
                            type: "dare"
                        }]
                    });
                }
            }, 500)


            if (scope.dare.response_type == "video"){
                scope.dare.videoSrc = angular.copy(scope.dare.response_image)
            }

            scope.progressPercentage = 0

            scope.dare.showPopup = false
            var submitting_dare = false;
            scope.submitDareResponse = function(){
                if (submitting_dare) {
                    return;
                }
                submitting_dare = true;

                // scope.dare.status=3;
                var data = {}

                if (scope.dare.response_type == 'video') {
                    if(scope.dare.videoAspectRatio && scope.dare.videoAspectRatio!= null)
                        data.inverseAspectRatio = scope.dare.videoAspectRatio;


                    var url1 = scope.dare.response_image.replace('https://eb-stichio.s3.amazonaws.com/videos/', '');
                    var fileName = url1.replace('.mp4', '.jpg');
                    scope.dare.video_cover = "https://eb-stichio.s3.amazonaws.com/images/" + fileName;
                    scope.dare.poster= scope.dare.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
                    data.video_cover = "https://eb-stichio.s3.amazonaws.com/images/" + fileName;
                    console.log('video cover added')
                }

                if (scope.dare.response_image_compressed) {
                    scope.dare.showExpandCollapse = true;
                    data.image = scope.dare.response_image_compressed
                }
                else
                    data.response_type =""

                if (scope.dare.response_type)
                    data.response_type = scope.dare.response_type;
                else
                    data.response_type = ""
                data.responseText = scope.dare.response_text
                data.dareId = scope.dare.id
                scope.dare.dare_response_upvote_count=0
                scope.dare.dare_response_downvote_count=0
                scope.dare.dare_response_vote_status=""
                scope.dare.stopAutoPlay=true;
                scope.dare.updated_date = new Date();


                if(scope.dare.response_text && !scope.dare.response_image){
                    var cnf = confirm('Sure, you want to submit without a DARE picture or video?');
                    if(!cnf) {
                        submitting_dare = false;
                        return;
                    }
                }
                else if(!scope.dare.response_text && scope.dare.response_image){
                    var cnf = confirm('Sure, you want to submit without a DARE Comment?');
                    if(!cnf) {
                        submitting_dare = false;
                        return;
                    }
                }

                $(".submitdareresponse").show()

                APIService.submitDareResponse(data).then(function(results){
                    scope.dare.status = 3;
                    scope.dare.answer_text=results.answer_text;
                    scope.dare.answer_image=results.answer_image;
                    scope.dare.dares_completed = results.dares_completed
                    scope.dare.text_background = results.text_background

                    scope.openShareDare('dare',scope.dare,true)
                    scope.dare.response_image_tmp = false;
                    scope.dare.videoSrc=scope.dare.response_image;
                    $(".submitdareresponse").hide()
                    submitting_dare = false;
                    try {
                        if (!commonService.user_rated_app)
                            AppRate.promptForRating(true);

                        if ($rootScope.dareStats) {
                            $rootScope.dareStats.completed_count++;
                            $rootScope.dareStats.remaining_count--;
                        }
                    } catch (e) {
                    }
                    APIService.getUnseenPendingDares(commonService.loggedInUserId).then(function (data) {
                        $rootScope.unseenPendingDares= data.count;
                    });
                })

                if (scope.userDaresStats)
                {
                    if (scope.isOwnProfile) {
                        scope.userDaresStats.completed_count++;
                        scope.userDaresStats.remaining_count--;
                    }
                }



            };
            scope.stars = [1,2,3,4,5]

            scope.rateDare = function(index)
            {

                if (scope.dare.user_type == "darer") {
                    scope.dare.status=4
                    scope.dare.rating = index + 1;
                    APIService.rateDare(scope.dare.id, scope.dare.rating, scope.dare.user1, scope.dare.user2);
                    if (scope.userDaresStats)
                        scope.userDaresStats.pending_review_count--;

                }


            }
            if (scope.dare) {
                console.log("videoUrl:", scope.dare.response_image)
                scope.dare.background_color = '#' + colors[Math.floor(Math.random() * colors.length)]
                scope.dare.resp_background_color = '#' + colors[Math.floor(Math.random() * colors.length)]


                scope.dare.dare_response_iar = parseFloat(scope.dare.dare_response_iar)
                if (scope.dare.response_type==="video") {
                    //For Videos for which inverseAspectRatios have not been calculated.
                    //if (scope.dare.dare_response_iar === 1) {
                    //    scope.dare.dare_response_iar = .7
                    //}
                    //Setting max inverseAspectRatio
                    scope.dare.dare_response_iar = Math.min(parseFloat(scope.dare.dare_response_iar), 1.30);
                }


                scope.dare.height = (dare_image_w*scope.dare.image_iar) + "px"
                scope.dare.resp_height = (dare_image_w*(scope.dare.dare_response_iar ))+ "px"
                scope.dare.ht = (dare_image_w * scope.dare.image_iar)
                scope.dare.resp_ht = (dare_image_w *scope.dare.dare_response_iar)
                console.log("scope.dare.resp_ht", scope.dare.resp_ht, scope.dare.dareText)

                if(scope.dare.response_image)
                    scope.dare.poster= scope.dare.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
                scope.dare.showPic = false
                scope.dare.showExpandCollapse = (scope.dare.status > 2)
            }

            scope.selectSource = function () {
                scope.dare.medium_selection = true;
                $('body').addClass('stop-scrolling');
                setTimeout(function () {
                    var scrollval = $('body').scrollTop();
                    if (scrollval > 0) {
                        $('.main-stiches-cnt .media-selection-popup').css('top', (scrollval - 45) + 'px')
                    }
                }, 100);
                console.log("inside dare feed open media popup")
                $ionicPlatform.registerBackButtonAction(
                    function(){
                        console.log("back from camera popup")
                        scope.closeSelectMedia();
                    }, 100);

                // openPopup('media-selection-popup');
            }

            scope.closeSelectMedia = function () {
                $timeout(function () {
                    scope.dare.medium_selection = false;
                }, 10);

                console.log("inside dare feed close media popup")
                $('body').removeClass('stop-scrolling');
                $rootScope.customBack();
                // closePopup('media-selection-popup');
            }

            scope.selectFile = function () {
                commonService.selectMedia(successCb, errorCb, progressCb);
            };

            scope.captureImage = function () {
                commonService.captureImage(successCb, errorCb, progressCb);
            };

            scope.captureVideo = function () {
                commonService.captureVideo(successCb, errorCb, progressCb);
            };

            function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
                resetLoadingOptions();
                scope.dare.response_type = fileType;
                scope.dare.sources = [ {src: $sce.trustAsResourceUrl(remoteUrl), type: "video/mp4"} ];
                scope.dare.response_image = remoteUrl;
                scope.dare.poster= scope.dare.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
                scope.dare.response_image_full = remoteUrl;
                scope.dare.response_image_compressed = remoteUrl;
                scope.dare.updated_date = new Date();
                scope.dare.videoSrc=remoteUrl
                if (fileType == "video"){
                    scope.dare.videoAspectRatio = videoInverseAspectRatio;
                    scope.dare.dare_response_iar = Math.min(videoInverseAspectRatio, 0.8);
                    scope.dare.resp_height = (dare_image_w*(scope.dare.dare_response_iar )) + "px"
                }
                scope.dare.poster= scope.dare.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
            }

            function errorCb(data) {
                resetLoadingOptions();
            }

            function progressCb(data) {
                if (!videoselected) {
                    videoselected=true;
                    scope.$apply(function () {
                        scope.dare.loading = data;
                    })
                }
                else{
                    scope.dare.loading = data;
                }
                scope.dare.response_image = null;
                scope.dare.response_image_full = null;
                scope.dare.response_image_compressed = null;
            }

            function resetLoadingOptions() {
                scope.dare.loading = {
                    status: false,
                    progress: 0,
                    message: ''
                };
                videoselected=false;
            };


            //scope.dare.dare_text = "I know that my directive is being used because I see 'color is: ' However, I can't figure out how to make it display 'blue' or 'yellow' at the end of that console statement. I want to get that value so I can do a conditional processing. How do I get the value of the attribute?"
            //scope.dare.image = "http://eb-stichio.s3.amazonaws.com/thumbnails/w466/20160323212102380683_stich.jpg"
            scope.likeDare = function(){
                if (!scope.dare.like_status) {
                    APIService.likeDare(scope.dare.id);
                    scope.dare.likes_count++;
                }
                else {
                    APIService.unlikeDare(scope.dare.id);
                    scope.dare.likes_count--;

                }
                scope.dare.like_status = !scope.dare.like_status
            };

            scope.acceptDare = function(){
                scope.dare.status =1
                APIService.acceptDare(scope.dare.id, scope.dare.user1, scope.dare.user2);
            };

            scope.rejectDare = function(dare){
                var response = confirm("Are you sure you want to reject this DARE?");
                if(response == true){
                    scope.dare.status =2
                    APIService.rejectDare(scope.dare.id,scope.dare.user1, scope.dare.user2)
                }
            };

            scope.unlikedDare = function(StitchId, stitchIndex, event){
                event.stopPropagation();
                scope.stiches[stitchIndex].like_status = false;
                scope.stiches[stitchIndex].imageLikes--;
                if(commonService.loggedInUserId == APIService.profileUserId)
                    scope.parentController.decrementLikeCount()
                APIService.unlikeStich(StitchId).then(function(){
                    //alert("Stich Unliked");
                });
            };

            scope.toggleUpvoteResponse = function () {
                if (scope.dare.dare_response_vote_status == 'upvote') {
                    unupvote_response();
                    if(scope.insidePopup && scope.passedDare){
                        scope.passedDare.upvoted=false;
                    }
                } else {
                    upvote_response();
                    if(scope.insidePopup && scope.passedDare){
                        scope.passedDare.upvoted=true;
                    }
                }

            }


            function upvote_response () {

                if (scope.dare.dare_response_vote_status != 'upvote')
                {
                    var data={}
                    data.vote_type = 'up'
                    data.dareResponseId = scope.dare.dareResponseId
                    APIService.voteDareResponse(data, scope.dare.id, scope.dare.user1, scope.dare.user2,scope.dare.dare_response_upvote_count)
                    scope.dare.dare_response_vote_status = 'upvote'
                    scope.dare.dare_response_upvote_count++;
                }

            }

            function unupvote_response () {
                var data={}
                data.vote_type = 'cancel'
                data.dareResponseId = scope.dare.dareResponseId
                APIService.voteDareResponse(data, scope.dare.id,scope.dare.user1, scope.dare.user2,scope.dare.dare_response_upvote_count)
                scope.dare.dare_response_vote_status = 'None'
                scope.dare.dare_response_upvote_count--;
            }

            scope.downvote_response = function (truth) {
                if(scope.dare.dare_response_vote_status != 'downvote') {
                    if (scope.dare.dare_response_vote_status == 'upvote') {
                        scope.dare.dare_response_upvote_count--;
                    }
                    scope.dare.dare_response_vote_status = 'downvote'
                    scope.dare.dare_response_downvote_count++;
                }
                var data={}
                data.vote_type = 'down'
                data.dareResponseId = scope.dare.dareResponseId
                APIService.voteDareResponse(data, scope.dare.id)

            }

            scope.undownvote_response = function (truth) {
                scope.dare.dare_response_vote_status = 'None'
                scope.dare.dare_response_downvote_count--;
                var data={}
                data.vote_type = 'cancel'
                data.dareResponseId = scope.dare.dareResponseId
                APIService.voteDareResponse(data, scope.dare.id)
            }
            //scope.unHideDare= function(){
            //    scope.dare.hidden=false;
            //    APIService.hideDare(scope.dare.id, false)
            //}
        }
    }
}]);