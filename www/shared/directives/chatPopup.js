/**
 * Created by Noel Varghese on 6/9/2016.
 */
angular.module('stichio').directive('chatPopup', [ '$rootScope','APIService','$http','Upload', '$timeout','$interval','$cookies', 'commonService', 'loginService', '$state', function($rootScope, APIService, $http, Upload, $timeout, $interval, $cookies, commonService, loginService, $state){
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/chat.html?0.09',
        link: function (scope, element, attribute) {


            $timeout(function () {
                //commonService.setLoggedInStatus();
                if (!$rootScope.userLoggedIn){
                    return;
                }
                var socket = commonService.socket;
                var last_notif_time = 0;
                var last_notifs_visit_time;
                var topLoadingIcon, bottomLoadingIcon;
                scope.selectedOptChat='alerts'

                commonService.contactUs = function () {
                    $rootScope.success_msg = false;
                    commonService.openPopup('stichio-form');
                    $rootScope.closeChatPopup();
                    var el = $('.contact-us-form-wrapper');
                    el.focus();
                    $('#stichioform input, #stichioform textarea').html('');
                }
                scope.contactUs = commonService.contactUs

                //console.log("socketttt", socket)
                //socket = io.connect('htttp://52.35.106.118:80/chat');
                if(socket) {
                    socket.on('logout', function () {
                        console.log('LOGGING OUT USER');
                        $('.logout-popup').addClass("target");
                        loginService.logout().then(function () {
                            localStorage.setItem("credChanged", true);
                            window.location.href = "";
                        });
                    });

                    socket.on('new_conversation_id', function (data) {
                        DEBUG && console.log('new conversationId arrived');
                        scope.userIdOfNewConvo = data.other_user_id;
                        scope.convoIdOfNewCOnvo = data.conversationId;
                        scope.newConvoArrived = true;
                    });


                    $rootScope.newActivityCount = 0;
                    var tempConvoId = '';
                    // socket.on('msg_to_room', function (data) {
                    //     scope.timenow = new Date().getTime();
                    //     var chat = {
                    //         'conversationId': data.conversationId,
                    //         'created_date': '',
                    //         'deleted_by': null,
                    //         'is_other_user': true,
                    //         'message': data.message,
                    //         'message_id': '',
                    //         'timestamp': data.timestamp,
                    //         'unread': true,
                    //         'userId': data.from_user_id,
                    //         'image_url': data.image_url,
                    //         'url': data.url
                    //     };
                    //
                    //     var from_user_exists = false;
                    //     if (chat.conversationId != tempConvoId) {
                    //         $rootScope.newActivityCount++;
                    //         tempConvoId = chat.conversationId;
                    //     }
                    //     $rootScope.unread_chats_alert = true
                    //     for (var index in scope.conversationsOld) {
                    //         if (scope.conversationsOld[index].other_user.userId == data.from_user_id) {   // message from existing user in convo
                    //             from_user_exists = true;
                    //             if (data.is_new)
                    //                 scope.conversationsOld[index].conversationId = data.conversationId;
                    //             scope.conversationsOld[index].messages.push(chat);
                    //             if (data.conversationId != scope.active_chat.conversationId) {
                    //                 scope.conversationsOld[index].unread = true;
                    //                 socket.emit(
                    //                     'read_conversation',
                    //                     $cookies.get("sti_authenticationToken"),
                    //                     data.conversationId
                    //                 );
                    //
                    //             }
                    //             else {
                    //                 $rootScope.unread_chats_alert = false
                    //             }
                    //             $rootScope.newMessage = true;
                    //
                    //             break;
                    //         }
                    //     }
                    //     if (!from_user_exists) {
                    //         $rootScope.newActivityCount++;
                    //         var conversation = {
                    //             'conversationId': data.conversationId,
                    //             'created_date': '',
                    //             'messages': [],
                    //             'other_user': {
                    //                 'avatar': data.avatar,
                    //                 'userId': data.from_user_id,
                    //                 'username': data.from_username
                    //             },
                    //             'unread': true,
                    //             'updated_date': '',
                    //             'user_one': '',
                    //             'user_two': '',
                    //             'index': scope.conversationsOld.length
                    //         };
                    //         $rootScope.newMessage = true;
                    //         //$rootScope.unread_chats_alert = true
                    //         conversation.messages.push(chat);
                    //         scope.conversationsOld.push(conversation);
                    //     }
                    //
                    //     scope.firstmessage = false;
                    //     scope.$apply('chat-main-chat');
                    //     $(".chat-window-body").animate({scrollTop: $(".chat-window-body").prop("scrollHeight")}, "slow");
                    // });

                    //detecting notification
                    socket.on('notification', function (data) {
                        $rootScope.newActivityCount++;
                        scope.tempArray = [];
                        scope.timenow = new Date().getTime();
                        console.log(data);
                        console.log("NotifArrived");
                        var notif = data;
                        notif.timestamp = scope.timenow;
                        notif.link = get_link(notif);
                        scope.tempArray.push(notif);
                        if (notif.action == 'send_contact_request' || notif.action == 'add_collaborator') {
                            scope.requests = scope.tempArray.concat(scope.requests);
                            $rootScope.newContactRequest = true;
                        }
                        else {
                            scope.notifications = scope.tempArray.concat(scope.notifications);
                        }
                        scope.newNotifications = true;
                        scope.allUnread = false;
                    });


                    //details of the active chat
                    scope.active_chat = {};
                    scope.user_details = {};
                    scope.conversationsOld = [];
                    scope.notifications = [];
                    scope.requests = [];
                    scope.search_sticher_value = '';
                    scope.newNotifications = false;
                    $rootScope.newContactRequest = false;
                    $rootScope.newMessage = false;
                    scope.allUnread = false;
                    scope.searchcount = 0;
                    scope.old_data = {"query": 10};
                    scope.max_result_length = 10;

                    // todo: avoid calling api for logged user again
                    DEBUG && console.log("userDetails");
                    if (!$rootScope.userLoggedInDetails) {
                        APIService.getLoggedInUserDetails().then(function (data) {
                            scope.user_details = data;
                        });
                    } else {
                        scope.user_details=$rootScope.userLoggedInDetails
                    }

                    var sug_index = 0,
                        sug_count = 10,
                        sug_flag = false,
                        notificationWindow = $('.notifications');

                    setTimeout(function() {
                        notificationWindow.on('scroll.notificationScroll', function () {
                            if (sug_flag && ($(this).scrollTop() > ($(this)[0].scrollHeight - APIService.scrollOffset))) {
                                sug_flag = false;
                                $rootScope.loadNotifications();
                            }
                        });
                    }, 100);
                    $rootScope.loadNotifications = function (reset) {
                        topLoadingIcon=$("#top-loading-notif");
                        bottomLoadingIcon=$("#bottom-loading-notif");
                        var old_notifs=scope.notifications
                        var old_sug_index=sug_index;
                        var old_sug_count=sug_count;

                        if (reset) {

                            sug_index=0;
                            sug_count=10;
                            if(commonService.checkOnline()) {
                                if (old_notifs.length > 0) {
                                    topLoadingIcon.show();
                                }
                                else {
                                    bottomLoadingIcon.show();
                                }
                            }
                        }
                        else {
                            bottomLoadingIcon.show();
                        }

                        APIService.getNotificationHistory(sug_index, sug_count).then(function (data) {
                            try {
                                topLoadingIcon.hide();
                                bottomLoadingIcon.hide();
                            } catch (e) {
                            }
                            var flag = 0;
                            console.log("noti", data);
                            if (reset){
                                scope.notifications=[]
                            }
                            for (var noti in data) {
                                data[noti].timestamp = (new Date(data[noti].updated_date)).getTime();
                                data[noti].link = get_link(data[noti]);
                                data[noti].image_url = data[noti].image_url || data[noti].image2
                                if (data[noti].unread == true) {
                                    flag = 1;
                                    scope.allUnread = false;
                                }
                                scope.notifications.push(data[noti])

                            }
                            if (sug_index==0 && scope.notifications.length > 0) {
                                if (APIService.loggedInUserDetailsGlobal) {
                                    if (sug_index==0) {
                                        last_notifs_visit_time = (new Date(APIService.loggedInUserDetailsGlobal.notifs_visit_time)).getTime()
                                        last_notif_time = scope.notifications[0].timestamp
                                        console.log("last_notif_time", last_notif_time, last_notifs_visit_time)
                                        if (last_notif_time > last_notifs_visit_time) {
                                            //    $rootScope.unread_notifs_alert = true

                                            try {
                                                if (scope.notifications[0].from_users[0]) {
                                                    $rootScope.unread_notif_pic = scope.notifications[0].from_users[0].avatar
                                                }
                                                else {
                                                    $rootScope.unread_notif_pic = 'images/hot_dares_green.png'
                                                }
                                            } catch (e) {
                                                console.log(e)
                                            }
                                        }
                                    }
                                }
                            }
                            if (flag == 0 && data.length > 0) {
                                scope.allUnread = true;
                            }
                            setTimeout(function() {
                                sug_index += sug_count;
                                sug_flag = true;
                                if(data.length === 0) { notificationWindow.off('scroll.notificationScroll'); }
                            }, 0)
                        }), function(e){
                            scope.notifications=old_notifs;
                            sug_index=old_sug_index;
                            sug_count=old_sug_count;
                            try {
                                topLoadingIcon.hide();
                                bottomLoadingIcon.hide();
                            } catch (e) {
                            }
                        };
                    };
                    $rootScope.loadNotifications(true);

                    var get_link = function (data) {
                        if (data.element_type) {
                            switch (data.element_type) {
                                case 'dare':
                                    if (data.dare_info) {
                                        return '#/profile/' + data.dare_info.user2 + '?dareId=' + data.element_id
                                    }
                                    else  return ""
                                case 'taken_dare':
                                    return '#/profile/' + data.dare_info.userId + '?adminDareId=' + data.element_id
                                case 'admin_dare':
                                    return '#/dares/' + data.element_id;
                                case 'user':
                                    return '#/profile/' + data.element_id;

                            }
                        }
                        else{
                            if (data.action == "notificaton_from_utility"){
                                return data.push_notification_link
                            }
                        }
                    };
                    scope.openNotif = function(notif){
                        try {
                            if (notif.element_type) {
                                switch (notif.element_type) {
                                    case 'dare':
                                       $rootScope.openDarePopup(null, false, true, false, notif.element_id, true);
                                       break;
                                    case 'taken_dare':
                                        $rootScope.openAdminDarePopup(null, false, true, false, notif.element_id, true)
                                        break;
                                    case 'admin_dare':
                                        $rootScope.openDareResponsesPopup(null, notif.element_id);
                                        $rootScope.closeAdminDarePopup();
                                        //$state.go('dare', {dareSuggestionId: notif.element_id}, {notify: true, reload: true});
                                        break;
                                    case 'user':
                                         $state.go('profile.dares', {profileId: notif.element_id}, {notify: true, reload: true});
                                        break;

                                }
                            }
                            else if (notif.push_notification_link) {
                                var link=notif.push_notification_link
                                commonService.openLink(link)
                            }
                        } catch (e) {
                            console.error(e)
                        }
                    }
                    scope.displayListInNotifs = function (notifobj, event) {
                        if (event) {
                            event.stopPropagation();
                            event.preventDefault();
                        }
                        DEBUG && console.log("notifs list opnd");
                        scope.notiflist = notifobj.from_users;
                        $('.notiflist').hide();
                        $('.chat-nav-cnt').hide();
                        $('.nav-close').hide();
                        $('.notif-close').show();
                        $('.notifnameslist-header').show();

                        $('.notifnameslist').show();

                    }

                    //scope.closeNotifListPopup = function () {
                    //    $('.notiflist').show();
                    //    $('.chat-nav-cnt').show();
                    //    $('.nav-close').show();
                    //    $('.notif-close').hide();
                    //    $('.notifnameslist-header').hide();
                    //
                    //    $('.notifnameslist').hide();
                    //}

                    $('.dual-notif-pic').click(function () {
                        console.log("noti opnd");
                    })




                    //setting ellipsis initially
                    setTimeout(function () {
                        var showChar = 60;
                        var ellipsestext = "...";
                        $('.chat-ellipsis').each(function () {
                            var content = $(this).html();
                            DEBUG && console.log("converted");
                            if (content.length > showChar) {

                                var c = content.substr(0, showChar);
                                var html = c + ellipsestext;
                                $(this).html(html);
                            }
                        });
                    }, 1000)

                    //setting ellipsis inidvusually
                    scope.setEllipsis = function (convoId) {
                        var showChar = 60;
                        var ellipsestext = "...";
                        var content = $('.' + convoId).html();
                        if (content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var html = c + ellipsestext;
                            $('.' + convoId).html(html);
                        }
                    }


                    // todo: change $index to conversation.index or something in chatpopup.html
                    // todo: api for more conversations, when calling it add index to conversation
                    // todo: when adding conversation during start_new_chat add index to it also

                    scope.itemOnLongPressOnImage = function (timeStampClass) {
                        console.log('Long press');
                        $('.' + timeStampClass).addClass("redbg");
                        $('.' + timeStampClass + 'img').addClass("showCross");
                    }
                    scope.selectedOptChat='alerts'

                    scope.showNotifTimeStamp = function (index) {
                        if (index > 0) {
                            var currentDateObject = {
                                "date": (new Date(scope.notifications[index].timestamp)).getDate(),
                                "month": (new Date(scope.notifications[index].timestamp)).getMonth(),
                                "year": (new Date(scope.notifications[index].timestamp)).getFullYear()
                            }

                            var prevDateObject = {
                                "date": (new Date(scope.notifications[index - 1].timestamp)).getDate(),
                                "month": (new Date(scope.notifications[index - 1].timestamp)).getMonth(),
                                "year": (new Date(scope.notifications[index - 1].timestamp)).getFullYear()
                            }
                        }

                        if (index == 0) {
                            return true;
                        }
                        else {
                            if (currentDateObject.date == prevDateObject.date && currentDateObject.month == prevDateObject.month && currentDateObject.year == prevDateObject.year) {
                                return false;
                            }
                            else {
                                return true;
                            }
                        }

                    }


                    scope.readNotification = function (notifId, index) {
                        if ($rootScope.newActivityCount > 0) {
                            $rootScope.newActivityCount--;
                        }
                        var flag = 0;
                        DEBUG && console.log(notifId);
                        scope.notifications[index].unread = false;
                        socket.emit(
                            'read_notification',
                            $cookies.get("sti_authenticationToken"),
                            notifId
                        );
                        for (var noti in scope.notifications) {
                            if (scope.notifications[noti].unread == true) {
                                flag = 1;
                                scope.allUnread = false;
                            }
                        }
                        if (flag == 0) {
                            scope.allUnread = true;
                        }
                    }

                    $(".mark-all-read-notif").click(function () {
                        for (var notif in scope.notifications) {
                            scope.notifications[notif].unread = false;
                            if ($rootScope.newActivityCount > 0) {
                                $rootScope.newActivityCount--;
                            }
                        }
                        scope.newNotifications = false;
                        scope.allUnread = true;
                        //$rootScope.unread_notifs_alert = false
                        $rootScope.unread_notif_pic = null;
                        socket.emit(
                            'read_notification',
                            $cookies.get("sti_authenticationToken"),
                            "",
                            true
                        );
                    })

                    //opening chat popup(rootScope as it should be accessible from anywhere
                    $rootScope.openChatPopup = function (event, openChat) {
                        if (event) {
                            event.stopPropagation();
                            event.preventDefault();
                        }
                        $rootScope.newNotification = false;
                        $('.nav1').show();
                        if (!openChat) {
                            socket.emit('view_notifs', $cookies.get("sti_authenticationToken"));
                            //$rootScope.unread_notifs_alert = false
                            $rootScope.unread_notif_pic = null;
                            last_notifs_visit_time = (new Date()).getTime()
                        }
                        //try {
                        //    if (commonService.currentPlayingVideo) {
                        //        commonService.currentPlayingVideo.pause()
                        //    }
                        //} catch (e) {
                        //}
                        $('.nav2').hide();
                        $('.nav3').hide();
                        $('.chat-window').hide();
                        $('.more-popup').hide();
                        console.log("hiding body scroll");
                        commonService.openPopup('chat-popup');

                        $('.notifnameslist').hide();
                        $('.frames-wrapper').css('display', 'none');
                        var el = $('.notifications');
                        el.focus();
                        el.keydown(function (e) {
                            if(e.keyCode == 27){
                                $rootScope.closeChatPopup();
                            }
                        });
                    }

                    //closing chat popup
                    $rootScope.closeChatPopup = function () {
                        try {
                            scope.closeConversation();
                        } catch (e) {
                        }
                        commonService.closePopup('chat-popup');
                        $rootScope.unread_notif_pic=null;
                        //$('.main-stiches-cnt').show();
                        $('body').removeClass('fix-position');

                    }

                    //setting the default selected nav
                    $('.nav2').hide();
                    $('.nav3').hide();
                    $('.chat-window').hide();

                    $('.chat-alerts').click(function () {
                        scope.selectedOptChat='alerts'
                        $('.nav2').hide();
                        $('.nav1').show();
                        socket.emit('view_notifs', $cookies.get("sti_authenticationToken"));
                        //$rootScope.unread_notifs_alert = false
                        $rootScope.unread_notif_pic = null;
                        last_notifs_visit_time = (new Date()).getTime()
                        $('.nav3').hide();
                        $('.chat-window').hide();
                        scope.newNotifications = false;
                        $('.mark-all-read').show();

                    })
                    var conversations_fetched="";

                    //calculate width of textarea

                    //hide the delete-full-chat when chat input is active


                    //retrieve chat message history

                    //retrieve notification history
                    // $('.notifications').on('scroll', function () {
                    //     if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    //         DEBUG && console.log("reached bottom");
                    //         scope.retrieveNotificationHistory();
                    //     }
                    // })

                    // scope.retrieveNotificationHistory = function () {
                    //     //call api
                    //     indexToRetrieveHistory = scope.conversationsOld.length;

                    //     APIService.getNotificationHistory(indexToRetrieveHistory, 10).then(function (data) {
                    //         console.log(data);
                    //         $timeout(function () {
                    //             scope.conversationsOld = scope.conversationsOld.concat(data);
                    //         }, 1000);

                    //         DEBUG && console.log(scope.conversationsOld);
                    //     })
                    //     //scope.$apply();
                    // }
                }
            }, 5000);

        }//link function
    }//return function

}]);//directive function
