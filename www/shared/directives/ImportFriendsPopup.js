angular.module('stichio').directive('importFriendsPopup',['APIService','commonService', '$http', '$window', function (APIService,commonService, $http, $window) {

    return {
        restrict: 'E',
        templateUrl: 'shared/templates/ImportFriendsPopup.html?0.01',
        link: function (scope, element, attribute) {

            scope.searchFriends = '';
            scope.add_count = 0;
            scope.req_count = 0;
            scope.invite_count = 0;
            scope.friend_count = 0;
            var inviteList = [];
            scope.friendList = [];

            scope.gp_friends = [];
            scope.fb_reg_friends = [];
            scope.invite_stage = 1;
            scope.contacts_retrieved = false;

            scope.stage0 = function() {
                scope.invite_stage = 0;
                // filter_contacts(scope.fb_friends.contacts_list_registered);
                $('.import-friends-search').attr('placeholder', 'Search Facebook Friends on STICHIO');
                $('.invite-user-list').css('height', 'calc(100% - 173px)');
                scope.friendList = scope.fb_friends.contacts_list_registered;
                stateChange();
                console.log('stage0', scope.friendList);
            }

            scope.stage1 = function() {
                scope.invite_stage = 1;
                // filter_contacts(scope.gp_friends.contacts_list_registered);
                $('.import-friends-search').attr('placeholder', 'Search G+ Friends on STICHIO');
                $('.invite-user-list').css('height', 'calc(100% - 213px)');
                scope.friendList = scope.gp_friends.contacts_list_registered;
                stateChange();
                console.log('stage1', scope.friendList);
            }

            scope.stage2 = function() {
                scope.invite_stage = 2;
                // filter_contacts(scope.gp_friends.contacts_list_not_registered);
                $('.import-friends-search').attr('placeholder', 'Search G+ Friends to Invite');
                $('.invite-user-list').css('height', 'calc(100% - 250px)');
                scope.friendList = scope.gp_friends.contacts_list_not_registered;
                stateChange();
                console.log('stage2', scope.friendList);
            }

            scope.stage3 = function() {
                scope.invite_stage = 3;
                scope.friendList = [];
                scope.searchFriends = '';
            }

            scope.clearSearch = function () {
                scope.searchFriends = '';
            };

            var stateChange = function () {
                scope.searchFriends = '';
                scope.contacts_retrieved = true;
                scope.friend_count = scope.friendList.length;
                scope.add_all();
            };

            // var filter_contacts = function(contacts) {
            // scope.friendList = [];
            // for(var i in contacts){
            //     if((scope.invite_stage == 0 || scope.invite_stage == 1) && contacts[i].status == 0) { //
            //         contacts[i].invited = false;
            //         scope.friendList.push(contacts[i]);
            //     }
            //     else if(scope.invite_stage == 2 && !contacts[i].invited) {
            //         scope.friendList.push(contacts[i]);
            //     }
            // }
            // scope.friendList = contacts;
            // scope.contacts_retrieved = true;
            // scope.friend_count = scope.friendList.length;
            // scope.add_all();
            // console.log('filtered_contacts', scope.friendList);
            // }

            var get_inviteList = function() {
                inviteList = [];
                if(scope.invite_stage == 0 || scope.invite_stage == 1){
                    for(var i in scope.friendList){
                        if(scope.friendList[i].added && scope.friendList[i].status == 0) { //
                            inviteList.push(scope.friendList[i].userId);
                            // scope.friendList[i].added = false;
                            scope.friendList[i].invited = true;
                            scope.friendList[i].status = 1;
                        }
                    }
                }
                else if(scope.invite_stage == 2){
                    for(var i in scope.friendList){
                        if(scope.friendList[i].added && !scope.friendList[i].invited) {
                            inviteList.push(scope.friendList[i].email);
                            // scope.friendList[i].added = false;
                            scope.friendList[i].invited = true;
                        }
                    }
                }
                console.log(inviteList);
            }

            var checkbox_status = function() {
                if(scope.friend_count == scope.add_count){
                    if(scope.friend_count == 0) {
                        scope.selectAll = false;
                    }
                    else {
                        scope.selectAll = true;
                    }
                }
                else {
                    scope.selectAll = false;
                }
                console.log(scope.add_count, scope.friend_count, scope.selectAll)
            }

            scope.add_all = function () {
                scope.add_count = 0;
                angular.forEach(scope.friendList, function(fr){
                    if (scope.invite_stage != 2 && fr.status == 0) {
                        fr.added = true;
                        scope.add_count++;
                    }
                    else if (scope.invite_stage == 2 && !fr.invited) {
                        fr.added = true;
                        scope.add_count++;
                    }
                });
                if (scope.add_count) {
                    scope.selectAll = true;
                }
                else {
                    scope.selectAll = false;
                }
                scope.friend_count = scope.add_count;
                console.log('add_all', scope.add_count, scope.selectAll);
            }

            scope.remove_all = function () {
                scope.selectAll = false;
                scope.add_count = 0;
                for(var i in scope.friendList){
                    scope.friendList[i].added = false;
                }
                console.log('remove_all', scope.friendList);
            }

            scope.add_to_invite = function (user) {
                user.added = true;
                scope.add_count++;
                checkbox_status();
                console.log(user.name + ' added to list!');
                console.log(scope.friendList);
                // console.log(scope.friendList);
            }

            scope.remove_from_invite = function(user) {
                user.added = false;
                scope.add_count--;
                checkbox_status();
                console.log(user.name + ' removed from list!');
                console.log(scope.friendList);
                // console.log(scope.friendList);
            }

            scope.send_invite = function (user) {
                var list = [];
                if(scope.invite_stage == 0 || scope.invite_stage == 1) {
                    list.push(user.userId);
                    APIService.followUser({ "followeeIdList" :  list });
                    user.invited = true;
                    user.status = 1;
                    scope.req_count++;
                    console.log(user.name + ' added! ', scope.req_count, 'req_count');
                }
                else if(scope.invite_stage == 2) {
                    list.push(user.email);
                    var invites = {emails: list};
                    APIService.sendEmailInvite(invites);
                    user.invited = true;
                    scope.invite_count++;
                    console.log(user.name + ' invited! ', scope.invite_count, 'invite_count');
                }
                if(user.added) {
                    user.added = false;
                    scope.add_count--;
                }
                scope.friend_count--;
                checkbox_status();
                console.log(list);
            }

            scope.send_invites = function () {
                get_inviteList();
                if(scope.invite_stage == 0 || scope.invite_stage == 1) {
                    APIService.followUser({ "followeeIdList" :  inviteList });
                    scope.req_count += inviteList.length;
                    console.log(scope.req_count + ' friends added!');
                    if (scope.invite_stage == 0) { scope.stage3(); }
                    else { scope.stage2(); }
                }
                else if(scope.invite_stage == 2) {
                    var invites = {emails: inviteList};
                    APIService.sendEmailInvite(invites);
                    scope.invite_count += inviteList.length;
                    console.log(scope.invite_count + ' friends invited!');
                    scope.stage3();
                }
                // checkbox_status();
                console.log(inviteList);
            }

            scope.close_import_friends = function (event) {
                //$('.noselect').css('overflow', 'auto');
                scope.friendList = [];
                commonService.closePopup('import-friends-popup')
                scope.openSearchContacts(event)
            }

            scope.get_contacts = function (from,event, from_place) {
                if(from_place){
                    commonService.analytics('event', {category : 'Friends', action : from, label : from_place});

                }
                else {
                    commonService.analytics('event', {category : 'Friends', action : from, label : 'Friends Popup'});
                }
                event.stopPropagation()
                scope.searchFriends = '';
                scope.friendList = [];
                scope.fb_friends = {};
                scope.gp_friends = {};
                scope.req_count = 0;
                scope.add_count = 0;
                scope.invite_count = 0;
                scope.selectAll = false;
                scope.contacts_retrieved = false;


                commonService.closePopup('search-contacts-popup');
                commonService.openPopup('import-friends-popup');
                //$('.noselect').css('overflow', 'hidden');
                $('.friends-loading-icon').css('display','block');
                $('.fb-icon').removeClass('selected-icon');
                $('.google-plus-icon').removeClass('selected-icon');
                scope.navFrom = from;

                if(scope.navFrom == 'facebook') {
                    scope.invite_stage = 0;
                    $('.fb-icon').addClass('selected-icon')
                    // $('.invite-user-list').css('max-height', 'calc(100% - 194px)')
                    $('.import-friends-search').attr('placeholder', 'Search Facebook Friends on STICHIO');
                    APIService.getFacebookFriends().then(function (result) {
                        if (result.error) {
                            var fbLoginError = function(error){
                                console.log('fbLoginError', error);
                                //$ionicLoading.hide();
                            };

                            //$cordovaOauth.facebook("126738297428696", ["email"]).then(function(success) {
                            var fbLoginSuccess = function(success) {
                                if (!success.authResponse){
                                    fbLoginError("Cannot find the authResponse");
                                    return;
                                };
                                console.log(JSON.stringify(success));
                                console.log('in success in login bk fb', success);
                                var token = "Token " + success.authResponse.accessToken;
                                var loginPromise = $http({
                                    method: 'GET',
                                    url: 'https://graph.facebook.com/me?access_token=' + success.authResponse.accessToken,
                                });

                                // loginService.loginUser(loginPromise);
                                loginPromise.success(function (result) {
                                    APIService.getFacebookFriends(result.id, result.email).then(function(result){
                                        $('.friends-loading-icon').css('display', ' none')
                                        scope.fb_friends = result
                                        scope.stage0();
                                        console.log('facebook', result)
                                    })
                                    //loginUser(result, true, result.interests_filled);
                                });
                            }
                            facebookConnectPlugin.login(['email', 'public_profile','user_friends'], fbLoginSuccess, fbLoginError);
                            //OAuth.popup('facebook', function (error, success) {
                            //    if (error) {
                            //        console.log('error in login bk fb', error);
                            //    }
                            //    else {
                            //        console.log('in success in login bk fb', success);
                            //        var token = "Token " + success.access_token;
                            //
                            //        var loginPromise = $http({
                            //            method: 'GET',
                            //            url: 'https://graph.facebook.com/me?access_token=' + success.access_token,
                            //        });
                            //
                            //        // loginService.loginUser(loginPromise);
                            //        loginPromise.success(function (result) {
                            //            APIService.getFacebookFriends(result.id, result.email).then(function(result){
                            //                $('.friends-loading-icon').css('display', ' none')
                            //                scope.fb_friends = result
                            //                scope.stage0();
                            //                console.log('facebook', result)
                            //            })
                            //            //loginUser(result, true, result.interests_filled);
                            //        });
                            //    }
                            //});
                        }
                        else {
                            $('.friends-loading-icon').css('display', ' none')
                            scope.fb_friends = result
                            scope.stage0();
                            console.log('facebook', result)
                        }
                    });
                }

                else{
                    scope.invite_stage = 1;
                    $('.google-plus-icon').addClass('selected-icon')
                    // $('.invite-user-list').css('max-height','calc(100% - 217px)')
                    $('.import-friends-search').attr('placeholder', 'Search G+ Friends on STICHIO');
                    function getGoogleContacts(){
                        window.plugins.googleplus.login({
                            'scopes' : 'email profile https://www.google.com/m8/feeds',
                            'webClientId': '298658191196-fo4n68kch0lrinuklovrkdbvdb9bb40q.apps.googleusercontent.com',
                            'offline': true,
                        }, function (success) {
                            if (!success) {
                                console.log('error in login bk g+', error);
                            }
                            else {
                                console.log('success in login bk g+', success);
                                var loginPromise;
                                if (success.refresh_token) {
                                    console.log('success in login bk g+', success);
                                    console.log("successss", success)
                                    var token = "Token " + success.access_token;
                                    var loginPromise;
                                    if (success.refresh_token) {
                                        var refresh_token = success.refresh_token;
                                        window.localStorage.setItem('grt', true);
                                        APIService.getGoogleFriends(refresh_token).then(function (result) {
                                            $('.friends-loading-icon').css('display',' none')
                                            scope.gp_friends = result
                                            if(scope.invite_stage === 1) {
                                                scope.stage1();
                                            }
                                            else {
                                                scope.stage2();
                                            }
                                            console.log('google', result)
                                        })
                                    }
                                }
                                else if (success.serverAuthCode){
                                    var serverAuthCode = success.serverAuthCode;
                                    APIService.getGoogleFriends(serverAuthCode, true).then(function (result) {
                                        $('.friends-loading-icon').css('display',' none')
                                        scope.gp_friends = result
                                        if(scope.invite_stage === 1) {
                                            scope.stage1();
                                        }
                                        else {
                                            scope.stage2();
                                        }
                                        console.log('google', result)
                                    })

                                }
                            }
                        },function(error) {
                            console.log(error);
                        });
                    }
                    APIService.getGoogleFriends().then(function (result) {
                        if (result.error ){
                            getGoogleContacts();
                        }
                        else {
                            $('.friends-loading-icon').css('display', ' none')
                            scope.gp_friends = result
                            if(scope.invite_stage === 1) {
                                scope.stage1();
                            }
                            else {
                                scope.stage2();
                            }
                            console.log('google', result)
                        }

                    },function () {
                            getGoogleContacts();
                    })
                }
            }
        }
    }
}]);