angular.module('stichio').directive('referralPopup',['$timeout', '$rootScope','commonService', 'APIService','$cookies', function($timeout, $rootScope, commonService,  APIService, $cookies) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/referralPopup.html?0.01',
        link: function (scope, element, attribute) {
            commonService.setLoggedInStatus();
            if (!$rootScope.userLoggedIn){
                return;
            }
            scope.referral_data={}
            scope.openReferralPopup = function () {
                // event.stopPropagation()
                scope.enterReferral=$rootScope.enterReferralCode
                // scope.enterReferral=true
                commonService.openPopup('referral-popup')
                $timeout(function(){
                    $("#referral_form").show()
                    $("#referral_phone_conf").hide()
            }, 100);

                console.log('instance created')
            }
            $rootScope.closeReferralForm = function () {
                commonService.closePopup('referral-popup');
                scope.referral_data={}
                // $('body').removeClass('prevent-scroll')
            }
            scope.showConfirmMessage = function () {
                $("#referral_form").hide()
                $("#referral_phone_conf").show()
            }
            scope.delayCloseDareMenu = function(){
                setTimeout(function(){
                    scope.closeDareMenu();
                }, 500)
            };
            scope.backToReferralForm = function(){
                $("#referral_form").show()
                $("#referral_phone_conf").hide()
            }
            scope.submitReferralForm = function(){

                var data={
                    referral_code : scope.referral_data.code,
                    phone_num : scope.referral_data.phone_num
                }
                APIService.applyReferralCode(data).then(function(resp){
                    if (resp.success){
                        $("#referral_success").show()
                        $("#referral_phone_conf").hide();
                        $rootScope.userLoggedInDetails.phone_num=data.phone_num.toString()
                        $rootScope.enterReferralCode=false;
                        $cookies.remove("enterReferralCode")
                    }
                    else{
                        scope.referral_data.num_error=resp["num_error"]
                        scope.referral_data.code_error=resp["code_error"]
                        $("#referral_form").show()
                        $("#referral_phone_conf").hide()
                    }
                })
            }
            scope.cancelHideDareClick = function(){
                $("#dare-menu-box").show()
                $("#hide-dare-conf").hide()
            }
        }

    }
}]);
