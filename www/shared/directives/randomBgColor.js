angular.module('stichio').directive('randomBgColor', randomBgColorFn);
randomBgColorFn.$inject = [];
function randomBgColorFn () {
	return {
		link: link
	};

	function link(scope, elem ,attrs) {
        var colors = ['#DDDDDD','#FFA18B','#D1E8FF','#FFD3BD','#C8C8FF','#FFDDFF','#E5E4CA','#FFE8C8'];
        var bg_color = colors[Math.floor(Math.random() * colors.length)];
        elem.css('background-color', bg_color);
	}
}