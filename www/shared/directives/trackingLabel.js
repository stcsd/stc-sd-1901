angular.module('stichio').directive('trLabel', ['commonService','$timeout', function(commonService, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $timeout(function(){
                element.bind('click', function () {
                    var label = commonService.loggedInUserId || "Not Logged In";
                    console.log("attrs.label_text", attrs.labelText)
                    if(!attrs.labelText) {
                        try {
                            var obj = scope.card || scope.dare;
                            if (obj) {
                                switch (obj.feed_type) {
                                    case "dare":
                                        label = "P2P: " + obj.id + " " + obj.dareText;
                                        break;
                                    case "taken_dare":
                                        label = "TD: " + obj.id + " " + obj.admin_dare.dareText;
                                        break;
                                    case "admin_dare":
                                        label = "DS: " + obj.id + " " + obj.dareText;
                                        break;
                                }
                            }
                        } catch (e) {
                        }
                    }
                    else{
                        label=attrs.labelText
                    }
                    if (scope.notif){
                        label = scope.notif.action + " : " + scope.notif.message;
                    }
                    if (scope.genreData){
                        label = scope.genreData.id || scope.genreData.item.id;
                    }

                    var data = {
                        category : 'Click',
                        action : attrs.trLabel,
                        label : label,
                        value : 1
                    }
                    commonService.analytics('event',data)
                })
            }, 1000);
        }
    }
}]);