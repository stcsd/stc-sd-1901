angular.module('stichio').directive('socialShare', ['$http','APIService','commonService','$rootScope','Upload','$timeout', '$sce', '$cordovaSocialSharing', function($http, APIService, commonService, $rootScope, Upload, $timeout, $sce,$cordovaSocialSharing) {
    function imageExists(image_url){

        try {
            var http = new XMLHttpRequest();

            http.open('HEAD', image_url, false);
            http.send();

            return http.status != 404;
        } catch (e) {
            return false;
        }

    }
    return {
        restrict: 'A',

        scope: {
            social_share: '=socialShare',
        },
        link: function (scope, element, attrs) {
            var default_share_pic = 'http://www.stichio.co.in/images/Default_Share_Pic_No_DARE_pic.jpg';
            var wp_share_link, fb_share_link,stichio_share_link;
            $timeout(function(){
                element.bind('click', function () {
                    //var date= 2;
                    //var date= new Date().getDate();
                    if (!commonService.userLoggedIn) {
                        return;
                    }

                    var title, wp_title, wp_image_title, url, picture, description,video_snapshot;
                    var utm_source="whatsapp",
                        utm_medium="",
                        utm_campaign=""
                    // var data = {};
                    var data = angular.copy(scope.social_share);
                    fb_share_link=data.fb_share_link;
                    console.log("fb_share_link", wp_share_link, fb_share_link)
                    var post_obj = {
                        isVisited: false,
                        isPosted: true
                    };
                    if(!wp_share_link) {
                        wp_share_link = data.wp_share_link;
                    }
                    if(!stichio_share_link){
                        stichio_share_link=data.stichio_share_link
                    }

                    console.log("fb_share", data);
                    // data.response_text = null;
                    // data.response_image = null;
                    // data.image_full = null;
                    if (!attrs.shareElement) {
                        if (data.feed_type == "admin_dare") {
                            picture = data.image_full || data.image || default_share_pic;
                            title = data.dareText;
                            utm_medium = "DS"
                            wp_title = "STICHIO DARE: " + data.dareText
                            wp_image_title = "*Click link above to take challenge.*" + "\r\n\r\n" + "*" + $rootScope.userLoggedInDetails.username + " DARED You:* " + data.dareText + "\r\n\r\n"
                            url = 'http://www.stichio.co.in/#/dares/' + data.id + "?source=facebook";
                            post_obj.sharedObjectId = data.id
                            post_obj.sharedObject = "admin_dare"
                            if (data.isEventDare && data.prize) {
                                description = "Winner gets Paytm Voucher worth Rs. " + data.prize + " !!"
                            }

                        }
                        else {
                            if (data.response_type == 'video') {
                                var url1 = data.response_image.replace('/videos/', '/images/').replace('/videos_mobile/', '/images/');
                                var coverUrl = url1.replace('.mp4', '.jpg');
                                video_snapshot = url1.replace('.mp4', '_snapshot.jpg');
                                utm_medium = "Video"
                                if (data.admin_dare && data.admin_dare.isEventDare)
                                    picture = coverUrl || data.admin_dare.videoSharingImage || 'http://www.stichio.co.in/images/STICHIO_Default_Video_Share_23Nov.jpg';

                                else {
                                    picture = data.video_cover || coverUrl || 'http://www.stichio.co.in/images/STICHIO_Default_Video_Share_23Nov.jpg';
                                }
                                wp_image_title = "*Click link above to watch video.*"
                                if (!data.admin_dare) {
                                    wp_image_title = wp_image_title + "\r\n\r\n" + "*STICHIO DARE:* " + data.dareText + "\r\n"
                                }
                                else {
                                    wp_image_title = wp_image_title + "\r\n\r\n" + "*STICHIO DARE:* " + data.admin_dare.dareText + "\r\n"
                                }
                            }
                            else {

                                if (data.feed_type=='dare') {
                                    if (data.response_image) {
                                        picture = data.response_image_full || data.response_image;
                                        wp_image_title = "*STICHIO DARE:* " + data.dareText + "\r\n"
                                        utm_medium = "Photo"
                                    }
                                    else {
                                        picture = data.full_image || data.image_full || data.image || default_share_pic;
                                        if (data.status > 2) {
                                            wp_image_title = "*STICHIO DARE:* " + data.dareText + "\r\n\r\n" + "*" + data.user2.username + " says*" + "\r\n" + data.response_text + "\r\n"
                                            utm_medium = "Text"
                                        }
                                        else {
                                            wp_image_title = "*STICHIO DARE:* " + data.dareText + "\r\n"
                                            utm_medium = "Incomplete"

                                        }
                                    }
                                }
                                else {
                                    if (data.response_image) {
                                        picture = data.response_image_full || data.response_image;
                                        wp_image_title = "*STICHIO DARE:* " + data.admin_dare.dareText + "\r\n"
                                        utm_medium = "Photo"
                                    }
                                    else {
                                        if (data.admin_dare.image) {
                                            picture = data.admin_dare.image_full || data.admin_dare.image;
                                        }
                                        else{
                                            picture = default_share_pic
                                        }
                                        wp_image_title = "*STICHIO DARE:* " + data.admin_dare.dareText + "\r\n\r\n" + "*" + data.user.username + " says*" + "\r\n" + data.response_text + "\r\n"
                                        utm_medium = "Text"
                                    }
                                }
                            }
                            console.log("step2", wp_share_link, fb_share_link)
                            if (data.eventId && data.response_image == undefined) {
                                picture = data.fbPageSharingImage;
                                title = data.fbSharingTitle;
                                description = data.fbSharingDescription;
                                url = 'www.stichio.in/#/events/' + data.uniqueUserName;
                            }
                            else {
                                if (data.feed_type=='dare') {
                                    title = data.user1.username + ' DARED ' + data.user2.username + ': ' + data.dareText;
                                    wp_title = "STICHIO DARE: " + data.dareText
                                    url = 'http://www.stichio.co.in/#/profile/' + data.user2.userId + '?dareId=' + data.id + '&source=facebook';
                                    description = data.response_text;
                                    post_obj.sharedObjectId = data.id
                                    post_obj.sharedObject = "dare"
                                    if (!description) {
                                        if (data.user_type == 'darer') {
                                            switch (data.status) {
                                                case 0:
                                                    description = data.user2.username + ' has not accepted yet';
                                                    break;
                                                case 1:
                                                    description = data.user2.username + ' has accepted' + data.user1.username + '\'s DARE';
                                                    break;
                                                case 2:
                                                    description = data.user2.username + ' has rejected' + data.user1.username + '\'s DARE';
                                                    break;
                                                case 3:
                                                    description = data.user2.username + ' has completed' + data.user1.username + '\'s DARE';
                                                    break;
                                                case 4:
                                                    description = data.user1.username + ' has reviewed this DARE'
                                                    break;
                                            }
                                        }
                                        else if (data.user_type == 'daree') {
                                            switch (data.status) {
                                                case 0:
                                                    description = data.user2.username + ' has not accepted yet';
                                                    break;
                                                case 1:
                                                    description = data.user2.username + ' accepted this DARE';
                                                    break;
                                                case 2:
                                                    description = data.user2.username + ' rejected this DARE';
                                                    break;
                                                case 3:
                                                    description = data.user2.username + ' completed this DARE';
                                                    break;
                                                case 4:
                                                    description = data.user1.username + ' has reviewed this DARE';
                                                    break;
                                            }
                                        }
                                        else if (data.user_type == 'visitor') {
                                            switch (data.status) {
                                                case 0:
                                                    description = data.user2.username + ' has not accepted yet';
                                                    break;
                                                case 1:
                                                    description = data.user2.username + ' accepted this DARE';
                                                    break;
                                                case 2:
                                                    description = data.user2.username + ' rejected this DARE';
                                                    break;
                                                case 3:
                                                    description = data.user2.username + ' completed this DARE. ' + data.user1.username + ' to review';
                                                    break;
                                                case 4:
                                                    description = data.user2.username + ' completed this DARE. ' + data.user1.username + ' reviewed';
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    title = data.user.username + ' took a DARE: ' + data.admin_dare.dareText;
                                    wp_title = "STICHIO DARE: " + data.admin_dare.dareText
                                    url = 'http://www.stichio.co.in/#/profile/' + data.user.userId + '?adminDareId=' + data.id + '&source=facebook';
                                    description = data.response_text;
                                    post_obj.sharedObjectId = data.id
                                    post_obj.sharedObject = "taken_dare"
                                    if (!description) {
                                        description = data.user.username + " has taken this DARE";
                                    }
                                }
                                console.log("step3", wp_share_link, fb_share_link)
                            }
                        }
                    } else {
                        if (attrs.shareElement=="profile"){
                            url= 'http://www.stichio.co.in/#/profile/' + data.userId + '?action=follow';
                            if (!$rootScope.userLoggedInDetails.is_featured){
                                wp_image_title = "*Follow me on STICHIO* ❤❤ Super Fun Exciting Challenges Everyday.";
                            }
                            else{
                                wp_image_title="Use my code *" + $rootScope.userLoggedInDetails.referral_code +  "* after sign-up & get Paytm Cash #superfun (sign-up with facebook button)"
                            }
                            wp_title="I am in love with STICHIO App ❤";
                            description="Super Fun Exciting Challenges Everyday. Get STICHIO App and Follow Me!";
                            if (data.gender == "male"){
                                picture = "https://eb-stichio.s3.amazonaws.com/inviteEmailimages/Whatsapp_Sharing_Default_Male.jpg"
                            }
                            else{
                                picture = "https://eb-stichio.s3.amazonaws.com/inviteEmailimages/Whatsapp_Sharing_Default.jpg"
                            }
                            utm_source="whatsapp";
                            utm_medium="invite";
                            utm_campaign=data.userId;
                            post_obj.sharedObjectId = data.userId
                            post_obj.sharedObject = "profile"

                        }
                    }
                    console.log({
                        "picture": picture,
                        "title": title,
                        "description": description,
                        "url": url
                    });


                    //You'll need to create a facebook app and replace APPID with it. No Facebook review is required. We just need an ID here.
                    //Also you can share links only with this method. In any case I believe that you can create pages for this feature. This pages can also
                    //contain some app promotional info
                    console.log("scope.share_type", scope.shareType, picture)

                     console.log("share data", url,picture, title, description)
                    switch (attrs.shareType) {

                        case 'facebook':
                            var fb_picture = "https://eb-stichio.s3.amazonaws.com/fb_snapshots/" + post_obj.sharedObjectId + ".jpg"
                            console.log("fb_picture", fb_picture)
                            if(imageExists(fb_picture)){
                                picture=fb_picture;
                            }
                            else{
                                picture = video_snapshot || picture;
                            }
                            if (!fb_share_link) {
                                if (!imageExists(picture)){
                                    picture=default_share_pic
                                }
                                commonService.generateShareLink({
                                    link: url,
                                    title: title,
                                    description: description,
                                    image: picture,
                                    utm_source:"facebook",
                                    utm_medium:utm_medium,
                                    utm_campaign:utm_campaign
                                }).then(function(link){
                                    scope.social_share.fb_share_link=link;
                                    console.log("step4", wp_share_link, fb_share_link)
                                    //shareOnFacebook(link, title, description, picture);
                                    $cordovaSocialSharing.shareViaFacebook(null, null, link);
                                });
                            }
                            else{
                                //shareOnFacebook(fb_share_link, title, description, picture);
                                $cordovaSocialSharing.shareViaFacebook(null, null, fb_share_link);
                            }
                            post_obj.platform="facebook";
                            APIService.dare_share(post_obj);
                            break;

                        case 'whatsapp':
                            if (!wp_share_link) {
                                if (!imageExists(picture)){
                                    picture=null;
                                }

                                $("#loadingTextPopup").show();
                                $rootScope.loadingText="Opening Whatsapp .."
                                commonService.generateShareLink({
                                    link: url,
                                    title: wp_title,
                                    description: description,
                                    image: picture,
                                    utm_source:utm_source,
                                    utm_medium:utm_medium,
                                    utm_campaign:utm_campaign
                                }).then(function (link) {
                                    wp_share_link = link;
                                    //scope.dareLink = link;
                                    try {
                                        //$cordovaSocialSharing.shareViaWhatsApp(wp_image_title, picture, null);
                                        if (attrs.shareElement !== 'profile'){
                                            wp_image_title = wp_share_link + "\n" + wp_image_title
                                            $cordovaSocialSharing.shareViaWhatsApp(null, null, wp_share_link);
                                        }
                                        else{
                                            wp_image_title = wp_share_link + "\n" + wp_title + "\n" +  wp_image_title;
                                            $cordovaSocialSharing.shareViaWhatsApp(wp_image_title, picture, null);
                                        }
                                    } catch (e) {
                                    }
                                    $("#loadingTextPopup").hide();
                                    $rootScope.loadingText=""
                                    post_obj.platform="whatsapp";
                                    post_obj.url=wp_share_link;
                                    APIService.dare_share(post_obj);
                                }, function(){
                                    $("#loadingTextPopup").hide();
                                    $rootScope.loadingText=""
                                });
                            } else {
                                try {
                                    wp_image_title = wp_share_link + "\n" + wp_image_title;
                                    if (!imageExists(picture)){
                                        picture=null;
                                    }
                                    //$cordovaSocialSharing.shareViaWhatsApp(wp_image_title, picture, null);
                                    $cordovaSocialSharing.shareViaWhatsApp(null, null, wp_share_link);

                                    post_obj.platform="whatsapp";
                                    post_obj.url=wp_share_link;
                                    APIService.dare_share(post_obj);
                                } catch (e) {
                                }
                            }

                            break;
                        case 'internal':

                            if (!stichio_share_link) {
                                if (!imageExists(picture)){
                                    picture=null;
                                }

                                $("#loadingTextPopup").show();
                                $rootScope.loadingText="Creating DARE Link..."
                                commonService.generateShareLink({
                                    link: url,
                                    title: wp_title,
                                    description: description,
                                    image: picture,
                                    utm_source:"stichio",
                                    utm_medium:utm_medium,
                                    utm_campaign:utm_campaign
                                }).then(function (link) {
                                    var internal_share_data = {
                                        link : link
                                    }
                                    //scope.dareLink = link;
                                    try {
                                        $rootScope.openNewChatPopup(null, null, internal_share_data);
                                    } catch (e) {
                                    }
                                    $("#loadingTextPopup").hide();
                                    $rootScope.loadingText=""
                                    post_obj.platform="stichio";
                                    post_obj.url=link;
                                    APIService.dare_share(post_obj);
                                }, function(){
                                    $("#loadingTextPopup").hide();
                                    $rootScope.loadingText="";
                                    var internal_share_data = {
                                        link : url
                                    }
                                    //scope.dareLink = link;
                                    try {
                                        $rootScope.openNewChatPopup(null, null, internal_share_data);
                                    } catch (e) {
                                    }
                                    post_obj.platform="stichio";
                                    APIService.dare_share(post_obj);
                                });
                            }

                            break;
                    }


                    //
                    //FB.ui({
                    //    method: 'share',
                    //    display: 'popup',
                    //    href: url,
                    //    title: title,
                    //    caption: 'www.stichio.in',
                    //    description: description,
                    //    picture: picture,
                    //}, function (response) {
                    //    if (response != undefined) {
                    //        userProfileService.dare_share(post_obj);
                    //    }
                    //    // console.log(response);
                    //});





                    // $cordovaSocialSharing
                    //   .shareViaFacebook({
                    //    method: 'share',
                    //    display: 'popup',
                    //    href: url,
                    //    title: title,
                    //    caption: 'www.stichio.in',
                    //    description: description,
                    //    picture: picture,
                    //}, function (response) {
                    //    if (response != undefined) {
                    //        userProfileService.dare_share(post_obj);
                    //    }
                    //    // console.log(response);
                    //});
                });
                function shareOnFacebook(url){
                    var url2 = "https://m.facebook.com/sharer.php?u=" + encodeURIComponent(url) + '&app_id=126738297428696&referrer=social_plugin'
                    inappBr = window.cordova.InAppBrowser.open(url2, '_blank', 'location=yes');
                    inappBr.addEventListener('loadstart', function (e) { //wait for loading
                        if (e.url.indexOf(url) == 0) {
                            //alert('posting was cancelled');
                            inappBr.close();
                            console.log(e.url, "1")
                        }

                        if (e.url.indexOf('https://m.facebook.com/story.php') == 0) {
                            inappBr.close();
                            console.log(e.url, "2")
                            //alert('You posted this on Facebook');
                        }
                        console.log(e.url, "1")

                        if (e.url.indexOf('facebook.com/dialog/return/close') > -1) {
                            inappBr.close();
                            console.log(e.url, "2")
                            //alert('You posted this on Facebook');
                        }

                    });
                }
            }, 100);
        }
    }
}]);