angular.module('stichio').directive('shareDare',['$http', '$sce', '$rootScope','commonService', '$state',function($http, $sce, $rootScope, commonService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/dareAnswer.html?0.01',
        link: function (scope, element, attribute) {
            commonService.setLoggedInStatus();
                if (!$rootScope.userLoggedIn){
                    return;
                }
            var congrats_phrases=[
                "Fundoo",
                "Mast",
                "Jhakaas",
                "Rocking",
                "Bombastic",
                "Sahiee",
                "WooHoo",
                "Cracking",
                "Tana-Tan",
                "Zabardast",
                "WahWah"
            ];
            var colors = ['#0096FF', '#EA8600', '#FF0080'];
            var smileys = [
                "angel.png",
                "blushing.png",
                "cool_1.png",
                "cool_2.png",
                "cool.png",
                "crazy_1.png",
                "crazy.png",
                "happy_1.png",
                "happy.png",
                "nerdy.png",
                "smile_1.png",
                "smile.png",
                "smiler.png",
                "smiling.png",
                "tongue.png",
                "ulta.png",
                "wink.png",
                "winkie.png"
            ];
            var celebration_msgs=[
                "Yay!",
                "Boo-yah!",
                "Woohoo!",
                "Magnificent!",
                "Yippee!",
                "Whoopie!",
                "Woop woop!",
                "Va-va-voom!",
                "Yee-haw!",
                "Yauwza!",
                "Hurray!",
                "Bravo!",
                "OMG!",
                "Kamaal!",
                "Shabash!",
                "Oye Hoye!",
                "Chak De!",
                "Fantabulous!",
                "Funtastic!",
                "Sweeeet!"
            ];
            var celebration_images=[
                "001-party-6.png",
                "002-party-5.png",
                "003-signs.png",
                "004-party-4.png",
                "006-hat.png",
                "007-rocket.png",
                "008-firework.png",
                "009-firecracker.png",
                "010-party-2.png",
                "013-fireworks-1.png",
                "015-fireworks.png",
                "016-confetti-1.png"
            ];
            var genresintervalID;
            scope.isIOS = false
            if( /iPhone|iPad/i.test(navigator.userAgent) ) {
                scope.isIOS = true;
            }
            var bodyScollpos = $('body').scrollTop()
            scope.takeNextDare=function(genreId){
                console.log("genreId", genreId)
                scope.nextGenres=[]
                var allGenres = angular.copy($rootScope.allDareGenres)
                angular.forEach(allGenres, function(genre){
                   if (genre.id == genreId){
                       scope.nextGenres[0]=genre
                       var index = allGenres.indexOf(genre);
                       allGenres.splice(index, 1);
                   }
                });
                if (allGenres[0] !==scope.nextGenres[0]){
                    scope.nextGenres.push(allGenres[0])
                    allGenres.splice(0, 1);
                }

                allGenres.sort( function() { return 0.5 - Math.random() } );
                scope.nextGenres.push(allGenres[0]);
                scope.nextGenres.push(allGenres[1]);


                scope.takeAnotherDare=true;
                setTimeout(function(){
                    scope.showAnotherDareBox=true;
                },10)
                //scope.genreIndex=0

                //setTimeout(function () {
                //    genresintervalID = setInterval(function () {
                //        if (scope.genreIndex == 10) {
                //            clearInterval(genresintervalID);
                //        }
                //        else if(scope.genreIndex< 9) {
                //            scope.genreIndex++;
                //        }
                //    }, 1000);
                //},10);

                setTimeout(function(){
                    $('.share-popup-content').click(function (e) {
                        console.log("click share")
                        e.stopPropagation();
                    })
                });
                //scope.closeShareDare();
                //if(($state.current.name == 'explore.dares')){
                //    $('body').animate({scrollTop: $('body').scrollTop() + 400}, 300)
                //}
                //else if($state.current.name == 'dare'){
                //
                //}
                //else{
                //    if (genreId)
                //        $state.go('explore.dares', {'genreId' : genreId})
                //    else{
                //        $state.go('explore.dares')
                //    }
                //}
            }
            scope.answer= "The great thing about exercise is that it burns fat.The great thing about exercise is that it burns fat.The great thing about exercise is that it burns fat."
            scope.openShareDare = function (objectType,object,justCompleted, type) {
                // event.stopPropagation()
                commonService.openPopup('share-dare')
                // $('.share-dare').addClass('target')

                var object = angular.copy(object);
                scope.showAnotherDareBox=false;
                object.type = objectType;
                console.log(object);
                scope.answer_text = object.answer_text;
                scope.answer_image = object.answer_text_image;
                scope.genreId= object.genre;
                console.log("genreId", scope.genreId)
                var dares_completed= object.dares_completed;
                scope.takeAnotherDare=false;


                scope.rand_color = getRandomItem(colors);
                scope.congrats_text = getRandomItem(congrats_phrases);
                scope.smiley = "images/smileys/" + getRandomItem(smileys);
                scope.comp_rand_image = "images/celebration/" + getRandomItem(celebration_images);
                if (dares_completed>0) {
                    scope.completed_text = getRandomItem(celebration_msgs) + " You completed your " + dares_completed + "<sup>th</sup> DARE";
                }
                else{
                    scope.completed_text=""
                }
                if (justCompleted) {
                    if (objectType == 'eventDare') { scope.shareMessage = 'Share Now using  ' + '<span style="color: #ec5d57; font-weight: bold;">' + $rootScope.eventDetails.hashtags[0] + '</span>'; }
                    else { scope.shareMessage = 'Share with Friends Now'; }

                } else {
                    scope.shareMessage = 'Share with Friends';
                }

                scope.shareMessage = $sce.trustAsHtml(scope.shareMessage);
                scope.showShareDarePopup = true;
                if (!$rootScope.isMobile) {
                    $('body').css('padding-right', '15px');
                }
                // $('body').addClass('prevent-scroll')
                scope.isCopied=false;
                setTimeout(function () {

                    $('.share-popup-content').click(function (e) {
                        console.log("click share")
                        e.stopPropagation();
                    })
                    $("#sharePopupBox").click(function (e) {
                        var $copyBtn=$('#copybtn')[0]
                        console.log("click share11", e.target,$copyBtn)

                        if (e.target !==  $copyBtn)
                            scope.closeShareDare();
                    });
                },10);
                //if(justCompleted) {
                //    bodyScollpos = $('body').scrollTop()
                //    $('body').animate({scrollTop: bodyScollpos - 381}, 0)
                //}
                //$('body').addClass('prevent-scroll')
                if(objectType == 'dare' || objectType == 'taken_dare' || objectType == 'eventDare' || objectType == 'dare_sug' || objectType == 'event'){
                    scope.dare = angular.copy(object);
                }
                else if(objectType == 'stich'){

                }
                scope.dare.justCompleted = justCompleted
                //var urlBase = window.location.protocol +'//'+window.location.hostname+":"+window.location.port;
                var urlBase = "http://www.stichio.co.in"

                if (objectType == 'event') {
                    scope.tempdareLink = urlBase + '/#/events/' + object.uniqueUserName;
                }
                else if(objectType == 'dare_sug'){
                    scope.tempdareLink = urlBase + '/#/dares/' + object.id;
                }
                else {
                    if (scope.dare.feed_type=='dare') {
                        scope.tempdareLink = urlBase +'/#/profile/'+ scope.dare.user2.userId + '?dareId=' + scope.dare.id ;
                    }
                    else {
                        scope.tempdareLink = urlBase + '/#/profile/' + scope.dare.user.userId + '?adminDareId=' + scope.dare.id;
                    }
                }

                scope.dareLink = scope.tempdareLink
                scope.copyDareLink();
                setTimeout(function () {
                    var clipboard = new Clipboard('.copybtn');
                    clipboard.on('success', function(e) {
                        scope.isCopied=true;
                        scope.$apply()
                        console.info('Action:', e.action);
                        console.info('Text:', e.text);
                        console.info('Trigger:', e.trigger);

                        e.clearSelection();
                    });

                    clipboard.on('error', function(e) {
                        console.error('Action:', e.action);
                        console.error('Trigger:', e.trigger);
                    });
                },300);

                console.log('instance created')
            }
            $('body').on('keyup',function (e) {
                e.preventDefault()
                e.stopPropagation()
                if(e.which == 27){
                    scope.closeShareDare()
                    scope.$apply();
                }
            });
            $rootScope.closeShareDare = function () {
                commonService.closePopup('share-dare');
                scope.showShareDarePopup = false;
                scope.answer_text=""
                scope.answer_image=""
                 scope.takeAnotherDare=false;
                if (!$rootScope.isMobile) {
                    $('body').css('padding-right', '0px');
                }
                if (genresintervalID)
                    clearInterval(genresintervalID);
                // $('body').removeClass('prevent-scroll')
            }
            scope.copyDareLink = function () {
                //var storeAuth = $http.defaults.headers.common.Authorization;
                //delete $http.defaults.headers.common.Authorization;
                // scope.dareLink = 'http://www.stichio.co.in/#/profile/ad59bd2c-1037-47b7-a890-15a7921602bf?dareId=b75ead5b-1de9-4309-9c46-1e3e02efd69f'

                //scope.getTinyUrl1(scope.tempdareLink)
                commonService.generateShareLink({link : scope.tempdareLink}).then(function(link){
                     scope.dareLink = link;
                });
                //$http.defaults.headers.common.Authorization = storeAuth;
                // console.log(scope.dareLink)
            }
            scope.getTinyUrl1 = function(url) {
                var myData = {};
                myData.longUrl = url;
                var key = "AIzaSyB0N1UrT-OxThltr9Lr1bb1IeCuYma-rro";
                $http({
                    method: 'post',
                    url: "https://www.googleapis.com/urlshortener/v1/url?key=" + key,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: myData
                }).then(function (response) {
                    scope.dareLink = response.data.id;
                    console.log('shortUrl',scope.dareLink)
                }, function (response) {
                    console.log(response);
                });

            }
            scope.delayCloseSharePopup = function(){
                setTimeout(function(){
                    scope.closeShareDare();
                }, 500)
            };
        }

    }
}]);
