angular.module('stichio')
.directive('header', ['$rootScope', 'APIService', 'commonService','$state', function ($rootScope, APIService, commonService, $state) {
    return {
        restrict : 'E',
        templateUrl: 'shared/templates/header.html?0.12',
        link: link
    };
	function link (scope, element, attrs) {
        scope.randomGenres = [];
        var homeTop = attrs.homeTop
        console.log("hometop", 1)
        if ($rootScope.allDareGenres){
            scope.list = angular.copy($rootScope.allDareGenres)
            angular.forEach(scope.list, function(item, key) {
                console.log("hometop", key, homeTop)
                if(homeTop){
                    if (item.isTopGenre) {
                        scope.randomGenres.push({
                            item: item,
                            rank: key
                        });
                    }
                }
                else {
                    scope.randomGenres.push({
                        item: item,
                        rank: 0.5 - Math.random()
                    });
                }
            });
        }
        else{
             APIService.getDareGenres().then(function (response) {
                 response = response.filter(function (val) {
                     return val.name !== ''
                 })
                 $rootScope.allDareGenres  = angular.copy(response);
                 scope.list = angular.copy($rootScope.allDareGenres)
                 angular.forEach(scope.list, function(item, key) {
                     console.log("hometop", key, homeTop)
                     if(homeTop){
                        if (item.isTopGenre) {
                            scope.randomGenres.push({
                                item: item,
                                rank: key
                            });
                        }
                     }
                     else {
                         scope.randomGenres.push({
                             item: item,
                             rank: 0.5 - Math.random()
                         });
                     }
                 });

             });
        }


    }
}]);