angular.module('stichio').directive('menuPopup', menuPopupFn)
menuPopupFn.$inject = ['commonService', '$rootScope', '$timeout']
function menuPopupFn(commonService, $rootScope, $timeout) {
	return {
		restrict: 'E',
		templateUrl: 'shared/templates/menuPopup.html?0.01',
		link: function (scope, elem, attrs) {
			setTimeout(function(){
				var isMenuOpen = false;

			var $menuPopup = $('.menuPopup');
			var $win = $(window);

			scope.toggleMenu = function () {
				event.stopPropagation()
				if(isMenuOpen) {
					$menuPopup.hide()
					$win.off('scroll.scroll_menu')
				} else {
					$menuPopup.show()
					$timeout(function () {
						if (!$rootScope.userLoggedInDetails.showCareers){
							$("#careers_opt_border").hide()
							$("#careers_opt").hide()
						}
						if ($rootScope.userLoggedInDetails.latestAppVersion){
							$("#update_opt").hide()
						}
					}, 100)
					$win.on('scroll.scroll_menu', function () {
						scope.toggleMenu()
					})
				}
				isMenuOpen = !isMenuOpen
			}

			$('ul.menu li, ul.menu2 li').click(function () {
				scope.toggleMenu();
			})

			$win.click(function() {
				if (isMenuOpen) {
					scope.toggleMenu();
				}
			})

			$('.menuPopup-content').click(function(event){
				event.stopPropagation()
			})

			$('#give-dare-menu').click(function () {
				//commonService.setLoggedInStatus();
				scope.userLoggedIn = commonService.userLoggedIn;
				if(scope.userLoggedIn == false){
					event.stopPropagation();
					$rootScope.displayloginpopup();
				}
			});
			
			var el = $('.contact-us-form-wrapper');
			console.log('el', el);
			scope.contactUs = function () {
				$rootScope.success_msg = false;
				commonService.openPopup('stichio-form')
				el.focus();
				$('#stichioform input, #stichioform textarea').html('');
			}

				scope.takeToPlayStore=function(){
					window.open("https://play.google.com/store/apps/details?id=com.stichio.stichioApp", '_system', 'location=no');
				}

			el.keydown(function (e) {
				if(e.keyCode == 27){
					commonService.closePopup('stichio-form')
				}
			});
			
	        el.click(function() {
	            commonService.closePopup('stichio-form')
	        })
			
		},3000)}
	}
}