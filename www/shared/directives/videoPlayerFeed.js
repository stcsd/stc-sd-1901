angular.module('stichio').directive('videoPlayerFeed', ['APIService','commonService','$rootScope','$timeout', '$sce','$window', 'cacheService', '$state', function(APIService, commonService, $rootScope, $timeout, $sce,$window,cacheService, $state) {
    return {
        restrict: 'E',
        scope:{
            videoSource : "=videoSource",
            stopAutoPlay : "=stopAutoPlay",
            dare : "=dare",
            iar : "=iar",
        },
        templateUrl: 'shared/templates/videoPlayerFeed.html?0.08',
        link : function(scope, element, attribute) {
            //To identify if the feed is inside a popup
            //if(element.closest('#admin-dare-popup-content').length > 0 || element.closest('#dareviewpopup').length > 0){
            //    scope.videoPlayer = true;
            //}

            //console.log("autoplay", scope.stopAutoPlay)
            scope.inViewConfig={
                //generateDirection:true,
                //throttle:100,
                generateParts:true,
                offset:[2000,0,-200,0]
            }

            var video, videosrc, videofile;
            var inview=true;
            scope.onPlayerReady = function onPlayerReady(API, index) {
                video = API;
                video.dareId=scope.dare.id;
                videosrc=scope.videoSource;

                API.iar = scope.iar
            };
            var socket = commonService.socket;
             var showHideButtons, pausetimeout;
            scope.onUpdateState = function onUpdateState(state, index) {
                if (state == "play"){
                    var videosources;
                    //if (!inview){
                    //    video.stop();
                    //    return;
                    //}
                    commonService.tempPlayingVideo=video;
                    if (commonService.currentPlayingVideo) {
                        if (commonService.currentPlayingVideo != video) {
                            if (commonService.currentPlayingVideo.currentState != 'pause') {
                                commonService.currentPlayingVideo.pause();
                            }
                            else {
                                if (commonService.previousPlayingVideo) {
                                    if ((commonService.previousPlayingVideo != video) && (commonService.previousPlayingVideo != commonService.currentPlayingVideo)) {
                                        commonService.previousPlayingVideo.stop()
                                    }
                                }
                                commonService.previousPlayingVideo = commonService.currentPlayingVideo;
                                commonService.currentPlayingVideo = commonService.tempPlayingVideo;
                            }
                        }
                    }
                    else{
                        commonService.currentPlayingVideo=video;
                    }
                    $("#card-" + scope.dare.id + " .video-volume-button").show();
                    $("#card-" + scope.dare.id + " .video-time-display").show();
                    //$("#card-" + scope.dare.id + " .video-cue-end").show();
                    clearTimeout(showHideButtons);
                    showHideButtons=setTimeout(function() {
                        $("#card-" + scope.dare.id + " .video-volume-button").hide();
                        $("#card-" + scope.dare.id + " .video-time-display").hide();
                        $("#card-" + scope.dare.id + " .video-cue-end").hide();
                    }, 3000);

                    try {
                        if (!cacheService.isFileCached(videosrc)) {
                            cacheService.download(videosrc).then(function (cache) {
                                console.log("Downloaded:", videosrc)
                            }, function (failedDownloads) {

                            })
                        }
                    } catch (e) {
                    }

                }
                else if (state =="stop"){
                    scope.autoplayvid = false;
                    $timeout(function () {
                        if (commonService.tempPlayingVideo && commonService.tempPlayingVideo != video) {
                            video.clearMedia();
                            scope.sources = "";
                            console.log("video source removed")
                        }
                        else{
                            video.seekTime(0)
                        }
                    },100);
                    clearTimeout(showHideButtons);
                    $("#card-" + scope.dare.id +  " .video-volume-button").show();
                    $("#card-" + scope.dare.id +  " .video-time-display").show();
                    //$("#card-" + scope.dare.id +  " .video-cue-end").show();

                    //scope.autoplayvid = false;
                }
                else if (state =="pause") {
                    try {
                        console.log("testinggggg", commonService.tempPlayingVideo, commonService.currentPlayingVideo, commonService.previousPlayingVideo)
                    } catch (e) {
                        console.error(e)
                    }
                    if (commonService.currentPlayingVideo) {
                        if (commonService.currentPlayingVideo != video) {
                            if (commonService.previousPlayingVideo) {
                                if ((commonService.previousPlayingVideo != video) && (commonService.previousPlayingVideo != commonService.tempPlayingVideo)) {
                                    if (commonService.previousPlayingVideo.currentState != 'stop') {
                                        commonService.previousPlayingVideo.stop()
                                    }
                                    else{
                                          commonService.previousPlayingVideo.clearMedia();
                                    }
                                }
                            }
                            commonService.previousPlayingVideo = commonService.currentPlayingVideo;
                            commonService.currentPlayingVideo = commonService.tempPlayingVideo;
                        }
                    }

                    clearTimeout(showHideButtons);
                    $("#card-" + scope.dare.id +  " .video-volume-button").show();
                    $("#card-" + scope.dare.id +  " .video-time-display").show();
                    $("#card-" + scope.dare.id +  " .video-cue-end").show();
                }
                else{
                    clearTimeout(showHideButtons);
                    $("#card-" + scope.dare.id +  " .video-volume-button").show();
                    $("#card-" + scope.dare.id +  " .video-time-display").show();
                    $("#card-" + scope.dare.id +  " .video-cue-end").show();
                }

            }
            function fileExists(url){

                var http = new XMLHttpRequest();

                http.open('HEAD', url, false);
                http.send();

                return (http.status != 404 && http.status != 403);

            }


            if (scope.dare.id){
                scope.analytics = {
                    category: "Video",
                    label: scope.dare.id  + " " + (scope.dare.dareText || scope.dare.admin_dare.dareText),
                    events: {
                        ready: false,
                        play: true,
                        pause: false,
                        stop: false,
                        complete: true,
                        progress: 25
                    },
                    dareId:scope.dare.id,
                    userId:commonService.loggedInUserId
                }
            }
            //scope.canPlay = function(d){
            //    console.log("canPlay", scope.videoSource)
            //}
            var sourceSet;
            scope.lineInView = function(inView, viewInfo) {
                console.log( "viewInfo", inView,viewInfo)
                if (inView) {
                    inview=true;
                    if (!videofile) {
                        //clearTimeout(commonService.videotimeout);
                            //console.log("viewInfo.parts.bottom",viewInfo.parts.bottom, viewInfo.parts.top)
                            //commonService.videotimeout = setTimeout(function () {

                                if (scope.videoSource.indexOf('videos_mobile') > -1) {
                                    scope.sources = [{
                                        src: $sce.trustAsResourceUrl(cacheService.getUrl(videosrc)),
                                        type: "video/mp4"
                                    }];
                                    console.log("video source set 1")
                                }
                                else {
                                    if (scope.dare.video_compressed) {
                                        var mob_video_url = scope.videoSource.replace('videos', 'videos_mobile').replace('.mp4', '_m.mp4');
                                        videosrc=mob_video_url;
                                        scope.sources = [{
                                            src: $sce.trustAsResourceUrl(cacheService.getUrl(videosrc)),
                                            type: "video/mp4"
                                        }];
                                        console.log("video source set 2")
                                    }
                                    else {
                                        scope.sources = [{
                                            src: $sce.trustAsResourceUrl(cacheService.getUrl(videosrc)),
                                            type: "video/mp4"
                                        }];
                                        console.log("video source set 3")
                                    }
                                }
                                videofile = angular.copy(scope.sources);
                            //}, 300);
                        scope.inViewConfig.offset = [-200, 0, -200, 0]
                    }
                    else {
                        if (!scope.sources) {
                            scope.sources = [{
                                src: $sce.trustAsResourceUrl(cacheService.getUrl(videosrc)),
                                type: "video/mp4"
                            }];
                            console.log("video source set 4")

                        }
                        //video.play();
                    }

                    if (viewInfo.parts.bottom && viewInfo.parts.top) {
                        if(commonService.videoplaytimeout){
                            $timeout.cancel(commonService.videoplaytimeout);
                        }
                        commonService.videoplaytimeout=$timeout(function () {
                            if (!scope.stopAutoPlay & inview) {
                                if (!scope.sources) {
                                    scope.sources = [{
                                        src: $sce.trustAsResourceUrl(cacheService.getUrl(videosrc)),
                                        type: "video/mp4"
                                    }];
                                    console.log("video source set 5")
                                    if ($state.current.name.indexOf('profile')<0) { //don't auto-play in profile page
                                        scope.autoplayvid = true;
                                    }
                                }

                                else {
                                    //scope.autoplayvid = true;
                                    try {
                                        if (scope.sources && video.currentState !== 'play') {
                                            if(!commonService.currentPopupOpen()) {
                                                if ($state.current.name.indexOf('profile')<0) { //don't auto-play in profile page
                                                    video.play();
                                                }
                                            }
                                        }
                                    } catch (e) {
                                        console.error("error playing", e)
                                    }
                                }

                            }
                        },100);

                    }
                    else{
                        //scope.autoplayvid = true;
                        try {
                            if(video.currentState=='play') {

                                video.pause();
                            }


                        } catch (e) {
                        }
                    }


                }
                else{
                    inview=false;
                    try {
                            if(video.currentState=='play') {

                                video.pause();
                            }

                    } catch (e) {
                    }

                }
            }
        }

    }
}]);