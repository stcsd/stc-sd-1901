
angular.module('stichio').directive('importantAlert', [ 'APIService','$cookies', 'commonService', '$rootScope', '$ionicPlatform', function(APIService, $cookies, commonService, $rootScope, $ionicPlatform){
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/importantAlert.html?0.09',
        link: function (scope, element, attribute) {
            try {
                APIService.getImportantAlert().then(function (data) {
                    scope.importantAlert = data;
                    if (!data.is_mandatory) {
                        var showCount = data.show_count;
                        data.alert_id = data.alert_id || "random"
                        console.log("!$rootScope.loggedIn", $rootScope.userLoggedIn)
                        if ($rootScope.new_user || (data.message_type == 'custom' && !$rootScope.userLoggedIn)) {
                            return;
                        }
                        var totalShows = parseInt($cookies.get('ia_' + data.alert_id)) || 0
                        if (totalShows < showCount) {
                            $cookies.put('ia_' + data.alert_id, totalShows + 1)
                            commonService.openPopup('imp-alert-popup')
                        }
                    }
                    else {
                        if (data.message_text) {
                            try {
                                $ionicPlatform.registerBackButtonAction(
                                    function () {
                                        console.log("back button disabled")
                                    }, 100);
                            } catch (e) {
                            }
                            commonService.openPopup('imp-alert-popup')
                        }
                    }
                });
            } catch (e) {
            }
            scope.openLink=function(link){
                commonService.openLink(link)
            }
            scope.closeImportantAlertsPopup = function () {
                //$rootScope.customBack();
                commonService.closePopup('imp-alert-popup')
            }

        }//link function
    }//return function

}]);//directive function
