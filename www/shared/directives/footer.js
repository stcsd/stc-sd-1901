angular.module('stichio').directive('footer', ['$rootScope', '$state', '$timeout', '$cookies','commonService', function($rootScope, $state, $timeout, $cookies,commonService){
    return {
        restrict: 'E',
       templateUrl: 'shared/templates/footer.html?0.04',
       link:function(scope, element, attribute){
             if (!$rootScope.userLoggedIn){
                    return;
                }
           $rootScope.currentState = $state.current.name;

           //scope.footer_tab = "home"

           //scope.setFooterTab = function(page){
           //    scope.footer_tab = page
           //}
           scope.footer_tab = ""
           scope.changeOption = function(opt){
               $rootScope.footerSelectedOption = opt;
           }

           console.log(scope.footer_tab, "footer_Tab")
           scope.loggedInUserId = commonService.loggedInUserId;
           //commonService.setLoggedInStatus();
           scope.userLoggedIn = commonService.userLoggedIn;
           $timeout(function () {

           //code for opening/closing footer more popup
           scope.popupOpen = false;
           scope.openMorePopup = function (event) {
               if (event) {
                   event.stopPropagation();
                   event.preventDefault();
               }
               if (scope.popupOpen == false) {
                   $('.more-popup').show();
                   scope.popupOpen = true;
               }
               else {
                   $('.more-popup').hide();
                   scope.popupOpen = false;
               }
           }
           $('body').click(function () {
               $('.more-popup').hide();
               scope.popupOpen = false;
           });
        $('.home-scroll-top').click(function(e){
            if($state.current.name == 'home.stiches' || $state.current.name == 'home1.stiches') {
                if (scope.userLoggedIn) {
                    $('footer').hide();
                    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                        console.log("USING jQUERY ANIMATE");
                        jQuery.fx.interval = 7;
                        console.log(jQuery.fx.interval);
                        $('body').animate('stop').animate({
                            'scrollTop': 0
                        }, 800, function () {
                            jQuery.fx.interval = 13;
                            console.log(jQuery.fx.interval);
                            $('footer').show();
                        });
                    } else {
                        console.log('USING VELOCITY.JS');
                        $('body').velocity('stop').velocity('scroll', {
                            'duration': 800
                        });
                        setTimeout(function () {
                            console.log('VELOCITY CALLBACK');
                            $('footer').show();
                        }, 800);
                    }
                }
            }
	    });
           
           }, 1000);

           scope.openProfileAddPopup = function (event) {
               if(scope.userLoggedIn == true){
                   event.stopPropagation();
                   $('.more-popup').hide();
                   $('.profile-add-popup').addClass("target");
                   setTimeout(function () {
                       $('.frames-wrapper').css('display', ' none');
                   }, 600);
               }else{
                   $rootScope.displayloginpopup();
               }
           }


        }
}

}]);

