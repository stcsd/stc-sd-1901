angular.module('stichio')
	.directive('adminDarePopup', ['$timeout','$state', '$rootScope', '$window', 'commonService', 'APIService', '$sce','$location', '$http', 'Upload',function ($timeout, $state, $rootScope, $window, commonService, APIService, $sce, $location, $http, Upload) {
		return {
			templateUrl: 'shared/templates/admin-dare-popup.html?0.03',
			link: link
		};
		function link(scope) {

			setTimeout(function () {
				var videoElem = $('.admin-dare-popup #video-player').detach();
				$('.admin-dare-popup .video-slot').append(videoElem);
			}, 10);

			var submitting_dare = false;
			var newDareId, lastDareId = '';

			var isDirectUrlOnly=false,
				isNotifOnly = false;



			var suggestedVideos = [];
			//scope.onUpdateState2 = function (state, index) {
			//	console.log("state", state)
			//	scope.showVideoSug = false;
			//	if (state === 'stop') {
			//		scope.showVideoSug = true;
			//	}
			//
			//	if(scope.card.id !== lastDareId) {
			//		suggestedVideos = [];
			//	} else {
			//		if (suggestedVideos && scope.card.is_following_daree) {
			//			scope.userVideos = suggestedVideos.slice(0, 6);
			//		} else if (suggestedVideos) {
			//			scope.userVideos = suggestedVideos.slice(0, 3);
			//		}
			//	}
			//
			//	if(scope.card.id !== lastDareId) {
			//		lastDareId = scope.card.id;
			//		APIService.getUserVideos(scope.card.userId, 6).then(function (data) {
			//			suggestedVideos = data;
			//			data.forEach(function (dare) {
			//				if (dare.admin_dare) {
			//					dare.videoLink = '#/profile/' + dare.user.userId + '?adminDareId=' + dare.id;
			//				} else {
			//					dare.videoLink = '#/profile/' + dare.user2.userId + '?dareId=' + dare.id;
			//				}
			//			})
			//			if (scope.card.is_following_daree) {
			//				scope.userVideos = suggestedVideos.slice(0, 6);
			//			} else {
			//				scope.userVideos = suggestedVideos.slice(0, 3);
			//			}
			//		})
			//	}
			//}

			var uploadFileForDareSuggestion = function (files, errFiles) {

				if (files.length > 0) {
					scope.progressPercentage = 0;
					scope.isUploaded = true
					var len = files.length;
					var mediaType = '';
					DEBUG && console.log("files", len)
					scope.errFiles = errFiles;
					//scope.generateSignature = function () {
					//    $http.post('https://angular-file-upload.appspot.com/s3sign?aws-secret-key=' + encodeURIComponent(scope.AWSSecretKey), scope.jsonPolicy).success(function (data) {
					//        scope.policy = data.policy;
					//        scope.signature = data.signature;
					//        DEBUG && console.log('policy', data.policy)
					//        DEBUG && console.log('signature', data.signature)
					//
					//    });
					//};
					scope.i = 0
					scope.uploadcount = 0
					scope.filenames = []
					var ImageIars = {}
					var filename, filenameurl;
					var x = 0;
					var y = 0
					//$('.scrape-images-popup.scrape').addClass('target')
					angular.forEach(files, function (file) {


						DEBUG && console.log('filesize', file.size)


						var fr = new FileReader;
						var j = 0;
						fr.onload = function () { // file is loaded
							var img = new Image;

							img.onload = function () {
								fname = file.name
								DEBUG && console.log("filename", fname)
								var iar1 = img.height / img.width; // image is loaded; sizes are available
								DEBUG && console.log('iar', iar1)
								ImageIars[fname] = iar1


							};
							img.src = fr.result; // is the data URL because called with readAsDataURL
						};
						fr.readAsDataURL(file);
						x++;
					});



					var file = files[0];

					var d = new Date();
					filename = d.yyyymmdd() + scope.i.toString() + '.jpg';
					filenameurl = 'https://eb-stichio.s3.amazonaws.com/images/' + filename;

					var filedata = {"url": filenameurl, "inverseAspectRatio": 1, "fname": file.name}

					scope.filenames.push(filedata)

					DEBUG && console.log("scope i", scope.i, scope.AWSAccessKeyId, scope.policy, scope.signature)

					var storeAuth = $http.defaults.headers.common.Authorization;
					DEBUG && console.log(storeAuth)
					delete $http.defaults.headers.common.Authorization;

					var filekey = ""

					filekey = 'images/' + filename

					file.upload = Upload.upload({
						url: 'https://eb-stichio.s3.amazonaws.com/', //S3 upload url including bucket name
						method: 'POST',
						data: {
							key: filekey,
							AWSAccessKeyId: scope.AWSAccessKeyId,
							acl: 'public-read',
							policy: scope.policy,
							signature: scope.signature,
							'Content-Type': file.type === null || file.type === '' ? 'application/octet-stream' : file.type,
							filename: file.name,
							file: file
						},

					})
					$http.defaults.headers.common.Authorization = storeAuth;
					return file.upload.then(function (response) {
						console.log(response);
						$timeout(function () {
							scope.uploadcount = scope.uploadcount + 1
							file.result = response.data;
							DEBUG && console.log("scope i after", scope.i)
							if (scope.uploadcount == len) {
								DEBUG && console.log("imageiars", ImageIars)
								DEBUG && console.log("filenames ", scope.filenames);

								//scope.uploadedChatImageUrl = scope.filenames[0].url;
								//scope.chat_img = scope.uploadedChatImageUrl;
								//scope.chat_img_url = '';
								//scope.sendChat();
								//console.log(scope.uploadedChatImageUrl);
								//for (k = 0; k < len; k++) {
								//    scope.filenames[k]['inverseAspectRatio'] = ImageIars[scope.filenames[k]['fname']]
								//}


								// card.mediaUrl = scope.filenames[0].url;
								// card.response_image = scope.filenames[0].url;
								// card.response_image_tmp = scope.filenames[0].url;
								console.log(scope.filenames[0].url)
							}
						});
						// $('.media-upload-progress').hide()
						return {
							"mediaUrl": scope.filenames[0].url,
							"mediaType": mediaType,
							"sources": [
								{src: $sce.trustAsResourceUrl(filenameurl), type: "video/mp4"}
								//{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
								//{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
							]
						};

					}, function (response) {
						if (response.status > 0)
							scope.errorMsg = response.status + ': ' + response.data;
					}, function (evt) {
						scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						console.log('progress: ' + scope.progressPercentage + '% ' + evt.config.data.file.name);
					});
				}


			}





			scope.openAdminDarePopup = function (card, isOwnProfile, isDirectUrl, from_loc, dareId, isNotif, autoplay, passedDare) {

				//newDareId = card.id;
				scope.peopleInvolved=[]
				scope.showVideoSug = false;
				isDirectUrlOnly = isDirectUrl
				isNotifOnly=isNotif
				if (isOwnProfile){
					scope.isOwnProfile = true
				}
				else{
					scope.isOwnProfile = false
				}
                scope.card={}
                scope.commentData = '';
                scope.friendDetails = [];
				scope.popup_display = true;
				scope.passedDare=passedDare
				try {
                    $rootScope.closeNewChatPopup();
                } catch (e) {
                }
				//try {
				//	commonService.currentPlayingVideo.pause();
				//} catch (e) {
				//	console.log(e)
				//}
				$rootScope.closeErrorPopup();
				commonService.openPopup('admin-dare-popup');

				if (isDirectUrl) {
					setTimeout(function () {
						APIService.getAdminDareResponseInfo(dareId).then(function (data) {
							if (!data.id){
								console.log("dare data error")
								$rootScope.closeAdminDarePopup();
								$rootScope.openErrorPopup();
								return;
							}
							showDareData(data, isOwnProfile, isDirectUrl, from_loc, autoplay)
						}, function(e){
							console.log("dare data error", e)
							$rootScope.closeAdminDarePopup();
							$rootScope.openErrorPopup();
							return;
						});
					},10);
				}
				else {
					showDareData(card, isOwnProfile, isDirectUrl, from_loc, autoplay)
				}
			}
			var showDareData = function(card, isOwnProfile, isDirectUrl, from_loc, autoplay){
				if (from_loc) {
					var pageUrl = '/profile/' + card.userId + '?adminDareId=' + card.id;
					if($rootScope.userLoggedInDetails) {
						commonService.analytics('event', {category : 'Trending Dares', action : pageUrl, label : $rootScope.userLoggedInDetails.userId});
					}
					else {
						commonService.analytics('event', {category : 'Trending Dares', action : pageUrl, label : 'not logged in'});
					}
				}
				scope.peopleInvolved.push(card.user);
				try {
                    if (card.admin_dare.creator_details.userId) {
                        scope.peopleInvolved.push(card.admin_dare.creator_details)
                    }
                } catch (e) {
                }
				console.log('open adminDarePopup', card, angular.copy(scope.peopleInvolved));
				card.showTicker=false;
				scope.card = card;
				if(!autoplay) {
					scope.card.stopAutoPlay = true;
				}
				scope.sharing_enabled = false;

				if (scope.card.response_type == "video" && !scope.card.videoSrc){
					if (window.StatusBar) {
						StatusBar.backgroundColorByHexString("#000000");
					}
					scope.card.videoSrc = angular.copy(scope.card.response_image)
				}
				var colors = ['DDDDDD','FFA18B','D1E8FF','FFD3BD','C8C8FF','FFDDFF','E5E4CA','FFE8C8'];
				scope.card.bg_color = '#' + colors[Math.floor(Math.random() * colors.length)];
				scope.card.res_bg_color = '#' + colors[Math.floor(Math.random() * colors.length)];
				var adminDare_res_w = Math.min(screen.width,600)
				scope.card.inverseAspectRatio = parseFloat(scope.card.inverseAspectRatio);
				if (scope.card.response_type==="video") {
					scope.card.inverseAspectRatio = Math.min(parseFloat(scope.card.inverseAspectRatio), 1.30);
				}

				//if (scope.card.inverseAspectRatio <= 1) {
				scope.card.adminDare_res_h = (adminDare_res_w * scope.card.inverseAspectRatio) + 'px';
				//}
				//else {
				//	scope.card.adminDare_res_h = adminDare_res_w + 'px';
				//}
				scope.card.autoPlayVideo = false;
				card.sources=[{src: $sce.trustAsResourceUrl(card.response_image), type: "video/mp4"}]
				if(card.response_image)
					card.poster= card.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
				scope.data = '';
				scope.card.popup_display = true;
				if (card.response_type == "video"){
					scope.card.videoSrc= angular.copy(card.response_image)
				}
				if (!card.comments) {
					if (card.comments_count > 0) {
						setTimeout(function () {

							scope.card.loadingComments=true;
							APIService.getAdminDareComment(card.id).then(function(result){
								console.log('admin dare comments', result.data);
								scope.card.comments = result.data;
								angular.forEach(card.comments, function(comment){
									scope.peopleInvolved.push({
										userId:comment.userId,
										username : comment.username,
										avatar : comment.avatar
									})
								})
								scope.card.loadingComments=false;
							}, function(error){
								scope.card.loadingComments=false;
							});
						}, 500);
					}
				}

				//openPopup('admin-dare-popup');
				if (scope.card.response_type==="video") {
					setTimeout(function () {
						var $vp = $('.admin-dare-popup .video-slot')[0]
						console.log($vp, "$vp")
						$('.admin-dare-popup .modal-content').animate({scrollTop: $vp.offsetTop}, 'fast');
					}, 100)
				}
				else{
					$('.admin-dare-popup .modal-content').scrollTop(0);
				}

				setTimeout(function () {
					$.screentime({
						fields: [{
							selector: '#card-' + scope.card.id,
							name: scope.card.id,
							media: (scope.card.response_type === 'video') ? true : false,
							gaLabel:"TD: " + scope.card.id + " " + scope.card.admin_dare.dareText,
							userId: commonService.loggedInUserId,
							type:"taken_dare"
						}]
					});
				}, 500)

				//scope.API2.sources = [{src: $sce.trustAsResourceUrl(scope.card.response_image), type: "video/mp4"}]
				var el = $('#admin-dare-popup-content');
				el.focus();
			};

			$rootScope.closeAdminDarePopup = function (pageChange) {
				if (window.StatusBar) {
					StatusBar.backgroundColorByHexString(defaultHexCodeStatusBar);
				}
				scope.commentData='';
				//scope.peopleInvolved=[]
				commonService.closePopup('admin-dare-popup');
				try {
					scope.card.stopAutoPlay = false;
				} catch (e) {
				}
				try {
					scope.card.popup_display = false;
					//scope.card.autoPlayVideo=false;
					scope.card.playVideo = false;
					scope.card = null;
				} catch (e) {
				}
				scope.passedDare=null;
				$rootScope.hideDarePopup();
				if (isDirectUrlOnly && !pageChange){
					isDirectUrlOnly=false;
					if (!isNotifOnly) {
						 if ($state.current.name == "profile.dares") {
                            $state.go($state.current.name, {
                                profileId: $state.params.profileId,
                                dareId: null,
                                adminDareId: null
                            }, {
                                reload: false,
                                notify: false,
								location : 'replace'
                            });
							 $location.path()
                        }
						$rootScope.loadActivityFeed('all')
					}
				}
				isNotifOnly=false;
				isDirectUrlOnly=false;
                //if (commonService.currentPlayingVideo){
                //    if (commonService.currentPlayingVideo) {
                //        commonService.currentPlayingVideo.stop();
                //    }
                //
                //}
			};

			scope.toggleDareUpvote = function (card) {
				if (card.vote_status == 'upvote') {
					adminDare_unupvote(card);
					if(scope.passedDare){
                        scope.passedDare.upvoted=false;
                    }
				} else {
					adminDare_upvote(card);
					if(scope.passedDare){
                        scope.passedDare.upvoted=true;
                    }
				}
			};

			function adminDare_upvote (card) {
				console.log(card);
				if (!card.upvote_count) {
					card.upvote_count = 0;
				}
				if (card.vote_status == 'downvote') {
					card.downvote_count--;
				}
				var data = {
				vote_type: 'up',
				dareResponseId: card.id
			}
				APIService.adminDareVote(data, card.id, card.userId, card.upvote_count)
				card.vote_status = 'upvote'
				card.upvote_count++;
			};

			function adminDare_unupvote (card) {
				var data = {
				vote_type: 'cancel',
				dareResponseId: card.id
			}
				APIService.adminDareVote(data, card.id, card.userId, card.upvote_count)
				card.vote_status = 'None'
				card.upvote_count--;
			};

			scope.adminDare_downvote = function (card) {
				if (!card.downvote_count) {
					card.downvote_count = 0;
				}
				if (card.vote_status == 'upvote') {
					card.upvote_count--;
				}
				card.vote_status = 'downvote'
				card.downvote_count++;
				var data = {
					vote_type: 'down',
					dareResponseId: card.id
				}
				APIService.adminDareVote(data, card.id)
			};

			scope.adminDare_undownvote = function (card) {
				card.vote_status = 'None'
				card.downvote_count--;
				var data = {
					vote_type: 'cancel',
					dareResponseId: card.id
				}
				APIService.adminDareVote(data, card.id)
			};

			var mediaUrl;
			var mediaType;


			scope.submit_comment = function (card) {
				if (!card.comments) {
					card.comments = [];
				}
				scope.commentData = parseUrlFn(scope.commentData)

				var tagged_users=[];
				angular.forEach(scope.friendDetails, function(value, key){
					console.log("comment data", value, scope.commentData)
					scope.commentData = scope.commentData.replace(value.convertLink,value.link);

				});

				angular.forEach(scope.friendDetails, function(value, key){
					var pattern = new RegExp(value.id, "g");
					var result = scope.commentData.match(pattern);

					if(result){
						if(tagged_users.indexOf(value.id)==-1){
							tagged_users.push(value.id)
						}
					}
				});



				var newComment = {
					action: 'comment',
					comment: scope.commentData,
					avatar: $rootScope.userLoggedInDetails.avatar,
					userId: $rootScope.userLoggedInDetails.userId,
                    username: $rootScope.userLoggedInDetails.username,
                    time: 'just now',
                    taggedUsers : tagged_users

                };


                if(mediaUrl) {
                    newComment.comment_image = mediaUrl;
                } else {
                    card.last_comment = newComment;
                }
                APIService.submitAdminDareComment(newComment, card.id, card.userId).then(function(result){
                    newComment.id = result.data.commentId;

                });
				scope.friendDetails=[];
				tagged_users=[]
                scope.commentData = '';
                mediaUrl = '';
                if (!card.comments_count) {
                    card.comments_count = 0;
                }
                card.comments_count++;
                card.comments.push(newComment);
                setTimeout(function () {
                    $('#admin-dare-popup-content').animate({scrollTop: 100000}, 0);
                }, .5);
                console.log(newComment);



            };

			scope.submit_comment_image = function (files, errFiles, card) {
				if (files.length > 0) {
					$('.comment-image-loading').show();
					setTimeout(function () {
						$('#admin-dare-popup-content').animate({scrollTop: 100000}, 0);
					}, .5);
				}
				uploadFileForDareSuggestion(files, errFiles).then(function (result) {
					console.log(result)
					mediaUrl = result.mediaUrl;
					mediaType = result.mediaType;
					scope.submit_comment(card)
					$('.progress').hide();
				});

			};

			scope.selectImageAdminDareComment = function (){
                    commonService.selectMedia(successCb, errorCb, progressCb, 'image');
                }
            function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
				mediaUrl = remoteUrl;
				scope.submit_comment(scope.card)
				resetLoadingOptions();


            }

            function errorCb(data) {
                resetLoadingOptions();
            }

            function progressCb(data) {
                $('.comment-image-loading').show()
				$('.progress').show();

                scope.progressPercentage=data.progress;

            }


            function resetLoadingOptions() {
                $('.comment-image-loading').hide()
				$('.progress').hide();
                scope.progressPercentage=0;
				mediaUrl=''
            };

			//scope.adminDare_res_image = function (files, errFiles, card) {
			//	if (files.length > 0) {
			//		var loading_icon_class = 'media-upload-progress-' + card.id;
			//		$('.' + loading_icon_class).show();
			//		console.log("file uploaded", files[0])
			//		card.videofile=files[0]
			//	}
			//	card.response_image_tmp = '';
            //
			//	scope.uploadFileForDareSuggestion(files, errFiles).then(function (result) {
			//		console.log(result)
			//		card.response_image_tmp = result.mediaUrl;
			//		card.response_type = result.mediaType;
			//		card.sources = result.sources;
			//		card.inverseAspectRatio = parseFloat(card.inverseAspectRatio)
			//		if (card.response_type==="video") {
			//			card.inverseAspectRatio = Math.min(parseFloat(card.inverseAspectRatio), 1.3);
			//		}
            //
			//		card.poster= card.response_image_tmp.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
            //
			//		if (card.response_type == 'video')
			//			commonService.extractFrame(card.response_image_tmp);
			//		$('.progress').hide();
			//	});
			//};

			scope.delete_comment = function (comment, card) {
				var choice = confirm("Are you sure you want to delete this comment?");
				if(choice == true) {
					var index = card.comments.indexOf(comment);
					card.comments.splice(index, 1);

					card.comments_count--;
					APIService.deleteAdminDareComment(comment.id, card.userId);
					if (card.last_comment && (card.last_comment.id==comment.id)){
						if(card.comments.length>0){
							console.log("card.comments.length", card.comments.length)
							if(!card.comments[card.comments.length-1].comment_image) {
								console.log(card.comments[card.comments.length - 1])
								card.last_comment = card.comments[card.comments.length - 1]
							}
							else{
								card.last_comment=""
							}
						}
						else{
							card.last_comment=""
						}
					}
				}
			}

			scope.follow_user = function (card) {
				card.is_following_daree = true;
				card.user.is_following = true;
				card.user.followStatus = true;
				card.user.justFollowed = true;
				console.log(card.user.userId);
				var followedUser = { "followeeId" :  card.user.userId };
				APIService.followUser(followedUser).then(function(){
					$rootScope.followee_index++;
				});
			}

		}
	}]);