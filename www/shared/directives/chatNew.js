angular.module('stichio')
    .directive('chatNew',['$rootScope', '$http', '$cookies', 'APIService', 'commonService',  '$timeout', '$window','$sce', '$ionicPlatform', '$location','$state', 'cacheService',
        function ($rootScope, $http, $cookies, APIService, commonService,  $timeout, $window, $sce, $ionicPlatform, $location, $state, cacheService)  {

            return {
                restrict: 'E',
                templateUrl: 'shared/templates/chat-new.html?0.42',
                link: function (scope, element, attribute) {
                    if (!$rootScope.userLoggedIn) {
                        return;
                    }
                    var peopleInCache=new Set()
                    var conv_index = 0;
                    var conv_count = 50;
                    var msg_index = 0;
                    var msgs_from_time;
                    var msg_count = 50;
                    scope.active_convo={}
                    scope.active_convo.messages = []
                    $rootScope.unread_chat_count = 0
                    console.log('rootscope chat count', $rootScope.unread_chat_count)
                    scope.default_users = []

                    var unread_count = 0
                    var cached_conversations = []
                    var loading_messages = false;
                    var chat_loaded=false;
                    var $search_input_box;
                    var cache_data_available=false;
                    var no_convos=false;
                    var preventBlur=false;
                    var socket_timeout;
                    var socket;
                    var socket_disconnect_time = 0;
                    var socket_connection_attempt = 0;
                    var temp_share_data=null;
                    commonService.socket_status;
                    var loading_conversations = false;

                    scope.shouldBlur = function(event) {
                        if (preventBlur) {
                            event.target.focus();

                            // Reset so other elements will blur the keyboard
                            preventBlur = false;
                        }
                    }
                    scope.resetBlur=function() {
                        preventBlur = false;
                    }

                    scope.flipBlur=function() {
                        preventBlur = true;
                    }

                    scope.scrollToBottom = function(){
                        $timeout(function () {
                            $(".message-body").scrollTop(100000);
                        }, 500)
                    }
                    $timeout(function () {
                        $rootScope.openNewChatPopup = function (conversationId, other_user, shareData) {
                            console.log("open new chat Popup");
                            scope.new_users=[];
                            scope.search_user='';
                            if (shareData){
                                temp_share_data=shareData;
                                scope.sharing=true;

                            }
                            $rootScope.unread_chat_count = 0;

                            $timeout(function () {
                                commonService.openPopup('new-chat-popup');
                                console.log('rootscope chat count', $rootScope.unread_chat_count)
                                try {
                                    socket.emit('view_chats', $cookies.get("sti_authenticationToken"));
                                } catch (e) {
                                }
                                if (scope.sharing){
                                    $timeout(function () {
                                        $('.user-search-input')[0].placeholder="Search users to send to";
                                    })
                                }
                                else{
                                    $timeout(function () {
                                        $('.user-search-input')[0].placeholder="Search users to chat";
                                    })
                                }
                                var cached_data = cacheService.get('myChat').data;
                                if (cached_data) {
                                    scope.conversations = cached_data.my_chats;
                                }
                                fetch_default_users(scope.conversations);
                                $search_input_box = $('.user-search-input');
                                if (conversationId || other_user) {
                                    var conversation_found = false;
                                    angular.forEach(scope.conversations, function (cached_conv) {
                                        if (cached_conv.conversationId == conversationId || cached_conv.other_user.userId == other_user.userId) {
                                            conversation_found = true;
                                            scope.showConversation(cached_conv)
                                        }
                                    });
                                    if (!conversation_found) {
                                        if (other_user) {
                                            scope.showConversation(null, other_user)
                                        }
                                    }
                                }
                            }, 10);

                        };

                        $rootScope.closeNewChatPopup = function () {
                            console.log("closeNewChatPopup");
                            try {
                                $('.user-search-input')[0].placeholder = "Search users to chat";
                            } catch (e) {
                            }
                            commonService.closePopup('new-chat-popup');
                            scope.chat_message=''
                            temp_share_data=null;
                            scope.sharing=false;


                        };

                        scope.closeConversation = function () {
                            $timeout(function(){
                                scope.active_convo = {};
                                $('#msgWindow').hide();
                                $('#chatWindow').show();
                                $timeout(function () {
                                    $('.user-search-input')[0].placeholder="Search users to chat";
                                })
                                commonService.active_convo_id=null;
                                $('#msgWindow').off('scroll.msgScroll')
                                scope.chat_message=''
                                temp_share_data=null;

                                scope.sharing=false;
                                msg_index = 0;
                                msgs_from_time=null;
                                try {
                                    $rootScope.customBack();
                                } catch (e) {
                                }
                            });

                        };

                        loadConversations();

                        check_connect_socket();

                        console.log('before connecting to socket: ');

                        function check_connect_socket () {
                            var socket_not_connected;
                            socket_timeout = 400;
                            try {
                                socket = commonService.socket;
                                commonService.socket_status = true;
                                socket_not_connected = false;
                                socket_timeout = 0
                            } catch (e) {
                            }
                            if (!socket) {
                                socket_not_connected = true
                            }

                            if (socket_not_connected) {
                                // commonService.connectSocket();
                                commonService.reconnectSocket();
                                $timeout(function () {
                                    socket = commonService.socket
                                }, 300)
                            }
                            return socket_timeout;
                        }

                        socket.on('connect', function () {
                            // socket.emit('join', $.cookie("sti_authenticationToken"));
                            commonService.socket_status = true;
                            console.log('socket connected');
                            try {
                                if (parseInt(socket_disconnect_time) > 0 && !loading_conversations) {
                                    loading_conversations = true;
                                    loadConversations(socket_disconnect_time)
                                }
                            } catch (e) {
                            }
                        });

                        socket.on('reconnecting', function () {
                            console.log('socket attempting to reconnect');
                            commonService.socket_status = false;

                            socket_connection_attempt ++;
                            if (socket_connection_attempt == 1) {
                                socket_disconnect_time = commonService.current_timestamp() - 200;
                                console.log('socket disconnection time=', socket_disconnect_time)
                            }
                        });

                        socket.on('error', function (e) {
                            console.log('socket error occured');
                            commonService.socket_status = false;
                            socket_connection_attempt ++;
                            if (socket_connection_attempt == 1) {
                                socket_disconnect_time = commonService.current_timestamp() - 200;
                                console.log('socket error time=', socket_disconnect_time);
                            }
                        });

                        socket.on('reconnect', function () {
                            console.log('socket connected at ', commonService.current_timestamp());
                            socket.emit('join', $.cookie("sti_authenticationToken"));
                            commonService.socket_status = true;
                            socket = commonService.socket;
                            socket_connection_attempt = 0;
                            console.log('loading convs and msgs after= ', socket_disconnect_time);
                            if (!loading_conversations) {
                                loading_conversations = true;
                                loadConversations(socket_disconnect_time);
                            }
                        });

                        if (commonService.socket_status) {
                            socket.on('join_msg', function(data){
                                var be_timestamp = data.timestamp;
                                calcDeviceTimeOffset(be_timestamp)
                            })
                        }

                        function calcDeviceTimeOffset(timestamp) {
                            var current_device_time = new Date().getTime()/1000;
                            console.log('current device time= ', current_device_time);
                            console.log('be timestamp= ', timestamp);
                            commonService.deviceTimeOffset = timestamp - current_device_time;
                            console.log('device time offset', commonService.deviceTimeOffset);
                            console.log('new time device will use',current_device_time + commonService.deviceTimeOffset )
                        }

                        commonService.current_timestamp = function() {
                            var current_device_time = new Date().getTime()/1000;
                            if (commonService.deviceTimeOffset){
                                current_device_time = current_device_time + commonService.deviceTimeOffset
                            }
                            return current_device_time;
                        }

                        // Controller to Search Users to chat with //

                        socket.on('msg_to_room', function (data) {
                            console.log('trying to read the msg received from socket= ', data);

                            var cached_data;
                            cached_data = cacheService.get('myChat').data;
                            var cached_conversations = [];
                            if (cached_data) {
                                cached_conversations = cached_data.my_chats;
                            }
                            var msg_object = { 'message_id': data.message_id,
                                'conversationId': data.conversationId,
                                'other_user': true,
                                'msg_time':data.msg_time,
                                'message': data.message,
                                'image': data.image
                            };

                            $timeout(function() {
                                scope.$apply(function() {
                                    console.log('trying to apply the new msg received');
                                    try {
                                        if (scope.active_convo.other_user) {
                                            if (scope.active_convo.other_user.userId == data.from_user.userId) {
                                                scope.active_convo.conversationId = data.conversationId;
                                                scope.active_convo.messages = scope.active_convo.messages || [];
                                                scope.active_convo.messages.push(msg_object)
                                            }
                                            $timeout(function () {
                                                $(".message-body").scrollTop(100000);
                                            }, 100)
                                        };
                                    } catch (e) {
                                    }
                                    var conversation_obj = {'conversationId': data.conversationId,
                                        'last_sender': "other_user",
                                        'last_msg_time': data.msg_time
                                    };
                                    var last_msg;
                                    if (!msg_object.image) {
                                        last_msg = data.message
                                    }
                                    conversation_obj.last_message = {};
                                    conversation_obj.last_message.message = last_msg;

                                    var conv_in_cache;
                                    if (!data.is_new) {
                                        angular.forEach(cached_conversations, function(conv){
                                            if (data.conversationId == conv.conversationId) {
                                                conv_in_cache = true;
                                                conv.messages = conv.messages || [];
                                                conv.messages.push(msg_object);
                                                conv.messages = limit_cache_message_size(conv.messages);
                                                var unread_count = conv.unread_count || 0;
                                                console.log('unread count before msg', conv.unread_count);
                                                try {
                                                    if (scope.active_convo.conversationId) {
                                                        if (scope.active_convo.conversationId == data.conversationId) {
                                                            unread_count = 0
                                                        }
                                                        else {
                                                            unread_count++
                                                        }
                                                    }
                                                    else {
                                                        unread_count++;
                                                        console.log('rootscope chat count', $rootScope.unread_chat_count)
                                                        $rootScope.unread_chat_count++
                                                    }
                                                } catch (e) {
                                                }
                                                conv.unread_count = unread_count;
                                                conv.last_message = conversation_obj.last_message;
                                                conv.last_sender = 'other_user';
                                                conv.last_msg_time = data.msg_time;
                                                console.log('unread count after msg', conv.unread_count)
                                            }
                                        });
                                    }

                                    if (data.is_new || !conv_in_cache) {
                                        conversation_obj.unread_count = 1;
                                        if (data.is_new) {
                                            conversation_obj.messages = [msg_object];
                                            conversation_obj.other_user = data.user_details;
                                            cached_conversations.push(conversation_obj)
                                        }
                                        else if (!conv_in_cache) {
                                            APIService.otheruserstats(data.from_user.userId).then(function (other_user_data){
                                                conversation_obj.other_user = other_user_data;
                                                cached_conversations.push(conversation_obj);
                                                update_cache(cached_conversations);
                                                try {
                                                    if (scope.active_convo.other_user.userId == data.from_user.userId) {
                                                        scope.active_convo.other_user = other_user_data;
                                                    }
                                                } catch (e) {
                                                }
                                                scope.new_users=[];
                                                scope.search_user='';

                                            } );
                                        }

                                        try {
                                            if (!scope.active_convo.other_user) {
                                                console.log('rootscope chat count', $rootScope.unread_chat_count)
                                                $rootScope.unread_chat_count++
                                            }
                                        } catch (e) {
                                        }

                                        $timeout(function () {
                                            for (var i = 0; i < scope.default_users.length; i++) {
                                                if (scope.default_users[i].userId == data.from_user.userId) {
                                                    scope.default_users.splice(i, 1);
                                                    break
                                                }
                                            }
                                        })
                                    }
                                    if (data.is_new || conv_in_cache) {
                                        update_cache(cached_conversations);
                                    }
                                });
                            });
                        } );

                        function limit_cache_message_size(messages) {
                            if (messages.length > 0) {
                                messages.slice(Math.max(messages.length - 50, 0))
                                return messages
                            }
                        }

                        var socket_error = false;
                        var timeout;
                        scope.search_user = '';
                        scope.$watch('search_user', function (newValue) {
                            console.log("search string ", newValue);
                            $timeout.cancel(timeout);
                            timeout = $timeout(function () {
                                if (newValue.length > 2) {
                                    console.log("socket", socket);
                                    if (commonService.socket_status) {
                                        try {
                                            socket_error = true;
                                            console.log(peopleInCache, "peopleInCache")
                                            socket.emit('search_users_new', commonService.loggedInUserId, newValue, Array.from(peopleInCache),10, 'chat' );

                                            socket.on('search_users_chat', function (data) {
                                                socket_error = false;
                                                $timeout(function() {
                                                    scope.$apply(function () {
                                                        scope.new_users = data.result;
                                                        console.log('suggested_users from Socket: ', data.result);
                                                    });
                                                });
                                            } )
                                        }
                                        catch (e) {
                                            socket_error = true
                                        }
                                    }
                                    else{
                                        APIService.searchContacts(newValue, 5).then(function (data) {
                                            scope.new_users = data;
                                            console.log('suggested_users from API call: ', data);
                                            if (scope.new_users.length === 0) {
                                                scope.search_error = 'No Match Found';
                                                scope.new_users = [];
                                            }
                                            else {
                                                scope.search_error = '';
                                                // $('.sugg-users-cnt').show()
                                            }
                                        });
                                    }
                                } else {
                                    scope.search_error = 'Enter at least 3 characters';
                                    scope.new_users=[]
                                }
                                if (scope.new_users.length === 0) {
                                    scope.search_error = 'No Match Found';
                                    scope.new_users = [];
                                }
                                console.log('search error', scope.search_error)
                            },300);
                        } )

                        scope.select_user = function(user) {
                            console.log('upon clicking select_user', user)
                            $('#msgWindow').show();
                            $('#chatWindow').hide()
                            $timeout(function(){
                                var $win = $(".message-body");
                            })
                            try {
                                $ionicPlatform.registerBackButtonAction(
                                    function () {
                                        scope.closeConversation();
                                        console.log("back from camera popup")
                                    }, 100);
                            } catch (e) {
                            }
                            scope.active_convo = {};
                            scope.active_convo.other_user = user;
                            APIService.otheruserstats(user.userId).then(function (other_user_data){
                                scope.active_convo.other_user = other_user_data;
                                scope.new_users=[];
                                scope.search_user=''
                            } );

                            var last_timestamp = 0;
                            data = { "userId": scope.active_convo.other_user.userId, 'count': 20,
                                "last_timestamp": last_timestamp };

                            loadMessages(data, true);
                        };

                        function loadMessages(data, save_msg_in_cache)  {
                            console.log('load messages');

                            return APIService.getChatHistory(data).then(function (result) {
                                var $win = $(".message-body");
                                if (result) {

                                    var current_position = $win.scrollTop();
                                    var current_div_height = $win[0].scrollHeight;
                                    var new_msgs = [];
                                    var old_msgs = [];
                                    if (result.new_messages) {
                                        if (result.new_messages.length > 0) {
                                            new_msgs = result.new_messages
                                        }
                                    }
                                    if (result.old_messages) {
                                        if (result.old_messages.length > 0){
                                            old_msgs = result.old_messages
                                        }
                                    }


                                    var all_msgs = [];
                                    if (new_msgs.length > 0) {
                                        if (scope.active_convo.messages) {
                                            scope.active_convo.messages = scope.active_convo.messages.concat(new_msgs);
                                        }
                                        else {
                                            scope.active_convo.messages = new_msgs;
                                        }
                                        all_msgs.push(new_msgs)
                                    }
                                    if (old_msgs.length > 0) {
                                        scope.active_convo.messages = old_msgs.concat(scope.active_convo.messages);
                                        all_msgs.push(old_msgs)
                                    }
                                    if (save_msg_in_cache) {
                                        if (!scope.active_convo.unread_count) {
                                            scope.active_convo.unread_count = 0
                                        }
                                        var cached_data = cacheService.get('myChat').data;
                                        var cached_conversations = [];
                                        if (all_msgs.length > 0 || scope.active_convo.unread_count > 0) {
                                            if (cached_data) {
                                                cached_conversations = cached_data.my_chats;
                                                var conv_in_cache;
                                                angular.forEach(cached_conversations, function (cached_conv) {
                                                    try {
                                                        if (cached_conv.other_user.userId == scope.active_convo.other_user.userId) {
                                                            conv_in_cache = true;
                                                            cached_conv.messages = angular.copy(scope.active_convo.messages);
                                                            cached_conv.unread_count = 0;
                                                        }
                                                    } catch (e) {
                                                    }
                                                });
                                                if (!conv_in_cache) {
                                                    try {
                                                        var last_message_obj = all_msgs[all_msgs.length - 1]
                                                        var conversation_obj = {
                                                            conversationId: all_msgs[0].conversationId,
                                                            last_sender: last_message_obj.other_user ? "other_user" : "self",
                                                            last_message: {message: last_message_obj.message},
                                                            last_msg_time: last_message_obj.msg_time,
                                                            other_user: angular.copy(scope.active_convo.other_user)
                                                        };
                                                        cached_conversations.push(conversation_obj)
                                                    } catch (e) {
                                                    }
                                                }
                                                update_cache(cached_conversations)
                                                console.log('conversation and messages cached...')
                                            }
                                        }
                                    }

                                    $timeout(function () {

                                        var new_div_height = $win[0].scrollHeight;

                                        loading_messages = false;
                                        if (current_position < 20){
                                            current_position = -20
                                        }
                                        $(".message-body").scrollTop(new_div_height+current_position-current_div_height)
                                    });

                                    // console.log('scope.active_convo.messages: ', scope.active_convo.messages, all_msgs)

                                    if (!save_msg_in_cache && old_msgs.length == 0) {
                                        loading_messages = true
                                        $('#msgWindow').off('scroll.msgScroll')
                                    }
                                }
                            }, function () {
                                console.log('no result from API call')
                                $('#msgWindow').off('scroll.msgScroll')
                            } )
                        };
                        var setHtTimeout;
                        scope.$watch('chat_message', function(){
                            // clearTimeout(setHtTimeout);
                            // setHtTimeout = setTimeout(function(){
                            var chattextht=Math.max($("#chattextbox").height(), 24);

                            $("#chattextcontainer").height(chattextht +20)
                            var boxht = $("#chattextcontainer").height() - 10
                            $("#msgWindow").css('height', "calc(100% - " + boxht + "px)");
                            $(".message-body").scrollTop(100000);
                            // },300)

                        })

                        scope.showConversation = function (selected_conv, user) {
                            try {
                                $ionicPlatform.registerBackButtonAction(
                                    function () {
                                        scope.closeConversation();
                                        console.log("back from camera popup")
                                    }, 100);
                            } catch (e) {
                            }
                            scope.search_user=''
                            if (!selected_conv){
                                try {
                                    scope.active_convo = {};
                                    scope.active_convo.other_user = user;
                                    if (scope.active_convo.other_user.dare_count == undefined) {
                                        APIService.otheruserstats(user.userId).then(function (other_user_data) {
                                            scope.active_convo.other_user = other_user_data;
                                        });
                                    }
                                } catch (e) {
                                }
                            }
                            else {
                                scope.active_convo = angular.copy(selected_conv);
                                commonService.active_convo_id = scope.active_convo.conversationId
                                try {
                                    if (scope.active_convo.other_user.dare_count == undefined) {
                                        APIService.otheruserstats(scope.active_convo.other_user.userId).then(function (other_user_data) {
                                            scope.active_convo.other_user = other_user_data;
                                        });
                                    }
                                } catch (e) {
                                }
                                if (selected_conv.messages) {
                                    scope.active_convo.messages = angular.copy(selected_conv.messages);
                                }
                            }


                            console.log('msg popup opened', scope.active_convo);
                            $('#msgWindow').show();
                            $('#chatWindow').hide();
                            $timeout(function(){
                                var $win = $(".message-body");
                            })

                            try {
                                socket.emit('read_conversation',
                                    $cookies.get("sti_authenticationToken"),
                                    scope.active_convo.conversationId,
                                    scope.active_convo.other_user.userId
                                )
                            } catch (e) {
                            }

                            var last_timestamp = 0;
                            var old_msg_timestamp = 0;
                            var current_msg_count = 0;
                            if (scope.active_convo.messages) {
                                if (scope.active_convo.messages.length > 0) {
                                    last_timestamp = find_latest_msg_time(scope.active_convo.messages)
                                    current_msg_count = scope.active_convo.messages.length;
                                    old_msg_timestamp = find_oldest_msg_time(scope.active_convo.messages)
                                }
                            }

                            var data = { "userId": scope.active_convo.other_user.userId, 'count': 20,
                                "last_timestamp": parseInt(last_timestamp), "old_msg_timestamp": parseInt(old_msg_timestamp), 'current_msg_length': current_msg_count };
                            if (temp_share_data){
                                console.log("temp_share_data", temp_share_data)
                                scope.chat_message=temp_share_data.link + "\n";
                            }

                            $timeout(function () {
                                $(".message-body").scrollTop(100000);
                            },1000);
                            loadMessages(data, true).then(function () {
                                console.log("inf scroll")
                                $timeout(function () {
                                    $(".message-body").scrollTop(100000);
                                },100);
                                $timeout(function () {
                                    var $win = $(".message-body")
                                    var onScrollFn = function () {
                                        console.log("old_msg_timestamp", old_msg_timestamp, $win.scrollTop(), loading_messages)
                                        if (!loading_messages && ($win.scrollTop() < 500)) {
                                            loading_messages = true;
                                            console.log("old_msg_timestamp1")
                                            if (scope.active_convo.messages) {
                                                console.log("old_msg_timestamp2")
                                                old_msg_timestamp = find_oldest_msg_time(scope.active_convo.messages);
                                                data = { "userId": scope.active_convo.other_user.userId, 'count': 20, "old_msg_timestamp": parseInt(old_msg_timestamp) };
                                                loadMessages(data);
                                                // loadMessages(scope.active_convo.other_user);
                                            }
                                        }
                                    };
                                    commonService.infiniteScroll($win, onScrollFn, 'scroll.chatWindowScroll')
                                }, 500);
                            })


                        };

                        function find_latest_msg_time(messages) {
                            if (messages.length > 0) {
                                var latest_msg_time = 0;
                                angular.forEach(messages, function (message) {
                                    if (message.msg_time > latest_msg_time) {
                                        latest_msg_time = message.msg_time
                                    }
                                });
                                return latest_msg_time;
                            }
                        }

                        function find_oldest_msg_time(messages) {
                            if (messages.length > 0) {
                                var oldest_msg_time = messages[0].msg_time;
                                angular.forEach(messages, function (message) {
                                    if (message.msg_time < oldest_msg_time) {
                                        oldest_msg_time = message.msg_time
                                    }
                                });
                                return oldest_msg_time;
                            }
                            else {
                                return 0;
                            }
                        }

                        // Controller to Show List of Conversations of the LoggedIn User //

                        function fetch_default_users (conversations) {
                            if (conversations.length <= 10 && scope.default_users.length == 0) {
                                console.log('only ', conversations.length, ' conversations in the list');
                                APIService.getUserFollowees(commonService.loggedInUserId, 0, 12-conversations.length).then(function (data) {
                                    var default_user_list = data.Followees;
                                    var peopleInConversation = [];
                                    angular.forEach(conversations, function (conv) {
                                        peopleInConversation.push(conv.other_user.userId);
                                    });
                                    scope.default_users = [];
                                    for (var i = 0; i < default_user_list.length; i++) {
                                        if (peopleInConversation.indexOf(default_user_list[i].userId) == -1) {
                                            scope.default_users.push(default_user_list[i]);
                                        }
                                    }
                                    console.log('default user inside load conversations= ', scope.default_users)
                                });
                            }
                        }
                        var clearChatCache = function () {
                            cacheService.remove('myChat');
                            cached_conversations=[];
                        };

                        function loadConversations(disconnect_time) {
                            console.log('disconnect time= ',disconnect_time);
                            console.log('inside load conversations function');
                            var cached_data;
                            cached_data = cacheService.get('myChat').data;
                            console.log('already available cached data ', cached_data);
                            cached_conversations = [];
                            if (cached_data) {
                                console.log('cache is present');
                                cached_conversations = cached_data.my_chats
                            }
                            else {
                                console.log('no cache available right now');
                                update_cache(cached_conversations)
                            }
                            console.log("cached_conversations", cached_conversations);
                            APIService.getConversationHistory(disconnect_time).then(function (resp) {
                                if (!disconnect_time && resp.version && cached_data && cached_data.version !== resp.version){
                                    clearChatCache();
                                }
                                var convs = resp.conversations;

                                if (convs) {
                                    if (convs.length >= 0) {
                                        var conversations = convs;
                                        // fetch_default_users (conversations);
                                        console.log('new convs after socket disconnection= ', convs);
                                        if (convs.length >0) {
                                            $rootScope.unread_chat_count = convs[0].total_unread_count || 0;
                                            if (!commonService.deviceTimeOffset) {
                                                calcDeviceTimeOffset(convs[0].timestamp)
                                            }
                                        }
                                        angular.forEach(convs, function (conv) {
                                            var new_conversation = true;
                                            angular.forEach(cached_conversations, function (cached_conv) {
                                                if (cached_conv.other_user.userId == conv.other_user.userId) {
                                                    new_conversation = false;
                                                    console.log("conv matched", conv.conversationId);
                                                    cached_conv.conversationId = conv.conversationId;
                                                    cached_conv.last_sender = conv.last_sender;
                                                    try {
                                                        cached_conv.other_user = conv.other_user;
                                                    } catch (e) {
                                                    }
                                                    cached_conv.last_msg_time = conv.last_msg_time;
                                                    if (disconnect_time) {
                                                        disconnect_time = null;
                                                        cached_conv.messages = conv.messages;
                                                        try {
                                                            if (scope.active_convo.conversationId == conv.conversationId) {
                                                                scope.active_convo.messages = conv.messages;
                                                            }
                                                        } catch (e) {
                                                        }

                                                    }
                                                    cached_conv.last_message = {};
                                                    cached_conv.last_message.message = conv.last_message.message;
                                                    cached_conv.unread_count = conv.unread_count;
                                                }
                                            });
                                            if (new_conversation) {
                                                cached_conversations.push(conv)
                                            }
                                        });
                                        update_cache(cached_conversations, resp.version)
                                    }
                                }
                            } )
                        };
                        // clearChatCache();

                        // Controller to Show Messages between 2 Users //

                        // Send Messages //
                        scope.chat_message = '';
                        var chat_msg = scope.chat_message;
                        var image = '';
                        scope.sendMessage = function(event) {
                            if (event) {
                                event.stopPropagation();
                                event.preventDefault();
                            }
                            if ($rootScope.online) {
                                chat_msg = angular.copy(scope.chat_message);
                                console.log("$rootScope.online1", chat_msg)
                                //scope.chat_message = '';
                                if (/\S/.test(chat_msg) && chat_msg != null) {
                                    console.log("$rootScope.online2", chat_msg)
                                    send_to_socket(chat_msg);
                                }
                            }
                        };

                        scope.chat_media_upload = function (files, errFiles) {
                            console.log('trying to upload media');
                            scope.active_convo.chatImageUploading=true;
                            scope.active_convo.chatImageLoadingPercentage=51;
                            setTimeout(function () {
                                $(".message-body").scrollTop(100000);
                            }, 100);
                            commonService.uploadFile(files).then(function (response) {
                                image = response.url;
                                send_to_socket('', image);
                                console.log('uploaded media file ', image);
                                scope.active_convo.chatImageUploading=false;
                            }, function () {
                                scope.active_convo.chatImageUploading=false;
                            })

                            console.log('after image upload ');
                        };
                        scope.selectChatImage = function (){
                            if ($rootScope.online) {
                                commonService.selectMedia(successCb, errorCb, progressCb, 'image');
                            }
                        }
                        function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
                            image = remoteUrl;
                            send_to_socket('', image);
                            resetLoadingOptions();

                        }

                        function errorCb(data) {
                            resetLoadingOptions();
                        }

                        function progressCb(data) {
                            var imageLoadingStarted = angular.copy(scope.active_convo.chatImageUploading)
                            scope.active_convo.chatImageUploading = true;
                            scope.active_convo.chatImageLoadingPercentage = data.progress;

                            if (!imageLoadingStarted) {
                                setTimeout(function () {
                                    $(".message-body").scrollTop(100000);
                                }, 100);
                            }

                        }


                        function resetLoadingOptions() {
                            image='';
                            scope.active_convo.chatImageUploading=false;
                            scope.active_convo.chatImageLoadingPercentage=0;
                        };

                        socket.on('acknowledge', function (data) {
                            console.log('acknowledge', data)
                            on_message_ack(data);

                        } );

                        var on_message_ack = function(data) {
                            try {
                                if (scope.active_convo.other_user.userId == data.from_user.userId) {
                                    scope.active_convo.conversationId = data.conversationId;
                                }
                            } catch (e) {
                            }
                            var cached_data;
                            cached_data = cacheService.get('myChat').data;
                            var cached_conversations = [];
                            if (cached_data) {
                                cached_conversations = cached_data.my_chats;
                            }
                            console.log('cached_conversations= ',cached_conversations);
                            var msg_object = { 'message_id': data.message_id,
                                'conversationId': data.conversationId,
                                'other_user': false,
                                'msg_time':data.msg_time,
                                'message': data.message,
                                'image': data.image
                            };
                            var conversation_obj = {'conversationId': data.conversationId,
                                'last_sender': "self",
                                'last_msg_time': data.msg_time
                            };
                            var last_msg;
                            if (!data.image) {
                                last_msg = data.message
                            }
                            conversation_obj.last_message = {};
                            conversation_obj.last_message.message = last_msg;
                            console.log('conversation_obj till this point: ', conversation_obj);


                            try {
                                if (scope.active_convo.conversationId) {
                                    if (scope.active_convo.conversationId == data.conversationId) {
                                        angular.forEach(scope.active_convo.messages, function (msg) {
                                            if (msg.message_id == data.message_id) {
                                                msg.msg_time = data.msg_time;
                                            }
                                        })
                                    }
                                }
                            } catch (e) {
                            }

                            var conv_in_cache;
                            console.log('data.is_new= ', data.is_new);
                            if (!data.is_new) {
                                angular.forEach(cached_conversations, function(conv){
                                    if (data.conversationId == conv.conversationId) {
                                        conv_in_cache = true;
                                        var messages = conv.messages || [];
                                        messages.push(msg_object);
                                        messages = limit_cache_message_size(messages);
                                        conv.messages = messages;
                                        conv.last_sender = "self";
                                        conv.last_msg_time = data.msg_time;
                                        conv.unread_count = 0;
                                        conv.last_message = {};
                                        conv.last_message.message = last_msg
                                    }
                                });
                            }
                            console.log('data.is_new= ', data.is_new, 'conv_in_cache= ', conv_in_cache);
                            if (data.is_new || !conv_in_cache) {
                                conversation_obj.unread_count = 0;
                                if (data.is_new) {
                                    conversation_obj.messages = [msg_object];
                                    try {
                                        conversation_obj.other_user = data.user_details;
                                    } catch (e) {
                                    }
                                    cached_conversations.push(conversation_obj);
                                    console.log('before caching this conversation, ', conversation_obj);
                                }
                                else if (!conv_in_cache) {
                                    try {
                                        APIService.otheruserstats(data.from_user.userId).then(function (other_user_data) {
                                            conversation_obj.other_user = other_user_data;
                                            cached_conversations.push(conversation_obj);
                                            update_cache(cached_conversations);
                                        });
                                    } catch (e) {
                                    }
                                }

                                $timeout(function() {
                                    for (var i=0; i < scope.default_users.length; i++)  {
                                        if (scope.default_users[i].userId == data.user_details.userId) {
                                            scope.default_users.splice(i, 1);
                                            break
                                        }
                                    }
                                })
                            }
                            console.log(data.is_new, conv_in_cache)
                            if (data.is_new || conv_in_cache) {
                                update_cache(cached_conversations);
                            }

                        }

                        function send_to_socket(chat_msg, image) {
                            console.log('new msg to be dispatched', chat_msg, image);

                            var timeout_sec = check_connect_socket();
                            console.log(socket);
                            console.log('current conversation', scope.active_convo);
                            console.log('chatting with: ',scope.active_convo.other_user.username);
                            var new_conversation, conversationId;

                            if (scope.active_convo.conversationId) {
                                conversationId = scope.active_convo.conversationId;
                                console.log('conversationId exists of this conversation= ', conversationId)
                            }
                            else {
                                conversationId = commonService.generateUUID();
                                console.log('conversationId didnot exist, new conversationId was created = ', conversationId);
                                new_conversation = true;
                                scope.active_convo.conversationId = conversationId;
                            }
                            var message_id = commonService.generateUUID();
                            console.log('uids generated', conversationId, message_id, 'new_conversation= ', new_conversation);
                            scope.chat_message = '';
                            if (chat_msg) {
                                image = ''
                            }
                            else if (image) {
                                chat_msg = ''
                            }

                            $timeout(function() {
                                if (scope.active_convo.other_user && scope.active_convo.other_user.userId) {
                                    var msg_time = commonService.current_timestamp();
                                    new_conversation = new_conversation || false;
                                    if (commonService.socket_status) {
                                        console.log("sending message via socket")
                                        socket.emit(
                                            'user_message_new',
                                            $cookies.get("sti_authenticationToken"),
                                            scope.active_convo.other_user.userId,
                                            chat_msg,
                                            image,
                                            conversationId,
                                            message_id,
                                            new_conversation
                                        );

                                    }
                                    else{
                                        var data = {
                                            other_userId : scope.active_convo.other_user.userId,
                                            message : chat_msg,
                                            image : image,
                                            conversationId : conversationId,
                                            message_id : message_id,
                                            new_conversation : new_conversation
                                        }
                                        APIService.sendChat(data).then(function (data1) {
                                            console.log("sendChat", data1)
                                            on_message_ack(data1);
                                        })
                                    }
                                    var new_msg = {
                                        'message_id': message_id, 'conversationId': conversationId, 'other_user': false, 'image': image,
                                        'msg_time': msg_time, 'message': chat_msg
                                    };
                                    setTimeout(function () {
                                        $(".message-body").scrollTop(100000);
                                    }, 100);
                                    scope.active_convo.messages = scope.active_convo.messages || [];
                                    scope.active_convo.messages.push(new_msg)
                                }
                            }, timeout_sec)
                        }

                        function update_cache(conversations, version) {
                            scope.conversations=conversations;

                            try {
                                angular.forEach(conversations, function (conv) {
                                    conv.username = conv.other_user.username;
                                    peopleInCache.add(conv.other_user.userId)
                                });
                            } catch (e) {
                            }

                            var new_cache = { 'my_chats': conversations.slice(Math.max(conversations.length - 20, 0)) };
                            if (version){
                                new_cache.version=version;
                            }
                            else{
                                try {
                                    var cached_data=cacheService.get('myChat').data
                                    if (cached_data) {
                                        new_cache.version = cached_data.version
                                    }
                                    else{
                                        new_cache.version=0
                                    }
                                } catch (e) {
                                    new_cache.version=0
                                }
                            }
                            cacheService.set('myChat', new_cache);
                            console.log('all conversations: ', cacheService.get('myChat').data);
                        }
                        scope.clearUserSearchBox=function(){
                            scope.search_user='';
                            setTimeout(function(){
                                $search_input_box[0].focus();
                                $search_input_box[0].click();
                            },100)

                        }

                    }, 2000 );
                }
            }
        }  ] )