angular.module('stichio').directive('darePopup', ['$window', '$sessionStorage', '$cookies', '$http','APIService','commonService','$rootScope','Upload','$timeout', '$sce','$state', function($window, $sessionStorage, $cookies, $http, APIService, commonService, $rootScope, Upload, $timeout, $sce, $state) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/darePopup.html?0.04',
        link : function(scope, element, attribute){
            //$timeout(function(){

            //setTimeout(function () {
            //    var videoElem = $('.dare-view-popup #video-player2');
            //    $('.dare-view-popup .video-slot2').append(videoElem);
            //}, 100);

            var newDareId, lastDareId = '', suggestedVideos;
            //commonService.setLoggedInStatus();
            //if (!$rootScope.userLoggedIn){
            //    return;
            //}
            var colors = ['DDDDDD','FFA18B','D1E8FF','FFD3BD','C8C8FF','FFDDFF','E5E4CA','FFE8C8'];
            var isDirectUrlOnly = false,
                isNotifOnly=false;
            var scrollTo;
            var dare_image_w = Math.min(screen.width-12,588);
            scope.ideal_image_iar = 0.7
            scope.ideal_resp_image_iar = 1
            scope.imageboxht = (dare_image_w* scope.ideal_image_iar)+ "px"
            scope.respimageboxht = (dare_image_w* scope.ideal_resp_image_iar)+ "px"
            scope.peopleInvolvedDare=[]

            $rootScope.hideDarePopup = function (pageChange) {
                if (window.StatusBar) {
                    StatusBar.backgroundColorByHexString(defaultHexCodeStatusBar);
                }
                scope.tagDetails=[]
                //scope.peopleInvolvedDare=[]
                scope.passedDare=null;
                commonService.closePopup("dare-view-popup")
                if (scope.dare) {
                    scope.dare.new_comment_text = ''
                }
                //scope.dare.showExpandCollapse = false
                scope.dare=null;
                scope.card=null;

                if (isDirectUrlOnly && !pageChange){
                    isDirectUrlOnly=false;
                    if(!isNotifOnly) {
                        if ($state.current.name == "profile.dares") {
                            $state.go($state.current.name, {
                                profileId: $state.params.profileId,
                                dareId: null,
                                adminDareId: null
                            }, {
                                reload: false,
                                notify: false,
                                location : 'replace'
                            });
                        }
                        $rootScope.loadActivityFeed('all')
                    }
                }
                //if (commonService.currentPlayingVideo){
                //    if (commonService.currentPlayingVideo) {
                //        commonService.currentPlayingVideo.pause();
                //    }
                //
                //}
                isNotifOnly=false;
				isDirectUrlOnly=false;

            };
             scope.selectImageDareComment = function (){
                    commonService.selectMedia(successCb, errorCb, progressCb, 'image');
                }
            function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
                scope.commentImageUpload = remoteUrl
                console.log(scope.commentImageUpload , "dare_comment")
                scope.submitComment();

                resetLoadingOptions();

            }

            function errorCb(data) {
                resetLoadingOptions();
            }

            function progressCb(data) {
                $('.comment-image-loading').show()
                scope.dare.dareResponseImageUpload=true;
                scope.progressPercentage=data.progress;

            }


            function resetLoadingOptions() {
                $('.comment-image-loading').hide()
                scope.progressPercentage=0;
                scope.dare.dareResponseImageUpload=false;
                scope.commentImageUpload=''
            };
            var tempLoc = '';
            scope.openDarePopup = function(dare, onlyComments, isDirectUrl, from_loc, dareId, isNotif, autoplay, passedDare){

                isDirectUrlOnly=isDirectUrl;
                isNotifOnly=isNotif;
                scope.passedDare=passedDare;
                scope.peopleInvolvedDare=[]
                $rootScope.closeErrorPopup();
                commonService.openPopup("dare-view-popup");
                try {
                    $rootScope.closeNewChatPopup();
                } catch (e) {
                }
                if (isDirectUrl){
                    setTimeout(function () {
                        APIService.getDareInfo(dareId).then(function (data) {
                            if (!data.id){
								console.log("dare data error")
								$rootScope.hideDarePopup();
								$rootScope.openErrorPopup();
                                return;
							}

                            showDareData(data, onlyComments, isDirectUrl, from_loc, autoplay)
                        }, function(){
                            $rootScope.hideDarePopup();
                            $rootScope.openErrorPopup();
                            return;
                        });
                    }, 10);
                }
                else{
                    showDareData(dare, onlyComments, isDirectUrl, from_loc, autoplay)
                }

            }

            var showDareData = function(dare, onlyComments, isDirectUrl, from_loc, autoplay){
                if (from_loc) {
                    var pageUrl = '/profile/' + dare.user2.userId + '?dareId=' + dare.id;
                    if($rootScope.userLoggedInDetails) {
                        commonService.analytics('event', {category : 'Trending Dares', action : pageUrl, label : $rootScope.userLoggedInDetails.userId});
                    }
                    else {
                        commonService.analytics('event', {category : 'Trending Dares', action : pageUrl, label : 'not logged in'});
                    }
                }
                //if (commonService.currentPlayingVideo) {
                //    if (commonService.currentPlayingVideo.currentState == "play") {
                //        commonService.currentPlayingVideo.pause();
                //    }
                //}
                console.log("dare", dare)

                scope.dare = dare;
                scope.card=dare;
                scope.peopleInvolvedDare.push(scope.dare.user2)
                scope.peopleInvolvedDare.push(scope.dare.user1)
                try {
                    if (scope.dare.admin_dare.creator_details.userId) {
                        scope.peopleInvolvedDare.push(scope.dare.admin_dare.creator_details)
                    }
                } catch (e) {
                }
                if(!autoplay) {
					scope.dare.stopAutoPlay = true;
				}
                scope.commentImageUpload = false
                dare = scope.dare

                scope.dare.popupOpen=true;
                scope.dare.autoPlayVideo=false;
                if (scope.dare.response_type == "video" && !scope.dare.videoSrc){
                    if (window.StatusBar) {
                        StatusBar.backgroundColorByHexString("#000000");
                    }
                    scope.dare.videoSrc = angular.copy(scope.dare.response_image)
                }

                //Change to dare popup URL
                if (!scope.dare.time) {
                    var myDate = new Date(scope.dare.created_date);
                    var result = myDate.getTime();
                    var myDate2 = new Date();
                    var result2 = myDate2.getTime();
                    var timeDiff = result2 - result
                    var timeDiffString = millisecondsToStr(timeDiff)
                    scope.dare.time = timeDiffString + " ago"
                    console.log("videoUrl:", scope.dare.response_image)
                    scope.dare.background_color = '#' + colors[Math.floor(Math.random() * colors.length)]
                    scope.dare.resp_background_color = '#' + colors[Math.floor(Math.random() * colors.length)]

                    scope.dare.dare_response_iar = parseFloat(scope.dare.dare_response_iar)
                    if (scope.dare.response_type==="video") {
                        //For Videos for which inverseAspectRatios have not been calculated.
                        //if (scope.dare.dare_response_iar === 1) {
                        //    scope.dare.dare_response_iar = .7
                        //}
                        //Setting max inverseAspectRatio
                        scope.dare.dare_response_iar = Math.min(parseFloat(scope.dare.dare_response_iar), 1.30);
                    }



                    scope.dare.height = (dare_image_w*scope.dare.image_iar) + "px"

                    scope.dare.resp_height = (dare_image_w*(scope.dare.dare_response_iar )) + "px"
                    scope.dare.ht = (dare_image_w * scope.dare.image_iar)
                    scope.dare.resp_ht = (dare_image_w *scope.dare.dare_response_iar)
                    console.log("scope.dare.resp_ht", scope.dare.resp_ht, scope.dare.dareText)
                    scope.dare.sources=[{src: $sce.trustAsResourceUrl(scope.dare.response_image), type: "video/mp4"}
                        //{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
                        //{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
                    ]
                    if(scope.dare.response_image && scope.dare.response_type=="video")
                        scope.dare.poster= scope.dare.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
                    scope.dare.showPic = false
                    scope.dare.showExpandCollapse = (scope.dare.status > 2)

                }
                dare.onlyComments = onlyComments;
                scope.dare.new_comment_text = ""

                //scope.dare.showExpandCollapse = ((dare.status === 0 || dare.status === 1 )&& dare.user_type=='daree') || (dare.status > 2) || dare.adminDareId
                //openPopup("dare-view-popup")
                if (scope.dare.response_type==="video") {
                    setTimeout(function () {
                        var $vp = $('.dare-view-popup .video-slot')[0]
                        console.log($vp, "$vp1")
                        $('#dareviewpopup').animate({scrollTop: $vp.offsetTop}, 'fast');
                    }, 100)
                }
                else{
                    $('#dareviewpopup').scrollTop(0);
                }

                setTimeout(function () {
                    $.screentime({
                        fields: [{
                            selector: '#card-' + scope.dare.id,
                            name: scope.dare.id,
                            media: (scope.dare.response_type === 'video') ? true : false,
                            gaLabel:"P2P: " + scope.dare.id + " " + scope.dare.dareText,
                            userId: commonService.loggedInUserId,
                            type:"dare"
                        }]
                    });
                }, 1000)
                if (scope.dare.comments_count > 0) {
                    setTimeout(function () {

                        scope.dare.loadingComments=true;
                        APIService.getDareComments(scope.dare.id).then(function (data) {
                            scope.dare.loadingComments=false;
                            console.log("comments", data)
                            scope.dare.comments = data
                            angular.forEach(scope.dare.comments, function(comment){
									scope.peopleInvolvedDare.push({
										userId:comment.userId,
										username : comment.username,
										avatar : comment.avatar
									})
								})
                            for (var i = 0; i < scope.dare.comments.length; i++) {
                                if (scope.dare.comments[i].userId == commonService.loggedInUserId)
                                    scope.dare.comments[i].isOwnComment = true
                                else
                                    scope.dare.comments[i].isOwnComment = false

                            }
                        }, function (error) {
                            scope.dare.loadingComments=false;
                        });
                    }, 500);
                }
                else{
                    scope.dare.comments=[]
                }
                var el =  $('.dare-view-popup');
                var elcontent = $('#dareviewpopup');
                el.focus();
            }
            scope.stars = [1,2,3,4,5]

            //scope.dare.comments = [{"id":473,"avatar":"http://eb-stichio.s3.amazonaws.com/avatars/604625bb-9dd0-4f2a-8d68-f3d123590d1b.jpg","stitchId":"e0619320-1fd4-4e1f-9800-7187b7ffe64f","comment":"Haha","userId":"604625bb-9dd0-4f2a-8d68-f3d123590d1b","username":"Janhavi","created_date":"2016-08-26T06:30:47.646775Z","updated_date":"2016-08-26T06:30:47.646809Z"},{"id":436,"avatar":"http://eb-stichio.s3.amazonaws.com/avatars/0502831c-de8a-4efc-b3e3-f42bd739ad03.jpg","stitchId":"e0619320-1fd4-4e1f-9800-7187b7ffe64f","comment":"sadsadsa dsa","userId":"0502831c-de8a-4efc-b3e3-f42bd739ad03","username":"ccc","created_date":"2016-08-22T15:20:35.019712Z","updated_date":"2016-08-22T15:20:35.019746Z"},{"id":435,"avatar":"http://eb-stichio.s3.amazonaws.com/avatars/0502831c-de8a-4efc-b3e3-f42bd739ad03.jpg","stitchId":"e0619320-1fd4-4e1f-9800-7187b7ffe64f","comment":"down vote\nfavorite\n1\nAfter routing around many other questions I have not found an answer that fixes my problem.\n\nI am writing a script to find out whether the div is overflowing. But when trying to retrieve the visible height with jQuery.height(), jQuery.innerHeight() or JavaScripts offsetHeight. I am given the value of the whole div (Including the part which","userId":"0502831c-de8a-4efc-b3e3-f42bd739ad03","username":"ccc","created_date":"2016-08-22T15:19:35.425035Z","updated_date":"2016-08-22T15:19:35.425070Z"},{"id":429,"avatar":"http://eb-stichio.s3.amazonaws.com/avatars/0502831c-de8a-4efc-b3e3-f42bd739ad03.jpg","stitchId":"e0619320-1fd4-4e1f-9800-7187b7ffe64f","comment":"666666","userId":"0502831c-de8a-4efc-b3e3-f42bd739ad03","username":"ccc","created_date":"2016-08-22T14:49:38.857903Z","updated_date":"2016-08-22T14:49:38.857938Z"},{"id":428,"avatar":"http://eb-stichio.s3.amazonaws.com/avatars/0502831c-de8a-4efc-b3e3-f42bd739ad03.jpg","stitchId":"e0619320-1fd4-4e1f-9800-7187b7ffe64f","comment":"5555555","userId":"0502831c-de8a-4efc-b3e3-f42bd739ad03","username":"ccc","created_date":"2016-08-22T14:49:31.684845Z","updated_date":"2016-08-22T14:49:31.684879Z"}]
            scope.commentsOpen = true;
            scope.tagDetails = [];

            scope.submitComment = function(){


                var new_comment_text = scope.dare.new_comment_text;
                if (/\S/.test(new_comment_text) && new_comment_text != null || scope.commentImageUpload) {
                    new_comment_text = parseUrlFn(new_comment_text);

                    var tagged_users=[];
                    angular.forEach(scope.tagDetails, function(value, key){
                        new_comment_text = new_comment_text.replace(value.convertLink,value.link);

                    });

                    angular.forEach(scope.tagDetails, function(value, key){
                        var pattern = new RegExp(value.id, "g");
                        var result = new_comment_text.match(pattern);

                        if(result){
                            if(tagged_users.indexOf(value.id)==-1){
                                tagged_users.push(value.id)
                            }
                        }
                    });

                    console.log("Tagged_Userslist",tagged_users)
                    

                    var newComment = angular.copy({
                        "action": "comment",
                        "comment": new_comment_text,
                        "avatar": APIService.loggedInUserDetailsGlobal.avatar,
                        "username": APIService.loggedInUserDetailsGlobal.username,
                        "time": "just now",
                        "taggedUsers":tagged_users
                    });
                    if (scope.commentImageUpload) {
                        newComment.comment_image = scope.commentImageUpload;
                    } else {
                        scope.dare.last_comment = newComment;
                    }
                    newComment.userId = commonService.loggedInUserId
                    //newComment.created_date = date_now
                    APIService.submitComment(scope.dare.id, newComment, scope.dare.user1, scope.dare.user2).then(function (result) {
                        console.log(result)
                        newComment.id = result.commentId;
                    })

                    scope.tagDetails=[];
                    tagged_users=[]


                    scope.dare.comments.push(newComment);
                    scope.dare.comments_count++;
                    setTimeout(function () {
                        $('#dareviewpopup').animate({scrollTop: 100000}, 0);
                    }, .5);
                }

                scope.commentImageUpload = false;
                scope.dare.new_comment_text = ""

                tagged_users.length=0;
            }
            scope.deleteDareComment = function(comment){
                var response = confirm("Are you sure you want to delete this comment?");
                if(response == true) {
                    var index = scope.dare.comments.indexOf(comment)
                    scope.dare.comments.splice(index, 1);
                    scope.dare.comments_count--;
                    APIService.deleteDareComment(comment.id, scope.dare.user1, scope.dare.user2)
                    if (scope.dare.last_comment && (scope.dare.last_comment.id==comment.id)){
                        if(scope.dare.comments.length>0){
                            if(!scope.dare.comments[scope.dare.comments.length-1].comment_image) {
                                scope.dare.last_comment = scope.dare.comments[scope.dare.comments.length - 1]
                            }
                            else{
                                scope.dare.last_comment=""
                            }
                        }
                        else{
                            scope.dare.last_comment=""
                        }
                    }

                }

                //scope.card.truth_flow_stage=1
            }

            scope.likeDare = function(){
                if (!scope.dare.like_status) {
                    APIService.likeDare(scope.dare.id);
                    scope.dare.likes_count++;
                }
                else {
                    APIService.unlikeDare(scope.dare.id);
                    scope.dare.likes_count--;

                }
                scope.dare.like_status = !scope.dare.like_status
            };

            scope.acceptDare = function(){
                scope.dare.status =1
                APIService.acceptDare(scope.dare.id, scope.dare.user1, scope.dare.user2);
            };

            scope.rejectDare = function(){
                scope.dare.status =2
                APIService.rejectDare(scope.dare.id, scope.dare.user1, scope.dare.user2)
            };
        }
    }
}]);