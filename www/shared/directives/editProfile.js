angular.module('stichio').directive('editProfile', ['$timeout', 'Upload', '$http', 'APIService', 'commonService','loginService','$rootScope', '$cookies', function($timeout, Upload, $http, APIService, commonService,loginService,$rootScope, $cookies) {
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/editProfile.html',
        link:function(scope, element, attribute) {

            $timeout(function() {
                var randomInt;

                $('.edit-profile-popup').click(function (e) {
                    e.stopPropagation();
                });
                //scope.userHasPassword = commonService.userHasPassword;

                
                APIService.getLoggedInUserDetails(true).then(function (data) {
                    $rootScope.userLoggedInDetails = data;
                    scope.genderB = angular.copy($rootScope.userLoggedInDetails.gender);
                    if ($rootScope.userLoggedInDetails.email.indexOf("facebook.com") > -1) {
                        scope.showEmailBox=false;
                    }
                    else{
                        scope.showEmailBox=true;
                    }
                    scope.userHasPassword=$rootScope.userLoggedInDetails.is_password_set
                    if (data.referral_code_applied){
                        $rootScope.enterReferralCode=false;
                    }
                });
                scope.editUsername=function () {
                    $rootScope.userLoggedInDetails.name_change=true;
                    $rootScope.userLoggedInDetails.new_username=$rootScope.userLoggedInDetails.username;

                }


                scope.editBio=function(){
                    $rootScope.userLoggedInDetails.bio_change=true;
                    $rootScope.userLoggedInDetails.user_bio_temp=$rootScope.userLoggedInDetails.user_bio

                }
                scope.editPhoneNum = function(){
                    $rootScope.userLoggedInDetails.phone_num_change=true;
                    $rootScope.userLoggedInDetails.phone_num_temp=parseInt($rootScope.userLoggedInDetails.phone_num)
                }

                scope.sendVerifyEmail = function(){
                    APIService.sendVerifyEmail().then(function(data){

                    });
                }


                $rootScope.openEditProfileNew = function(){

                    event.stopPropagation();
                    commonService.openPopup('edit-profile-popup');

                    scope.username_error=false;
                    $('.frames-wrapper').hide();


                    $('.edit-profile-popup').focus()
                    if ($rootScope.userLoggedInDetails && !$rootScope.userLoggedInDetails.verified) {
                        APIService.getLoggedInUserDetails(true).then(function (data) {
                            $rootScope.userLoggedInDetails = data;
                        });
                    }
                }

                $('.edit-profile-popup').on('click',function (e) {
                    if(e.target != this) return;
                    $('.close-edit-popup').trigger('click')
                })
                scope.openEditProfileFromHam = function(){
                    event.stopPropagation();
                    //scope.openMorePopup();
                    commonService.openPopup('edit-profile-popup')
                    $('.profile-add-popup').removeClass("target");

                    $('body').addClass("prevent-scroll");
                    $('.edit-profile-popup').focus()
                }
                $(".close-edit-popup").click(function () {
                    $rootScope.closeEditProfile();
                    //window.removeEventListener("orientationchange", restackTopics);
                });
                $rootScope.closeEditProfile = function(){

                    $rootScope.userLoggedInDetails.name_change=!true;
                    $rootScope.userLoggedInDetails.bio_change=!true;
                    $rootScope.userLoggedInDetails.phone_num_change=false;
                    //$('body').css('overflow', 'visible');
                    if (scope.changeProfilePicReq) {
                        scope.changeProfilePicReq = false;
                        $('.header').html('Edit Profile');
                    }
                    $('.edit-profile-clear').val('')
                    $('.edit-profile-clear').trigger('input')
                    $('.clear_input').hide()

                    scope.profilePicSet = false;
                    scope.image.imageDataURI = "";
                    scope.fileName = "";

                    $('.email-error-msg').html("");
                    scope.editEmailDet.newEmail = ""
                    scope.editEmailDet.passCheck = ""
                    if (scope.editEmailReqStarted) {
                        scope.editEmailReqStarted = false
                    }
                    $('.error-msg-cp').html("");
                    $('.edit-profile-password').css('border-color', '#53585f')
                    scope.changePassword.new_password = ""
                    scope.changePassword.old_password = ""
                    if (scope.passwordChangeReqStarted = true) {
                        scope.passwordChangeReqStarted = false;
                    }


                    if (scope.genderChanged == true && scope.genderSaved == false) {
                        $rootScope.userLoggedInDetails.gender = scope.genderB;
                        scope.genderChanged = false;
                    }
                    if (scope.genderSaved == true) {
                        scope.genderSaved = false;
                    }
                    commonService.closePopup('edit-profile-popup')
                    $('body').removeClass("prevent-scroll");
                    $('.frames-wrapper').show();
                    $('.edit-profile-popup').removeClass('target');
                }
                var el =  $('.edit-profile-popup');
                var elcontent = $('#editProfilepopup');
                el.focus();
                el.on('keydown',function (e) {
                    if(e.keyCode == 27){
                        $(".close-edit-popup").trigger('click')
                    }
                    else if(e.keyCode == 38){
                        elcontent.animate({scrollTop: elcontent.scrollTop() - 35},0);

                        console.log('keyup',elcontent.scrollTop())
                    }else if(e.keyCode == 40){
                        elcontent.animate({scrollTop: elcontent.scrollTop() + 35},0);

                    }
                })
                var time = new Date().getTime() + 20000;
                //var interval = setInterval(function () {
                //    console.log("AFTER TIMEOUT");
                //    scope.topics = APIService.topics;
                //    scope.topicsB = angular.copy(scope.topics);
                //    scope.selectedTopics = APIService.selectedTopics;
                //    scope.selectedTopicsB = angular.copy(scope.selectedTopics);
                //    console.log(APIService.topics);
                //    console.log(APIService.selectedTopics);
                //    var d = new Date();
                //    if (APIService.topicsComputed || d.getTime() >= time) {
                //        console.log(scope.topics);
                //        console.log(scope.selectedTopics);
                //        clearInterval(interval);
                //    }
                //}, 1000);

                scope.filterTopics = ""
                scope.saveAllDetails = function () {

                    var data = {
                        gender: $rootScope.userLoggedInDetails.gender,
                    }
                    if ($rootScope.userLoggedInDetails.new_username && ($rootScope.userLoggedInDetails.username!==$rootScope.userLoggedInDetails.new_username)){
                        data.username = $rootScope.userLoggedInDetails.new_username;
                        if(data.username.toLocaleLowerCase().indexOf('stichio') > -1){
                            scope.username_error = true;
                            return;
                        }
                        else{
                            scope.username_error=false;
                        }
                    }
                    if($rootScope.userLoggedInDetails.bio_change) {
                        $rootScope.userLoggedInDetails.bio_change = false;
                        $rootScope.userLoggedInDetails.user_bio = $rootScope.userLoggedInDetails.user_bio_temp || "";
                        data.user_bio=$rootScope.userLoggedInDetails.user_bio;
                        console.log("data.user_bio", data.user_bio)
                    }
                    if ($rootScope.userLoggedInDetails.phone_num_change) {
                        $rootScope.userLoggedInDetails.phone_num_temp = $rootScope.userLoggedInDetails.phone_num_temp || ""
                        data.phone_num = $rootScope.userLoggedInDetails.phone_num_temp.toString();
                        console.log("data.phone_num", data.phone_num)
                        if (data.phone_num.length >0 && data.phone_num.length < 10){
                            scope.phone_num_error="invalid phone number"
                            return;
                        }
                    }


                    $("#editProfileSaveBtn").hide();
                    $("#editProfileLoadingIcon").show();
                    console.log(data);
                    scope.genderSaved = true;
                    if (scope.profilePicSet == true) {
                        scope.uploadProfilePic();
                        if (randomInt){
                            data.profile_pic = "avatars/" + $rootScope.userLoggedInDetails.userId + "_" + randomInt+".jpg"
                        }
                        APIService.updateProfileInfo(data).then(function (results) {
                            commonService.showMessage("Profile Details Saved");

                            APIService.getLoggedInUserDetails(true).then(function (res) {
                                $("#editProfileSaveBtn").show();
                                $("#editProfileLoadingIcon").hide();
                                DEBUG && console.log('logged in user data........', res);

                                $rootScope.userLoggedInDetails = res;
                                // scope.home.userDetails = res;
                                $rootScope.userDetailsGlobal = res
                                scope.genderB = angular.copy($rootScope.userLoggedInDetails.gender);
                                // $rootScope.userLoggedInDetails.avatar += "?" + new Date().getTime();
                                scope.profilePicSet = false;
                                scope.croppedImage = ""
                                scope.image.imageDataURI = ""
                                scope.image.resImageDataURI = ""
                                resultImageGlobal = ""
                                scope.fileName = ""
                                // $rootScope.userDetailsGlobal = data;
                                // scope.userProfile.userDetails.avatar += "?"+new Date().getTime();
                                // scope.home.userDetails.avatar += "?"+new Date().getTime();
                                // alert('PROFILE DATA UPDATED')
                                console.log("appended", $rootScope.userLoggedInDetails);
                                 $rootScope.userLoggedInDetails.avatar += "?" + new Date().getTime();
                                //$rootScope.$apply();
                                // APIService.userDetailsGlobal.avatar += "?" + new Date().getTime();
                                // console.log(" APIService.userDetailsGlobal.avatar", APIService.userDetailsGlobal.avatar);
                            });
                            // $rootScope.userLoggedInDetails.avatar = scope.croppedImage
                            // alert($rootScope.userLoggedInDetails.avatar)

                            console.log(results)
                            // console.log(" APIService.userDetails.avatar", APIService.userDetails.avatar);
                        });
                    } else {

                        APIService.updateProfileInfo(data).then(function (results) {
                            commonService.showMessage("Profile Details Saved")
                            console.log("result", results)
                            var phone_temp;
                            if (results.num_error) {
                                scope.phone_num_error = results.num_error
                                phone_temp=angular.copy($rootScope.userLoggedInDetails.phone_num_temp)
                            }
                            else{
                                if ($rootScope.userLoggedInDetails.phone_num_change) {
                                    $rootScope.userLoggedInDetails.phone_num = $rootScope.userLoggedInDetails.phone_num_temp;
                                }
                                $rootScope.userLoggedInDetails.phone_num_change = false;
                                scope.phone_num_error=null;
                                phone_temp=null;

                            }
                             $("#editProfileSaveBtn").show();
                                $("#editProfileLoadingIcon").hide();
                            APIService.getLoggedInUserDetails(true).then(function (data) {
                                $rootScope.userLoggedInDetails = data;
                                $rootScope.userLoggedInDetails.phone_num_temp=phone_temp;
                                DEBUG && console.log('logged in user data', data, phone_temp, $rootScope.userLoggedInDetails.phone_num_temp);
                                scope.genderB = angular.copy($rootScope.userLoggedInDetails.gender);
                                scope.croppedImage = ""
                                scope.image.imageDataURI = ""
                                scope.image.resImageDataURI = ""
                                resultImageGlobal = ""
                            });
                        });

                    }
                    $rootScope.userLoggedInDetails.name_change=false;
                };

                scope.editEmail = false;
                scope.newEmail = {}
                scope.newEmail.email = ""

                scope.emailError = false

                scope.passwordChangeReqStarted = false
                scope.changePassword = {
                    old_password: "",
                    new_password: ""
                }
                scope.editEmailDet = {
                    passCheck: "",
                    newEmail: ""
                }
                scope.genderChanged = false;
                scope.genderSaved = false;
                scope.$watch("loggedInUserDetails.gender", function (newValue, oldValue) {
                    if (newValue == scope.genderB) {
                        scope.genderChanged = false;
                    } else {
                        scope.genderChanged = true;
                    }
                });
                scope.emailReqSent = false
                scope.changeProfilePicReq = false;

                scope.changeProfilePic = function () {
                    scope.changeProfilePicReq = true;
                    // alert('0')
                    $('.header').html('Change Profile Picture');
                    if (scope.passwordChangeReqStarted == true) {
                        scope.cancelReq();

                    }
                    if (scope.editEmailReqStarted == true) {
                        scope.closeEditEmailAddress();

                    }
                    // clearing input field
                    $('.edit-profile-clear').val('')
                    $('.edit-profile-clear').trigger('input')
                    $('.clear_input').hide()
                }

                scope.displayProfileEditPage = function () {
                    scope.changeProfilePicReq = false;
                    $('.header').html('Edit Profile');
                    scope.image.imageDataURI = ""
                    scope.image.resImageDataURI = ""
                    scope.fileName = ""
                }

                function bindValidation() {
                    $(".edit-profile-popup .edit-profile-email").blur(function () {
                        scope.editEmail = false;
                        scope.emailError = false;
                        $('.email-error').html("");
                        scope.$apply();
                    });

                    var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                    function checkEmail() {
                        if (scope.newEmail.email == "") {
                            scope.emailError = true;
                            $('.email-error').html("Please enter an email");
                            console.log('empty');

                            $('.edit-profile-popup .edit-profile-email').css("border-color", "red");
                            $('.email-error').html("Please enter an email");
                        } else if (scope.newEmail.email != "" && emailRegEx.test(scope.newEmail.email) == false) {
                            scope.emailError = true;
                            $('.email-error').html("Invalid Email");
                            $('.edit-profile-popup .edit-profile-email').css("border-color", "red");
                        } else {
                            scope.emailError = false;
                            $('.email-error').html("");
                            displayVerifyLinkPopup();
                            scope.verificationMessage = "To change registered email address, please confirm on the link sent to your current registered email address";
                        }
                    }

                    $('.edit-profile-popup .edit-profile-email').keypress(function (e) {
                        if (e.which == 13) {
                            checkEmail();
                            setTimeout(function () {
                                checkEmail();
                            }, 20);
                        }
                    });
                }

                scope.editEmailAddress = function () {
                    scope.editEmail = true;
                    scope.newEmail.email = scope.userProfile.userDetails.email;
                    setTimeout(function () {
                        bindValidation();
                    }, 20);
                    $('.edit-profile-popup .edit-profile-email').focus();

                }
                scope.editEmailAddress1 = function () {
                    if (scope.passwordChangeReqStarted == true) {
                        scope.passwordChangeReqStarted = false;
                    }
                    scope.editEmailReqStarted = true;
                    // $('.email-ellipsis').css({
                    //     "max-width": "none",
                    //     "text-ellipsis": "initial"
                    // });
                    scope.newEmail.email = $rootScope.userLoggedInDetails.email;
                }
                scope.checkEmail1 = function () {
                    var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (scope.editEmailDet.passCheck == "" || scope.editEmailDet.newEmail == "") {
                        $('.email-error-msg').html("Enter all details");
                    }
                    else {
                        if (emailRegEx.test(scope.editEmailDet.newEmail) == false) {
                            $('.email-error-msg').html("Enter a valid email");
                        }
                        else {
                            var data = {
                                password: scope.editEmailDet.passCheck,
                                new_email: scope.editEmailDet.newEmail
                            }
                            console.log(data);
                            $('.email-loading-div').css('display', 'block');
                            $('.change-email-btn').html("");

                            APIService.updateEmail(data).then(function (results) {
                                if (results.data.message == "wrong password") {
                                    $('.email-error-msg').html("Enter correct password");
                                    $('.email-loading-div').css('display', 'none');
                                    $('.change-email-btn').html("Change");
                                } else if (results.data.message == "same email sent") {
                                    $('.email-error-msg').html("You have entered same email");
                                    $('.email-loading-div').css('display', 'none');
                                    $('.change-email-btn').html("Change");
                                } else if (results.data.message == "user with given email already exists") {
                                    $('.email-error-msg').html("A user with this email address is already registered with STICHIO");
                                    $('.email-loading-div').css('display', 'none');
                                    $('.change-email-btn').html("Change");
                                } else {
                                    $('.email-loading-div').css('display', 'none');
                                    $('.change-email-btn').html("Change");
                                    scope.emailReqSent = true;
                                    scope.verificationMessage = "Your registered email has been changed.";
                                    window.localStorage.setItem('logged_in', false);
                                    loginService.logout().then(function () {
                                        // localStorage.setItem("credChanged", true);
                                        $cookies.put("credChanged","true");
                                        window.location.href = "";
                                    });

                                }
                            });
                        }
                    }
                }
                scope.closeEditEmailAddress = function () {
                    scope.editEmailReqStarted = false;
                    scope.editEmailDet.newEmail = "";
                    scope.editEmailDet.passCheck = "";
                    $('.email-error-msg').html("");
                    $('.email-ellipsis').css({
                        "max-width": "110px",
                        "text-ellipsis": "ellipsis"
                    });
                }

                function displayVerifyLinkPopup() {
                    scope.emailReqSent = true;
                    scope.verificationMessage = "To change registered email address, please confirm on the link sent to your current registered email address"
                }
                scope.allPasswordChecks = false;
                scope.$watch('changePassword.new_password', function (newValue, oldValue) {
                    var length = newValue.length;
                    var lengthCheck = false;
                    var numCheck = false;
                    var letterCheck = false;
                    if (length >= 8) {
                        $('.password-words p:first-child i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:first-child i').css('color', 'green');
                        lengthCheck = true;
                    } else {
                        $('.password-words p:first-child i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:first-child i').css('color', 'grey');
                        lengthCheck = false;
                    }
                    var numMatches = newValue.match(/\d+/g);
                    if (numMatches != null) {
                        $('.password-words p:nth-child(2) i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:nth-child(2) i').css('color', 'green');
                        numCheck = true;
                    } else {
                        $('.password-words p:nth-child(2) i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:nth-child(2) i').css('color', 'grey');
                        numCheck = false;
                    }
                    var letterMatches = newValue.match(/[a-zA-Z]+/g);
                    if (letterMatches != null) {
                        $('.password-words p:last-child i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:last-child i').css('color', 'green');
                        letterCheck = true;
                    } else {
                        $('.password-words p:last-child i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:last-child i').css('color', 'grey');
                        letterCheck = false;
                    }
                    if (lengthCheck && numCheck && letterCheck) {
                        scope.allPasswordChecks = true;
                    }
                    else {
                        scope.allPasswordChecks = false;
                    }
                    console.log(scope.allPasswordChecks)

                });
                scope.resetPassword = function () {
                    if (scope.userHasPassword == "true") {
                        if (scope.changePassword.old_password != "" && scope.changePassword.new_password != "") {
                            if (scope.allPasswordChecks == true) {
                                $('#resetPasswordButton').html("");
                                $('.password-loading-div').show();
                                loginService.updatePassword(scope.changePassword).then(function (result) {
                                    if (result.data.message == "wrong password") {
                                        $('.password-loading-div').hide();
                                        $('#resetPasswordButton').html("Change");
                                        $('.error-msg-cp').html("Enter correct password")
                                        $('.edit-profile-password:first-child').css('border-color', 'red')
                                    }
                                    else {
                                        //password changed successfully
                                        //logging out user
                                        window.localStorage.setItem('logged_in', false);
                                        localStorage.setItem("credChanged", true);
                                        // loginService.logout().then(function (results) {
                                        //     window.location.href = "";
                                        // })
                                    }
                                });
                            } else {
                                $('.error-msg-cp').html("Please enter stronger password")
                                $('.edit-profile-password:last-child').css('border-color', 'red')
                            }
                        } else {
                            $('.error-msg-cp').html("Enter all details")
                            $('.edit-profile-password').css('border-color', 'red')
                        }
                    }
                    if(scope.userHasPassword == "false"){
                        if(scope.changePassword.new_password != ""){
                            if(scope.allPasswordChecks == true){
                                loginService.updatePassword(scope.changePassword).then(function (result) {
                                    window.localStorage.setItem('logged_in', false);
                                    localStorage.setItem("credChanged", true);
                                });
                            }else{
                                $('.error-msg-cp').html("Please enter stronger password");
                                $('.edit-profile-password:last-child').css('border-color', 'red')
                            }
                        }else{
                            $('.error-msg-cp').html("Enter all details")
                            $('.edit-profile-password').css('border-color', 'red')
                        }
                    }
                }
                scope.requestNewPassword = function () {
                    //displayVerifyLinkPopup();
                    //scope.verificationMessage = "To reset password, please go to the verification email that has been sent to your registered Email ID";
                    scope.passwordChangeReqStarted = true

                };
                scope.cancelReq = function () {
                    scope.passwordChangeReqStarted = false
                    $('.error-msg-cp').html("")
                    $('.edit-profile-password:first-child').css('border-color', '#53585f')
                    scope.changePassword.old_password = ""
                    scope.changePassword.new_password = ""
                }
                resultImageGlobal = "";
                scope.changeOnFly = false;
                scope.size = 'small';
                scope.type = 'circle';
                scope.image = {
                    imageDataURI: "",
                    resImageDataURI: ""
                };
                scope.croppedImage = "";
                scope.resImgFormat = 'image/png';
                scope.resImgQuality = 1;
                scope.selMinSize = 100;
                scope.resImgSize = 200;
                //scope.aspectRatio=1.2;
                scope.onChange = function ($dataURI) {
                    console.log('onChange fired');
                };
                scope.onLoadBegin = function () {
                    console.log('onLoadBegin fired');
                };
                scope.onLoadDone = function () {
                    console.log('onLoadDone fired');
                };
                scope.onLoadError = function () {
                    console.log('onLoadError fired');
                };
                scope.selectProfilePic = function (files, errFiles, images) {
                    if (files.length > 0) {
                        var item = files[0];
                        scope.fileName = item.name;
                        console.log(item);
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            scope.$apply(function () {
                                scope.image.imageDataURI = event.target.result;
                                resultImageGlobal = scope.image.imageDataURI;
                            });
                        };
                        reader.readAsDataURL(item);
                    }

                };
                scope.profilePicSet = false;
                scope.profilePicSaved = false;
                scope.setProfilePic = function () {
                    scope.profilePicSet = true;
                    scope.croppedImage = angular.copy(scope.image.resImageDataURI);
                    scope.displayProfileEditPage();
                    scope.saveAllDetails();
                }

                scope.uploadProfilePic = function () {
                     randomInt=getRandomInt(1,1000);
                    scope.jsonPolicy = scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "stichio-test"},\n    ["starts-with", "$key", "$filename"],\n    {"acl": "public-read"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
                    scope.jsonPolicy = scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
                    var filename = $rootScope.userLoggedInDetails.userId + "_" + randomInt+'.jpg';
                    var filenameurl = 'https://eb-stichio.s3.amazonaws.com/avatars/' + filename
                    DEBUG && console.log('filenameURL', filenameurl)
                    var storeAuth = $http.defaults.headers.common.Authorization;
                    delete $http.defaults.headers.common.Authorization;
                    var file = dataURItoBlob();
                    console.log(file)
                    file.upload = Upload.upload({
                        url: 'https://eb-stichio.s3.amazonaws.com/', //S3 upload url including bucket name
                        method: 'POST',
                        data: {
                            key: 'avatars/' + filename,
                            AWSAccessKeyId: scope.AWSAccessKeyId,
                            acl: 'public-read',
                            policy: scope.policy,
                            signature: scope.signature,
                            'Content-Type': file.type === null || file.type === '' ? 'application/octet-stream' : file.type,
                            filename: filename,
                            file: file
                        },
                    });
                    $http.defaults.headers.common.Authorization = storeAuth;
                };

                var dataURItoBlob = function () {
                    dataURI = scope.croppedImage;
                    var binary = atob(dataURI.split(',')[1]);
                    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
                    var array = [];
                    for (var i = 0; i < binary.length; i++) {
                        array.push(binary.charCodeAt(i));
                    }
                    return new Blob([new Uint8Array(array)], {type: mimeString});
                };

                // });
            }, 3000);
        }
    }
}]);