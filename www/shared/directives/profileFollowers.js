
angular.module('stichio').directive('profileFollowers', ['$rootScope', '$http', 'APIService', 'commonService', '$filter', 'Upload', '$timeout', '$window', function ($rootScope, $http, APIService, commonService, $filter, Upload, $timeout, $window) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/profileFollowers.html?0.04',
        link: function (scope, element, attribute) {
            //commonService.setLoggedInStatus();
            if (!$rootScope.userLoggedIn) {
                return;
            }


            scope.unFollowUser = function (User) {
                // scope.user.showfollowDropdown = false
                User.is_following = false
                User.followStatus = false
                APIService.unFollowUser(User.userId).then(function () {

                });
            }

            //APIService.scrollOff();


            $timeout(function () {
                if ($rootScope.isLoggedIn) {


                    scope.searchFollowers = "";
                    scope.FollowerCount ="";

                    scope.FolloweeCount ="";
                    scope.userFriends = [];
                    scope.userFriendsFollowing = [];
                    var index,count, sug_flag, $win;
                    index=0;
                    count=10

                    var loadUserFollowers = function () {

                        scope.loadingFollowers=true;
                        APIService.getAllFollowers(scope.userProfile.userDetails.userId, index, count).then(function (data) {
                            scope.loadingFollowers=false;

                            console.log('followerss', data);
                            scope.userFriends = scope.userFriends.concat(data.Followers);
                            sug_flag=false;
                            setTimeout(function () {

                                index += count;
                                if (data.Followers.length === 0) {

                                    $win.off('scroll.followersScroll');
                                }
                            }, 0)

                        })


                    }
                    var loadUserFollowing = function () {

                        scope.loadingFollowers=true;
                        APIService.getUserFollowees(scope.userProfile.userDetails.userId, index, count).then(function (data) {
                            scope.loadingFollowers=false;

                            console.log('followerss', data);
                            scope.userFriends = scope.userFriends.concat(data.Followees);

                            setTimeout(function () {

                                index += count;
                                sug_flag = false;
                                if (data.Followees.length === 0) {

                                    $win.off('scroll.followersScroll');
                                }
                            }, 0)


                        })

                    }
                    var onScrollFn = function () {
                        console.log("$win", $win)
                        // do the onscroll stuff you want here
                        if (!sug_flag && ($win.scrollTop() > ($win[0].scrollHeight - 1000))) {
                            sug_flag = true;
                            console.log("scope.selectedOptFoll", scope.selectedOptFoll)
                            if (scope.selectedOptFoll=='follower'){
                                loadUserFollowers();
                            }
                            else{
                                loadUserFollowing();
                            }
                        }
                    };
                    scope.switchFollowers = function (popupTitle) {
                        index=0;
                        count=10
                        scope.userFriends=[]
                        scope.selectedOptFoll=popupTitle;
                        if ($win){
                            $win.scrollTop(0);
                        }
                        if (popupTitle == "follower"){
                            loadUserFollowers();
                        }
                        else{
                            loadUserFollowing();
                        }
                        setTimeout(function () {
                            $win = $('.user_followers');
                            $win.off('scroll.followersScroll');
                            commonService.infiniteScroll($win, onScrollFn, 'scroll.followersScroll');
                        },100);

                    }
                    $rootScope.openFollowerPopup = function (opt) {
                        commonService.openPopup('user_followers')

                        scope.switchFollowers(opt);

                    }
                    $rootScope.closeFollowerPopup = function () {
                        commonService.closePopup('user_followers');
                        scope.userFriends = [];
                    }


                }
            }, 1000);
        }
    }
}]);
