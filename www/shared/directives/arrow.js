angular.module('stichio')
.directive('arrow', [function () {
	return {
		restrict: 'A',
		link: link
	};
	function link(scope, elem, attrs) {
		var side = attrs.side || 'top',
			color = attrs.color || '#3987cc',
			hd = attrs.hd || 10,
			vd = attrs.vd || 9;
		elem.width(0);
		elem.height(0);
		switch(side) {
			case 'top': {
				elem.css('border-top', vd + 'px solid ' + color);
				elem.css('border-left', (hd / 2) + 'px solid transparent');
				elem.css('border-right', (hd / 2) + 'px solid transparent');
				elem.css('border-bottom', 'none');
				break;
			}
			case 'right': {
				elem.css('border-right', vd + 'px solid ' + color);
				elem.css('border-top', (hd / 2) + 'px solid transparent');
				elem.css('border-bottom', (hd / 2) + 'px solid transparent');
				elem.css('border-left', 'none');
				break;
			}
			case 'bottom': {
				elem.css('border-bottom', vd + 'px solid ' + color);
				elem.css('border-left', (hd / 2) + 'px solid transparent');
				elem.css('border-right', (hd / 2) + 'px solid transparent');
				elem.css('border-top', 'none');
				break;
			}
			case 'left': {
				elem.css('border-left', vd + 'px solid ' + color);
				elem.css('border-top', (hd / 2) + 'px solid transparent');
				elem.css('border-bottom', (hd / 2) + 'px solid transparent');
				elem.css('border-right', 'none');
				break;
			}
			default: {
				elem.css('border-top', vd + 'px solid ' + color);
				elem.css('border-left', (hd / 2) + 'px solid transparent');
				elem.css('border-right', (hd / 2) + 'px solid transparent');
				elem.css('border-bottom', 'none');
			}
		}
	}
}]);