angular.module('stichio').directive('loginPopup', ['$rootScope', 'loginService', 'commonService' , '$window', '$cookies', '$state','$stateParams','$http', '$ionicPlatform', '$location', function($rootScope, loginService, commonService , $window, $cookies, $state,$stateParams,$http, $ionicPlatform, $location) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/loginPopup.html?0.02',
        link:function(scope, element, attribute){
            var displayLoginTimer=null;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                $rootScope.isMobile = true;
                console.log('isMobile', $rootScope.isMobile)
            } else {
                $rootScope.isMobile = false;
                console.log('isMobile', $rootScope.isMobile)
            }
            setTimeout(function(){
                try {
                    var script = document.createElement('script');
                    script.src = "js_external/fd08e56826.js";
                    document.getElementsByTagName('body')[0].appendChild(script);
                } catch (e) {
                }
                try {
                    var script = document.createElement('script');
                    script.src = "js_external/fbEvents.js";
                    document.getElementsByTagName('body')[0].appendChild(script);
                } catch (e) {
                }
            },10000);
            var showLoginPopupScrollHt = 5000
            //if (commonService.loggedIn === false) {
            //    $(window).on('scroll.login', loginPrompt);
            //}
            function loginPrompt () {
                if (commonService.loggedIn === false && $(window).scrollTop() > showLoginPopupScrollHt) {
                     commonService.analytics('event', {category : 'Login', action : 'LoginPopupOpened', label : 'Scrolled'});
                    //$('body').css('position', 'fixed')
                    $('.not-now-login').hide();
                    $rootScope.displayloginpopup();

                }
            }

            $(window).resize(function () {
                $('.login-popup').scrollTop(1500);
            });
            //$rootScope.new_user=true;
            //commonService.new_user=true;

            if(localStorage.getItem("logged_in") === "true") {


                if ($cookies.get('sti_authenticationToken') && $cookies.get('userId')) {
                    console.log("$cookies.get('sti_authenticationToken')", $cookies.get('sti_authenticationToken'), $cookies.get('userId'))
                    $rootScope.isLoggedIn = true
                    if($cookies.get("enterReferralCode")){
                        $rootScope.enterReferralCode=true;
                    }
                    //ga('set', 'userId', $cookies.get('userId'));
                    try {
                        if ($cookies.get('new_user')) {
                            // $cookies.put("takeDareHelpBox", "true")
                            // $cookies.put("watchDareHelpBox", "true")
                            // $cookies.put("exploreHelpBox", "true")
                            // $cookies.put("exploreSearchHelpBox", "true")
                            $cookies.put("exploreHelpBoxHome", "true")
                            commonService.new_user = $cookies.get('new_user')
                            $rootScope.new_user=true;
                            $cookies.remove('new_user')
                            var vals = commonService.new_user.split(":")
                            var curr_page = vals[1]
                            var type = vals[0]
                            if (!curr_page){
                                curr_page = "/"
                            }
                            var expireDate = new Date();
                            expireDate.setDate(expireDate.getDate() + (5/(24*60))); //One day
                            if(type.indexOf("Facebook")>-1){
                                $rootScope.enterReferralCode=true;
                                // Setting a cookie
                                $cookies.put("enterReferralCode", "true",{'expires': expireDate});
                            }
                            $cookies.put("newUserProfileHelp", "true",{'expires': expireDate});
                            $cookies.put("newUserHomeHelp", "true",{'expires': expireDate});
                            $cookies.put("newUserExploreHelp", "true",{'expires': expireDate});

                            console.log("new user", type, curr_page)
                            commonService.analytics('event', {
                                category: 'SignUp',
                                action: curr_page,
                                label: type
                            })

                        }
                    } catch (e) {
                    }
                }
                else {
                    //if (commonService.loggedIn === false)
                    //    $(window).on('scroll.login', loginPrompt)
                    $rootScope.isLoggedIn = false
                    loginService.logout();
                }
            }
            // loginService.authenticateUserToken().then(function(authenticationResult){
            // alert("success")
            // DEBUG &&  console.log("authenticationResultuser.status");
            // DEBUG &&  console.log(authenticationResult);
            scope.loggedInUserId = commonService.loggedInUserId;
            $rootScope.loggedIn = commonService.loggedIn;

            commonService.setLoggedInStatus();
            scope.userLoggedIn = commonService.userLoggedIn;

            //alert(scope.loggedIn);

            // if($cookies.get('stichio')){
            //     scope.loggedIn = true;
            //     commonService.setLoggedInUserId();
            // }else{
            //     scope.loggedIn = false;
            // }

            // scope.loggedIn = true;
            console.log("state:current", $state.current)
            if($state.current.name == "reset"){
                $('#login-index-popup').hide();
                DEBUG &&  console.log("$stateParams.token "  + $stateParams.token);
                loginService.getPasswordResetUser($stateParams.token).then(function(result){
                    DEBUG &&  console.log("reset esu")
                    DEBUG &&  console.log(result);
                    if (typeof result !== "undefined") {
                        $rootScope.displayloginpopup();
                    }
                })

            }

            $("body").click(function(){
                $(".password-info-msg").css("display", "none");
                if(popupShowCount < 3){
                    document.body.style.overflow = "visible";
                    scope.form.$setPristine();
                    if(resetPassSuccess){
                        clearTimeout(displayLoginTimer);
                        $('.login-popup > div > div').hide();
                        $('.sign-in-cnt').show();
                        $('.actions-cnt').show();
                        $('.action-btns-cnt').show();
                    }else{
                        $('.login-popup').removeClass('target');
                    }

                }
            });

            function setoffset(){

                var height = $(window).height();
                //var loginboxht = $('.sign-in-cnt').height()
                //console.log('loginboxht', loginboxht)
                var marginTop = (height-398)/2;
                if(marginTop > 50) {
                    marginTop += 'px';
                    $('.login-popup .sign-in-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .sign-up-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .forgot-pass-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .reset-pass-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                }
                else{
                    marginTop='50px';
                    $('.login-popup .sign-in-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .sign-up-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .forgot-pass-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                    $('.login-popup .reset-pass-cnt').css('margin-top', marginTop).css('margin-bottom',marginTop);
                }
                //var windowwidth = $(window).width();
                //if(windowwidth > 640){
                //    var stiMainBtnWidth = '130px';
                //}else{
                //    var stiMainBtnWidth = Math.round(windowwidth / 4.2).toString() + 'px';
                //}
                //$('.sti-main-btn').css('width',stiMainBtnWidth);


            }

            function scalesocialicons(){
                var width = $(window).width();
                var externalDivWidth = (0.32078125*width).toFixed(2) + 'px';
                var imageDim = (0.1234375*width).toFixed(2) + 'px';
                $('.external-login-cnt > div').css('width',externalDivWidth);
                $('.external-login-cnt  img').css('width',imageDim).css('height',imageDim);
            }
            if( /Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                $("input").focus(function(e){
                    if($(e.target).is("#chkKeepLoggedIn")){
                    }else{
                        // var marginTop='-100px';
                        // $('.login-popup .sign-in-cnt').css('margin-top', marginTop);
                        // $('.login-popup .sign-up-cnt').css('margin-top', marginTop);
                        // $('.login-popup .forgot-pass-cnt').css('margin-top', marginTop);
                        // $('.login-popup .reset-pass-cnt').css('margin-top', marginTop);
                    }
                });
                $("input").blur(function(){
                    setoffset();
                });
            }
            setoffset();



            // $(".container").click(function(){
            //     alert(1331);
            // });

            $(".password-info-btn").hover(function(){
                var buttonPosition = $(this).offset();
                $(".password-info-msg").css('top', buttonPosition.top + 3 + 'px').css('left', buttonPosition.left +  22 +  'px').css("display", "block");
            }, function(){
                $(".password-info-msg").css("display", "none");
            });

            // $(".password-info-btn").click(function(e){
            //     e.stopPropagation();
            //     var buttonPosition = $(this).offset();
            //     $(".password-info-msg").css('top', buttonPosition.top + 3 + 'px').css('left', buttonPosition.left +  22 +  'px').css("display", "block");
            // });

            // scope.$on('$stateChangeStart',
            //      function(event, toState, toParams, fromState, fromParams){
            //      	if(scope.loggedIn == false){
            //      		DEBUG &&  console.log("preventingChange");
            //          event.preventDefault();
            //           $rootScope.displayloginpopup($event);
            //        }
            //          // transitionTo() promise will be rejected with
            //          // a 'transition prevented' error
            // })

            $('body').on('click', ".create-frame-main-btn, .stich-content, .todayspicks-content, .frame-content, .sti-cursor:not('.exclude'), button:not('.exclude'), a:not('.exclude'), .home-icon-box", function (e) {
                //commonService.setLoggedInStatus();
                scope.userLoggedIn = commonService.userLoggedIn;
                if($rootScope.userLoggedIn == false){
                    e.stopPropagation();
                    e.preventDefault();
                    DEBUG &&  console.log("not logged innnnnnnnnnnnnnnnnnbj,");
                    if(e.target.id != "btnHeaderLogin"){
                        $rootScope.displayloginpopup();
                    }
                }
            });
            function storageChange(event) {
                //scope.loggedIn = false;
                console.log("storageEvent", event)
                if (event.key=="logged_in" ) {
                    $window.location.reload();
                    //loginService.logout();

                }
                //$window.location.reload();
            }

            window.addEventListener('storage', storageChange, false);

            // Prevent events from getting pass .popup
            $(".header-login-btn").click(function(e){
                e.stopPropagation();
            });

            $('.login-popup > div').click(function(e){
                e.stopPropagation();
            });

            scope.loginDetails = {
                email: "",
                password: "",
                emailErrorMsg: "",
                passwordErrorMsg: "",
                //confirmPassword: "",
                //confirmPasswordErrorMsg: "",
                gender:"female",
                displayName: "",
                displayNameErrorMsg: ""
            };

            function clearLoginErrors(){
                scope.loginDetails.emailErrorMsg = "";
                scope.loginDetails.passwordErrorMsg = "";
            }

            scope.displayResetPassword = function(){
                $('.sign-in-cnt').hide();
                $('.forgot-pass-cnt').show();
            }

            this.displayLoginPop = function(){
                $rootScope.displayloginpopup();
            };


            scope.takeToHome = function(){
                window.location = "/"


            };
            var popupShowCount = 0;

            function clearLoginData(){
                scope.loginDetails.email= "";
                scope.loginDetails.password= "";
                scope.loginDetails.emailErrorMsg= "";
                scope.loginDetails.passwordErrorMsg= "";
                //scope.loginDetails.confirmPassword= "";
                scope.loginDetails.emailErrorMsg= "";
                //scope.loginDetails.confirmPasswordErrorMsg= "";
                scope.loginDetails.displayName= "";
                scope.loginDetails.displayNameErrorMsg= ""
            }

            scope.alreadyRegistered = function(){
                setTimeout(function () {
                    $('.sign-in-cnt').show();
                }, 100);
                $('.sign-up-cnt').hide();
                clearLoginErrors();
                //scope.$apply();
            }

            $rootScope.displayloginpopup = function(e){

                DEBUG &&  console.log("display login")
                popupShowCount++;
                clearLoginData();

                $(".loading-div1").css('display','none');

                $('.login-popup > div > div').hide();

                if($state.current.name != "reset"){
                    $('.sign-in-cnt').show();
                    $('.actions-cnt').show();
                    $('.action-btns-cnt').show();
                }else{
                    $('.reset-pass-cnt').show();
                }

                scope.$watch('loginDetails.email', function(newValue, oldValue) {
                    scope.loginDetails.emailErrorMsg= "";
                    $(".sign-up-input").prop('disabled', false);
                    $(".email-error-msg span").css("border-bottom", "none").removeClass("sti-cursor")
                    scope.loginDetails.emailErrorMsg= "";
                    $(".already-registered").hide();
                });

                scope.$watch('loginDetails.displayName', function(newValue, oldValue) {
                    scope.loginDetails.displayNameErrorMsg= ""
                });

                /*scope.$watch('loginDetails.confirmPassword', function(newValue, oldValue) {
                 if(scope.loginDetails.password != newValue && scope.loginDetails.password != ""){
                 scope.loginDetails.confirmPasswordErrorMsg = "Passwords not matching";
                 }else{
                 scope.loginDetails.confirmPasswordErrorMsg = "";
                 }
                 });*/

                $(".sign-up-email").blur(function(){
                    if(scope.loginDetails.email != ""){
                        var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        if(emailRegEx.test(scope.loginDetails.email) == false){
                            scope.loginDetails.emailErrorMsg = "Invalid email";
                        }else{
                            var signinDetails = {
                                email: scope.loginDetails.email,
                                password: ""
                            }
                            loginService.signin(signinDetails).then(function(result){
                                if(result.state == 3){
                                    $(".already-registered").show();
                                    $(".sign-up-input").prop('disabled', true);
                                    $(".email-error-msg span").css("border-bottom", "1px solid #cb0000").addClass("sti-cursor")
                                    scope.loginDetails.emailErrorMsg= "Already registered";
                                    $(".email-error-msg span").click(function(){
                                        scope.alreadyRegistered();
                                    })
                                }else if(result.state == 2){
                                }else if(result.state == 1){
                                }
                            });
                        }
                    }
                });

                $('.sign-in-password').keypress(function (e) {
                    if (e.which == 13) {
                        scope.signin();
                        return false;
                    }
                });

                /*scope.$watch('loginDetails.password', function(newValue, oldValue) {
                 scope.loginDetails.passwordErrorMsg= "";
                 var strongRegex = new RegExp("^(?=.{9,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
                 var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
                 var enoughRegex = new RegExp("(?=.{6,}).*", "g");
                 if (false == enoughRegex.test(newValue)) {
                 scope.strength = 1;
                 } else if (strongRegex.test(newValue)) {
                 scope.strength = 4;
                 } else if (mediumRegex.test(newValue)) {
                 scope.strength = 3;
                 } else {
                 scope.strength = 2;
                 }
                 if(newValue == ""){
                 scope.strength = 0;
                 }
                 var strengthPercent = scope.strength / 4 * 100;
                 $(".password-bar").css("width", strengthPercent + "%");
                 });
                 */scope.allPasswordChecks = false;
                scope.$watch('loginDetails.password',function(newValue,oldValue){
                    var length = newValue.length;
                    var lengthCheck = false;
                    var numCheck = false;
                    var letterCheck = false;
                    console.log(popupShowCount, "popupshowcount");
                    if(length >= 8){
                        $('.password-words p:first-child i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:first-child i').css('color','green');
                        lengthCheck = true;
                        // console.log("correct password.......");
                    }else{
                        $('.password-words p:first-child i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:first-child i').css('color','grey');
                        lengthCheck = false;
                    }
                    var numMatches = newValue.match(/\d+/g);
                    if (numMatches != null) {
                        $('.password-words p:nth-child(2) i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:nth-child(2) i').css('color','green');
                        numCheck = true;
                    }else{
                        $('.password-words p:nth-child(2) i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:nth-child(2) i').css('color','grey');
                        numCheck = false;
                    }
                    var letterMatches = newValue.match(/[a-zA-Z]+/g);
                    if (letterMatches != null) {
                        $('.password-words p:last-child i').removeClass("fa-times").addClass("fa-check");
                        $('.password-words p:last-child i').css('color','green');
                        letterCheck = true;
                    }else{
                        $('.password-words p:last-child i').removeClass("fa-check").addClass("fa-times");
                        $('.password-words p:last-child i').css('color','grey');
                        letterCheck = false;
                    }
                    if(lengthCheck && numCheck &&  letterCheck){
                        scope.allPasswordChecks = true;
                        scope.loginDetails.passwordErrorMsg = "";
                        clearLoginErrors();
                    }else if(popupShowCount > 1){
                        scope.allPasswordChecks = false;
                        //scope.loginDetails.passwordErrorMsg = "Please enter stronger password";
                    }


                });


                // document.body.style.overflow = "hidden";
                //var popupHeight = 67 * $('.login-popup > div').width() / 100;
                var loginMarginTop =  ($(window).height() - 550)/2;
                if(loginMarginTop > 20){
                    $('.login-popup > div').css('margin-top', loginMarginTop + 'px');
                }
                //$('.sign-in-cnt').css('margin-top', 21.75 * popupHeight / 100 + 'px');
                // $('.login-popup').addClass('target');
                commonService.openPopup('login-popup')
                commonService.analytics('event', {category : 'Login', action : 'LoginPopupOpened', label : popupShowCount});
                if (e)
                    e.stopPropagation()
                // $(".login-popup").click(function(){
                //     alert(13341);
                // });
            };

            $rootScope.closeLoginPopup = function(){
                //if(popupShowCount < 3){
                    // document.body.style.overflow = "visible";
                    if ($(window).scrollTop() > showLoginPopupScrollHt) { return 0; }
                    // $('.login-popup').removeClass('target');
                    commonService.closePopup('login-popup')

                //}
            };
            commonService.analytics('event', {category : 'Login', action : 'LoginPopupClosed', label : popupShowCount});

            scope.backToLogin = function(){

                $('.sign-up-cnt').hide();
                $('.reset-pass-cnt').hide();
                $('.forgot-pass-cnt').hide();
                setTimeout(function () {
                    $('.sign-in-cnt').show();
                }, 100);
                $('.reset-pass-cnt').hide();
            };

            function loginUser(userDetails, isSignin,numInterests){

                console.log(userDetails, "userdetails")
                if(userDetails.is_password_set == false){
                    // localStorage.setItem("uhasP","false");
                    $cookies.put("uhasP","false");
                }else{
                    // localStorage.setItem("uhasP","true");
                    $cookies.put("uhasP","true");
                }

                if(isSignin) {
                    // localStorage.setItem("uhasI","true");
                    loginService.setCookie("sti_authenticationToken", userDetails.token);
                    loginService.setCookie("userId", userDetails.userId);
                    loginService.clearCache();
                    commonService.setLoggedInUserId(userDetails.userId);
                    commonService.setLoggedInUserAvatar(userDetails.avatar);
                    //ga('set', 'userId', userDetails.userId); // Set the user ID using signed-in user_id.

                    try {

                        window.localStorage.setItem('logged_in', true);
                        //$cookies.put("logged_in","true")
                    }

                    catch (err) {
                        console.log(err, "localstorage error");
                        $cookies.put("logged_in", "true")
                    }
                    //return;
                    $rootScope.loggedIn = true;

                    scope.$broadcast('loggedIn', {myMsg: "loggedIn"});
                    if ($window.location.toString().indexOf("reset") > -1) {
                        //$window.location.reload();
                        //$state.go('home.stiches', {}, {reload: true});
                        $window.location.reload();

                    } else {

                        $window.location.reload();

                    }
                    //$window.location.reload();
                }
            }

            scope.logout = function(){
                loginService.logout().then(function(){
                    scope.$broadcast('loggedOut', {myMsg: "loggedOut"});
                    //if(commonService.loggedIn === false)
                    //    $(window).on('scroll.login', loginPrompt)
                },function(){
                    scope.$broadcast('loggedOut', {myMsg: "loggedOut"});
                })

                //$(".header-login-btn").show();

            };

            scope.authenticating = false;
            scope.login_bk = function (backend) {
                console.log('login_bk', backend);
                commonService.clickedAuth=true;

                if (backend == 'facebook') {


                    var fbLoginError = function(error){
                        console.log('fbLoginError', error);
                        setTimeout(function(){commonService.clickedAuth = false;}, 100)
                        //$ionicLoading.hide();
                    };

                   //$cordovaOauth.facebook("126738297428696", ["email"]).then(function(success) {
                   var fbLoginSuccess = function(success) {
                       if (!success.authResponse){
                            fbLoginError("Cannot find the authResponse");
                            return;
                        };
                     console.log(JSON.stringify(success));
                     console.log('in success in login bk fb', success);
                            var token = "Token " + success.authResponse.accessToken;
                            scope.authenticating = true;
                            var loginPromise = $http({
                                method: 'POST',
                                url: commonService.baseUrl + 'api/v1/socialLogin/' + backend + '/?device_reg_id=' +$rootScope.device_reg_id,
                                headers: {'Authorization': token},
                                data:{'device_reg_id' : $rootScope.device_reg_id}
                            });

                            // loginService.loginUser(loginPromise);
                            loginPromise.success(function (result) {
                                if (result.new_user){
                                    $cookies.put("new_user","Facebook:"+$location.url());
                                }
                                console.log(result);
                                commonService.setLoggedInUserId(result.userId);
                                $(window).off('scroll.login');
                                loginUser(result, true, result.interests_filled);
                                scope.authenticating = false;
                            });
                            loginPromise.error(function(result){
                                //alert('promise failed');
                                scope.authenticating = false;
                            });
                   }
                    facebookConnectPlugin.login(['email', 'public_profile', 'user_friends'], fbLoginSuccess, fbLoginError);
                    //OAuth.popup('facebook', function (error, success) {
                    //    if (error) {
                    //        console.log('error in login bk fb', error);
                    //    }
                    //    else {
                    //        console.log('in success in login bk fb', success);
                    //        var token = "Token " + success.access_token;
                    //
                    //        var loginPromise = $http({
                    //            method: 'POST',
                    //            url: commonService.baseUrl + 'api/v1/socialLogin/' + backend + '/',
                    //            headers: {'Authorization': token}
                    //        });
                    //
                    //        // loginService.loginUser(loginPromise);
                    //        loginPromise.success(function (result) {
                    //
                    //            console.log(result);
                    //            commonService.setLoggedInUserId(result.userId);
                    //
                    //            loginUser(result, true, result.interests_filled);
                    //        });
                    //        loginPromise.error(function(result){
                    //            //alert('promise failed');
                    //        });
                    //    }
                    //});
                } else if (backend == 'google') {
                    //if(localStorage.getItem("grt") === "true")
                    //{
                    //    google_auth = OAuth.popup('google', {
                    //    });
                    //}
                    //else {
                    //    google_auth = OAuth.popup('google', {
                    //        authorize: {
                    //            approval_prompt: 'force'
                    //        }
                    //    })
                    //}
                   //google_auth = OAuth.popup('google', {
                   //         authorize: {
                   //             approval_prompt: 'force'
                   //         }
                   //     })
                   // window.plugins.googleplus.disconnect();
                    window.plugins.googleplus.login({
                      'scopes' : 'email profile https://www.google.com/m8/feeds',
                      'webClientId': '298658191196-fo4n68kch0lrinuklovrkdbvdb9bb40q.apps.googleusercontent.com',
                        'offline': true,
                    }, function (success) {
                        if (!success) {
                            console.log('error in login bk g+', error);
                            scope.authenticating = false;
                            setTimeout(function(){commonService.clickedAuth = false;}, 100)
                        }
                        else {
                            console.log('success in login bk g+', success);
                            console.log("successss", success)
                            scope.authenticating = true;
                            var token = "Token " + success.idToken;
                            var loginPromise;
                            if (success.refresh_token) {
                                var refresh_token = success.refresh_token;
                                window.localStorage.setItem('grt', true);


                                loginPromise = $http({
                                    method: 'POST',
                                    url: commonService.baseUrl + 'api/v1/socialLogin/' + backend + '/?refresh_token=' + refresh_token + '&device_reg_id=' +$rootScope.device_reg_id,
                                    headers: {'Authorization': token},
                                    data:{'device_reg_id' : $rootScope.device_reg_id}
                                });
                            }
                            else if (success.serverAuthCode){
                                var serverAuthCode = success.serverAuthCode;


                                loginPromise = $http({
                                    method: 'POST',
                                    url: commonService.baseUrl + 'api/v1/socialLogin/' + backend + '/?serverAuthCode=' + serverAuthCode + '&device_reg_id=' +$rootScope.device_reg_id,
                                    headers: {'Authorization': token},
                                    data:{'device_reg_id' : $rootScope.device_reg_id}
                                });

                            }
                            else {
                                loginPromise = $http({
                                    method: 'POST',
                                    url: commonService.baseUrl + 'api/v1/socialLogin/' + backend+'/?device_reg_id=' +$rootScope.device_reg_id,
                                    headers: {'Authorization': token},
                                    data:{'device_reg_id' : $rootScope.device_reg_id}
                                });
                            }


                             //loginService.loginUser(loginPromise);
                            loginPromise.success(function (result) {
                                if (result.new_user){
                                    $cookies.put("new_user","Google:"+$location.url());
                                }
                                console.log(result);
                                commonService.setLoggedInUserId(result.userId);
                                $(window).off('scroll.login');
                                loginUser(result, true, result.interests_filled);
                                scope.authenticating = false;

                            });
                            loginPromise.error(function(result){
                                //alert('promise failed');
                                scope.authenticating = false;
                            });
                        }
                    },function(error) {
                     console.log(error);
                        scope.authenticating=false;
                   });
                }

            };

          $ionicPlatform.ready(function() { OAuth.initialize('8X-djdpMobZMAoYvtFTlcGKYSRg');});


            scope.signin = function(){
                if (!commonService.introVideoOpened) {
                    console.log("clicked sign in button")
                    var isValidEmail = true;
                    var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (scope.loginDetails.email == "") {
                        clearLoginErrors();
                        $('.sign-in-cnt').hide();
                        setTimeout(function () {
                            $('.sign-up-cnt').show();
                        }, 100);

                        $(".already-registered").hide();
                        isValidEmail = false;
                        commonService.analytics('event', {category: 'Login', action: 'SignUpOpened', label: popupShowCount});
                    } else if (emailRegEx.test(scope.loginDetails.email) == false) {
                        scope.loginDetails.emailErrorMsg = "Invalid email";
                        isValidEmail = false;
                    }

                    if (isValidEmail) {
                        var signinDetails = {
                            email: scope.loginDetails.email,
                            password: scope.loginDetails.password,
                            device_reg_id: $rootScope.device_reg_id
                        }
                        $(".loading-div1").css('display', 'inline-block');
                        $(".sign-in-btn").html('');

                        loginService.signin(signinDetails).then(function (result) {
                            $(".loading-div1").css('display', 'none');
                            $(".sign-in-btn").html('Login / Sign Up');
                            if (result.state == 3) {
                                if (scope.loginDetails.password == "") {
                                    scope.loginDetails.passwordErrorMsg = "Please enter password";
                                } else {
                                    scope.loginDetails.passwordErrorMsg = "Invalid email / password";
                                }
                            } else if (result.state == 2) {
                                clearLoginErrors();
                                $('.sign-in-cnt').hide();
                                setTimeout(function () {
                                    $('.sign-up-cnt').show();
                                }, 100);
                                $(".already-registered").hide();
                                commonService.analytics('event', {category: 'Login', action: 'SignUpOpened', label: popupShowCount});
                            } else if (result.state == 1) {
                                commonService.setLoggedInUserId(result.userId);
                                loginUser(result, true, result.interests_filled);
                                $(window).off('scroll.login');
                                $('.login-popup').removeClass('target');
                                $('#sign-in-form').submit(function (e) {
                                    e.preventDefault();
                                });
                            } else if (result.state == -2) {
                                $('.login-popup').removeClass("target");
                                $('body').addClass("prevent-scroll");
                                $('.reactivate-popup').addClass("target");
                            }
                        });
                    }
                }
            };



            scope.reactivateAccount = function(){

            };
            scope.noReactivateAccount = function(){
                $('.reactivate-popup').removeClass("target");
                $('.login-popup').addClass("target");
            };
            scope.signup = function(){
                clearLoginErrors();
                var showSignUpSucess = true;
                var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var gmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[com]{3}))$/;

                if(scope.loginDetails.email == ""){
                    scope.loginDetails.emailErrorMsg = "Please enter email";
                    showSignUpSucess = false;
                }else if(emailRegEx.test(scope.loginDetails.email) == false){
                    scope.loginDetails.emailErrorMsg = "Invalid email";
                    showSignUpSucess = false;
                }else if(emailRegEx.test(scope.loginDetails.email) == true){
                    if(scope.loginDetails.email.match(/@gmail./g)){
                        if(gmailRegEx.test(scope.loginDetails.email) == false){
                            scope.loginDetails.emailErrorMsg = "Invalid email";
                            showSignUpSucess = false;
                        }


                    }

                }

                if(scope.loginDetails.password == ""){
                    scope.loginDetails.passwordErrorMsg = "Please enter password";
                    showSignUpSucess = false;
                }

                if(scope.loginDetails.displayName == ""){
                    scope.loginDetails.displayNameErrorMsg = "Please enter display name";
                    showSignUpSucess = false;
                }
                if(scope.loginDetails.displayName.toLocaleLowerCase().indexOf('stichio') > -1){
                    scope.loginDetails.displayNameErrorMsg = "Oops! " + scope.loginDetails.displayName + " is not a valid username";
                    showSignUpSucess = false;
                }

                /*if(scope.loginDetails.confirmPassword == ""){
                 scope.loginDetails.confirmPasswordErrorMsg = "Please enter confirm password";
                 showSignUpSucess = false;
                 }else if(scope.loginDetails.password != scope.loginDetails.confirmPassword && scope.loginDetails.password != ""){
                 scope.loginDetails.confirmPasswordErrorMsg = "Passwords not matching";
                 showSignUpSucess = false;
                 }*/

                if(scope.allPasswordChecks == false){
                    showSignUpSucess = false;
                    scope.loginDetails.passwordErrorMsg = "Please enter stronger password";
                }

                if(showSignUpSucess){
                    var signupDetails = {
                        email: scope.loginDetails.email,
                        password: scope.loginDetails.password,
                        username: scope.loginDetails.displayName,
                        gender: scope.loginDetails.gender,
                        device_reg_id : $rootScope.device_reg_id


                    }

                    $(".loading-div1").css('display','inline-block');
                    $(".sign-up-btn").html('');

                    loginService.signup(signupDetails).then(function(result){
                        if(result.state == 2){
                            scope.loginDetails.emailErrorMsg = "Email already registered";

                            $(".loading-div1").css('display','none');
                            $(".sign-up-btn").html('Sign Up');
                        }else if(result.state == 1){
                            var curr_page = $state.current.name
                            if (curr_page.indexOf('home') > -1){
                                curr_page = 'home'
                            }
                            $cookies.put("new_user","Email:"+$location.url());
                            $('.actions-cnt').hide();
                            $('.action-btns-cnt').hide();
                            //$window.ga('send', 'event', {eventCategory:"Login", eventAction:"SignUp", eventValue:"success"});
                            commonService.analytics('event', {category : 'Login', action : 'SignUp', label : 'Success'});
                            $('.sign-up-success-cnt').show();
                            loginUser(result, false,0);
                            setTimeout(function(){
                                // commonService.userHasInterests = false;
                                $('.login-popup').removeClass('target');
                                $http.defaults.headers.common.Authorization = 'Token ' + result.token ;
                                commonService.userLoggedIn = true;
                                commonService.setLoggedInUserId(result.userId);
                                // localStorage.setItem("fromSignUpPage","true");
                                //$cookies.put("fromSignUpPage","true");
                                loginUser(result, true, 24);
                            }, 2000);
                        }
                    });
                }
            };

            scope.getForgotPassword = function(){
                clearLoginErrors();
                var showForgotSucess = true;
                var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(scope.loginDetails.email == ""){
                    scope.loginDetails.emailErrorMsg = "Please enter email";
                    showForgotSucess = false;
                }else if(emailRegEx.test(scope.loginDetails.email) == false){
                    scope.loginDetails.emailErrorMsg = "Invalid email";
                    showForgotSucess = false;
                }

                if(showForgotSucess){
                    $(".loading-div1").css('display','inline-block');
                    $(".forgot-pass-btn").html('');
                    loginService.generateResetPassLink(scope.loginDetails.email).then(function(result){
                        $(".loading-div1").css('display','none');
                        $(".forgot-pass-btn").html('Submit');
                        $('.actions-cnt').hide();
                        $('.action-btns-cnt').hide();
                        $('.reset-link-cnt').show();
                    });
                }
            };

            resetPassSuccess = false;

            scope.resetPassword = function(){
                clearLoginErrors();
                var showResetSucess = true;
                if(scope.loginDetails.password == ""){
                    scope.loginDetails.passwordErrorMsg = "Please enter password";
                    showResetSucess = false;
                }

                /*if(scope.loginDetails.confirmPassword == ""){
                 scope.loginDetails.confirmPasswordErrorMsg = "Please enter confirm password";
                 showResetSucess = false;
                 }else if(scope.loginDetails.password != scope.loginDetails.confirmPassword && scope.loginDetails.password != ""){
                 scope.loginDetails.confirmPasswordErrorMsg = "Passwords not matching";
                 showResetSucess = false;
                 }*/

                if(scope.allPasswordChecks == false){
                    showResetSucess = false;
                    scope.loginDetails.passwordErrorMsg = "Please enter stronger password";
                }

                if(showResetSucess){
                    $(".loading-div1").css('display','inline-block');
                    $(".reset-pass-btn").html('');
                    loginService.resetPassword({password: scope.loginDetails.password, token: $stateParams.token}).then(function(){
                        DEBUG &&  console.log("Password reset successfull");
                        $(".loading-div1").css('display','none');
                        $(".reset-pass-btn").html('Submit');
                        $('.actions-cnt').hide();
                        $('.action-btns-cnt').hide();
                        $('.reset-success-cnt').show();
                        resetPassSuccess = true;
                        displayLoginTimer = setTimeout(function(){
                            //$('.login-popup > div > div').hide();
                            //$('.sign-in-cnt').show();
                            //$('.actions-cnt').show();
                            //$('.action-btns-cnt').show();
                            scope.loginDetails.password = ""
                            scope.takeToHome()

                        }, 3000);
                    })
                }
            };
        }
    }
}]);

// var globalLoggedInUserId = "";
//
// angular.element(document).ready(function() {
//
//     angular.bootstrap(document, ['stichio']);
//     var tokenData = "d0689f1e3ac6a820ce598c372b92e920a18a58cd";
//     // var tokenData = "d3171ccf12d5546a1f7d6d6665360e4b8cc4b2ef";
//
//     if($.cookie("sti_authenticationToken")){
//         tokenData = $.cookie("sti_authenticationToken");
//     }
//
//     $.ajax
//     ({
//       type: "GET",
//       url: "https://uueam7ohlk.execute-api.us-east-1.amazonaws.com/preAlpha/api/v1/tokenvalidation/",
//         // url: "http://192.168.1.187:8000/api/v1/tokenvalidation/",
//       dataType: 'json',
//       async: false,
//       headers: {
//         "Authorization": 'Token ' + tokenData,
//       },
//       data: '',
//       success: function (result){
//         DEBUG &&  console.log('Thanks for your comment!');
//         DEBUG &&  console.log(result);
//           if(result.detail){
//             console.log(result.detail,'result');
//             if(result.detail == "Invalid token."){
//                 console.log('REMOVING COOKIES');
//                 $.removeCookie("sti_authenticationToken");
//                 localStorage.setItem("logged_in","false");
//                 console.log("REMOVED");
//                 // $http.defaults.headers.common.Authorization = 'Token ' + tokenData ;
//
//             }
//         }
//         if(result.status == 1){
//             globalLoggedInUserId = result.userId;
//         }else{
//             globalLoggedInUserId = "";
//         }
//           console.log("yourUserId "+globalLoggedInUserId);
//
//       }
//     });
//
//
// });

