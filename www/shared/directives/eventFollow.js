angular.module('stichio').directive('eventFollow', eventFollowFn);
eventFollowFn.$inject = ['homeService', 'commonService', '$rootScope', 'APIService', '$stateParams'];
function eventFollowFn (homeService, commonService, $rootScope, APIService, $stateParams) {
	return {
		templateUrl: 'shared/templates/eventFollow.html?0.01',
		link: link
	};

	function link(scope, elem ,attrs) {
		// APIService.scrollOff();

		scope.viewEventFollowers = function () {
            scope.showUserLikesPopup();
			$rootScope.showFollowers = true;
			scope.addEventsOnPopup();
            $rootScope.userLists = $rootScope.eventFollowers;
		};
		var eventName = $stateParams.eventName;

		var index = 0,
            count = 20,
            flag = false,
            scrollHeight,
            win = $(window);

        $rootScope.eventFollowers = [];

        var activateScroll = function () {
            setTimeout(function() {
                win.on("orientationchange", function () {
                    scrollHeight = $('.userLikesModal').height();
                });
                win.on('scroll.getEventFollowers', function () {
                    if (flag && win.scrollTop() > (scrollHeight - APIService.scrollOffset)) {
                        flag = false;
                        getEventFollowers();
                    }
                });
            }, 100);
        };

        function getEventFollowers () {
        	APIService.getEventFollowers(eventName, index, count).then(function (data) {
                data.forEach(function (follower, index) {
                    if (follower.userId === APIService.loggedInUserDetailsGlobal.userId) {
                        data.splice(index, 1);
                        data.unshift(follower);
                    }
                });
        		$rootScope.eventFollowers = $rootScope.eventFollowers.concat(data);
                $rootScope.userLists = $rootScope.eventFollowers;
                var followerDisplayCount = parseInt(($('.eventFollowersList').width()) / 86);
                if (!scope.isMobile) { followerDisplayCount *= 2; }
        		$rootScope.topFollowers = $rootScope.eventFollowers.slice(0, followerDisplayCount);
        		setTimeout(function() {
                    scrollHeight = $('.userLikesModal').height();
                    index += count;
                    flag = true;
                    if(data.length === 0) { win.off('scroll.getEventFollowers'); }
                }, 0)
        	})
        }

        setTimeout(function () {
            getEventFollowers();
            activateScroll();
        }, 2000);
	}
}