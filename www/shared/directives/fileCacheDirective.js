angular.module('stichio').directive('fileCacheSrc', ['cacheService', function (cacheService) {
    function link(scope, element, attrs) {
        //for (var k in attrs) {
        //    if (!angular.isFunction(scope[k])) {
        //        scope[k] = attrs[k];
        //    }
        //}
        var file_url=attrs.fileCacheSrc;
        var srcIs=attrs.srcIs;
        var image_type=attrs.imageType;
        var cache_url;
        try {
            cache_url = cacheService.getUrl(file_url);
            if (srcIs == 'background') {
                element.css('background-image', 'url(' + cache_url + ')');
            }
            else {
                element.css('src', cache_url);
            }
            if (!cacheService.isFileCached(file_url)) {
                console.log("file not cached",file_url )
                try {
                    cacheService.download(file_url).then(function () {
                        console.log("Downloaded Image", file_url)
                        cache_url = cacheService.getUrl(file_url);
                        cacheService.addFileInfo(file_url, image_type);
                    }, function (failedDownloads) {
                    })
                } catch (e) {
                }
            }
            else {
                console.log("file cached",file_url )
                cache_url = cacheService.getUrl(file_url);
                if (srcIs == 'background') {
                    element.css('background-image', 'url(' + cache_url + ')');
                }
                else {
                    element.css('src', cache_url);
                }
            }
        } catch (e) {

        }
    }
    return {
        restrict: 'A',
        link: link,
        scope:{}
    }
}]);