angular.module('stichio')
.directive('adminDare', ['commonService', '$sce', '$rootScope', '$timeout', 'APIService', function (commonService, $sce, $rootScope, $timeout,APIService) {
    return {
        restrict : 'E',
        templateUrl: 'shared/templates/admin-dare.html?0.18',
        link: link
    };
    function link (scope, element, attrs) {
        //commonService.setLoggedInStatus();
                if (!$rootScope.userLoggedIn){
                    return;
                }
        scope.insidePopup = attrs.popup
        console.log("insidepopup", scope.insidePopup)
        scope.card = scope.card || {};
        setTimeout(function () {
            try {
                $.screentime({
                    fields: [{
                        selector: '#card-' + scope.card.id,
                        name: scope.card.id,
                        media: (scope.card.response_type === 'video') ? true : false,
                        gaLabel: "TD: " + scope.card.id + " " + scope.card.admin_dare.dareText,
                        userId: commonService.loggedInUserId,
                        type: "taken_dare"
                    }]
                });
            } catch (e) {
            }

            console.log('response_image', scope.card.response_image)

            // scope.video = {};
            // scope.video.sources = [{src: $sce.trustAsResourceUrl(scope.card.response_image), type: "video/mp4"}]
            // console.log('video', scope.video)
        }, 500)

        scope.loggedInUserId = commonService.loggedInUserId;

        if (scope.card.response_type == "video"){
            scope.card.videoSrc= angular.copy(scope.card.response_image)
        }
        scope.card.autoPlayVideo=false;
    	scope.ideal_iar = 1;
        var colors = ['DDDDDD','FFA18B','D1E8FF','FFD3BD','C8C8FF','FFDDFF','E5E4CA','FFE8C8'];
    	scope.card.bg_color = '#' + colors[Math.floor(Math.random() * colors.length)];
        scope.card.res_bg_color = '#' + colors[Math.floor(Math.random() * colors.length)];

	    var adminDare_res_w = Math.min(screen.width-12, 588);
        if (scope.card.response_type==="video") {
            //For Videos for which inverseAspectRatios have not been calculated.
            //if (scope.card.inverseAspectRatio === 1) {
            //    scope.card.inverseAspectRatio = .7
            //}
            //Setting max inverseAspectRatio
            scope.card.inverseAspectRatio = Math.min(parseFloat(scope.card.inverseAspectRatio), 1.30);
        }
        //if (scope.card.inverseAspectRatio <= 1) {

        scope.card.adminDare_res_h = (adminDare_res_w * scope.card.inverseAspectRatio) + 'px';
        //}
        //else {
        //	scope.card.adminDare_res_h = adminDare_res_w + 'px';
        //}
         // console.log("videoUrl12:", scope.card.response_image)
        if (scope.card.admin_dare && scope.card.admin_dare.taken_dares_count > 5 && !commonService.dareResponsesTickerShown){
            scope.card.showTicker=true;
            commonService.dareResponsesTickerShown=true;
        }
        if (scope.card.response_image) {
            scope.card.poster = scope.card.response_image.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
        }
        //scope.unHideDare= function(){
        //    scope.card.hidden=false;
        //    APIService.hideDare(scope.card.id, false)
        //}


    }
}]);