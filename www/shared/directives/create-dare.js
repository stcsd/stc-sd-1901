angular.module('stichio')
    .controller('createDareCtrl', ['$scope', '$rootScope', '$http', 'Upload', '$timeout', 'commonService', 'APIService', '$state', '$stateParams', function (scope, $rootScope, $http, Upload, $timeout, commonService, APIService, $state , $stateParams){
        //if (!commonService.userLoggedIn) { return }
        //commonService.setLoggedInStatus();
        if (!$rootScope.userLoggedIn){
            return;
        }

        scope.friends = [];
        scope.top_friends = [];
        scope.selected_friends = [];
        scope.searchText = '';
        scope.mediaUrl = ' ';
        scope.api = commonService;
        scope.dare1 = {};
        scope.dare1.stage = 1;
        var socket;

        var top4Friends=null;
        var submit_flag = true;
        var enable_submission = true;

        var dareLink;
        var urlBase = "http://www.stichio.co.in"

        setTimeout(function () {



            scope.othersProfileData = APIService.userDetailsGlobal;


            if (!$rootScope.allDareGenres) {
                APIService.getDareGenres().then(function (response) {
                    response = response.filter(function (val) {
                        return val.name !== ''
                    })
                    $rootScope.allDareGenres = angular.copy(response);
                    response.sort(function (a, b) {
                        var nameA = a.name.toUpperCase()
                        var nameB = b.name.toUpperCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        return 0;
                    });
                    scope.dareGenres = response;
                });
            }
            else{
                var resp = angular.copy($rootScope.allDareGenres)
                resp.sort(function (a, b) {
                    var nameA = a.name.toUpperCase();
                    var nameB = b.name.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                });
                scope.dareGenres = resp;

            }

            var copyDareLink = function (url) {
                //var storeAuth = $http.defaults.headers.common.Authorization;
                //delete $http.defaults.headers.common.Authorization;
                // scope.dareLink = 'http://www.stichio.co.in/#/profile/ad59bd2c-1037-47b7-a890-15a7921602bf?dareId=b75ead5b-1de9-4309-9c46-1e3e02efd69f'

                //scope.getTinyUrl1(scope.tempdareLink)
                try {
                    var picture = scope.forward_card.image_full || scope.forward_card.image;
                    var utm_medium = "DS"
                    var utm_source = "copyLink"
                    var title = "STICHIO DARE: " + scope.forward_card.dareText

                    commonService.generateShareLink({
                        link: url,
                        title: title,
                        image: picture,
                        utm_source:utm_source,
                        utm_medium:utm_medium
                    }).then(function (link) {
                        dareLink = link;
                    });
                } catch (e) {
                    console.log(e)
                }
                //$http.defaults.headers.common.Authorization = storeAuth;
                // console.log(scope.dareLink)
            }
            function copyTextToClipboard(text) {
                    var textArea = document.createElement("textarea");
                    textArea.style.position = 'fixed';
                    textArea.style.top = 0;
                    textArea.style.left = 0;

                    // Ensure it has a small width and height. Setting to 1px / 1em
                    // doesn't work as this gives a negative w/h on some browsers.
                    textArea.style.width = '2em';
                    textArea.style.height = '2em';

                    // We don't need padding, reducing the size if it does flash render.
                    textArea.style.padding = 0;

                    // Clean up any borders.
                    textArea.style.border = 'none';
                    textArea.style.outline = 'none';
                    textArea.style.boxShadow = 'none';

                    // Avoid flash of white box if rendered for any reason.
                    textArea.style.background = 'transparent';


                    textArea.value = text;

                    document.body.appendChild(textArea);

                    textArea.select();

                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'successful' : 'unsuccessful';
                        console.log('Copying text command was ' + msg);
                        scope.isCopied = true;
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }

                    document.body.removeChild(textArea);
                }
            $rootScope.open_dare_form = function (card) {
                console.log(card);
                if (!card) {
                    card = {};
                }
                if (card.admin_dare) {
                    scope.forward_card = angular.copy(card.admin_dare);
                }
                else {
                    scope.forward_card = angular.copy(card);
                }
                scope.forward_card.response_image = '';

                scope.num_other_users = 0;
                console.log('open_dare_form')
                scope.searching = false;
                if (!scope.forward_card.selGenre){
                    scope.forward_card.selGenre={}
                }
                 scope.forward_card.feed_type="admin_dare";


                commonService.openPopup('create-dare-popup');
                try {
                    scope.closeAdminDarePopup();
                } catch (e) {
                }
                try {
                    var $cd_ta;

                    $timeout(function () {
                        try {
                            $cd_ta = $('.cd_ta');
                            $cd_ta[0].focus();
                            $cd_ta[0].click();
                            $cd_ta.height(20);
                        } catch (e) {
                        }
                    }, 10);

                    var el = $('#create-dare-popup-content');
                    el.focus();
                } catch (e) {
                }

                scope.selected_friends = [];
                if (!scope.forward_card.top_friends) {
                    if (!top4Friends) {
                        APIService.getTopFriends().then(function (response) {
                            top4Friends = response;
                            scope.forward_card.top_friends = angular.copy(top4Friends.slice(0, 4));
                            scope.forward_card.friends = angular.copy(scope.forward_card.top_friends);
                            angular.forEach(scope.forward_card.top_friends, function (user) {
                                user.selected = false;
                            });
                        });
                    }
                    else{
                        scope.forward_card.top_friends = angular.copy(top4Friends.slice(0, 4));
                        scope.forward_card.friends = angular.copy(scope.forward_card.top_friends);
                        angular.forEach(scope.forward_card.top_friends, function (user) {
                            user.selected = false;
                        });
                    }

                }

                enable_submission = true;

                scope.isCopied=false;

                if(scope.forward_card.id) {
                    dareLink = urlBase + '/#/dares/' + scope.forward_card.id;
                    copyDareLink(dareLink);
                    console.log("copieeed")
                    setTimeout(function () {

                        var copyBobBtn = document.querySelector('.copybtn1');

                        copyBobBtn.addEventListener('click', function (event) {
                            copyTextToClipboard(dareLink);
                        });

                        socket = commonService.socket;
                        scope.forward_card.searchText = ''
                        scope.$watch('forward_card.searchText', function (newValue) {
                            console.log("newValll", newValue)
                            if (newValue.length > 2) {
                                //console.log(socket, "socket12");

                                if (socket && socket.socket && socket.socket.open) {
                                    socket.emit('search_users_new', commonService.loggedInUserId, newValue, [],10, 'nominate_friend' )
                                    socket.on('search_users_nominate_friend', function (data) {
                                        scope.$apply(function(){
                                            scope.suggested_users = data.result;

                                            console.log('suggested_users: ', data.result);
                                            if (scope.suggested_users.length === 0) {
                                                scope.dare1.search_error = 'No Match Found';
                                                scope.suggested_users = [];
                                            }
                                            else {
                                                scope.dare1.search_error = '';
                                            }
                                        });
                                    })
                                }
                                else {
                                    APIService.searchUsers(newValue, 0, 0, 5).then(function (data) {
                                        $timeout(function(){
                                            scope.suggested_users = data.users;
                                            if (scope.suggested_users.length === 0) {
                                                scope.dare1.search_error = 'No Match Found';
                                                scope.suggested_users = [];
                                            }
                                            else {
                                                scope.dare1.search_error = '';
                                            }
                                        });

                                     });
                                }
                            } else {
                                scope.dare1.search_error = '';
                                scope.suggested_users = [];
                            }
                        })

                    }, 500);
                }
            };
            scope.media_upload = function (files, errFiles) {
                if (files.length > 0) {
                    scope.forward_card.image = true;
                    $('.cd_progress').show();
                }
                commonService.uploadFile(files).then(function (response) {
                    console.log(response);
                    scope.forward_card.image = response.url;
                    $('.cd_progress').hide();
                });
                scope.resetDimension();
            };

            scope.resetDimension = function () {
                $timeout(function () {
                    $cd_ta.height(1);
                    $cd_ta.height($cd_ta[0].scrollHeight);
                }, 10)
            };
            scope.selectImageCreateDare = function (){
                    commonService.selectMedia(successCb, errorCb, progressCb, 'image');
                }
            function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
                resetLoadingOptions();
                scope.forward_card.image = remoteUrl;

            }

            function errorCb(data) {
                resetLoadingOptions();
            }

            function progressCb(data) {
                $('.cd_progress').show();
                scope.forward_card.image=true;
                scope.progressPercentage=data.progress
            }


            function resetLoadingOptions() {
                $('.cd_progress').hide();
                scope.progressPercentage=0;
                scope.resetDimension();
            };
            $rootScope.close_create_dare = function () {
                scope.forward_card=null;
                scope.dare1.stage = 1;
                commonService.closePopup('create-dare-popup');
            };

            scope.open_search = function () {
                scope.forward_card.searching = true;
                $('.search-friends').show();
            }

            scope.select_friend = function (user) {
                console.log(user)
                user.selected = true;
                scope.selected_friends.push(user);
            };

            scope.unselect_friend = function (user) {
                user.selected = false;
                var index = scope.selected_friends.indexOf(user);
                scope.selected_friends.splice(index, 1);
            };

            scope.removePhoto=function(){
                $timeout(function(){
                    scope.forward_card.image='';
                    scope.resetDimension();
                }, 200)

            }

            scope.select_friend_from_search = function (user) {
                console.log('select_friend_from_search')
                scope.searchText = '';
                scope.forward_card.searching = false;
                angular.forEach(scope.forward_card.top_friends, function(top_friend, index) {
                    if (top_friend.user.userId == user.userId) {
                        top_friend.selected = true
                        if (scope.selected_friends.indexOf(top_friend) < 0) {
                            scope.selected_friends.push(top_friend)
                        }
                    }
                });
                var userdata = {"user": user, "selected": true}
                scope.forward_card.top_friends.unshift(userdata)
                if (scope.selected_friends.indexOf(userdata) < 0) {
                    scope.selected_friends.push(userdata)

                }
                if (scope.selected_friends.length < 5)
                    scope.forward_card.top_friends = scope.forward_card.top_friends.slice(0, 4)
                else
                    scope.forward_card.top_friends = scope.forward_card.top_friends.slice(0, 5)
            };

            scope.suggest_dare = function () {
                var data = {
                    "dareText": scope.data,
                    "image": scope.mediaUrl,
                    "media_type": "image",
                    "genre": scope.genre
                }
                APIService.suggestDare(data);
            }

            scope.giveDare = function () {
                if (!enable_submission) { return }
                if (!scope.forward_card.image) {
                    var conf = confirm("Sure, you want to give a DARE without a photo?\nDARES with Photos become more popular.")
                    if (!conf) { return }
                }
                console.log("scope.genre", scope.forward_card.selGenre)
                enable_submission = false;
                var data = {
                    // "dare_type": "new",
                    "url" : scope.forward_card.image,
                    "dareText": scope.forward_card.dareText,
                    "genre": scope.forward_card.selGenre.id
                };
                if (submit_flag && scope.dare1.stage === 1) {
                    submit_flag = false;
                    submit_dare1(data);
                }
                if (submit_flag && scope.dare1.stage === 5) {
                    submit_flag = false;
                    $('.fd-loading-icon').show();
                    data.userIds = [scope.daree.userId];
                    submit_dare2(data);
                }
                // alert(JSON.stringify(data));
            };
            scope.dareFriend=function(){
                scope.dare1.stage=3;
                $timeout(function() {
                    var copyBobBtn = document.querySelector('.copybtn1');

                    copyBobBtn.addEventListener('click', function (event) {
                        console.log("copy clicked", dareLink)
                        copyTextToClipboard(dareLink);
                    });
                }, 100);
            }
            scope.giveDareToSelectedUser = function (user) {
                scope.daree = user;
                scope.dare1.stage = 5;
                $rootScope.open_dare_form();
            };

            scope.submit_public_dare = function(is_public){
                if(scope.forward_card.submitting){
                    return;
                }
                if (!scope.forward_card.image) {
                    var conf = confirm("Sure, you want to create a DARE without a photo?\nDARES with Photos become more popular.")
                    if (!conf) { return }
                }


                console.log("scope.genre", scope.forward_card.selGenre)
                is_public=is_public||false;
                var next_stage;
                scope.forward_card.submitting=true;
                if(is_public) {
                    next_stage = 2;
                    $("#public_ds_icon").hide();
                    $("#public_ds_loading").show();
                }
                else{
                    next_stage = 3;
                    $("#dare_friend_icon").hide();
                    $("#dare_friend_loading").show();
                }
                var data={}
                data.is_public=is_public;
                if (scope.forward_card.image) {
                    data.image = scope.forward_card.image;
                    data.media_type="image";
                }
                if(scope.forward_card.selGenre.id) {
                    data.genre = scope.forward_card.selGenre.id
                    scope.forward_card.genre=scope.forward_card.selGenre.id
                }
                data.dareText=scope.forward_card.dareText;

                APIService.createPublicDare(data).then(function(id){
                    scope.forward_card.id=id;
                    scope.dare1.stage=next_stage;
                    $("#dare_friend_icon").show();
                    $("#dare_friend_loading").hide();
                     $("#public_ds_icon").show();
                    $("#public_ds_loading").hide();
                    scope.forward_card.submitting=false;
                    scope.forward_card.creator=$rootScope.userLoggedInDetails.username;
                    $timeout(function () {
                        dareLink = urlBase + '/#/dares/' + scope.forward_card.id;
                        copyDareLink(dareLink);
                        if (next_stage == 3){
                            var copyBobBtn = document.querySelector('.copybtn1');

                            copyBobBtn.addEventListener('click', function (event) {
                                console.log("copy clicked", dareLink)
                                copyTextToClipboard(dareLink);
                            });
                        }
                    },100);
                },function(){
                    //$("#dare_friend_icon").show();
                    //$("#dare_friend_loading").hide();
                    // $("#public_ds_icon").show();
                    //$("#public_ds_loading").hide();
                });
                var val=0;
                if(is_public){
                    val=1
                }
                try {
                    commonService.analytics('event', {category: "Public Dare",
                        action: $rootScope.userLoggedInDetails.username + ' ' + $rootScope.userLoggedInDetails.username,
                        label: scope.forward_card.dareText,
                        value :val
                    });
                } catch (e) {
                }

            }

            scope.forwardDare = function () {
                if (!enable_submission) { return }
                enable_submission = false;
                var data = {
                    stichId: null,
                    dareText: scope.forward_card.dareText,
                    genre: scope.forward_card.genre,
                    parentId: scope.forward_card.id,
                    parent_type: scope.forward_card.feed_type || 'dare'
                };
                if (scope.forward_card.adminDareId) {
                    data.dareSuggestionId = scope.forward_card.adminDareId;
                } else if (scope.forward_card.isAdmin) {
                    data.dareSuggestionId = scope.forward_card.id;
                }
                if (scope.forward_card.image) {
                    data.url = scope.forward_card.orig_image || scope.forward_card.image;
                    data.iar = scope.forward_card.inverseAspectRatio
                }
                data.forwarded = (scope.dare1.stage === 3);
                if (submit_flag) {
                    submit_flag = false;
                    submit_dare1(data);
                }
            };

            var submit_dare1 = function (data) {
                $('.fd-loading-icon').show();
                var user_ids = [];
                angular.forEach(scope.selected_friends, function (selected_friend, index1) {
                    user_ids.push(selected_friend.user.userId);
                    angular.forEach(scope.forward_card.friends, function (friend, index2) {
                        if(friend.user.userId == selected_friend.user.userId) {
                            scope.forward_card.friends.splice(index2, 1);
                        }
                    });
                });
                scope.num_other_users = scope.selected_friends.length - 1;
                scope.daree = scope.selected_friends[scope.num_other_users].user;
                data.userIds = user_ids;
                submit_dare2(data);
            };

            var submit_dare2 = function (data) {
                scope.forward_card.dare=angular.copy(scope.forward_card)
                APIService.submitDare(data).then(function (response) {
                    console.log(response);
                    scope.forward_card.dare.id = response.dareId;
                    scope.forward_card.id = response.adminDareId;
                    $('.fd-loading-icon').hide();
                    submit_flag = true;
                    scope.selected_friends = [];
                    try {
                        scope.forward_card.top_friends = angular.copy(scope.forward_card.friends.slice(0, 4));
                    } catch (e) {
                    }
                    scope.dare1.stage += 1;
                    //if($state.current.name == 'profile.dares') {
                    //
                    //}

                    enable_submission = true;

                    if (!commonService.user_rated_app) {
                        try {
                            AppRate.promptForRating(true);
                        } catch (e) {
                        }
                    }
                    $timeout(function () {
                        dareLink = urlBase + '/#/dares/' + scope.forward_card.id;
                        copyDareLink(dareLink);
                        var copyBobBtn = document.querySelector('.copybtn1');

                        copyBobBtn.addEventListener('click', function (event) {
                            copyTextToClipboard(dareLink);
                        });
                    },100);
                });

                console.log(data);
                scope.forward_card.dare.user1 = $rootScope.userLoggedInDetails;
                scope.forward_card.dare.user2 = scope.daree;
                scope.forward_card.dare.userId = $rootScope.userLoggedInDetails.userId;
                scope.forward_card.dare.user_type = 'darer';
                scope.forward_card.dare.feed_type = 'dare';
                scope.forward_card.dare.status = 0;
                scope.forward_card.dare.genre = data.genre;
            };

            scope.goToGiven=function () {
                console.log("$stateParams", $stateParams);
                $rootScope.close_create_dare();
                $state.go('profile.dares', {dareType:'given', profileId: $rootScope.userLoggedInDetails.userId}, {notify: true, reload: true});

            }


        }, 2000)
    }])
    .directive('createDare', ['APIService', function (APIService) {
        return {
            // scope: {},
            templateUrl: 'shared/templates/create-dare.html?0.04',
            controller: 'createDareCtrl'
        };
    }]);