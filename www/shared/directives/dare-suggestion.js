angular.module('stichio')
.directive('dareSuggestion', ['$rootScope', '$http', '$sce', 'commonService','APIService','$state','$ionicPlatform', '$timeout', '$window', function ($rootScope, $http, $sce, commonService,APIService, $state, $ionicPlatform, $timeout, $window) {
    function link (scope, element, attrs) {
        var uuid;
		var videoselected=false;

        setTimeout(function () {
			if (scope.card && scope.card.dares && scope.card.dares[0].response_type=='text') {
				$.screentime({
					fields: [{
						selector: '#card-' + scope.card.id,
						name: scope.card.id,
						media: false,
						gaLabel:"DS: " + scope.card.id + " " + scope.card.dareText,
						userId: commonService.loggedInUserId,
						type:"admin_dare"
					}]
				});
			}
        }, 500);

		var dare_image_w = Math.min(screen.width-12,588);
        scope.onPlayerReady1 = function (API,index) {
		    scope.API = API;
		};
		scope.loggedIn=$rootScope.loggedIn


		if (attrs.index == 1){
			scope.showToolTipBookmark=true;
		}
		scope.onUpdateState1 = function (state, index) {
		    if(state=='play') {
		        if($rootScope.playingVideo && $rootScope.playingVideo != scope.API)
		            $rootScope.playingVideo.pause();
		        $rootScope.playingVideo = scope.API;
		        if (!scope.API.isFullScreen)
		            scope.API.toggleFullScreen()
		    }
		};
		//if(scope.card && !scope.card.creator){
		//	scope.card.creator = 'STICHIO';
		//}
        scope.selectSource = function () {
            scope.card.medium_selection = true;
            $('body').addClass('stop-scrolling');
			setTimeout(function () {
				var scrollval = $('body').scrollTop();
				if (scrollval > 0) {
					$('.main-stiches-cnt .media-selection-popup').css('top', (scrollval - 45) + 'px')
				}
			}, 100);
			console.log("inside dare sugg open media popup")
			$ionicPlatform.registerBackButtonAction(
				function(){
					console.log("back from camera popup")
					scope.closeSelectMedia();
				}, 100);

             //openPopup('media-selection-popup');
        }

        scope.closeSelectMedia = function () {
			$timeout(function () {
				scope.card.medium_selection = false;
			}, 10);



			console.log("inside dare sugg close media popup")
            $('body').removeClass('stop-scrolling');
            	$rootScope.customBack();
             //closePopup('media-selection-popup');
        }

        scope.selectFile = function () {
            commonService.selectMedia(successCb, errorCb, progressCb);
        };

        scope.captureImage = function () {
            commonService.captureImage(successCb, errorCb, progressCb);
        };

        scope.captureVideo = function () {
            commonService.captureVideo(successCb, errorCb, progressCb);
        };

        function successCb(fileType, remoteUrl, videoInverseAspectRatio) {
            resetLoadingOptions();
            scope.card.response_type = fileType;
            scope.card.response_image_tmp = remoteUrl;
            scope.card.created_date = new Date();
            scope.card.poster= scope.card.response_image_tmp.replace('videos', 'images').replace('.mp4', '_snapshot.jpg');
            scope.card.sources = [ {src: $sce.trustAsResourceUrl(remoteUrl), type: "video/mp4"} ];
            scope.card.videoSrc=remoteUrl;
            if (fileType == "video"){
                scope.card.inverseAspectRatio = videoInverseAspectRatio;
            }

        }

        function errorCb(data) {
            resetLoadingOptions();
        }

        function progressCb(data) {
			if (!videoselected) {
				videoselected=true;
				scope.$apply(function () {
					scope.card.loading = data;
				})
			}
			else{
				scope.card.loading = data;
			}
            scope.card.response_image_tmp = null;
        }

        function resetLoadingOptions() {
            scope.card.loading = {
                status: false,
                progress: 0,
                message: ''
            }
			videoselected=false;
        };
		if (scope.card) {
			scope.card.submitting_dare = false;
		}

		scope.openDare = function(dare_type, dareId, dare){
			console.log("dare_type1", dare_type)
			if (dare_type=='dare') {
				$rootScope.openDarePopup(null, false, true, false, dareId, true, true, dare);
			}
			else
			{
				$rootScope.openAdminDarePopup(null, false, true, false, dareId, true, true, dare)
			}
			try {
				if (video.currentState == 'play') {
					video.stop()
				}
			} catch (e) {
			}
		}

		scope.toggleUpvote= function(response){
			var data={}
			if(!response.upvoted){
				data.vote_type = 'up';
			}
			else{
				data.vote_type = 'cancel';
			}
			if (response.dare_type=="dare"){
				data.dareResponseId = response.responseId;
				APIService.voteDareResponse(data, response.dareId, null, null);
			}
			else {
				data.dareResponseId = response.dareId;
				APIService.adminDareVote(data, response.dareId, null);
			}
		};

		scope.chooseSubmitOption = function() {
			$('#selectPostOption-'+scope.card.id).show()
		};

		scope.selectAnonymous = function() {
			$('#selectPostOption-'+scope.card.id).hide()
			scope.card.anonym=true;
		}

		scope.selectOwn = function() {
			$('#selectPostOption-'+scope.card.id).hide()
			scope.card.anonym=false;
		}

		scope.submit_dare_admin = function (card) {
				if (scope.card.submitting_dare) {
					return;
				}
				scope.card.submitting_dare = true;
				console.log(card);
				var response_text = angular.copy(card.response_text_tmp);
				card.response_text = response_text;
				card.response_image = angular.copy(card.response_image_tmp);
				card.response_image_full = card.response_image;
				if (card.response_type == 'video') {
					//commonService.extractFrame(card.response_image);
					var url1 = card.response_image.replace('https://eb-stichio.s3.amazonaws.com/videos/', '');
					var fileName = url1.replace('.mp4', '.jpg');
					card.video_cover = "https://eb-stichio.s3.amazonaws.com/images/" + fileName;

					console.log('video cover added')
				}

				var data = {
					action: "submitResponse",
					responseText: card.response_text || '',
					response_type: card.response_type || '',
					stichId: '',
					video_cover: card.video_cover,
				};
				if(card.inverseAspectRatio && card.inverseAspectRatio!= null){
					data.inverseAspectRatio=card.inverseAspectRatio
				}

				if (card.isEventDare)
				{
					data.isEventDare=true;
					data.eventId=card.eventId

				}
				if (card.response_image) {
					data.image = card.response_image;
				}

				if (!card.adminDareId) {
					card.admin_dare = angular.copy(card)
					card.adminDareId = angular.copy(card.id)
				}

				if(card.response_text && !card.response_image){
					var  cnf = confirm('Sure, you want to submit without a DARE picture or video?');
					if(cnf == false) {
						scope.card.submitting_dare = false;
						return;
					}
				}
				else if(!card.response_text && card.response_image){
					var  cnf = confirm('Sure, you want to submit without a DARE Comment?');
					if(cnf == false) {
						scope.card.submitting_dare = false;
						return;
					}
				}



				$("#submitsuggestion").show()
				APIService.submitAdminDareResponse(data, card.admin_dare.id).then(function(results){

					card.upvote_count = 0;
					card.downvote_count = 0;
					card.comments_count = 0;
					if(card.admin_dare) {
						card.admin_dare.taken_dares_count= card.admin_dare.taken_dares_count || 0;
						card.admin_dare.taken_dares_count++;
					}
					card.taken_dares_count = card.taken_dares_count || 0;
					card.taken_dares_count++;
					card.updated_date = Date();
					card.user = {
						avatar: $rootScope.userLoggedInDetails.avatar,
						username: $rootScope.userLoggedInDetails.username,
						userId: $rootScope.userLoggedInDetails.userId
					};
					card.userId = $rootScope.userLoggedInDetails.userId;
					card.response_text_tmp = '';
					card.response_image_tmp = '';
					card.dare_submitted = true;
					card.created_date = new Date();
					$("#submitsuggestion").hide()
					DEBUG && console.log(results);
					card.id = results.data.dareId;
                    card.answer_text=results.data.answer_text
                    card.answer_image=results.data.answer_image
                    card.dares_completed = results.data.dares_completed
					card.feed_type = 'taken_dare';
					card.user_type= 'daree';
					card.stopAutoPlay= true;

                    if (card.dares) {
                        setTimeout(function() {
                            var win = $(window)
                            var bodyscroll=win.scrollTop()
                            console.log("$('bodyscroll",bodyscroll)
                            win.scrollTop(bodyscroll-350);
                        },200);
                    }
				    card.text_background = results.data.text_background
					if (card.admin_dare && card.admin_dare.isEventDare) {
						if ($state.current.name == "events.dares") {
							$rootScope.eventAdminDareResponses.unshift(card)
							card.dare_submitted = false
							card.accepted = false;
						}
						setTimeout(function(){
							scope.openShareDare('eventDare', card, true);
						},400);

					}
					else {
						setTimeout(function(){
							scope.openShareDare('dare', card, true);
						},400);
					}
					scope.card.submitting_dare = false;
					console.log('admin dare res_id:', card.id);
					console.log('Current Page', $state.current.name);
					var cur_page = $state.current.name;
					if ($state.current.name == "home.stiches" || $state.current.name == "home1.stiches")
						cur_page='home'
					else if($state.current.name == "explore.dares")
						cur_page='explore'
					try {
						commonService.analytics('event', {
							category: 'Dare Submit',
							action: cur_page,
							label: card.admin_dare.id + " " + card.admin_dare.dareText
						})
					} catch (e) {
					}
					try {
						if (!commonService.user_rated_app) {
							AppRate.promptForRating(true);
						}
						if ($rootScope.dareStats) {
							$rootScope.dareStats.completed_count++;
						}
						if (scope.userDaresStats) {
							if (scope.isOwnProfile)
								scope.userDaresStats.completed_count++;
						}
					} catch (e) {
					}
					try {
						if (scope.cards && scope.cards[0] && scope.cards[0].dare_submitted) {
							scope.cards[0].just_completed_dares.unshift(angular.copy(card));
						}
					} catch (e) {
					}
				});


			};
    }
    return {
        restrict : 'E',
        templateUrl: 'shared/templates/dare-suggestion.html?0.08',
        link: link
    };
}]);