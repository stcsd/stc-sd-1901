angular.module('stichio').directive('newUserThumbnail', ['APIService', function(APIService) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/newUserThumbnail.html?0.02',
        link: function (scope, element, attribute) {

            scope.sendFriendReq = function (user) {
                user.contact_status = 1;
                 var data1 = [user.userId]
                APIService.sendContactRequest(data1).then(function (results) {
                       DEBUG && console.log('REQUEST SENT');
                       // APIService.getUserDetails().then(function (results) {
                       //     scope.userData = results;
                       // });
                   });
            }
            scope.acceptFriendReq = function (user) {
                user.contact_status = 2;
                APIService.sendResponse(user.userId, true);
            }
            scope.rejectFriendReq = function (user) {
                user.contact_status = 0;
                APIService.sendResponse(user.userId, false);
            }
        }
    }
}]);
