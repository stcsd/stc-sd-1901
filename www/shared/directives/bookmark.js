angular.module('stichio').directive('bookmark', bookmark);
bookmark.$inject = ['APIService'];
function bookmark(APIService) {
    return {
        restrict: 'E',
        templateUrl: 'shared/templates/bookmark.html?0.01',
        link: link
    }

    function link(scope, elem, attrs) {
        elem.css({
            'width': '36px',
            'height': '34px',
            'position': 'absolute',
            'top': '0',
            'right': '0'
        })
        scope.toggle_bookmark = function (obj, e) {
            if (obj.is_bookmarked) {
                APIService.unbookmarkDare(obj.id).then(function () {
                    // obj.is_bookmarked = !obj.is_bookmarked;
                });
            } else {
                var bookmark_data = {
                    elementId: obj.id,
                    element_type: obj.feed_type
                };
                APIService.bookmarkDare(bookmark_data).then(function () {
                    // obj.is_bookmarked = !obj.is_bookmarked;
                });
            }

            $(e.target).css({width: '0', height: '0'});
            obj.is_bookmarked = !obj.is_bookmarked;
            $(e.target).animate({width: '100%', height: '100%'}, 200)
        }
    }
}
