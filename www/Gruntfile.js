module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngAnnotate: {
		    options: {
		        singleQuotes: true,
				DEBUG : false
		    },
		    app: {
		        files: {
		            './dist/directives.js': ['./shared/directives/*.js']
		        }
		    }
		},
		concat: {
		    directives: { //target
		        src: ['./shared/directives/*.js'],
		        dest: './dist/directives_concat.js'
		    },
			sharedServices: { //target
		        src: ['./shared/services/*.js'],
		        dest: './dist/sharedsvcs.js'
		    },
		    home: { //target
		        src: ['./components/home/*.js'],
		        dest: './dist/home_concat.js'
		    },
			contact: { //target
		        src: ['./components/contact/*.js'],
		        dest: './dist/contact_concat.js'
		    },
			events: { //target
		        src: ['./components/Events/*.js'],
		        dest: './dist/events_concat.js'
		    },
			explore: { //target
		        src: ['./components/ExplorePage/*.js'],
		        dest: './dist/explore_concat.js'
		    },
			libs: { //target
		        src: ['./libs/*.js'],
		        dest: './dist/libs_concat.js'
		    },
			policies: { //target
		        src: ['./components/policies/*.js'],
		        dest: './dist/policies_concat.js'
		    },
		    userProfile: { //target
		        src: ['./components/userProfile/*.js'],
		        dest: './dist/userProfile_concat.js'
		    },
			 verify: { //target
		        src: ['./components/verify/*.js'],
		        dest: './dist/verify_concat.js'
			 },

			 app: { //target
		        src: ['./js/*.js'],
		        dest: './dist/app_concat.js'
		    }
		},
		uglify: {
			options: {
				compress: {
					global_defs: {
						'DEBUG': false
					},
					dead_code: true,
					drop_console:true
				}
			},
		    directives: { //target
		        src: ['./dist/directives_concat.js'],
		        dest: './dist/d.min.js'
		    },
			sharedServices: { //target
		        src: ['./dist/sharedsvcs.js'],
		        dest: './dist/s.min.js'
		    },
		    home: { //target
		        src: ['./dist/home_concat.js'],
		        dest: './dist/h.min.js'
		    },
		    login: { //target
		        src: ['./dist/login_concat.js'],
		        dest: './dist/l.min.js'
		    },
			contact: { //target
		        src: ['./dist/contact_concat.js'],
		        dest: './dist/c.min.js'
		    },
			events: { //target
		        src: ['./dist/events_concat.js'],
		        dest: './dist/e.min.js'
		    },
			explore: { //target
		        src: ['./dist/explore_concat.js'],
		        dest: './dist/ex.min.js'
		    },
			libs: { //target
		        src: ['./dist/libs_concat.js'],
		        dest: './dist/l.min.js'
		    },
			policies: { //target
		        src: ['./dist/policies_concat.js'],
		        dest: './dist/p.min.js'
		    },
		    userProfile: { //target
		        src: ['./dist/userProfile_concat.js'],
		        dest: './dist/u.min.js'
		    },
			verify: { //target
		        src: ['./dist/verify_concat.js'],
		        dest: './dist/v.min.js'
		    },
			app: { //target
		        src: ['./dist/app_concat.js'],
		        dest: './dist/a.min.js'
		    }

		},
		clean: {
			js: ["./dist/*.js", "!./dist/*.min.js"]
		},
        includeSource: {
            options: {
                //This is the directory inside which grunt-include-source will be looking for files
                basePath: '.'
            },
            app: {
                files: {
                    //Overwriting index.html
                    'index_dev.html': 'index_dev.html'
                }
            }
        }
          //grunt task configuration will go here     
    });

    //load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-include-source');

    

    //register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify', 'clean']);
    grunt.registerTask('include', ['includeSource']);
}