angular.module('me-lazyload', [])
.directive('lazySrc', ['$window', '$document', '$rootScope', function($window, $document, $rootScope){
    var doc = $document[0],
        body = doc.body,
        win = $window,
        $win = angular.element(win),
        uid = 0,
        elements = {};
    function getUid(el){
        var __uid = el.data("__uid");
        if (! __uid) {
            el.data("__uid", (__uid = '' + ++uid));
        }
        return __uid;
    }

    function getWindowOffset(){
        var t,
            pageXOffset = (typeof win.pageXOffset == 'number') ? win.pageXOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollLeft == 'number' ? t : body).ScrollLeft,
            pageYOffset = (typeof win.pageYOffset == 'number') ? win.pageYOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollTop == 'number' ? t : body).ScrollTop;
        return {
            offsetX: pageXOffset,
            offsetY: pageYOffset+500
        };
    }

    function isVisible(iElement){
        var elem = iElement[0],
            elemRect = elem.getBoundingClientRect(),
            windowOffset = getWindowOffset(),
            winOffsetX = windowOffset.offsetX,
            winOffsetY = windowOffset.offsetY,
            elemWidth = elemRect.width,
            elemHeight = elemRect.height,
            elemOffsetX = elemRect.left + winOffsetX,
            elemOffsetY = elemRect.top + winOffsetY,
            viewWidth = Math.max(doc.documentElement.clientWidth, win.innerWidth || 0),
            viewHeight = Math.max(doc.documentElement.clientHeight, win.innerHeight || 0) + 500,
            xVisible,
            yVisible;

        if(elemOffsetY <= winOffsetY){
            if(elemOffsetY + elemHeight >= winOffsetY){
                yVisible = true;
            }
        }else if(elemOffsetY >= winOffsetY){
            if(elemOffsetY <= winOffsetY + viewHeight){
                yVisible = true;
            }
        }

        if(elemOffsetX <= winOffsetX){
            if(elemOffsetX + elemWidth >= winOffsetX){
                xVisible = true;
            }
        }else if(elemOffsetX >= winOffsetX){
            if(elemOffsetX <= winOffsetX + viewWidth){
                xVisible = true;
            }
        }

        return xVisible && yVisible;
    };

    var checkImage = function(){

        Object.keys(elements).forEach(function(key){
            var obj = elements[key],
                iElement = obj.iElement,
                $scope = obj.$scope;
            if(isVisible(iElement)){
                iElement.attr('src', $scope.lazySrc);
            }
        });
    }
    var waiting = false;
    var scrolltimeout = null;
    var endScrollHandle;
    var onScroll = checkImage;
    function onScrollingCheckImage() {

        if (waiting) {
            return;
        }
        waiting = true;
        if (endScrollHandle)
            clearTimeout(endScrollHandle);

        onScroll();

        setTimeout(function () {
            waiting = false;
        }, 300);

        endScrollHandle = setTimeout(function () {
            onScroll();
        }, 400);
    }


    $win.bind('scroll', onScrollingCheckImage);
    $win.bind('resize', onScrollingCheckImage);
    $rootScope.startLazy = function (window) {
          window.bind('scroll', onScrollingCheckImage);
          window.bind('resize', onScrollingCheckImage);
    };

    function onLoad(){
        var $el = angular.element(this),
            uid = getUid($el);

        $el.css('opacity', 1);

        if(elements.hasOwnProperty(uid)){
            delete elements[uid];
        }
    }
    function onError(){
        var $el = angular.element(this),
            uid = getUid($el);

        if(elements.hasOwnProperty(uid)){
            delete elements[uid];
        }
    }

    return {
        restrict: 'A',
        scope: {
            lazySrc: '@',
            animateVisible: '@',
            animateSpeed: '@'
        },
        link: function($scope, iElement){
            iElement.bind('load', onLoad);
            iElement.bind('error', onError);

            

            $scope.$watch('lazySrc', function(){
                var speed = "0.5s";
                if ($scope.animateSpeed != null) {
                    speed = $scope.animateSpeed;
                }

                setTimeout(function(){
                    if(isVisible(iElement)){
                        if ($scope.animateVisible) {
                            iElement.css({
                                'background-color': '#fff',
                                'opacity': 0,
                                '-webkit-transition': 'opacity ' + speed,
                                'transition': 'opacity ' + speed
                            });
                        }
                        iElement.attr('src', $scope.lazySrc);
                    }else{
                        var uid = getUid(iElement);
                        iElement.css({
                            'background-color': '#fff',
                            'opacity': 0,
                            '-webkit-transition': 'opacity ' + speed,
                            'transition': 'opacity ' + speed
                        });
                        elements[uid] = {
                            iElement: iElement,
                            $scope: $scope
                        };
                    }
                },0.1);
            });

            $scope.$on('$destroy', function(){
                iElement.unbind('load');
                var uid = getUid(iElement);
                if(elements.hasOwnProperty(uid)){
                    delete elements[uid];
                }
            });

        }
    };
}]);