module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngAnnotate: {
		    options: {
		        singleQuotes: true,
				DEBUG : false
		    },
		    app: {
		        files: {
		            './www/dist/directives.js': ['./shared/directives/*.js']
		        }
		    }
		},
		concat: {
		    directives: { //target
		        src: ['./www/shared/directives/*.js'],
		        dest: './www/dist/directives_concat.js'
		    },
			sharedServices: { //target
		        src: ['./www/shared/services/*.js'],
		        dest: './www/dist/sharedsvcs.js'
		    },
		    home: { //target
		        src: ['./www/components/home/*.js'],
		        dest: './www/dist/home_concat.js'
		    },
			contact: { //target
		        src: ['./www/components/contact/*.js'],
		        dest: './www/dist/contact_concat.js'
		    },
			dare: { //target
		        src: ['./www/components/dare/*.js'],
		        dest: './www/dist/dare_concat.js'
		    },
			events: { //target
		        src: ['./www/components/Events/*.js'],
		        dest: './www/dist/events_concat.js'
		    },
			explore: { //target
		        src: ['./www/components/ExplorePage/*.js'],
		        dest: './www/dist/explore_concat.js'
		    },
			libs: { //target
		        src: ['./www/libs/*.js'],
		        dest: './www/dist/libs_concat.js'
		    },
			policies: { //target
		        src: ['./www/components/policies/*.js'],
		        dest: './www/dist/policies_concat.js'
		    },
			Popular: { //target
		        src: ['./www/components/Popular/*.js'],
		        dest: './www/dist/popular_concat.js'
		    },
			Search: { //target
		        src: ['./www/components/Search/*.js'],
		        dest: './www/dist/search_concat.js'
		    },
		    userProfile: { //target
		        src: ['./www/components/userProfile/*.js'],
		        dest: './www/dist/userProfile_concat.js'
		    },
			 verify: { //target
		        src: ['./www/components/verify/*.js'],
		        dest: './www/dist/verify_concat.js'
			 }
		},
		uglify: {
			options: {
				compress: {
					global_defs: {
						'DEBUG': false
					},
					dead_code: true,
					drop_console:false
				}
			},
		    directives: { //target
		        src: ['./www/dist/directives_concat.js'],
		        dest: './www/dist/d.min.js'
		    },
			sharedServices: { //target
		        src: ['./www/dist/sharedsvcs.js'],
		        dest: './www/dist/s.min.js'
		    },
		    home: { //target
		        src: ['./www/dist/home_concat.js'],
		        dest: './www/dist/h.min.js'
		    },
			contact: { //target
		        src: ['./www/dist/contact_concat.js'],
		        dest: './www/dist/c.min.js'
		    },
			dare: { //target
		        src: ['./www/dist/dare_concat.js'],
		        dest: './www/dist/da.min.js'
		    },
			events: { //target
		        src: ['./www/dist/events_concat.js'],
		        dest: './www/dist/e.min.js'
		    },
			explore: { //target
		        src: ['./www/dist/explore_concat.js'],
		        dest: './www/dist/ex.min.js'
		    },
			libs: { //target
		        src: ['./www/dist/libs_concat.js'],
		        dest: './www/dist/l.min.js'
		    },
			policies: { //target
		        src: ['./www/dist/policies_concat.js'],
		        dest: './www/dist/p.min.js'
		    },
			popular: { //target
		        src: ['./www/dist/popular_concat.js'],
		        dest: './www/dist/pop.min.js'
		    },
			search: { //target
		        src: ['./www/dist/search_concat.js'],
		        dest: './www/dist/srch.min.js'
		    },
		    userProfile: { //target
		        src: ['./www/dist/userProfile_concat.js'],
		        dest: './www/dist/u.min.js'
		    },
			verify: { //target
		        src: ['./www/dist/verify_concat.js'],
		        dest: './www/dist/v.min.js'
		    }

		},
		clean: {
			js: ["./www/dist/*.js", "!./www/dist/*.min.js"]
		},
        includeSource: {
            options: {
                //This is the directory inside which grunt-include-source will be looking for files
                basePath: '.'
            },
            app: {
                files: {
                    //Overwriting index.html
                    'www/index_dev.html': 'index_dev.html'
                }
            }
        }
          //grunt task configuration will go here     
    });

    //load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-include-source');

    

    //register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify', 'clean']);
    grunt.registerTask('include', ['includeSource']);
}